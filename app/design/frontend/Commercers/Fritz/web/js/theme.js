define(
    ["jquery",
        'owl_carousel',
    ], function ($) {
        var banner = $('.banner-carousel');
        banner.owlCarousel({
            items:1,
            loop:true,
            autoplay: true,
            animateOut: 'fadeOut',
            autoplayHoverPause:true,
            nav: false,
            dots: false,
            navText: ['<span class="links-icon icon-arrow-left"></span>',
                '<span class="links-icon icon-arrow-right"></span>'],
        });
        setTimeout(function(){
            banner.find('.owl-item.active .desc').addClass('active');
            }, 700);

        banner.on('translate.owl.carousel', function(){
            $(this).find('.owl-item.active').siblings().find('.desc').addClass('active');
            $(this).find('.owl-item.active .desc').removeClass('active');
        });
        banner.on('translated.owl.carousel', function(){
            $(this).find('.owl-item:not(.active)').find('.desc').removeClass('active');
        });
    });
