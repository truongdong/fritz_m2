var config = {
    paths: {
        'owl_carousel': 'js/owl-carousel/owl.carousel',
        'enquire'     : 'js/enquire/enquire',
    },
    shim: {

        'owl_carousel': {
            deps: ['jquery']
        },

        'enquire': {
            deps: ['jquery']
        },

        'price_range_slider': {
            deps: ['jquery']
        },
    }
};