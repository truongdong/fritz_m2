<?php



namespace Commercers\OrderProposalInterface\Data\Receiving\Import;



class DataSource {

    

    public function __construct(

            \Commercers\OrderProposal\Model\OrderProposalFactory $orderProposalFactory,

            \Magento\Catalog\Model\ProductFactory $productFactory,

            \Magento\Catalog\Model\ProductRepository $productRepository,

            \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,

            

            \Magento\Framework\Event\Manager $eventManager

            

            ) {

        

        $this->orderProposalFactory = $orderProposalFactory;

        

        $this->productFactory = $productFactory;

        $this->_productRepository = $productRepository;

        $this->_stockRegistry = $stockRegistry;

        $this->eventManager = $eventManager;

        

    }

    

    public function execute($data,$profiler,$file) {

        //echo '<pre>';print_r($data);exit;

        if (isset($data['items']['item'])) {

            foreach ($data['items']['item'] as $item) {

                

                $sku = isset($item['sku']) ? $item['sku'] : '';

                

                $qty = trim($item['qty']);

                $orderNumber = trim($item['order_number']);

                $deliOrderNumber = trim($item['order_number']);

                $diff = $qty;



                // echo 'Updating product SKU: ' . $sku . ', with Qty: ' . $qty . ' and is_in_stock:' . $isInStock . '<br/>';



                try {

                    $product = $this->_productRepository->get($sku);

                } catch (\Exception $e) {

                    

                    continue;

                }

                // You can set other product data with $product->setAttributeName() if you want to update more data if you set price => use getPrice() and setPrice()

                try {

                    $stockItem = $this->_stockRegistry->getStockItemBySku($sku);

                } catch (\Exception $e) {

                    

                    continue;

                }

                /**

                 * check qty other new updates. note : if qty = $stockItem->getQty() (example : 5 = 5) do not update else (example : 6 vs 5) then update

                 */

                $stockQty = $stockItem->getQty();

                $orderQty = $stockQty + $qty;

                if ($orderQty < 0) {

                    $logger->info("Not to go stock qty go below 0");

                } else {

                    $stockItem->setQty($orderQty);

                    $stockItem->save();

                    $eventData = array(

                        'product' => $product,

                        'oldcount' => $stockQty,

                        'newcount' => $orderQty,

                        'sku' => $sku,

                        'qty' => $qty,

                        'diff' => $diff,

                        'ordernumber' => $orderNumber,

                        'delivernoteid' => $deliOrderNumber,

                        'type' => 1,

                        'is_import' => 1,

                        'posex' => $item['posex']

                    );

                    $this->eventManager->dispatch('commercers_stocklog_updated', $eventData);

                }

            }

        }

    }

}

