<?php

namespace Commercers\OrderProposalInterface\Data\Receiving\Export;

class DataSource implements \Commercers\Profilers\Service\DataSource{


    protected $_proceededIds = array();


    public function __construct(
        \Commercers\StockLog\Model\ResourceModel\StockLog\CollectionFactory $receivingCollectionFactory,
        \Commercers\Profilers\Model\ObjectLogFactory $objectFactory,
        \Commercers\OrderProposal\Model\ResourceModel\OrderProposal\CollectionFactory $orderProposalCollectionFactory
    ) {

        $this->receivingCollectionFactory = $receivingCollectionFactory;
        $this->objectFactory = $objectFactory;
        $this->orderProposalCollectionFactory = $orderProposalCollectionFactory;
    }


    protected function _getCollection($profiler){
        $collection = $this->receivingCollectionFactory->create();
        $collection->addFieldToFilter('type', \Commercers\StockLog\Model\Constant::TYPE_RECEIVING );
        $collection->getSelect()->where(" ordernumber IS NOT NULL  ");
        if($profiler->getData('object_log_status')){
            $collection->getSelect()->where('id NOT IN (SELECT object_id FROM commercers_profilers_object_log WHERE profiler_id = '.$profiler->getId().')');
        }
        return $collection;
    }


    public function getCount($profiler) {
        $collection = $this->_getCollection($profiler);
        return $collection->count();
    }


    public function getData($profiler, $limits) {

        $collection = $this->_getCollection($profiler);
        return $this->exportCollectionToData($collection, true);
    }

    public function getDataManual($dataForm, $profiler) {

    }



    public function getItemById($id, $profiler, $isIncrement = false) {

        $collection = $this->receivingCollectionFactory->create();
        if($isIncrement){
            $collection->addFieldToFilter('ordernumber', $id);
        }else{
            $collection->addFieldToFilter('id', $id);
        }

        //echo $collection->getSelect(); exit;
        return $this->exportCollectionToData($collection);

    }

    protected function exportCollectionToData($collection, $isLog = true){


        $data['orders']['order'] = array();
        $orders = array();

        foreach ($collection as $item) {
            $this->_proceededIds[] = $item->getId();
            
            $itemData = $item->getData();
            $itemData['order_date'] = $item->getDate();
            $items = $this->getOrderProposalOrderAndSku( $item->getOrdernumber(), $item->getSku() ,$itemData['posex']);
            $items['difference'] = $itemData['difference'];
            $items['ordernumber'] = $itemData['ordernumber'];
            $items['order_item']['posex'] = $itemData['posex'];
            $itemData['items'] = $items;
            
            $data['orders']['order'][] = $itemData;


        }
         
        return $data;
    }

    protected function _orderNumberIsExist(){

    }
    protected function getOrderProposalOrder($orderNumber){

        $orderProposals = $this->orderProposalCollectionFactory->create();

        $orderProposals->addFieldToFilter('order_number', $orderNumber);

        if($orderProposals->getSize()){

            return $orderProposals->getFirstItem()->getData();

        }
        return array();


    }

    protected function getOrderProposalOrderAndSku($orderNumber, $sku,$posex){

        $orderProposals = $this->orderProposalCollectionFactory->create();

        $orderProposals->addFieldToFilter('order_number', $orderNumber);
        $orderProposals->addFieldToFilter('sku', $sku);
        if(trim($posex) != '') $orderProposals->addFieldToFilter('posex', $posex);
        if($orderProposals->getSize()){

            return $orderProposals->getFirstItem()->getData();

        }
        return array();


    }


    public function log($profiler, $processLog){
        if(count($this->_proceededIds)){
            foreach($this->_proceededIds as $proceededId){
                $objectLog = $this->objectFactory->create();
                $objectLog->addData(array(
                    'profiler_id' => $profiler->getId(),
                    'datasource' => 'receving_advice',
                    'process_log_id' => $processLog->getId(),
                    'object_id' => $proceededId
                ));
                $objectLog->save();
            }
        }
    }

}