<?php

namespace Commercers\OrderProposalInterface\Data\Export;

class DataSource implements \Commercers\Profilers\Service\DataSource{
    
    protected $_proceededIds;
    public function __construct(
            \Commercers\OrderProposal\Model\ResourceModel\OrderProposal\CollectionFactory $orderProposalCollectionFactory,
            \Commercers\Profilers\Model\ObjectLogFactory $objectFactory
            ) {
        
        $this->orderProposalCollectionFactory = $orderProposalCollectionFactory;
        $this->objectFactory = $objectFactory;
        
    }
    
    
    protected function _getCollection($profiler){
        $collection = $this->orderProposalCollectionFactory->create();
        if($profiler->getData('object_log_status')){
           $collection->getSelect()->where('order_number NOT IN (SELECT object_id FROM commercers_profilers_object_log WHERE profiler_id = '.$profiler->getId().')');
        //    echo 'order_number NOT IN (SELECT object_id FROM commercers_profilers_object_log WHERE profiler_id = '.$profiler->getId().')';
    
        }
      // print_r($collection->getData()); exit;
        return $collection;
    }


    public function getCount($profiler) {
        $collection = $this->_getCollection($profiler);
        return $collection->count();
    }
    

    public function getData($profiler, $limits) {
        $collection = $this->_getCollection($profiler);
     //   echo "<pre>"; print_r($collection);exit;
        return $this->exportCollectionToData($collection);
    }

    public function getDataManual($dataForm, $profiler) {
        
    }

    

    public function getItemById($id, $profiler, $isIncrement = false) {
        
        $collection = $this->orderProposalCollectionFactory->create();
        if($isIncrement == true){
            $collection->addFieldToFilter('order_number', $id);
        }else{
            $collection->addFieldToFilter('orderproposal_id', $id);
        }
        
        //echo $collection->getSelect();exit;
        
        return $this->exportCollectionToData($collection);
        
    }

    protected function exportCollectionToData($collection){
        //print_r($collection->getData());exit;
        $data['orders']['order'] = array();
        $orders = array();
        
        foreach ($collection as $item) {
            $date_time = $item->getOrderDate();
            $order_date = date('Y-m-d',strtotime($date_time));
            if(!isset($orders[$item->getOrderNumber()])){
                 $orders[$item->getOrderNumber()] = array(
                     'order_number' => $item->getOrderNumber(),
                     'order_date' => $order_date,
                     'items' => array('item' => array())
                 );
            }
            $itemData = $item->getData(); 
            $orders[$item->getOrderNumber()]['items']['item'][] = $itemData;
            
        }
        
        foreach($orders as $orderNumber => $orderData){
          //  echo $orderNumber;
            $data['orders']['order'][] = $orderData;
            $this->_proceededIds[] = $orderNumber;
        }
        return $data;
        
    }
    // protected function exportCollectionToData($collection, $isLog = true){


    //     $data['orders']['order'] = array();
    //     $orders = array();

    //     foreach ($collection as $item) {
    //         $this->_proceededIds[] = $item->getId();
            
    //         $itemData = $item->getData();
    //         $itemData['order_date'] = $item->getDate();
    //         $itemData['items'] = $this->getOrderProposalOrderAndSku( $item->getOrdernumber(), $item->getSku() ,$itemData['posex']);
    //         $data['orders']['order'][] = $itemData;


    //     }
    //      //echo "<pre>"; print_r($data);exit;
    //     return $data;
    // }
    public function log($profiler, $processLog){
        
        if(count($this->_proceededIds)){
            foreach($this->_proceededIds as $proceededId){
                $objectLog = $this->objectFactory->create();
                $objectLog->addData(array(
                    'profiler_id' => $profiler->getId(),
                    'datasource' => 'order',
                    'process_log_id' => $processLog->getId(),
                    'object_id' => $proceededId
                ));
                $objectLog->save();
            }
        }
    }

}