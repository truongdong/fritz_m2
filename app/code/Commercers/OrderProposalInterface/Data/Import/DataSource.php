<?php



namespace Commercers\OrderProposalInterface\Data\Import;



class DataSource {

    

    public function __construct(

            \Commercers\OrderProposal\Model\OrderProposalFactory $orderProposalFactory,

            \Magento\Catalog\Model\ProductFactory $productFactory

            ) {

        

        $this->orderProposalFactory = $orderProposalFactory;

        

        $this->productFactory = $productFactory;

        

    }

    public function execute($data,$profiler,$file){

        if(isset($data['items']['item'])){

            foreach($data['items']['item'] as $item){

                $orderProposal = $this->orderProposalFactory->create();

                /*

                 * find product

                 */

                $product = $this->productFactory->create();

                

                $product = $product->loadByAttribute( 'sku' , $item['sku'] );

                //echo $item['sku'];

                //print_r($product->getData());

                //exit;

                if($product){                    

                    

                    $item['name'] = $product->getName();     

                    $orderProposal->addData($item)->save();

                    

                }else{

                    

                }

                 

                

            }

        }

        

    }

}

