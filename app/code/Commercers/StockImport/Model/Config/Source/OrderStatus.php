<?php
namespace Commercers\StockImport\Model\Config\Source;

class OrderStatus implements \Magento\Framework\Option\ArrayInterface
{
    protected $_collectionFactory;
    protected $_objectManager;
    protected $_statusCollectionFactory;
    
    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $statusCollectionFactory
    ) {
        $this->statusCollectionFactory = $statusCollectionFactory;
    }

    public function toOptionArray()
    {
        $options = $this->getAttributes();
        return $options;
    }

    public function getAttributes() {

        $collection = $this->statusCollectionFactory->create();

        $orderGroups = array();

        foreach ($collection as $item) {
            $orderGroups[] = ['value' => $item->getData()['status'], 'label' => $item->getData()['label']];
        }

        return $orderGroups;
    }
}