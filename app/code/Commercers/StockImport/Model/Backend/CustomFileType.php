<?php
 
namespace Commercers\StockImport\Model\Config\Backend;
 
class CustomFileType extends \Magento\Config\Model\Config\Backend\File
{
    /**
     * @return string[]
     */
    public function getAllowedExtensions() {
        return ['csv']; // chỉ bao gồm csv nhờ vào chức năng getAllowedExtensions()
    }
}