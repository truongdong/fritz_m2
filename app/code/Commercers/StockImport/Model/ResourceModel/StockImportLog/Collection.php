<?php
namespace Commercers\StockImport\Model\ResourceModel\StockImportLog;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    public function _construct(){
    $this->_init("Commercers\StockImport\Model\StockImportLog","Commercers\StockImport\Model\ResourceModel\StockImportLog");
        /* Lop Model + Lop Resource Model*/
    }
}