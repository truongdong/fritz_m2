<?php
namespace Commercers\StockImport\Model\ResourceModel;

class StockImportLog extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('stock_import_log', 'id'); /* tên bảng , Id của bảng*/
    }

}