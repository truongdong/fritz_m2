<?php
namespace Commercers\StockImport\Model;

class StockImportLog extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Commercers\StockImport\Model\ResourceModel\StockImportLog'); /*định nghĩa lớp ResourceModel liên kết*/
    }
}
