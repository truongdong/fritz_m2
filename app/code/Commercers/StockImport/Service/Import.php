<?php
namespace Commercers\StockImport\Service;

class Import 
{
    protected $_selectProfilers;
    protected $_myFtp;
    protected $_csv;
    protected $_xml;
    protected $_processLog;

    public function __construct(
        \Commercers\StockImport\Model\Config\Source\SelectProfilers $selectProfilers,
        \Commercers\Ftp\Model\Service\Ftp $myFtp,
        \Commercers\StockImport\Service\Import\Csv $csv,
        \Commercers\StockImport\Service\Import\Xml $xml,
        \Commercers\Profilers\Service\Log $processLog
    ) 
    {
        $this->_selectProfilers = $selectProfilers;
        $this->_myFtp = $myFtp;
        $this->_csv = $csv;   
        $this->_xml = $xml;
        $this->_processLog = $processLog;
    }

    public function execute($profiler) {
        //lay profilers nao dang su dung
        // $profilers = $this->_selectProfilers->getProfilersSelected();
        // if($profilers){
        //     foreach ($profilers as $profiler) {
        //         $this->executeByProfiler($profiler);
        //     }
        // }
        $this->executeByProfiler($profiler);
    }

    protected function executeByProfiler($profiler) {
        $listFiles = array();
        $root = $this->_myFtp->getRoot() . DIRECTORY_SEPARATOR . "var" . DIRECTORY_SEPARATOR;
        /**
         * ftp
         */
        $dataProfiler = $profiler->getData();
        foreach($dataProfiler as $value){
            $id = $value['id'];
            $enableFtp = $value['enableftp'];
            $enableLocal = $value['enablelocal'];
            $ftpFolder = $value['folderftp'];
            $ftpHost = $value['hostname'];
            $ftpUsername = $value['username'];
            $ftpPassword = $value['password'];
            $ftpLocalFolder = $value['localfolderftp'];
            $ftpDoneFolder = $value['donefolderftp'];
            $doneFolder = $value['done'];
            $isLocalFolder = $value['localfolder'];
            $delimiter = $value['delimiter'];
            $profiler = $value;
        }
        if ($enableFtp == 1) {

            $params = array(
                'localhost' => $ftpHost,
                'username' => $ftpUsername,
                'password' => $ftpPassword,
            );
            $localFolder = $root . $ftpLocalFolder;
            $this->_myFtp->readAndMove($params, $ftpFolder, $localFolder, $ftpDoneFolder);
        }
        //write log to start export
        $dataProcessLog = $this->_processLog->execute($id);
        /**
         * local
         */
        if($enableLocal == 1){
            $doneFolder = $root . $doneFolder;
            $localFolder = $root . $isLocalFolder;
        }

        $iterator = new \FilesystemIterator($localFolder);
        if ($iterator->getPathName() != '') {

            /** @var \FilesystemIterator $file */
            foreach ($iterator as $file) {
                if(is_dir($file))
                    continue;
                $filePath[] = $file->getPathname();
                $fileName[] = $file->getFilename();
            }
        }
        $doneFolder = isset($doneFolder)?$doneFolder:'';
        try {
            if (isset($filePath)) {
                for($i = 0; $i < count($filePath);$i++){
                    $importService = $this->getImportService($profiler)->execute($filePath[$i], $fileName[$i], $profiler, $doneFolder,$delimiter);
                }
            }
            $status = 1;
            $message = "IMPORT STOCK SUCCESS!";
        } catch (Exception $e) {
            $status = 1;
            $message = "IMPORT STOCK FAILED!";
        }
        //write log to end export
        $this->_processLog->executeAfter($dataProcessLog['process_id'],$status,$message);   
    }

    protected function getImportService($profiler) {
        if ($profiler['format'] == 'csv') {
            return $this->_csv;
        }

        if ($profiler['format'] == 'xml') {
            return $this->_xml;
        }
    }
}