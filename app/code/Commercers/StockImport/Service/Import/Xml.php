<?php
namespace Commercers\StockImport\Service\Import;

class Xml 
{
    protected $_importStock;
    protected $_ioFile;
    protected $_parser;
    
    public function __construct(
        \Commercers\StockImport\Service\Import\Resource\Stock $importStock,
        \Magento\Framework\Filesystem\Io\File $ioFile,
        \Magento\Framework\Xml\Parser $parser
    ) 
    {
        $this->_importStock = $importStock;
        $this->_ioFile = $ioFile;
        $this->_parser = $parser;
    }
    
    public function execute($filePath, $fileName, $profiler, $doneFolder) {
        //procress
        $dataXml = $this->readXmlFile($filePath);
        foreach ($dataXml as $valueXml) {
            $headerXml = $valueXml[0];
            $rows[] = $valueXml[1];
        }
        $mapping = $this->getMappings($profiler['mapping']);
        foreach ($mapping as $key => $values) {
            $result[] = array($key, $values);
        }
        $params = [
            'header' => $headerXml,
            'rows' => $rows,
            'mapping' => $result
        ];
        // print_r($params);exit;
        $cleanRow = null;//$this->_mappingXml->convert($params); //mapping
        //goi ham cap nhat du lieu vao database
        $this->_importStock->execute($cleanRow);
        //sau khi xu ly xong thi di chuyen file vao folder done
        $this->moveFile($filePath,$fileName,$doneFolder);
    }

    public function readXmlFile($pathFile) {
        /**
         * get Data
         */
        $dataXml = $this->_parser->load($pathFile)->xmlToArray();
        foreach($dataXml['products'] as $values){
            $data = $values;
        }
        //$data is_array
        if(isset($data[0])){
            $headers = $data[0];
            $datas = $data;
        }else{
            $headers = $data;
            $datas[] = $data;
        }
        /**
         * get header xml
         */
        foreach($headers as $key => $value){
            $header[] = $key;
        }
        /**
         * get value and combine with header
         */
        for($i = 0; $i < count($datas); $i++){
            $row = array();
            foreach($datas[$i] as $value){
                $row[] = $value;
            }
            $rows[] = array($header,$row);
        }
        return $rows;
    }

    public function moveFile($filePath, $fileName, $doneFolder) {
        if($doneFolder != ''){
            $pathNew = $doneFolder .DIRECTORY_SEPARATOR. $fileName;
            $this->_ioFile->mv($filePath, $pathNew);
        }
    }

    public function getMappings($mapping){
        if(!is_null($mapping)){
           $mapping = preg_split('/\\r\\n|\\r|\\n/',$mapping);
          
            $value = array();
            foreach ($mapping as $_mapping){
                $attribute = explode('=',trim($_mapping));
                $value[$attribute[0]] = $attribute[1];
            }

            return $value;
       }
       return false;
    }

}
