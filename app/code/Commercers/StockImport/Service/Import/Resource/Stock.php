<?php
namespace Commercers\StockImport\Service\Import\Resource;

use \Psr\Log\LoggerInterface;

class Stock 
{
    const ORDER_STATUS_STOCK = 'section_stockimport/group_stockimport/order_status_stock';
    const ENABLE_COUNTING_ORDERED_QTY = 'section_stockimport/group_stockimport/enable_counting_ordered_qty';

    protected $_stockImportLogFactory;
    protected $_productRepository;
    protected $_logger;
    protected $_stockRegistry;
    protected $_email;
    protected $_orderCollectionFactory;
    protected $_orderItemCollection;
    protected $_resource;
    protected $_scopeConfig;
    private $eventManager;
    private $_productFactory;


    public function __construct(
        \Commercers\StockImport\Model\StockImportLogFactory $stockImportLogFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        LoggerInterface $logger,
        \Commercers\StockImport\Helper\Email $email,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\Event\Manager $eventManager,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $orderItemCollection,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Backend\Model\Auth\Session $authSession
    )
    {
        $this->_stockImportLogFactory = $stockImportLogFactory;
        $this->_productRepository = $productRepository;
        $this->_stockRegistry = $stockRegistry;
        $this->_email = $email;
        $this->_productFactory = $productFactory;
        $this->eventManager = $eventManager;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_orderItemCollection = $orderItemCollection;
        $this->_resource = $resource;
        $this->_scopeConfig = $scopeConfig;  
        $this->authSession = $authSession;
    }
    
    public function execute($dataImport) {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/import-update.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $dataMessage = array();
        //import 
        foreach ($dataImport[0] as $data) {
            /**
             * import data
             */
            $sku = isset($data['sku']) ? $data['sku'] : '';

            $qty = trim($data['qty']);
            //$isInStock = trim($data['is_in_stock']);

            // echo 'Updating product SKU: ' . $sku . ', with Qty: ' . $qty . '<br/>';

            try {
                $product = $this->_productRepository->get($sku);
            } catch (\Exception $e) {
                $logger->info("Cant not found " . $sku);
                $message = "Cant not found : " . $sku;
                $this->addMessageToDatabase($message, $sku);
                $dataMessage[] = $message;
                continue;
            }
            // You can set other product data with $product->setAttributeName() if you want to update more data if you set price => use getPrice() and setPrice()
            // if (isset($data['is_in_stock']) ) {
            //     $isInStock = trim($data['is_in_stock']);
            //     if($product->getIsInStock() != $isInStock){
            //         $product->setIsInStock($isInStock)
            //         ->setStoreId(0) // this is needed because if you have multiple store views, each individual store view will get "Use default value" unchecked for multiple attributes - which causes issues.
            //         ->save();
            //     }
                
            // }
            try {
                $stockItem = $this->_stockRegistry->getStockItemBySku($sku);
            } catch (\Exception $e) {
                $logger->info("Cant not found stock item for SKU " . $sku);
                $message = "Cant not found stock item for SKU: " . $sku;
                $this->addMessageToDatabase($message, $sku);
                $dataMessage[] = $message;
                continue;
            }
            $stockQty = $stockItem->getQty();
            /**
             * check qty other new updates. note : if qty = $stockItem->getQty() (example : 5 = 5) do not update else (example : 6 vs 5) then update
             */
            $isEnableCountingOrderQty = $this->isEnableCountingOrderQty();
            $countOrders = $this->getCountOrderValidate($sku);
            //check
            if($isEnableCountingOrderQty == 1){
                $availableQty = $qty - $countOrders; // qty lưu vào bằng qty nhập - qty đã đếm trong order
            }else if($isEnableCountingOrderQty == 0){
                $availableQty = $qty; 
            }
            //update
            $stockItem->setQty($availableQty);
            if ($availableQty > 0) {
                $stockItem->setIsInStock(1);
            }
            $this->_stockRegistry->updateStockItemBySku($sku, $stockItem);
            //write log for stock movements
            $this->writeLog($product,$sku,$qty,$stockQty);
        }
        if($dataMessage != NULL){
            $messages = json_encode($dataMessage);
            $this->_email->sendEmail($messages);
        }
    }

    public function addMessageToDatabase($message, $sku) {
        //Get Database stock import Log
        $stocklog = $this->_stockImportLogFactory->create();
        //add data
        $stocklog->addData(['sku' => $sku, 'message' => $message])->save();
    }

    public function writeLog($product,$sku,$qty,$stockQty){
        $eventData = array(
            'product' => $product,
            'sku'      => $sku,
            'qty'      => $qty,
            'oldcount' => $stockQty,
            'newcount' => $qty,
            'diff' => $qty - $stockQty,
            'type' => 7,
            'user' => 1254,
            'is_import' => 1,
        );
        $this->eventManager->dispatch('commercers_stocklog_updated',$eventData);
    }

    public function getCountOrderValidate($sku){
        $getOrderStatus = $this->getOrderStatus(); // lấy order status trong config
        $orderStatus = explode(",", $getOrderStatus); // tách chuỗi
        $collection = $this->_orderCollectionFactory->create();
        $collection->getConnection();
        $salesOrderItemTable = $this->_resource->getTableName('sales_order_item');
        $collection->getSelect()->reset(\Zend_Db_Select::COLUMNS);
        $collection->getSelect()->join(array('sales_order_item' => $salesOrderItemTable),
            'main_table.entity_id = sales_order_item.order_id');
        $orderItems = $collection->addAttributeToFilter('status', $orderStatus)->addAttributeToFilter('sku', $sku);
        $orderItems->getSelect()->reset(\Zend_Db_Select::COLUMNS);
        $orderItems->getSelect()->columns(array('count' => 'SUM(qty_ordered)')); // dùng SUM để tính tổng của tất cả qty
        $countOrders = $orderItems->getFirstItem()->getData('count');
        return $countOrders;
    }

    public function getOrderStatus()
    {
        return $this->_scopeConfig->getValue(self::ORDER_STATUS_STOCK, \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
    }
    public function isEnableCountingOrderQty(){
        return $this->_scopeConfig->getValue(self::ENABLE_COUNTING_ORDERED_QTY, \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
    }

}