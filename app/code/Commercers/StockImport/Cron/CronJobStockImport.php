<?php

namespace Commercers\StockImport\Cron;

use \Psr\Log\LoggerInterface;

class CronJobStockImport 
{ 
    
    protected $_import;
    protected $_profilerFactory;

    public function __construct(
        \Commercers\StockImport\Service\Import $import,
        \Commercers\Profilers\Model\ProfilersFactory $profilerFactory,
        \Magento\Framework\Registry $registry
    )
    {
        $this->_profilerFactory = $profilerFactory;
        $this->_import = $import;
        $this->registry = $registry;
    }
    
    public function execute($schedule){
        $this->registry->register('stop_log_event', 1);
        $jobCode = $schedule->getJobCode();
        $profiler = $this->_profilerFactory->create()->getCollection()->addFieldToFilter('code',array('eq' => $jobCode));
        $this->_import->execute($profiler);
    }
}