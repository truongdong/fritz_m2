<?php

namespace Commercers\ExtraFee\Block\Sales\Order;

use Magento\Framework\DataObject;
use Magento\Framework\View\Element\Template;

class ExtraFee extends Template
{
	public function initTotals()
	{
		$parent = $this->getParentBlock();
		$source = $parent->getSource();

		if (abs($source->getExtraFee()) > 0.001) {
			$parent->addTotal(new DataObject(
				[
					'code'       => 'extra_fee',
					'value'      => $source->getExtraFee(),
					'base_value' => $source->getExtraFee(),
					'label'      => __('Extra Fee')
				]
			), 'tax');
		}

		return $this;
	}
}
