define(
    [
        'ko',
        'jquery',
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/totals',
        'mage/translate'
    ],
    function (ko, $, Component, totals, $t) {
        "use strict";

        return Component.extend({
            defaults: {
                template: 'Commercers_ExtraFee/totals/extra-fee'
            },

            isDisplayed: ko.computed(function () {
                return !!totals.getSegment('extra_fee');
            }),

            /**
             * Initial component
             */
            initialize: function () {
                var self = this;

                this._super();

                this.titleDisplay = ko.computed(function () {
                    return self.getTitle();
                });
            },

            /**
             * Gift Card Title
             * @returns {*}
             */
            getTitle: function () {
                var segment = totals.getSegment('extra_fee');

                if (segment) {
                    return segment.title;
                }

                return $t('Extra Fee');
            },

            /**
             * get Value
             *
             * @returns {*|String}
             */
            getValue: function () {
                return this.getFormattedPrice(totals.getSegment('extra_fee').value);
            }
        })
    }
);
