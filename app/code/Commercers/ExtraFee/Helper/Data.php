<?php
namespace Commercers\ExtraFee\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\RequestInterface;

class Data extends AbstractHelper {

	/**
	 * @var CheckoutSession
	 */
	protected $_checkoutSession;

	/**
	 * @var \Magento\Framework\App\Config\ScopeConfigInterface
	 */
	protected $_scopeConfig;

	/**
	 * @var \Magento\Framework\App\RequestInterface
	 */
	protected $_request;

    public function __construct(
	    ScopeConfigInterface $scopeConfig,
	    CheckoutSession $checkoutSession,
	    RequestInterface $request
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_checkoutSession = $checkoutSession;
        $this->_request = $request;
    }
}
