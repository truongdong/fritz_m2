<?php
namespace Commercers\ExtraFee\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Config extends AbstractHelper {

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    const SYS_XML_SECTION = 'commercers_extrafee';

    const SYS_XML_GENERAL_GROUP = 'general_group';

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->_scopeConfig = $scopeConfig;
    }

    public function getConfigValue($section, $group, $field , $storeId = null)
    {
        return $this->_scopeConfig->getValue(
            $section . "/" . $group . "/" . $field,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function getGeneralConfigValue($field, $store_id = null)
    {
        return $this->getConfigValue(static::SYS_XML_SECTION, static::SYS_XML_GENERAL_GROUP,$field, $store_id);
    }

    public function getAttributeSetForExtraFee($store_id = null)
    {
        return explode(',',$this->getGeneralConfigValue('attribute_set', $store_id));
    }

}
