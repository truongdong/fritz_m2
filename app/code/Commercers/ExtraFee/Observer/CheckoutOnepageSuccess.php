<?php
namespace Commercers\ExtraFee\Observer;

use Magento\Framework\Event\ObserverInterface;

class CheckoutOnepageSuccess implements ObserverInterface
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    protected $_collection;

    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Collection $collection
    ) {
        $this->_collection = $collection;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $orderIds = $observer->getEvent()->getOrderIds();
        if (!$orderIds || !is_array($orderIds)) {
            return $this;
        }

        try {
            $this->_collection->addFieldToFilter( 'entity_id', [ 'in' => $orderIds ] );

            foreach ( $this->_collection as $order ) {
                if ( $order->getMolliePaymentFee() && $order->getExtraFee() ) {
                    $extraFee = round( $order->getExtraFee(), 2 );
                    $molliePaymentFee = round($order->getMolliePaymentFee(), 2 );
                    if ( $extraFee == $molliePaymentFee ) {
                        $order->setMolliePaymentFee(0);
                        $order->setBaseMolliePaymentFee(0);
                        $order->save();
                    }
                }
            }
        } catch (\Exception $e) {

        }

        return $this;
    }
}
