<?php
namespace Commercers\ExtraFee\Observer;

use Magento\Framework\Event\ObserverInterface;
use Commercers\ExtraFee\Model\ExtraFeeFactory;
use Commercers\ExtraFee\Helper\Config as ExtraFeeConfig;

class SalesConvertQuote implements ObserverInterface
{
    /**
     * @var ExtraFeeFactory
     */
    protected $_extraFeeFactory;

    /**
     * @var ExtraFeeConfig
     */
    protected $_extraFeeConfig;

    public function __construct(
        ExtraFeeFactory $extraFeeFactory,
        ExtraFeeConfig $extraFeeConfig
    ) {
        $this->_extraFeeFactory = $extraFeeFactory;
        $this->_extraFeeConfig = $extraFeeConfig;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
	{
	    try {
            $order = $observer->getEvent()->getOrder();
            $quote = $observer->getEvent()->getQuote();

            if (abs($quote->getExtraFee()) > 0.001) {
                $order->setExtraFee($quote->getExtraFee());
                $order->setExtraBaseFee($quote->getExtraBaseFee());
                $order->save();

                foreach ($quote->getAllItems() as $item) {
                    $attributeSet = $item->getProduct()->getAttributeSetId();
                    if(in_array($attributeSet,$this->_extraFeeConfig->getAttributeSetForExtraFee())) {
                        $data = [
                            'order_id' => $order->getId(),
                            'item_id' => $item->getId(),
                            'product_sku' => $item->getSku(),
                            'extra_fee' => $item->getProduct()->getDeposit()
                        ];
                        $this->_extraFeeFactory->create()->addData($data)->save();
                    }
                }
            }
        } catch (\Exception $e) {

        }

		return $this;
	}
}
