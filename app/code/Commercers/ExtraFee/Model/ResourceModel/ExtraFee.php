<?php

namespace Commercers\ExtraFee\Model\ResourceModel;

class ExtraFee extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	) {
		parent::__construct($context);
	}

	protected function _construct() {
		$this->_init('commercers_extrafee_order_item', 'id');
	}
}
