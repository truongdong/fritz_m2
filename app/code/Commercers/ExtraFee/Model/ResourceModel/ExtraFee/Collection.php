<?php

namespace Commercers\ExtraFee\Model\ResourceModel\ExtraFee;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'commercers_extrafee_collection';
	protected $_eventObject = 'extrafee_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Commercers\ExtraFee\Model\ExtraFee', 'Commercers\ExtraFee\Model\ResourceModel\ExtraFee');
	}

}
