<?php
namespace Commercers\ExtraFee\Model\Total\Quote;

use Magento\Framework\Message\ManagerInterface;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address;
use Magento\Quote\Model\Quote\Address\Total;
use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Checkout\Helper\Data as CheckoutHelper;
use Commercers\ExtraFee\Helper\Config as ExtraFeeConfig;
class ExtraFee extends AbstractTotal {

	/**
	 * @var ManagerInterface
	 */
	protected $_messageManager;

	/**
	 * @var CheckoutSession
	 */
	protected $_checkoutSession;

	/**
	 * @var CheckoutHelper
	 */
	protected $_checkoutHelper;

    /**
     * @var ExtraFeeConfig
     */
    protected $_extraFeeConfig;

	public function __construct(
		ManagerInterface $messageManager,
		CheckoutSession $checkoutSession,
		CheckoutHelper $checkoutHelper,
        ExtraFeeConfig $extraFeeConfig
	) {
		$this->_messageManager  = $messageManager;
		$this->_checkoutSession = $checkoutSession;
		$this->_checkoutHelper = $checkoutHelper;
		$this->_extraFeeConfig = $extraFeeConfig;

		$this->setCode('extra_fee');
	}

	public function collect(
		Quote $quote,
		ShippingAssignmentInterface $shippingAssignment,
		Total $total
	) {
		parent::collect($quote, $shippingAssignment, $total);

		$this->calculateExtraFee($quote, $total);

		return $this;
	}

	protected function calculateExtraFee(Quote $quote, Total $total){
		$quote->setExtraFee(0);
		$quote->setExtraBaseFee(0);
		$quote->save();

		$total->setExtraFee(0);
		$total->setExtraBaseFee(0);

		if (!$quote->getId()) {
			return $this;
		}

		$items = $quote->getAllItems();
		if (!count($items)) {
			return $this;
		}

		$baseExtraFee = $this->getExtraFee($quote);

		if (!$baseExtraFee > 0 ) {
			return $this;
		}

		$quote->setExtraFee($baseExtraFee);
		$quote->setExtraBaseFee($baseExtraFee);
		$quote->save();

		$total->setBaseGrandTotal($total->getBaseGrandTotal() + $baseExtraFee);
		$total->setGrandTotal($total->getGrandTotal() + $baseExtraFee);

		$total->setExtraFee($baseExtraFee);
		$total->setExtraBaseFee($baseExtraFee);

		return $this;
	}

	public function fetch(Quote $quote, Total $total)
	{
		$totalArray = [];
		$amount = $quote->getExtraFee();

		if ($amount!=0) {
			$totalArray[] = [
				'code' => $this->getCode(),
				'title' => __('Extra Fee'),
				'value' => $amount,
			];
		}
		return $totalArray;
	}

	protected function getExtraFee($quote){
	    $extraFee = 0;

        foreach ($quote->getAllItems() as $item) {
            $attributeSet = $item->getProduct()->getAttributeSetId();
            if(in_array($attributeSet,$this->_extraFeeConfig->getAttributeSetForExtraFee())) {
                $extraFee += $item->getProduct()->getDeposit() * $item->getQty();
            }
        }

        return $extraFee;
    }
}
