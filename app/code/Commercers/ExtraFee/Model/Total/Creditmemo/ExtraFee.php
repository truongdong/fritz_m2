<?php

namespace Commercers\ExtraFee\Model\Total\Creditmemo;

use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal;
use Commercers\ExtraFee\Model\ExtraFeeFactory;

class ExtraFee extends AbstractTotal
{
    /**
     * @var ExtraFeeFactory
     */
    protected $_extraFeeFactory;

    public function __construct(
        ExtraFeeFactory $extraFeeFactory
    ) {
        $this->_extraFeeFactory = $extraFeeFactory;
    }

    public function collect(Creditmemo $creditmemo)
    {
        $order = $creditmemo->getOrder();
        $orderExtraFee  = $order->getExtraFee();

        if (!$orderExtraFee) {
            return $this;
        }

        $depositPrice = 0;
        $totalQty = 0;
        foreach ($creditmemo->getAllItems() as $creditmemoItem) {
            $extraFeeItem = $this->_extraFeeFactory->create()->getCollection()
                ->addFieldToFilter('order_id',$order->getId())
                ->addFieldToFilter('product_sku',$creditmemoItem->getSku())
                ->getFirstItem()
            ;
            if($creditmemoItem->getSku() == $extraFeeItem->getProductSku()) {
                $totalQty += $creditmemoItem->getQty();
                $totalQty += $creditmemoItem->getQty();
                $depositPrice += $extraFeeItem->getExtraFee() * $creditmemoItem->getQty();
            }
        }

        if (!$totalQty) {
            return $this;
        }

        // Fix floating-point
        $depositPrice = round($depositPrice, 2);
        $orderExtraFee = round($orderExtraFee, 2);

        if (!$depositPrice || $depositPrice > $orderExtraFee) {
            throw new \Exception('Deposit price is not calculated correctly');
            return $this;
        }

        $creditmemo->setExtraFee($depositPrice);
        $creditmemo->setExtraBaseFee($depositPrice);

        $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $depositPrice);
        $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $depositPrice);

        return $this;
    }
}
