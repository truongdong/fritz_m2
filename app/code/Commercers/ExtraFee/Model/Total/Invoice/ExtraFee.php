<?php

namespace Commercers\ExtraFee\Model\Total\Invoice;

use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Invoice\Total\AbstractTotal;
use Commercers\ExtraFee\Model\ExtraFeeFactory;
use Commercers\ExtraFee\Helper\Config as ExtraFeeConfig;

class ExtraFee extends AbstractTotal
{
    /**
     * @var ExtraFeeFactory
     */
    protected $_extraFeeFactory;

    public function __construct(
        ExtraFeeFactory $extraFeeFactory
    ) {
        $this->_extraFeeFactory = $extraFeeFactory;
    }

    public function collect(Invoice $invoice)
    {
        $order = $invoice->getOrder();
        $orderExtraFee = $order->getExtraFee();

        if (!$orderExtraFee) {
            return $this;
        }

        $depositPrice = 0;
        foreach ($invoice->getAllItems() as $invoiceItem) {
            $extraFeeItem = $this->_extraFeeFactory->create()->getCollection()
                ->addFieldToFilter('order_id',$order->getId())
                ->addFieldToFilter('product_sku',$invoiceItem->getSku())
                ->getFirstItem()
            ;

            if($invoiceItem->getSku() == $extraFeeItem->getProductSku()) {
                $depositPrice += $extraFeeItem->getExtraFee() * $invoiceItem->getQty();
            }
        }

        // Fix floating-point
        $depositPrice = round($depositPrice, 2);
        $orderExtraFee = round($orderExtraFee, 2);

        if (!$depositPrice || $depositPrice > $orderExtraFee) {
            throw new \Exception('Deposit price is not calculated correctly');
            return $this;
        }

        $invoice->setExtraFee($depositPrice);
        $invoice->setExtraBaseFee($depositPrice);

        $invoice->setGrandTotal($invoice->getGrandTotal() + $depositPrice);
        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $depositPrice);

        return $this;
    }
}
