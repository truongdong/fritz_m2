<?php

namespace Commercers\ExtraFee\Model;

class ExtraFee extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface {

	const CACHE_TAG = 'commercers_extrafee';

	protected $_cacheTag = 'commercers_extrafee';

	protected $_eventPrefix = 'commercers_extrafee';

	public function __construct(
		\Magento\Framework\Model\Context $context,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
		\Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
		array $data = []
	) {
		parent::__construct(
			$context,
			$registry,
			$resource,
			$resourceCollection
		);
	}

	protected function _construct() {
		$this->_init('Commercers\ExtraFee\Model\ResourceModel\ExtraFee');
	}

	public function getIdentities() {
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues() {
		$values = [];

		return $values;
	}
}
