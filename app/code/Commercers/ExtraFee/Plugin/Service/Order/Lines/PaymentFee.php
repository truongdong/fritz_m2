<?php

namespace Commercers\ExtraFee\Plugin\Service\Order\Lines;

use Magento\Sales\Api\Data\OrderInterface;

class PaymentFee {

    public function beforeOrderHasPaymentFee(
        \Mollie\Payment\Service\Order\Lines\PaymentFee $subject,
        OrderInterface $order
    )
    {
        if($extraFee = $order->getData('extra_fee')) {
            $order->setData('mollie_payment_fee',$extraFee);
            $order->setData('base_mollie_payment_fee',$extraFee);
        }
    }
}
