<?php

namespace Commercers\ProductExport\Console;

use Magento\Catalog\Api\ProductRepositoryInterfaceFactory;
use Magento\Catalog\Model\Config;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Action;
use Magento\Catalog\Model\Product\Gallery\Processor;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product\Gallery;
use Magento\Cms\Model\BlockFactory;
use Magento\Cms\Model\PageFactory;
use Magento\Eav\Api\AttributeManagementInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State;

class CreateProduct extends Command
{
    protected function configure()
    {
        $this->setName('commercers:importdata:product');
        $this->setDescription('Create Product By Commercers ');

        parent::configure();
    }

    public function __construct(
        ResourceConnection $resource,
        ProductRepositoryInterfaceFactory $productRepositoryInterface,
        \Magento\Eav\Model\Config $eavConfig,
        ProductFactory $productFactory,
        DirectoryList $directoryList,
        Action $productAction,
        EavSetup $eavSetup,
        Config $catalogConfig,
        AttributeManagementInterface $attributeManagement,
        Csv $csvProcessor,
        Filesystem $filesystem,
        BlockFactory $blockFactory,
        PageFactory $pageFactory,
        Processor $imageProcessor,
        Gallery $productGallery,
        \Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\Product\Gallery\Processor $mediaGalleryProcessor,
        State $state
    )
    {
        $state->setAreaCode('adminhtml');
        $this->_eavConfig = $eavConfig;
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_productAction = $productAction;
        $this->_productFactory = $productFactory;
        $this->directoryList = $directoryList;
        $this->eavSetup = $eavSetup;
        $this->catalogConfig = $catalogConfig;
        $this->attributeManagement = $attributeManagement;
        $this->filesystem = $filesystem;
        $this->csvProcessor = $csvProcessor;
        $this->_blockFactory = $blockFactory;
        $this->_pageFactory = $pageFactory;
        $this->_imageProcessor = $imageProcessor;
        $this->_productGallery = $productGallery;
        $this->_productUrlRewriteGenerator = $productUrlRewriteGenerator;
        $this->_resource = $resource;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->_mediaGalleryProcessor = $mediaGalleryProcessor;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        return;
        //echo 1; exit;
        $this->importImage();
    }
    protected function _updateHideGroup(){
        return;
        exit;
        $obj = \Magento\Framework\App\ObjectManager::getInstance();

        $fileName = '20210503 Hidegroups additions.csv';
        $csvData = $this->_readCsvData($fileName);
        foreach ($csvData as  $key => $datum){
            if(trim($datum['hide_group']) != ""){
                echo $key ."\n";
                echo $datum['sku'] ."\n";
                $productRepository = $obj->create('\Magento\Catalog\Api\ProductRepositoryInterface');
                $productId = $productRepository->get($datum['sku'])->getId();
                $productFac = $obj->create('\Magento\Catalog\Model\Product');
                $product = $productFac->load($productId);
                $product->setData('hide_product_groups',$this->getValuesHideGroup($datum));
                $product->getResource()->saveAttribute($product, 'hide_product_groups');

            }

            //print_r($datum);exit;
        }exit;
    }
    protected function updateAndCreateProduct(){
        return;
        $obj = \Magento\Framework\App\ObjectManager::getInstance();

        $fileName = '20210501 egfra_additional products 2nd file.csv';
        $csvData = $this->_readCsvData($fileName);

        foreach ($csvData as $key => $datum) {

            if (!$this->checkProductExist($datum['sku'])) {
                $product = $this->_productFactory->create();
            }else{
                $productRepository = $obj->create('\Magento\Catalog\Api\ProductRepositoryInterface');
                $productId = $productRepository->get($datum['sku'])->getId();
                $productFac = $obj->create('\Magento\Catalog\Model\Product');
                $product = $productFac->load($productId);
            }

            $product->setVisibility(4); // visibilty of product (catalog / search / catalog, search / Not visible individually)
            $product->setTaxClassId(2); // Tax class id
            $product->setTypeId('simple'); // type of product (simple/virtual/downloadable/configurable)

            $product->setWebsiteIds(array(1));


            $productData = array();
            $productData['sku'] = $datum['sku'];

            if(trim($datum['egfra_brand']) != ''){
                $productData['egfra_brand'] = $this->_getOptionIdOfAttribute(trim($datum['egfra_brand']), 'egfra_brand');
            }

            $productData['egfra_kundensegment'] = $this->getValuesKundensegment($datum, 'egfra_kundensegment');
            if(trim($datum['egfra_brand_owner']) != ''){
                $productData['egfra_brand_owner'] = $this->_getOptionIdOfAttribute(trim($datum['egfra_brand_owner']), 'egfra_brand_owner');
            }

            $productData['name'] = trim($datum['name']);
            $productData['egfra_org'] = $this->_getOptionIdOfAttribute(trim($datum['egfra_org']), 'egfra_org');
            $productData['egfra_notify_low_stock'] = strtolower($datum['egfra_notify_low_stock']);
            $productData['price'] = $datum['price'];
            $productData['weight'] = $datum['weight'];
            $productData['length'] = $datum['length'];
            $productData['width'] = $datum['width'];
            $productData['height'] = $datum['height'];
            if(trim($datum['egfra_qty_carton']) != ''){
                $productData['egfra_qty_carton'] = $datum['egfra_qty_carton'];
            }
            if(trim($datum['egfra_qty_pallet']) != ''){
                $productData['egfra_qty_pallet'] = $datum['egfra_qty_pallet'];
            }

            $productData['egfra_leihartikel'] = (boolean)$datum['egfra_leihartikel'];
            if(trim($datum['egfra_produktverantwortlicher']) != ''){
                $productData['egfra_produktverantwortlicher'] = $this->_getOptionIdOfAttribute(trim($datum['egfra_produktverantwortlicher']), 'egfra_produktverantwortlicher');
            }
            $productData['status'] = 1;
            if ((int)$productData['price'] == 0) {
                $productData['price'] = '0.01';
            }

            $productData['short_description'] = $datum['short_description'];
            $product->addData($productData);
            $product->setPrice($productData['price']);
            try{
                $product->save();
            }catch (\Exception $exception){
                var_dump($exception->getMessage());exit;
            }

            $this->updateStock($datum);


        }
    }
    protected function updateStock($datum)
    {
        $obj = \Magento\Framework\App\ObjectManager::getInstance();

        $productRepository = $obj->create('\Magento\Catalog\Api\ProductRepositoryInterface');
        $productId = $productRepository->get($datum['sku'])->getId();
        $productFac = $obj->create('\Magento\Catalog\Model\Product');
        $product = $productFac->load($productId);
        $product->setAttributeSetId(4);
        $product->setStockData(
            array(
                'qty' => 0,
                'min_sale_qty' => $datum['min_cart_qty'],
                'use_config_min_sale_qty' => false,
                'qty_increments' => $datum['qty_increments'],
                'use_config_qty_increments' => false,
                'enable_qty_increments' => true,
                'use_config_enable_qty_inc' => false,
                'notify_stock_qty' => $datum['notify_on_stock_below'],
                'use_config_notify_stock_qty' => false,
                'is_in_stock' => false
            )
        )->save();

    }
    protected function updateCategories($datum)
    {
        $obj = \Magento\Framework\App\ObjectManager::getInstance();
//        $productRepository = $obj->create('\Magento\Catalog\Api\ProductRepositoryInterface');
//        $productId = $productRepository->get($datum['sku'])->getId();
//        $productFac = $obj->create('\Magento\Catalog\Model\Product');
//        $product = $productFac->load($productId);
        $value1 = $this->getCategoryIds($datum['category_1']);
        $value2 = $this->getCategoryIds($datum['category_2']);
        $value3 = $this->getCategoryIds($datum['category_3']);
        $values = array($value1,$value2,$value3);
        var_dump(implode(',',$values));exit;
        //$product->setCategoryIds(explode(',', $datum['category_ids']));
        //$product->save();
        //print_r("DONE " . $product->getSku() . "\n");
    }

    protected function getCategoryIds($value)
    {
        $obj = \Magento\Framework\App\ObjectManager::getInstance();
        $categoryFac = $obj->create('\Magento\Catalog\Model\Category');
        $collection = $categoryFac->getCollection()->addAttributeToFilter('name', $value);
        if ($collection->getSize()) {
            return $collection->getFirstItem()->getId();
        }
    }

    protected function importImage()
    {
        return;
        $obj = \Magento\Framework\App\ObjectManager::getInstance();

        $fileName = '20210511 Import.csv';
        $csvData = $this->_readCsvData($fileName);

        foreach ($csvData as $key => $datum) {

            echo $key . "\n";
            if ($this->checkProductExist($datum['sku'])) {
                if($datum['sku'] == 'FR1000') continue;
                $image_path = '/home/eggersfranke/public_html/pub/media/import/29042020/'.$datum['image'];


                $productRepository = $obj->create('\Magento\Catalog\Api\ProductRepositoryInterface');
                $productId = $productRepository->get($datum['sku'])->getId();
                $productFac = $obj->create('\Magento\Catalog\Model\Product');
                $product = $productFac->load($productId);

                $mediaAttribute = array('thumbnail', 'small_image', 'image');

                $product->addImageToMediaGallery($image_path, $mediaAttribute, false, false);
                $product->save();
                $existingMediaGalleryEntries = $product->getMediaGalleryEntries();
                foreach ($existingMediaGalleryEntries as $key => $entry) {
                    if ($key == (count($existingMediaGalleryEntries) - 1)) {
                        $entry->setLabel($product->getName());

                    }

                }

                $product->setMediaGalleryEntries($existingMediaGalleryEntries);
                $product->save();
                echo $product->getSku() . "\n";

            }

        }
    }

    protected function getValuesHideGroup($datum)
    {
        $obj = \Magento\Framework\App\ObjectManager::getInstance();
       // $hideGroupValues = array($datum['hide_group_1'], $datum['hide_group_2'], $datum['hide_group_3'], $datum['hide_group_4'], $datum['hide_group_5'], $datum['hide_group_6']);
        $hideGroupValues = explode(',',$datum['hide_group']);
        $resource = $obj->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('customer_group');
        $returnValues = array();
        foreach ($hideGroupValues as $hideGroupValue) {
            $hideGroupValue = trim($hideGroupValue);
            if($hideGroupValue == '') continue;

            //if($hideGroupValue == 'Channel Strategy Activation Management' OR $hideGroupValue == 'R&U Vertrieb GVL') continue;
            $sql = "SELECT `main_table`.* FROM `customer_group` AS `main_table` WHERE (`customer_group_code` = '{$hideGroupValue}')";
            if ($connection->fetchOne($sql) == '0') {
                var_dump($hideGroupValue);
                print_r($hideGroupValues);
                var_dump($datum['sku']);
                exit;
            }
            $returnValues[] = $connection->fetchOne($sql);
        }
        $returnValues = array_unique($returnValues);
        return implode(',', $returnValues);
    }

    protected function getValuesKundensegment($datum, $attrCode)
    {
        $returnValues = array();
        $value1 = $this->_getOptionId($datum['egfra_kundensegment_1'], $attrCode);
        array_push($returnValues, $value1);

        if (trim($datum['egfra_kundensegment_2']) != "") {
            $value2 = $this->_getOptionId($datum['egfra_kundensegment_2'], $attrCode);
            array_push($returnValues, $value2);
        }
        if (trim($datum['egfra_kundensegment_3']) != "") {
            $value3 = $this->_getOptionId($datum['egfra_kundensegment_3'], $attrCode);
            array_push($returnValues, $value3);
        }
        if (trim($datum['egfra_kundensegment_4']) != "") {
            $value4 = $this->_getOptionId($datum['egfra_kundensegment_4'], $attrCode);
            array_push($returnValues, $value4);
        }
        if (trim($datum['egfra_kundensegment_5']) != "") {
            $value5 = $this->_getOptionId($datum['egfra_kundensegment_5'], $attrCode);
            array_push($returnValues, $value5);
        }
        return implode(',', $returnValues);
    }

    protected function _getOptionIdOfAttribute($value, $attrCode)
    {
        $value = trim($value);
        $attribute = $this->_eavConfig->getAttribute('catalog_product', $attrCode);

        $options = $attribute->getSource()->getAllOptions();
        foreach ($options as $option) {
            if (is_object($option['label'])) {
                if ($option['label']->getText() == $value) return $option['value'];
            }
            if ($option['label'] == $value) return $option['value'];
        }
    }

    protected function _getOptionId($value, $attrCode)
    {
        $value = trim($value);
        switch ($value) {
            case 'A':
                $returnValue = 211;
                break;
            case 'B':
                $returnValue = 212;
                break;
            case 'C':
                $returnValue = 213;
                break;
            case 'D':
                $returnValue = 214;
                break;
            case 'E':
                $returnValue = 215;
                break;
            default:
                $returnValue = null;
        }
        return $returnValue;
    }

    protected function checkProductExist($sku)
    {
        $obj = \Magento\Framework\App\ObjectManager::getInstance();
        $productFac = $obj->create('\Magento\Catalog\Model\Product');
        $productCol = $productFac->getCollection();
        $productCol->addFieldToFilter('sku', $sku);
        return (boolean)$productCol->getSize();
    }

    protected function _readCsvData($fileName)
    {
        $file = fopen($fileName, "r");
        while (!feof($file)) {
            $csv[] = fgetcsv($file, 0, ',');
        }
        $keys = array_shift($csv);

        foreach ($csv as $data) {
            if (is_array($data)) {

                $returnValue[] = array_combine($keys, $data);
            }
        }
        if (isset($returnValue))
            return $returnValue;
        return false;
    }
}