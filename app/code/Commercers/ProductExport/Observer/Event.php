<?php
/*
 * Commercers Vietnam
 */
namespace Commercers\ProductExport\Observer;
 
use Magento\Framework\Event\ObserverInterface;
 
class Event implements ObserverInterface
{
    
    public function __construct(
            \Commercers\Profilers\Model\ResourceModel\Profilers\CollectionFactory $profilerCollectionFactory
            ) {
     
        $this->profilerCollectionFactory = $profilerCollectionFactory;
    }



    public function execute(\Magento\Framework\Event\Observer $observer)     
    {
     
        //echo 1; exit;
        $profilers = $this->getProfilers();
        foreach($profilers as $profiler){
            $actionsSerialized = $profiler->getActionsSerialized();
            $actions = unserialize($actionsSerialized);
            //print_r($actions);exit;
            if(isset($actions['events'])){
                foreach($actions['events'] as $eventName => $eventValue){
                        
                    if($eventName == $observer->getEvent()->getName() && $eventValue == 1){
                        $type = $profiler->getData("type");
                        
                        if($type == \Commercers\Profilers\Model\Constant::EXPORT_PROFILER){
                            $process = \Magento\Framework\App\ObjectManager::getInstance()->get("\Commercers\Profilers\Service\Profiler\Process\Export");
                            $process->executeForOneItem($profiler, $observer->getProduct()->getId() );
                        }
                       
                    }
                }
            }
        }
         
        

    }
    
    protected function getProfilers(){
        $profilers = $this->profilerCollectionFactory->create();
        $profilers->addFieldToFilter('data_source', array('in' => 'product'));
        return $profilers;
        
    }
    
}