<?php

namespace Commercers\ProductExport\Block\Adminhtml\Form\Profiler\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Rule\Model\Condition\AbstractCondition;

class Conditions extends Generic implements TabInterface
{
    /**
     * Core registry
     *
     * @var \Magento\Backend\Block\Widget\Form\Renderer\Fieldset
     */
    protected $rendererFieldset;

    /**
     * @var \Magento\Rule\Block\Conditions
     */
    protected $conditions;
    /**
     * @var \Commercers\AutoProductRelation\Model\RuleFactory
     */
    protected $ruleFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Rule\Block\Conditions $conditions
     * @param \Magento\Backend\Block\Widget\Form\Renderer\Fieldset $rendererFieldset
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Rule\Block\Conditions $conditions,
        \Commercers\ProductExport\Model\RuleFactory $ruleFactory,
        \Magento\Backend\Block\Widget\Form\Renderer\Fieldset $rendererFieldset,
        \Commercers\Profilers\Model\ProfilersFactory $profilerFactory,    
        array $data = []
    ) {
        $this->rendererFieldset = $rendererFieldset;
        $this->conditions = $conditions;
        $this->_conditions = $conditions;
        $this->_rendererFieldset = $rendererFieldset;
        $this->ruleFactory = $ruleFactory;
        $this->profilerFactory  = $profilerFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }
    
    
    protected function getProfiler(){
        $profilerId = $this->_request->getParam('id');
        
        if(!$profilerId)
            return false;
        
        $profiler = $this->profilerFactory->create()->load($profilerId);
        return $profiler;
    }


    protected function getRule(){
        
        if($profiler = $this->getProfiler()){
            $rules = $this->ruleFactory->create()->getCollection();
            $rules->addFieldToFilter('data_source', $profiler->getDataSource());
            $rules->addFieldToFilter('profiler_id', $profiler->getId());

            if( $rules->getSize() ){
                $rule = $rules->getFirstItem();
                return $rule;
            }
        }
        
        return $this->ruleFactory->create();
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Filters');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Filters');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        
        if(!$this->getProfiler() || $this->getProfiler()->getDataSource() != 'product'){
            
            return false;
        }
            
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        if(!$this->getProfiler() || $this->getProfiler()->getDataSource() != 'product')
            return true;
        return false;
    }
    
    /**
     * Tab should be loaded trough Ajax call
     *
     * @return bool
     * @codeCoverageIgnore
     */
    public function isAjaxLoaded()
    {
        return false;
    }

    
    protected function _prepareForm()
    {
        
        if(!$this->getProfiler() || $this->getProfiler()->getDataSource() != 'product'){
            
            return $this;
        }
        
        $rule = $this->getRule();
        
        $this->_coreRegistry->register(
                'current_rule',
                $rule
            );
        $form = $this->addTabToForm($rule);
        $this->setForm($form);
        
       

        return parent::_prepareForm();
    }

    /**
     * @param \Magento\CatalogRule\Api\Data\RuleInterface $model
     * @param string $fieldsetId
     * @param string $formName
     * @return \Magento\Framework\Data\Form
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function addTabToForm($model, $fieldsetId = 'filter_conditions_fieldset', $formName = 'profilers_form')
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('rule_');
        
        $conditionsFieldSetId = $model->getConditionsFieldSetId($formName);
        //echo $conditionsFieldSetId;exit;
        $newChildUrl = $this->getUrl(
            'product_export/rule/newConditionHtml/form/' . $conditionsFieldSetId,
            ['form_namespace' => $formName]
        );
        

        $renderer = $this->_rendererFieldset->setTemplate('Magento_CatalogRule::promo/fieldset.phtml')
            ->setNewChildUrl($newChildUrl)
            ->setFieldSetId($conditionsFieldSetId);

        $fieldset = $form->addFieldset(
            $fieldsetId,
            ['legend' => __('Conditions (don\'t add conditions if rule is applied to all products)')]
        )->setRenderer($renderer);

        $fieldset->addField(
            'conditions',
            'text',
            [
                'name' => 'conditions',
                'label' => __('Conditions'),
                'title' => __('Conditions'),
                'required' => true,
                'data-form-part' => $formName
            ]
        )
            ->setRule($model)
            ->setRenderer($this->_conditions);

        $form->setValues($model->getData());
        $this->setConditionFormName($model->getConditions(), $formName, $conditionsFieldSetId);
        return $form;
    }

    /**
     * @param AbstractCondition $conditions
     * @param string $formName
     * @param string $jsFormName
     * @return void
     */
    private function setConditionFormName(AbstractCondition $conditions, $formName, $jsFormName)
    {
        $conditions->setFormName($formName);
        $conditions->setJsFormObject($jsFormName);

        if ($conditions->getConditions() && is_array($conditions->getConditions())) {
            foreach ($conditions->getConditions() as $condition) {
                $this->setConditionFormName($condition, $formName, $jsFormName);
            }
        }
    }
    

    /**
     * Prepare form before rendering HTML
     *
     * @return Generic
     */
    protected function _prepareForm__()
    {
        $rule = $this->ruleFactory->create();
        $this->_coreRegistry->register(
            'current_rule',
            $rule
        );

        $model = $this->_coreRegistry->registry('current_rule');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('rule_');

        $renderer = $this->rendererFieldset->setTemplate(
            'Magento_CatalogRule::promo/fieldset.phtml'
        )->setNewChildUrl(
            $this->getUrl('profilers/index_rule/newConditionHtml/form/rule_conditions_fieldset')
        );

        $fieldset = $form->addFieldset(
            'conditions_fieldset',
            [
                'legend' => __(
                    'Apply the rule only if the following conditions are met.'
                )
            ]
        )->setRenderer(
            $renderer
        );

        $fieldset->addField(
            'filer_conditions',
            'text',
            ['name' => 'conditions', 'label' => __('Conditions'), 'title' => __('Conditions')]
        )->setRule(
            $model
        )->setRenderer(
            $this->conditions
        );

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
    
    
}