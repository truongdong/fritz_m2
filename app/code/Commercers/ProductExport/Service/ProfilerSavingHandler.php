<?php
namespace Commercers\ProductExport\Service;

class ProfilerSavingHandler {
    
    public function __construct(
            \Commercers\ProductExport\Model\RuleFactory $ruleFactory
            ) {
        $this->ruleFactory = $ruleFactory;
    }
    
    public function execute($profiler, $data){
        
        if (isset($data['rule'])) {
            
            $rules = $this->ruleFactory->create()->getCollection();
            if($profiler->getDataSource() == 'product'){
                $rules->addFieldToFilter('data_source', $profiler->getDataSource());
                $rules->addFieldToFilter('profiler_id', $profiler->getId());

                if($rules->getSize()){
                    $rule = $rules->getFirstItem();
                }else{
                    $rule = $this->ruleFactory->create();
                }


                $rule->addData([
                    'profiler_id' => $profiler->getId(),
                    'data_source' => $profiler->getDataSource()
                ]);

                $rule->loadPost($data['rule'])->save();
            }
            
        }
            
    }
}