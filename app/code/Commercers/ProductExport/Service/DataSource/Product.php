<?php

namespace Commercers\ProductExport\Service\DataSource;

use mysql_xdevapi\Exception;

class Product implements \Commercers\Profilers\Service\DataSource
{

    protected $productAttributeRepository;
    protected $_attributeFactory;

    public function __construct(
        \Magento\Framework\Api\ExtensibleDataObjectConverter $dataArrayConvert,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Commercers\ProductExport\Model\RuleFactory $ruleFactory,
        \Commercers\ProductExport\Model\Condition\Sql\Builder $conditionSqlBuilder,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Commercers\Profilers\Model\ObjectLogFactory $objectFactory,
        \Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository,
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $attributeFactory

    )
    {

        $this->_productCollectionFactory = $productCollectionFactory;
        $this->dataArrayConvert = $dataArrayConvert;
        $this->productFactory = $productFactory;
        //$this->_stockItemRepository = $stockItemRepository;
        $this->conditionSqlBuilder = $conditionSqlBuilder;
        $this->ruleFactory = $ruleFactory;
        $this->stockItemRepository = $stockItemRepository;
        $this->stockRegistry = $stockRegistry;
        $this->objectFactory = $objectFactory;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->_attributeFactory = $attributeFactory;
    }

    protected $_proceededIds = array();


    public function _getCollection($profiler = null)
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        if ($profiler) {
            $rules = $this->ruleFactory->create()->getCollection();
            $rules->addFieldToFilter('data_source', $profiler->getDataSource());
            $rules->addFieldToFilter('profiler_id', $profiler->getId());

            if ($rules->getSize()) {
                $rule = $rules->getFirstItem();

                if ($rule->getConditions()) {
                    //$this->conditionSqlBuilder->attachConditionToCollection($collection, $rule->getConditions());

                }

            }
        }
        //echo $collection->getSelect();exit;
        return $collection;
    }

    public function getCount($profiler)
    {
        $collection = $this->_getCollection($profiler);
        return $collection->count();
    }


    public function getData($profiler = null, $limits = array())
    {
        //echo 1; exit;
        $collection = $this->_getCollection($profiler);

        if ($limits && isset($limits['limit']) && isset($limits['offset'])) {
            $collection->getSelect()->limit($limits['limit'], $limits['offset']);

        }

        $data = array(
            'items' => array('item' => array())
        );
        //echo $collection->getSelect();exit;

        foreach ($collection as $product) {

            $productData = $product->getData();
            $attributes_code = $this->getAllAttributesCode();

            foreach ($productData as $key => &$value) {
                if (in_array($key, $attributes_code)) {
                    $attr = $product->getResource()->getAttribute($key);

                    $optionText = $value;
                    if ($attr->usesSource()) {
                        $optionText = $attr->getSource()->getOptionText($value);
                    }

                    if (is_object($optionText)) {
                        $value = $optionText->getText();
                    } else {
                        $value = $optionText;
                    }
                }

            }
            //can't use html tag inside xsl condition
            if (isset($productData['short_description']) AND $productData['short_description'] == '<p>to come</p>'
            OR !isset($productData['short_description'] ))
            {
                $productData['short_description'] = null;
            }

           // $productData['images'] = $this->_getMediaGallery($product->getId());
            try {
                $stockItem = $this->stockRegistry->getStockItem($productData['entity_id']);
                $productData['stock'] = $stockItem->getData();

            } catch (\Magento\Framework\Exception\NoSuchEntityException\NoSuchEntityException $ex) {
                //echo 1; exit; 
                $productData['stock'] = array(
                    'qty' => 0,
                    'qty_increment' => 0
                );
            }
            $this->_proceededIds[] = $productData['entity_id'];

            $item['images'] = $this->_getMediaGallery($product->getId());
            
            $data['items']['item'][] = $productData;
        }
        //echo "<pre>"; print_r($data);exit;
        return $data;
    }

    public function getItemById($id, $profiler = null, $isIncrement = false)
    {

        $data = array(
            'items' => array('item' => array())
        );
        $item = array();

        $collection = $this->_getCollection($profiler);

        $collection = $collection->addFieldToFilter('entity_id', $id);
        $product = $collection->getFirstItem();

        if ((int)$product->getId() <= 0) {
            return false;
        } else {
            $item = $product->getData();
            $attributes_code = $this->getAllAttributesCode();

            foreach ($item as $key => &$value) {
                if (in_array($key, $attributes_code)) {
                    $attr = $product->getResource()->getAttribute($key);
                    $optionText = $value;
                    if ($attr->usesSource()) {
                        $optionText = $attr->getSource()->getOptionText($value);
                    }
                    if (is_object($optionText)) {
                        $value = $optionText->getText();
                    } else {
                        $value = $optionText;
                    }
                }

            }
            //can't use html tag inside xsl condition
            if (isset($productData['short_description']) AND $productData['short_description'] == '<p>to come</p>'
                OR !isset($productData['short_description'] )){
                $item['short_description'] = null;
            }

            try {
                $stockItem = $this->stockRegistry->getStockItem($item['entity_id']);
                //$stockItem = $this->stockItemRepository->get($item['entity_id']);

                $item['stock'] = $stockItem->getData();

            } catch (\Magento\Framework\Exception\NoSuchEntityException\NoSuchEntityException $ex) {
                $productData['stock'] = array(
                    'qty' => 0,
                    'qty_increment' => 0
                );
            }
            //media gallery

            $item['images'] = $this->_getMediaGallery($product->getId());

            $data['items']['item'][] = $item;


            return $data;
        }
    }

    public function getDataManual($dataForm, $profiler)
    {

    }


    public function log($profiler, $processLog)
    {
        /*
        if(count($this->_proceededIds)){
            $actionsSerialized = $profiler->getActionsSerialized();
            $actions = unserialize($actionsSerialized);
            if(count($actions) && isset($actions['actions'])){
                foreach($this->_proceededIds as $proceededId){
                    //$this->doAction($actions['actions'], $proceededId);
                }
            }
            
            
            //print_r($actions);exit;
        }
         * 
         */


        if (count($this->_proceededIds)) {
            foreach ($this->_proceededIds as $proceededId) {
                $objectLog = $this->objectFactory->create();
                $objectLog->addData(array(
                    'profiler_id' => $profiler->getId(),
                    'datasource' => 'product',
                    'process_log_id' => $processLog->getId(),
                    'object_id' => $proceededId
                ));
                $objectLog->save();
            }
        }

    }

    public function getAllAttributesCode()
    {
        $attribute_data = [];
        $attributeInfo = $this->_attributeFactory->create();
        foreach ($attributeInfo as $items) {
            if ($items->getFrontendInput() === 'select' || $items->getFrontendInput() === 'multiselect')
                $attribute_data[] = $items->getAttributeCode();
        }
//        echo '<pre>';
//        print_r($attribute_data);exit();
        return $attribute_data;
    }

    protected function _getMediaGallery($productId)
    {
        $product = $this->productFactory->create()->load($productId);
        $mediaGallery = $product->getMediaGalleryImages();
        $images = array();
        foreach ($mediaGallery as $media) {
            $images[] = $media->getData();
        }
        return $images;
    }
}
