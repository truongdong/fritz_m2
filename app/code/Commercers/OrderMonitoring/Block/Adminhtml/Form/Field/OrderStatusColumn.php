<?php

namespace Commercers\OrderMonitoring\Block\Adminhtml\Form\Field;

use Magento\Framework\View\Element\Html\Select;
use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory;
use Magento\Backend\Block\Template\Context;

class OrderStatusColumn extends Select
{
    /**
     * @var CollectionFactory $statusCollectionFactory
     */
    protected $_orderStatusCollectionFactory;

    public function __construct(
        Context $context,
        CollectionFactory $orderStatusCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_orderStatusCollectionFactory = $orderStatusCollectionFactory;
    }

    /**
     * Set "name" for <select> element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Set "id" for <select> element
     *
     * @param $value
     * @return $this
     */
    public function setInputId($value)
    {
        return $this->setId($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml(): string
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->getSourceOptions());
        }
        return parent::_toHtml();
    }

    private function getSourceOptions(): array
    {
        $options = $this->_orderStatusCollectionFactory->create()->toOptionArray();
        return $options;
    }
}