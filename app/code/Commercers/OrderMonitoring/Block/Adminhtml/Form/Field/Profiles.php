<?php
namespace Commercers\OrderMonitoring\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Commercers\OrderMonitoring\Block\Adminhtml\Form\Field\OrderStatusColumn;
use Commercers\OrderMonitoring\Block\Adminhtml\Form\Field\IncludeWeekendColumn;
use Commercers\OrderMonitoring\Block\Adminhtml\Form\Field\TextAreaColumn;

/**
 * Class Profiles
 */
class Profiles extends AbstractFieldArray
{
    /**
     * @var OrderStatusColumn
     */
    private $orderStatusRenderer;

    /**
     * @var IncludeWeekendColumn
     */
    private $includeWeekendRenderer;

    /**
     * @var IncludeWeekendColumn
     */
    private $textAreaRenderer;

    /**
     * Prepare rendering the new field by adding all the needed columns
     */
    protected function _prepareToRender()
    {
        $this->addColumn('current_status', [
            'label' => __('Status'), 
            'style' => 'width:110px',
            'renderer' => $this->getOrderStatusRenderer()
        ]);
        $this->addColumn('delay', [
            'label' => __('Delay In Days'),
            'style' => 'width:30px',
            'class' => 'required-entry validate-number',
        ]);

        $this->addColumn('include_weekend', [
            'label' => __('Include Weekend'), 
            'renderer' => $this->getIncludeWeekendRenderer()
        ]);
        $this->addColumn('headline', [
            'label' => __('Headline'), 
            'style' => 'width:150px',
            'class' => 'required-entry'
        ]);
        $this->addColumn('warningtext', [
            'label' => __('Text'), 
            'renderer' => $this->getTextAreaRenderer()
        ]);
        $this->addColumn('emailaddresses', [
            'label' => __('Email Addresses'), 
            'style' => 'width:150px',
            'class' => 'required-entry'
        ]);
        $this->addColumn('cron_expression', [
            'label' => __('Cron'), 
            'style' => 'width:100px',
            'class' => 'required-entry'
        ]);
        $this->addColumn('new_status', [
            'label' => __('New Status'),
            'style' => 'width:110px',
            'renderer' => $this->getOrderStatusRenderer()
        ]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
        parent::_construct();
    }

    /**
     * Prepare existing row data object
     *
     * @param DataObject $row
     * @throws LocalizedException
     */
    protected function _prepareArrayRow(DataObject $row): void
    {
        $options = [];

        // $tax = $row->getTax();
        // if ($tax !== null) {
        //     $options['option_' . $this->getTaxRenderer()->calcOptionHash($tax)] = 'selected="selected"';
        // }

        $row->setData('option_extra_attrs', $options);
    }

    /**
     * @return OrderStatusColumn
     * @throws LocalizedException
     */
    private function getOrderStatusRenderer()
    {
        if (!$this->orderStatusRenderer) {
            $this->orderStatusRenderer = $this->getLayout()->createBlock(
                OrderStatusColumn::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->orderStatusRenderer;
    }

    /**
     * @return IncludeWeekendColumn
     * @throws LocalizedException
     */
    private function getIncludeWeekendRenderer()
    {
        if (!$this->includeWeekendRenderer) {
            $this->includeWeekendRenderer = $this->getLayout()->createBlock(
                IncludeWeekendColumn::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->includeWeekendRenderer;
    }

    /**
     * @return TextAreaColumn
     * @throws LocalizedException
     */
    private function getTextAreaRenderer()
    {
        if (!$this->textAreaRenderer) {
            $this->textAreaRenderer = $this->getLayout()->createBlock(
                TextAreaColumn::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->textAreaRenderer;
    }

    
}