<?php
namespace Commercers\OrderMonitoring\Cron;

use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Commercers\OrderMonitoring\Helper\Email\Template\TransportBuilder;

use Psr\Log\LoggerInterface;

class ReportScript {
    protected $_logger;

    // protected $_orderCollectionFactory;

    // protected $_iterator;

    // protected $_orderRepository;

    // protected $_logFactory;

    // protected $_directoryList;

    // protected $_fp;

    /**
     * @var CollectionFactory $statusCollectionFactory
     */
    protected $_orderStatusCollectionFactory;

    protected $_scopeConfig;

    protected $_orderCollectionFactory;

    protected $_directoryList;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    private $_inlineTranslation;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    private $_transportBuilder;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    public function __construct(LoggerInterface $logger,
            // \Magento\Quote\Model\ResourceModel\Quote\CollectionFactory $quoteCollectionFactory,
            // \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
            // \Magento\Framework\Model\ResourceModel\Iterator $iterator,
            // \Magento\Sales\Model\OrderRepository $orderRepository,
            // \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
            // \Magento\Customer\Model\LoggerFactory $logFactory
            CollectionFactory $orderStatusCollectionFactory,
            ScopeConfigInterface $scopeConfig,
            \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
            \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
            \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
            TransportBuilder $transportBuilder,
            \Magento\Store\Model\StoreManagerInterface $storeManager) {
        $this->_logger = $logger;
        // $this->_quoteCollectionFactory = $quoteCollectionFactory;
        // $this->_orderCollectionFactory = $orderCollectionFactory;
        // $this->_iterator = $iterator;
        // $this->_orderRepository = $orderRepository;
        // $this->_directoryList = $directoryList;
        // $this->_logFactory = $logFactory;
        $this->_orderStatusCollectionFactory = $orderStatusCollectionFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_directoryList = $directoryList;
        $this->_storeManager = $storeManager;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
    }

   /**
    * Write to system.log
    *
    * @return void
    */
    public function execute() 
    {
        $this->_logger->info('Update commercers monitoring data - START');
        $this->proceedMonitoringProfile();
        $this->_logger->info('Update commercers monitoring data - FINISH');
    }

    public function proceedMonitoringProfile()
    {
        $configDataEnabled = $this->_scopeConfig->getValue('order_monitoring/general/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
        if ($configDataEnabled) 
        {
            //email template
            $configDataEmail = $this->_scopeConfig->getValue('order_monitoring/general/email_template', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);


            $configDataProfile = $this->_scopeConfig->getValue('order_monitoring/general/profiles', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $arr = (array)json_decode($configDataProfile, true);

            foreach ($arr as $profileItem) 
            {
                //Array ( [current_status] => pending [delay] => 1 [include_weekend] => 0 [headline] => 1111 [warningtext] => 222 [emailaddresses] => minhbk87@gmail.com [cron_expression] => **** [new_status] => complete ) 

                //1. check match cron
                // echo $this->parse_crontab(date("Y-m-d h:i:sa"), '* * * * *');
                if($this->parse_crontab(date("Y-m-d h:i:sa"), $profileItem['cron_expression']))
                {
                    //find orders matches with status and delay
                    $orderCollection = $this->_orderCollectionFactory->create()->addAttributeToSelect('*');

                    //TODO: check include weekend
                    if($profileItem['include_weekend'])
                    {
                        $toDate = date('Y-m-d', strtotime("-". $profileItem['delay'] . " day"));
                    } 
                    else 
                    {
                        $toDate = date('Y-m-d', strtotime("-". $profileItem['delay'] . " weekday"));
                    }

                    echo $toDate;

                    $orderCollection
                        ->addFieldToFilter('created_at', array('lteq' => $toDate))
                    // ->addFieldToFilter('increment_id', '2000000008')
                        ->addAttributeToFilter('status',$profileItem['current_status'])
                        ->load();
                    // echo $orderCollection->getSelect();
                    // print_r($orderCollection->getData());

                    //2. export to csv
                    $fileDirectoryPath = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR);

                    if(!is_dir($fileDirectoryPath))
                        mkdir($fileDirectoryPath, 0777, true);
                    
                    $fileName = preg_replace('/[[:space:]]+/', '_',$profileItem['headline']) . '.csv';
                    $filePath =  $fileDirectoryPath . '/export/' . $fileName;
                    
                    $this->_fp = fopen($filePath,"w");

                    fputcsv($this->_fp, ['Headline:' . $profileItem['headline']]);
                    fputcsv($this->_fp, ['']);

                    $dataCsv = array();
                    $dataCsv[] = 'Order#';
                    $dataCsv[] = 'Status';
                    $dataCsv[] = 'Status since (date)';
                    $dataCsv[] = 'Status unchanged for days';
                    
                    fputcsv($this->_fp, $dataCsv); 

                    foreach ($orderCollection as $_order) {
                        $orderCsv = array();
                        $orderCsv[] = $_order->getIncrementId();
                        $orderCsv[] = $_order->getStatusLabel();
                        $orderCsv[] = $_order->getCreatedAt();

                        $diff = abs(floor((strtotime(date('Y-m-d'))-strtotime($_order->getCreatedAt()))/(60*60*24)));

                        $orderCsv[] = $diff;
                        fputcsv($this->_fp, $orderCsv);
                        
                        //update order status
                        if($profileItem['new_status'] != 'NotChange')
                        {
                            $_order->setStatus($profileItem['new_status']);
                            $_order->addStatusToHistory($_order->getStatus(), 'changed by Order Monitoring');
                            $_order->save(); 
                        }
                    }

                    fclose($this->_fp);

                    //3. send email
                    //3.1 split email address
                    
                    /* Receiver Detail */

                    $receiverInfo = [
                        'name' => 'Admin',
                        'email' => explode(";", $profileItem['emailaddresses'])
                    ];

                    /* Assign values for your template variables  */
                    $templateVars = [
                        'headline' => $profileItem['headline'],
                        'warningtext' => $profileItem['warningtext']
                    ];

                    $this->inlineTranslation->suspend();
                    $this->generateTemplate($templateVars, $receiverInfo, $configDataEmail, $fileName);
                    $transport = $this->_transportBuilder->getTransport();

                    $transport->sendMessage();
                    $this->inlineTranslation->resume();

                }
            }
        }
    }
    
    /**
     * Return email for sender header
     * @return mixed
     */
    public function getEmailSender()
    {
        return $this->_scopeConfig->getValue(
            self::XML_PATH_EMAIL_SENDER,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param $variable
     * @param $receiverInfo
     * @param $templateId
     * @return $this
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function generateTemplate($variable, $receiverInfo, $templateId, $fileName)
    {
        $content = file_get_contents('var/export/' . $fileName);
        $from = [
                'email' => $this->_scopeConfig->getValue(
            'trans_email/ident_general/email',\Magento\Store\Model\ScopeInterface::SCOPE_STORE, 'name' => $this->_scopeConfig->getValue(
            'trans_email/ident_general/name',\Magento\Store\Model\ScopeInterface::SCOPE_STORE];

        $this->_transportBuilder->setTemplateIdentifier($templateId)
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_ADMINHTML,
                    'store' => $this->_storeManager->getStore()->getId(),
                ]
            )
            ->setTemplateVars($variable)
            ->setFrom($from)
            ->addAttachment($content, $fileName, 'csv')
            ->addTo($receiverInfo['email'], $receiverInfo['name']);

        return $this;
    }

    function parse_crontab($time, $crontab) {
        // Get current minute, hour, day, month, weekday
        $time = explode(' ', date('i G j n w', strtotime($time)));
        // Split crontab by space
        $crontab = explode(' ', $crontab);
        // Foreach part of crontab
        foreach ($crontab as $k => &$v) {
            // Remove leading zeros to prevent octal comparison, but not if number is already 1 digit
            $time[$k] = preg_replace('/^0+(?=\d)/', '', $time[$k]);
            // 5,10,15 each treated as seperate parts
            $v = explode(',', $v);
            // Foreach part we now have
            foreach ($v as &$v1) {
                // Do preg_replace with regular expression to create evaluations from crontab
                $v1 = preg_replace(
                    // Regex
                    array(
                        // *
                        '/^\*$/',
                        // 5
                        '/^\d+$/',
                        // 5-10
                        '/^(\d+)\-(\d+)$/',
                        // */5
                        '/^\*\/(\d+)$/'
                    ),
                    // Evaluations
                    // trim leading 0 to prevent octal comparison
                    array(
                        // * is always true
                        'true',
                        // Check if it is currently that time, 
                        $time[$k] . '===\0',
                        // Find if more than or equal lowest and lower or equal than highest
                        '(\1<=' . $time[$k] . ' and ' . $time[$k] . '<=\2)',
                        // Use modulus to find if true
                        $time[$k] . '%\1===0'
                    ),
                    // Subject we are working with
                    $v1
                );
            }
            // Join 5,10,15 with `or` conditional
            $v = '(' . implode(' or ', $v) . ')';
        }
        // Require each part is true with `and` conditional
        $crontab = implode(' and ', $crontab);
        // Evaluate total condition to find if true
        return eval('return ' . $crontab . ';');
    }

    // public function proceedMonitoringProfile()
    // {
    //     try{

    //         //load data from Monitoring Config Table

    //         //proceed for each row
    //         $this->_iterator
    //             ->walk(
    //                 $collection->getSelect(),
    //                 array(array($this, 'callbackProceedProfileItem'))
    //             );
    //         fclose($this->_fp);  
    //     } catch (Exception $e) {
    //         $this->logger->error($e);
    //     }
    // }

    // public function callbackProceedProfileItem($args)
    // {
        //1. check match cron
        //1.1 load order matched with status and delay in days
        //1.2 check next status

        //$orderId = 3;
        // $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        // $order = $objectManager->create('\Magento\Sales\Model\Order')->load($orderId);
        // $state = $order->getState();
        // $status = 'custom_cancel';
        // $comment = '';
        // $isNotified = false;
        // $order->setState($state);
        // $order->setStatus($status);
        // $order->addStatusToHistory($order->getStatus(), $comment);
        // $order->save(); 

        //2. export to csv
        // $fileDirectoryPath = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR);

        // if(!is_dir($fileDirectoryPath))
        //     mkdir($fileDirectoryPath, 0777, true);
        
        // $filePath =  $fileDirectoryPath . '/export/commercers_report_sales.csv';
        
        // $this->_fp = fopen($filePath,"w");

        // fputcsv($this->_fp, "variable headline");

        // $dataCsv = array();
        // $dataCsv[] = 'Order#';
        // $dataCsv[] = 'Status';
        // $dataCsv[] = 'Status since (date)';
        // $dataCsv[] = 'Status unchanged for days';
        
        // fputcsv($this->_fp, $dataCsv); 

        //3. send email
        //3.1 split email address
        //3.2 load config template
        //3.3 fill data to body
        //3.4 attach csv
        //3.5 send
        
    // }
}