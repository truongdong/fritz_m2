<?php

namespace Commercers\OrderMonitoring\Observer\Adminhtml;

class LayoutHandler implements \Magento\Framework\Event\ObserverInterface
{
    public function __construct(\Magento\Framework\App\Request\Http\Proxy $request)
    {
        $this->request = $request;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $params = $this->request->getParams();

        if (! empty($params['section'])) {
            $moduleName = $this->getModuleName();
            if ($params['section'] == "order_monitoring") { //Here you can use your section ID which is available in your adminhtml/system.xml file's <section id='whatever'>
                $layout = $observer->getData('layout');
                $layout->getUpdate()->addHandle('adminhtml_system_config_edit_section_custom_handler');
            }
        }
    }

    private function getModuleName()
    {
        $class = get_class($this);
        $moduleName = strtolower(
            str_replace('\\', '_', substr($class, 0, strpos($class, '\\Observer')))
        );
        return (string) $moduleName;
    }
}