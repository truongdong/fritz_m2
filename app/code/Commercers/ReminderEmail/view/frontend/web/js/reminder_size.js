
define([
    'jquery',
    'mage/translate'
], function ($,$t) {
    'use strict';
    $.widget(
        'mage.reminder_size',
        {
            _create: function () {
                var self = this;
                var options = self.options;
                self.actionInit(options);
            },
            actionInit: function (options) {
                var self = this;
                self.actionClick(options);
            },
            actionClick: function (options) {
                var self = this;
                $(document).on('click', '.size-out-stock-item', function(){
                    var currentButtonSize = this;
                    $(this).addClass("active");
                    var valueSize = $(this).val();
                    var textField = $('.product-id-customer-select');
                    if(textField.val()){
                        var arrTextField = textField.val().split(",");
                        if(self.checkValueSize(valueSize,arrTextField)){
                            var arrTextField = self.removeValueOutInput(valueSize,arrTextField);
                            $(currentButtonSize).removeClass("active");
                            textField.val(arrTextField);
                        }else{
                            textField.val(textField.val() + ',' + valueSize);
                        }
                    }else{
                        textField.val(textField.val() + valueSize);
                    }
                });
                $(document).on('click', '.submit-data', function(){
                    var emailEnter = $('#email-enter').val();
                    var productId = $('#product-id-customer-select').val();
                    var dataCheck = self.checkFieldEnter(emailEnter,productId);
                    if(dataCheck["error"]){
                        if(dataCheck["message_email"]){
                            $(".message-error").append('<p>'+dataCheck["message_email"]+'</p>');
                        }
                        if(dataCheck["message_size"]){
                            $(".message-error").append('<p>'+dataCheck["message_size"]+'</p>');
                        }
                    }else{
                        self.ajaxSubmit(emailEnter,productId,options);

                    }
                    $('#submit-data').attr('disabled', 'disabled');
                    $("#email-enter").val('');
                    $(".size-out-stock-item").removeClass('active');
                    setTimeout(function(){
                        $(".message-error").html('');
                        $(".message-success").html('');
                        $('#submit-data').removeAttr("disabled");
                    }, 3000);
                });
            },

            checkFieldEnter:function(emailEnter,productId){
                var data = [];
                data["error"] = false;
                if(!emailEnter){
                    data["error"] = true;
                    data["message_email"] = $t('Please enter your email address.');
                }
                if(!productId){
                    data["error"] = true;
                    data["message_size"] = $t('Please choose your size.');
                }
                return data;
            },
            checkValueSize: function(valueSize,arrTextField){
                var valueExists = false;
                for(var i=0; i<arrTextField.length; i++){
                    var valueCur = arrTextField[i];
                    if(valueCur == valueSize){
                        var valueExists = true;
                    }
                }
                return valueExists;
            },
            removeValueOutInput: function(valuetoremove, myarray){
                var indexofmyvalue = myarray.indexOf(valuetoremove);
                myarray.splice(indexofmyvalue, 1);
                return myarray;
            },
            ajaxSubmit: function(emailEnter,productIds,options){
                $.ajax({
                    url: options.urlReminderCustomer,
                    data: {
                        emailEnter: emailEnter,
                        productIds:productIds
                    },
                    async: false,
                    type: "POST",
                    dataType: 'json'
                }).done(function(response) {
                    var messageNotice = '';
                    if(response.message){
                        $(".message-success").append('<p>'+response.messageNotice+'</p>');
                    }else{
                        $(".message-error").append('<p>'+response.messageNotice+'</p>');
                    }

                }).error(function() {
                });
            },

        }
    );
    return $.mage.reminder_size;
});
