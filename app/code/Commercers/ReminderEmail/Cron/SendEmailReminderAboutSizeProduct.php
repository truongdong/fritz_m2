<?php
namespace Commercers\ReminderEmail\Cron;

class SendEmailReminderAboutSizeProduct
{
    protected $_scopeConfig;

    protected $_reminderEmailCollectionFactory;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Commercers\ReminderEmail\Model\ResourceModel\ReminderEmail\CollectionFactory $reminderEmailCollectionFactory,
        \Magento\Framework\App\Action\Context $context
    )
    {
        $this->_reminderEmailCollectionFactory = $reminderEmailCollectionFactory;
        $this->_scopeConfig = $scopeConfig;
    }

    public function execute()
    {
        $reminderEmailCollection = $this->_reminderEmailCollectionFactory->create();
        $reminderEmailCollection->addFieldToFilter('status', array('eq' => \Commercers\ReminderEmail\Model\ReminderEmail::STATUS_PENDING));
        $reminderEmails = $reminderEmailCollection->getData();
        $reminderArr = [];

        foreach($reminderEmails as $reminderEmail){
            if($approval["order_id"]){
                if (!$this->_scopeConfig->getValue(self::XML_IS_SEND_REMINDER)){
                    continue;
                }
                try {
                    $hours = floor((time() - strtotime($approval['last_send_reminder_at'])) / 86400);
                    if ($hours >= floor($this->_scopeConfig->getValue(self::XML_REMINDER_DELAY))) {
                        if (!isset($reminderApprovals[$approval["order_id"]])) {
                            $reminderApprovals[$approval["order_id"]] = array();
                        }
                        $reminderApprovals[$approval["order_id"]][] = $approval;
                    }
                } catch (Exception $ex) {
                    echo $ex->getMessage();exit;
                }
            }
        }

        foreach ($reminderApprovals as $orderId => $approvalItems) {
            $order = $this->_orderFactory->create()->load($orderId);
            foreach ($approvalItems as $approvalItem) {

                $supervisorInformation = [];
                $arrApprovalName = explode(",", $approvalItem["approval_name"]);
                $arrApprovalEmail = explode(",", $approvalItem["approval_email"]);
                foreach ($arrApprovalEmail as $key => $approvalEmail){
                    $supervisorInformation["email"] = $approvalEmail;
                    $supervisorInformation["name"] = $arrApprovalName[$key];
                    $helperEmail = $this->_helperEmailFactory->create();
                    $helperEmail->sendReminderEmail($approvalItem,$order,$typeEmail = 2,$supervisorInformation["name"],$supervisorInformation["email"]);
                }
            }

        }
        return;
    }
}
