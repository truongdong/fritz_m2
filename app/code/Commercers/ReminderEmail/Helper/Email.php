<?php

namespace Commercers\ReminderEmail\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Exception\MailException;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Message\ManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;

class Email extends AbstractHelper
{
    const EMAIL_TEMPLATE_REMINDER_CUSTOMER = 'reminder/email/template_reminder_customer';

    /**
     * @var StateInterface
     */
    protected $_inlineTranslation;

    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var UrlInterface
     */
    protected $_url;

    protected $_template;

    protected $_messageManager;

    public function __construct(
        Context $context,
        StateInterface $inlineTranslation,
        TransportBuilder $transportBuilder,
        ManagerInterface $messageManager,
        StoreManagerInterface $storeManager,
        CustomerSession $customerSession,
        UrlInterface $url
    ) {
        parent::__construct($context);
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_customerSession = $customerSession;
        $this->_storeManager = $storeManager;
        $this->_messageManager = $messageManager;
        $this->_url = $url;
    }

    protected function getConfigValue($path, $storeId)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    protected function getTemplateId($xmlPath)
    {
        return $this->getConfigValue($xmlPath, $this->_storeManager->getStore()->getId());
    }

    protected function generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo)
    {
        $template = $this->_transportBuilder->setTemplateIdentifier($this->_template)
            ->setTemplateOptions(
                [
//                     'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'area' => \Magento\Framework\App\Area::AREA_ADMINHTML,
                    'store' => $this->_storeManager->getStore()->getId(),
                ]
            )
            ->setTemplateVars([
                'data' => $emailTemplateVariables,
                'url' => $this->_url->getBaseUrl()
            ])
            ->setFrom($senderInfo)
            ->addTo($receiverInfo['email'], $receiverInfo['name'])
            ->setReplyTo($senderInfo['email'], $senderInfo['name']);
        return $template;
    }

    public function sendEmail($template, $emailTemplateVariables)
    {

        $result = [
            'error' => false
        ];
        $this->_template = $this->getTemplateId($template);
        $this->_inlineTranslation->suspend();
        $senderInfo = [
            'email' => $emailTemplateVariables['customer_email'],
//            'name' => $emailTemplateVariables['name']
            'name' => 'Truong'
        ];
        $receiverInfo = [
            'email' => $this->getConfigValue('trans_email/ident_general/email', $this->_storeManager->getStore()->getId()),
            'name' => $this->getConfigValue('trans_email/ident_general/name', $this->_storeManager->getStore()->getId())
        ];

        $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);

        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $result = [
                'error' => false,
                'message' => 'Email is sent successfully'
            ];
        } catch (\Exception $e) {
            $result = [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }

        $this->_inlineTranslation->resume();
    }

    public function sendReminderEmail($emailTempVariables)
    {
        $this->sendEmail(self::EMAIL_TEMPLATE_REMINDER_CUSTOMER, $emailTempVariables);
    }

}
