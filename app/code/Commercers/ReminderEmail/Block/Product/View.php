<?php

namespace Commercers\ReminderEmail\Block\Product;

use Magento\Customer\Model\ResourceModel\GroupRepository;
use Magento\Eav\Model\ConfigFactory;

class View extends \Magento\Framework\View\Element\Template
{
    protected $_urlInterface;

    protected $_registry;

    protected $_productFactory;

    protected $_productrepository;

    protected $_configFactory;

    protected $_localeResolver;

    protected $_customerSession;
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Locale\Resolver $localeResolver,
        \Magento\Eav\Model\ConfigFactory $configFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productrepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Framework\View\Element\Template\Context $context
    ) {
        $this->_customerSession = $customerSession;
        $this->_localeResolver = $localeResolver;
        $this->_configFactory = $configFactory;
        $this->_productrepository = $productrepository;
        $this->_productFactory = $productFactory;
        $this->_registry = $registry;
        $this->_request = $request;
        $this->_urlInterface = $urlInterface;
        parent::__construct($context);
    }
    public function getBaseUrls()
    {
        return $this->_urlInterface->getBaseUrl();
    }

    public function getCurrentProduct(){
        return $this->_registry->registry('current_product');
    }

    public function getProductDataUsingId($productid) {
        return $this->_productrepository->getById($productid);
    }

    public function getChildsProductOutStock(){
        $currProductId = $this->getCurrentProduct()->getId();
        $configProduct = $this->_productFactory->create()->load($currProductId);
        $_children = $configProduct->getTypeInstance()->getUsedProductCollection($configProduct);
        $arrChildProduct = [];
        $count = 0;
        foreach ($_children as $child){
            $productData = $this->getProductDataUsingId($child->getId());
//            $eavConfig = $this->_configFactory->create();
//            $attribute = $eavConfig->getAttribute('catalog_product', 'size');
//            $optionlabel =  $attribute->getSource()->getOptionText($productData->getSize());
//            var_dump($optionlabel);exit;
            $productSizeLabel = $this->getProductSizeLabel($productData->getSize());
            $productSockSizeLabel = $this->getProductSockSizeLabel($productData->getSockSize());
            if(!$productData->getQuantityAndStockStatus()["is_in_stock"]){
                $arrChildProduct[$count]['entity_id'] = $productData->getEntityId();
                $arrChildProduct[$count]['is_in_stock'] = $productData->getQuantityAndStockStatus()["is_in_stock"];
                $arrChildProduct[$count]['size_value'] = $productData->getSize();
                $arrChildProduct[$count]['size_label'] = $productSizeLabel;
                $arrChildProduct[$count]['sock_size_value'] = $productData->getSockSize();
                $arrChildProduct[$count]['sock_size_label'] = $productSockSizeLabel;
            }
            $count++;
        }
        return $arrChildProduct;
    }
    public function getProductSizeLabel($productSize)
    {
        $eavConfig = $this->_configFactory->create();
        $attribute = $eavConfig->getAttribute('catalog_product', 'size');
        return $attribute->getSource()->getOptionText($productSize);
    }
    public function getProductSockSizeLabel($productSize)
    {
        $eavConfig = $this->_configFactory->create();
        $attribute = $eavConfig->getAttribute('catalog_product', 'sock_size');
        return $attribute->getSource()->getOptionText($productSize);
    }
    public function getEmailPlaceHolder()
    {
        $currentLocaleCode = $this->_localeResolver->getLocale();
        $emailPlaceHolder = 'meine@e-mail.de';
        if($currentLocaleCode == 'en_US'){
            $emailPlaceHolder = 'my@email.com';
        }
        return $emailPlaceHolder;
    }

    public function customerIsLoggedIn()
    {
        $customerSession = $this->_customerSession->isLoggedIn();
        return $customerSession;
    }

}
