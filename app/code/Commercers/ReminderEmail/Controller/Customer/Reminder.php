<?php
namespace Commercers\ReminderEmail\Controller\Customer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;

class Reminder extends Action{

    const STATUS_SEND = 1;
    const STATUS_PENDING = 0;
    protected $customerRepository;

    protected $_storeManager;

    protected $_productRepository;

    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        Context $context,
        PageFactory $pageFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ){
        $this->_productRepository = $productRepository;
        $this->customerRepository = $customerRepository;
        $this->_pageFactory = $pageFactory;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }
    public function execute()
    {
        $data['message'] = false;
        $params = $this->getRequest()->getParams();
        $productIdsSelect = $this->getRequest()->getParam('productIds');
        $arrProductIds = explode(",",$productIdsSelect);
        try{
            $customerId = $this->getCustomerId($this->getRequest()->getParam('emailEnter'));
            $store = $this->_storeManager->getStore();
            foreach ($arrProductIds as $productId) {
                /* @var $product \Magento\Catalog\Model\Product */
                $product = $this->_productRepository->getById((int)$productId);
                /** @var \Magento\ProductAlert\Model\Stock $model */
                $model = $this->_objectManager->create(\Magento\ProductAlert\Model\Stock::class)
                    ->setCustomerId($customerId)
                    ->setProductId($product->getId())
                    ->setWebsiteId($store->getWebsiteId())
                    ->setStoreId($store->getId());
                $model->save();
                $data['message'] = true;
                $data['messageNotice'] = __("Alert subscription has been saved.");
            }
        } catch (\Exception $e) {
            $data['message'] = false;
            $data['messageNotice'] = __("The alert subscription couldn't update at this time. Please try again later.");
        }
        $response = $this->resultFactory
            ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
            ->setData($data);
        return $response;
    }

    public function getCustomerId($email)
    {
        $customer = $this->customerRepository->get($email);
        $customerId = $customer->getId();
        return $customerId;
    }
}
