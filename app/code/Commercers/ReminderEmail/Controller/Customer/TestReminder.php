<?php
namespace Commercers\ReminderEmail\Controller\Customer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;

class TestReminder extends Action
{
    const STATUS_SEND = 1;
    const STATUS_PENDING = 0;

    protected $_scopeConfig;

    protected $_reminderEmailCollectionFactory;

    protected $_helperEmailFactory;

    public function __construct(
        \Commercers\ReminderEmail\Helper\EmailFactory $helperEmailFactory,
        \Commercers\ReminderEmail\Model\ResourceModel\ReminderEmail\CollectionFactory $reminderEmailCollectionFactory,
        StoreManagerInterface $storeManager,
        Context $context,
        PageFactory $pageFactory
    ){
        $this->_helperEmailFactory = $helperEmailFactory;
        $this->_reminderEmailCollectionFactory = $reminderEmailCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $reminderEmailCollection = $this->_reminderEmailCollectionFactory->create();
        $reminderEmailCollection->addFieldToFilter('status', array('eq' => self::STATUS_PENDING));
        $reminderEmails = $reminderEmailCollection->getData();
        echo "</pre>";
//        print_r($reminderEmails);exit;
        $reminderArr = [];

        foreach($reminderEmails as $reminderEmail){
            if(!$reminderEmail["status"]){
                $reminderArr[] = $reminderEmail;
            }
        }

        foreach ($reminderArr as $reminder) {
            $helperEmail = $this->_helperEmailFactory->create();
            try {
                $helperEmail->sendReminderEmail($reminder);
                echo 123;exit;
                $this->messageManager->addSuccessMessage(__('You send mail success.'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the record.'));
            }

        }
        return;
    }
}
