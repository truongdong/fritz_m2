<?php

namespace Commercers\ReminderEmail\Model;

use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\ProductFactory as ProductResourceFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;

class ReminderEmail extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{

    const CACHE_TAG = 'commercers_reminder_email_product_size';

    protected $_cacheTag = 'commercers_reminder_email_product_size';

    protected $_eventPrefix = 'commercers_reminder_email_product_size';

    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var ProductResourceFactory
     */
    protected $_productResourceFactory;
    protected $_date;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        ProductFactory $productFactory,
        ProductResourceFactory $productResourceFactory,
        DateTime $date,
        array $data = []
    ) {
        $this->_productFactory = $productFactory;
        $this->_productResourceFactory = $productResourceFactory;
        $this->_date = $date;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection
        );
    }

    protected function _construct()
    {
        $this->_init('Commercers\ReminderEmail\Model\ResourceModel\ReminderEmail');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }

    /**
     * Prepare rule's statuses, available event cms_rule_get_available_statuses to order_approval statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Send'), self::STATUS_DISABLED => __('Pending')];
    }
}
