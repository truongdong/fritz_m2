<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Commercers\ReminderEmail\Model\Status\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */
class IsActive implements OptionSourceInterface
{
    /**
     * @var \Commercers\ReminderEmail\Model\ReminderEmail
     */
    protected $_reminderEmail;

    /**
     * Constructor
     *
     * @param \Commercers\OrderApproval\Model\ReminderEmail $reminderEmail
     */
    public function __construct(\Commercers\ReminderEmail\Model\ReminderEmail $reminderEmail)
    {
        $this->_reminderEmail = $reminderEmail;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->_reminderEmail->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
