<?php
namespace Commercers\ReminderEmail\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ReminderEmail extends AbstractDb {

    public function __construct(
        Context	$context
    ) {
        parent::__construct($context);
    }

    protected function _construct() {
        $this->_init('commercers_reminder_email_product_size', 'reminder_email_id');
    }
}
