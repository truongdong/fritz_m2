/**
*Commercers VietNam
* Chuc VB
*/
require([
    'jquery',
    'jquery/ui'], function ($) {
	
	$(document).ready(function(){
		orderProposalFunction();
		$("#commercers_orderproposal_order_product_grid").on("contentUpdated",function(){
			orderProposalFunction();
			
		});
		
	});
    
    function orderProposalFunction(){
        //set data for field ui component => dataProduct
        $('#save').on('click',function (event) {
           
            var stringData = $('input[name="products"]').val();
            if(stringData == ''){
                alert('Please select product and set quantity');
                event.preventDefault();
            }
            $('input[name="dataProduct"]').val(stringData).change();
            
            
        });
        //input qty only number
        $(".qty").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
    }
});

