<?php
/**
 * Commercers VietNam
 * Chuc VB
 */
namespace Commercers\OrderProposal\Model\ResourceModel;

class OrderProposal extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
    public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context, $connectionName = null) {
        parent::__construct($context, $connectionName);
    }
    public function _construct() {
        $this->_init('orderproposal','orderproposal_id');
    }
}