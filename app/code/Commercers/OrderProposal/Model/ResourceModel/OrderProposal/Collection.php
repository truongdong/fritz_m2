<?php
/**
 * Commercers VietNam
 * Chuc VB
 */
namespace Commercers\OrderProposal\Model\ResourceModel\OrderProposal;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    public function _construct()
    {
        $this->_init("Commercers\OrderProposal\Model\OrderProposal","Commercers\OrderProposal\Model\ResourceModel\OrderProposal");
    }
}