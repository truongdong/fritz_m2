<?php

namespace Commercers\OrderProposal\Cron;

class CronJobOrderProposal {

    protected $importServices;
    protected $_profilerFactory;

    public function __construct(
        \Commercers\OrderProposal\Services\Import $importServices,
        \Commercers\Profilers\Model\ProfilersFactory $profilerFactory
    ) {
        $this->importServices = $importServices;
        $this->_profilerFactory = $profilerFactory;
    }

    public function execute($schedule){
        $jobCode = $schedule->getJobCode();
        $profiler = $this->_profilerFactory->create()->getCollection()->addFieldToFilter('code',array('eq' => $jobCode));
        $this->importServices->execute($profiler);
    }
}