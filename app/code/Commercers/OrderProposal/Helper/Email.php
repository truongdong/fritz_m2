<?php

namespace Commercers\OrderProposal\Helper;

class Email extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_ERROR_NOTIFICATION = 'section_tracking_import/group_commercers_import/enabled_commercers_import_error_notification';

    protected $transportBuilder;
    protected $inlineTranslation;
    protected $escaper;
    protected $logger;
    protected $scopeConfig;
    protected $arrayMessages = array();
    public function __construct(
            \Magento\Framework\App\Helper\Context $context,
            \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
            \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
            \Magento\Framework\Escaper $escaper,
            \Commercers\OrderProposal\Services\Logger\Logger $logger,
            \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
            
    ) {
        parent::__construct($context);
        $this->inlineTranslation = $inlineTranslation;
        $this->escaper = $escaper;
        $this->transportBuilder = $transportBuilder;
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
    }
    public function sendEmail($messages){
        try{
            $this->inlineTranslation->suspend();
            
            
            $email = $this->scopeConfig->getValue('trans_email/ident_general/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
            $transport = $this->transportBuilder
                    ->setTemplateIdentifier('commercers_order_proposal_cron_error')
                    ->setTemplateOptions([
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store'=> \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ])
                    ->setTemplateVars([
                        'messages' => $messages
                    ])
                    
                    ->addTo("tranthitravin2@gmail.com")
                    ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (Exception $ex) {
            $this->logger->info('Can\'t send an email');
        }
    }
}