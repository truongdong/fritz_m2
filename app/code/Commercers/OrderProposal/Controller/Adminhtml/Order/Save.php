<?php
/**
 * Commercers VietNam
 * ChucVB
 */
namespace Commercers\OrderProposal\Controller\Adminhtml\Order;

class Save extends \Magento\Framework\App\Action\Action {
    protected $_resultPageFactory;
    protected $_orderProposalFactory;
    protected $_date;
    protected $_productFactory;
    protected $_redirectFactory;
    protected $_messageManager;
    public function __construct(
            \Magento\Backend\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $resultPageFactory,
            \Magento\Framework\Stdlib\DateTime\DateTime $date,
            \Commercers\OrderProposal\Model\OrderProposalFactory $orderProposalFactory,
            \Magento\Catalog\Model\ProductRepository $productFactory,
            \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory,
            \Magento\Framework\Message\ManagerInterface $messageManager
            
            ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_orderProposalFactory = $orderProposalFactory;
        $this->_date = $date;
        $this->_productFactory = $productFactory;
        $this->_redirectFactory = $redirectFactory;
         $this->_messageManager = $messageManager;
        parent::__construct($context);
    }
    public function execute() {

        $date = $this->_date->gmtDate();
        $resultRedirect = $this->_resultPageFactory->create();
        $data = $this->getRequest()->getPostValue();
        //get order number and check it
        $orderNumber = $data['orderNumber'];
        $check = $this->checkOrderNumber($orderNumber);
        if($check == true){
			//process data 
			$stringQtyAndId = $data['dataProduct'];
			$explodeString = explode('&',$stringQtyAndId);
			$standardDatas = array();
			
			for($i = 0; $i < count($explodeString); $i++){
				$arrayQtyAndId = explode('=',$explodeString[$i]);
				$productId = $arrayQtyAndId[0];
				$qtyDecode = base64_decode(trim($arrayQtyAndId[1],'%3D'));
				$qty = explode('=',$qtyDecode)[1];
				if($qty <= 0 || !$qty){
					$this->_messageManager->addErrorMessage(__('Can\'t save order.'));
					return $this->_redirectFactory->create()->setPath('*/*/index');
				}
				$standardDatas[] = [
					'productId' => $productId,
					'qty'=> $qty
				];
			}
            
            //save data
            foreach ($standardDatas as $standardData) {
				
                $productId = (int)$standardData['productId'];
                $qty = (int)$standardData['qty'];
                $product = $this->_productFactory->getById($productId);
                $sku = $product->getSku();
                $name = $product->getName();
                $deliveredQty = NULL;
                $openQty = NULL;
                $this->saveDataInDataBase($orderNumber, $productId, $sku, $name, $date, $qty, $deliveredQty, $openQty);
                
            }
            $this->_messageManager->addSuccessMessage(__('Saved.'));
        }else{
            $this->_messageManager->addErrorMessage(__('Can\'t save order.'));
        }
        
        return $this->_redirectFactory->create()
        ->setPath('*/*/index');
    }
    public function checkOrderNumber($orderNumber){
        $orderProposal = $this->_orderProposalFactory->create()->getCollection();
        $orderProposal->addFieldToSelect('order_number')->addFieldToFilter('order_number',['eq'=>$orderNumber]);
        $size = $orderProposal->getSize();
        if($size > 0){
            return false;
        }
        return true;
    }

    public function saveDataInDataBase($orderNumber,$id,$sku,$name,$date,$qty,$deliveredQty,$openQty){
       try{
           $data = array(
            'order_number' => $orderNumber,
            'product_id' => $id,
            'sku' => $sku,
            'name'=> $name,
            'order_date'=>$date,
            'orderqty' => $qty,
            'delivered_qty'=> $deliveredQty,
            'open_qty' =>$openQty
           
        );
        $orderProposal = $this->_orderProposalFactory->create()->setData($data)->save();
       } catch (Exception $ex) {
          $this->_messageManager->addErrorMessage($e->getMessage());
       }
       
   }
}