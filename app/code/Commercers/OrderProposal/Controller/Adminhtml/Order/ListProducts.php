<?php
/**
 * Commercers VietNam
 * Chuc VB
 */
namespace Commercers\OrderProposal\Controller\Adminhtml\Order;

class ListProducts extends \Magento\Sales\Controller\Adminhtml\Order\Create {

    protected $resultRawFactory;
    protected $resultLayoutFactory;
    public function __construct(
            \Magento\Backend\App\Action\Context $context, 
            \Magento\Catalog\Helper\Product $productHelper,
            \Magento\Framework\Escaper $escaper,
            \Magento\Framework\View\Result\PageFactory $resultPageFactory,
            \Magento\Framework\Controller\Result\RawFactory $rersultRawFactory,
            \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
            \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
    ) {
        $this->resultRawFactory = $rersultRawFactory;
        $this->resultLayoutFactory = $resultLayoutFactory;
        parent::__construct(
            $context,
            $productHelper,
            $escaper,
            $resultPageFactory,
            $resultForwardFactory
        );
    }

    
	public function execute() {
       $resultPage = $this->resultPageFactory->create();
	   //$resultPage->addHandle('commercers_orderproposal_order_product');
	   //$resultPage->addHandle('commercers_orderproposal_order_product_grid');
       $result = $resultPage->getLayout()->renderElement('content');
        return $this->resultRawFactory->create()->setContents($result);
    }
}
