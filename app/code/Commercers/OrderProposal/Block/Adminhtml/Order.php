<?php
/**
 * Commercers VietNam
 * Chuc VB
 */
namespace Commercers\OrderProposal\Block\Adminhtml;

class Order extends \Magento\Sales\Block\Adminhtml\Order\Create\AbstractCreate {
    protected function _construct()
    {
        parent::_construct();
        $this->setId('commercers_orderproposal_order_orderproposal');
    }
    public function getHeaderText()
    {
        return __('Please select products');
    }
    public function getHeaderCssClass()
    {
        return 'head-catalog-product';
    }
}