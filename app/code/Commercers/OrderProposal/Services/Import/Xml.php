<?php

namespace Commercers\OrderProposal\Services\Import;

class Xml {
    protected $orderProposal;
    protected $scopeConfig;
    protected $parser;
    protected $io;
    public function __construct(
        \Commercers\OrderProposal\Services\Import\Resource\Save $orderProposal,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Xml\Parser $parser,
            \Magento\Framework\Filesystem\Io\File $io
            ) {
        $this->orderProposal = $orderProposal;
        $this->scopeConfig = $scopeConfig;
        $this->parser = $parser;
        $this->io  = $io;
    }
    public function execute($filePath,$fileName,$profiler,$doneFolder){
        /**
         * read Xml file
         */
        $result = $this->readXmlFile($filePath);
        foreach($result as $valueCsv){
            $headerXml = $valueCsv[0];
            $allDataXml[] = $valueCsv[1];
        }
        /**
         * get mapping
         */
        $mapping = $this->getMappings($profiler['mapping']);
        foreach ($mapping as $key => $values) {
            $result[] = array($key, $values);
        }
        /**
         * get clean row
         */
        $params = [
            'header' => $headerXml,
            'mapping' => $result,
            'rows' => $allDataXml
        ];
        
        $cleanRow = null;//$this->mappingXml->convert($params);
        /**
         * import
         */
        $this->orderProposal->execute($cleanRow);
        /**
         * move file
         */
        $this->moveFile($doneFolder, $filePath, $fileName);
    }
    public function readXmlFile($pathFile) {
        /**
         * get Data
         */
        $dataXml = $this->parser->load($pathFile)->xmlToArray();

        foreach($dataXml['sale'] as $values){
            $data = $values;
        }
        /**
         * get header xml
         */
        if(isset($data[0])){
            foreach($data as $value){
                foreach($value as $keys =>$values){
                    $key[] = $keys;
                }
                $header[] = $key;
            }
            /**
             * get value and combine with header
             */
            for($i = 0; $i < count($data); $i++){
                $row = array();
                foreach($data[$i] as $value){
                    $row[] = $value;
                }
               $rows[] = array($header[0],$row);
            }
        }else{
            foreach($data as $keys =>$values){
                $key[] = $keys;
            }
            $header[] = $key;
            $row = array();
                foreach($data as $value){
                    $row[] = $value;
                }
            $rows[] = array($header[0],$row);
        }
        return $rows;
    }
    
     public function moveFile($doneFolder, $pathFile, $fileName) {
        $newFileName = $doneFolder . DIRECTORY_SEPARATOR . $fileName;
        $this->io->mv($pathFile, $newFileName);
    }

    public function getMappings($mapping){
        if(!is_null($mapping)){
           $mapping = preg_split('/\\r\\n|\\r|\\n/',$mapping);
          
            $value = array();
            foreach ($mapping as $_mapping){
                $attribute = explode('=',trim($_mapping));
                $value[$attribute[0]] = $attribute[1];
            }

            return $value;
       }
       return false;
    }
}