<?php

/**
 * Commercers VietNam
 * ChucVB
 */

namespace Commercers\OrderProposal\Services\Import\Resource;

class Save {

    protected $_resultPageFactory;
    protected $orderProposalFactory;
    protected $_date;
    protected $_productFactory;
    protected $_redirectFactory;
    protected $logger;
    protected $email;
    protected $saveController;
    protected $_messages = array();

    public function __construct(
    \Magento\Framework\View\Result\PageFactory $resultPageFactory, 
            \Magento\Framework\Stdlib\DateTime\DateTime $date, 
            \Commercers\OrderProposal\Model\OrderProposalFactory $orderProposalFactory, 
            \Magento\Catalog\Model\ProductFactory $productFactory, 
            \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory, 
            \Commercers\OrderProposal\Services\Logger\Logger $logger, 
            \Commercers\OrderProposal\Helper\Email $email, 
            \Commercers\OrderProposal\Controller\Adminhtml\Order\Save $saveController
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->orderProposalFactory = $orderProposalFactory;
        $this->_date = $date;
        $this->_productFactory = $productFactory;
        $this->_redirectFactory = $redirectFactory;
        $this->logger = $logger;
        $this->email = $email;
        $this->saveController = $saveController;
    }

    public function execute($cleanRows) {

        if ($cleanRows) {
            $standardDatas = array();
            $orderNumberExist = array();
            //check order number 
            foreach ($cleanRows[0] as $cleanRow) {
                $checkOrderNumber = $this->saveController->checkOrderNumber($cleanRow['order_number']);
                if ($checkOrderNumber == true) {
                    $standardDatas[] = [
                        'orderNumber' => $cleanRow['order_number'],
                        'sku' => $cleanRow['sku'],
                        'qty' => $cleanRow['qty']
                    ];
                } else {
                    $orderNumberExists[] = $cleanRow['order_number'];
                }
            }
        }

        if (!empty($standardDatas)) {
            
            //save order
            foreach ($standardDatas as $standardData) {
                
                $orderNumber = $standardData['orderNumber'];
                $productSku = $standardData['sku']; 
                $qty = (int) $standardData['qty'];
                if ($qty <= 0) {
                    $messages = 'Order Number: ' . $orderNumber . ' with SKU ' . $productSku . ': qty must not be less zero';
                    $this->_messages[] = $messages;
                    $this->logger->info($messages);
                    continue;
                }
                //get product by sku

                $product = $this->_productFactory->create();
                $collection = $product->getCollection();
                $collection->addFieldToFilter('sku',array('eq'=>$productSku));
                
                if(empty($collection->getData())){
                    $messages = 'SKU: '.$productSku.' doesn\'t belong to Store, Order Number: '.$orderNumber.'.';
                    $this->_messages[] = $messages;
                    $this->logger->info($messages);
                    continue;
                    
                }
                $product = $product->load($productSku,'sku');
                $productId = $product->getId();
                $productName = $product->getName();
                //get date time
                $date = $this->_date->gmtDate();

                $deliveredQty = NULL;
                $openQty = NULL;
                //sku is exist in order number
                $orderProposalFactory = $this->orderProposalFactory->create();
                $collection = $orderProposalFactory->getCollection();
                $collection->addFieldToFilter('order_number', array('eq'=>$orderNumber))->addFieldToFilter('sku', array('eq' => $productSku));
                
                if ($collection->getSize() > 0) {
                    
                    $orderProposal = $collection->getData()[0];
                    $oldQty = $orderProposal['orderqty'];
                    $newQty = $oldQty + $qty; 
                    $orderProposalFactory->load($orderProposal['orderproposal_id'])->setData('orderqty',$newQty)->save(); 
                }else{
                    $this->saveController->saveDataInDataBase($orderNumber, $productId, $productSku, $productName, $date, $qty, $deliveredQty, $openQty);
                }
            }
        }
        //write log and notification
        if (!empty($orderNumberExists)) {
            foreach (array_unique($orderNumberExists) as $orderNumberExist) {
                $messages = 'Order Number: ' . $orderNumberExist . ' is exist.';
                $this->_messages[] = $messages;
                $this->logger->info($messages);
            }
        }
        if(!empty($this->_messages)){
            $this->email->sendEmail($this->_messages);
        }
    }
}
