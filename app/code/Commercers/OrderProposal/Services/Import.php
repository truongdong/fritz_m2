<?php

namespace Commercers\OrderProposal\Services;

class Import {

	protected $ftp;
	protected $csv;
	protected $xml;
	protected $logger;
	protected $email;
        protected $_messages =array();
        public function __construct(
		\Commercers\Ftp\Model\Service\Ftp                               $ftp,
		\Commercers\OrderProposal\Services\Import\Csv                  $csv,
		\Commercers\OrderProposal\Services\Import\Xml                  $xml,
		\Commercers\OrderProposal\Services\Logger\Logger               $logger,
		\Commercers\OrderProposal\Helper\Email           $email
	) {
		$this->ftp                      =           $ftp;
		$this->csv                      =           $csv;
		$this->xml                      =           $xml;
		$this->logger                   =           $logger;
		$this->email                    =           $email;
	}
	public function execute($profiler){
       
		$this->executeByProfiler($profiler);
	}
	protected function executeByProfiler($profiler){
		$root = $this->ftp->getRoot().DIRECTORY_SEPARATOR.'var'.DIRECTORY_SEPARATOR;
		$listFiles = array(); 
        /**
         * ftp
         */
        $dataProfiler = $profiler->getData();
        foreach($dataProfiler as $value){
            $id = $value['id'];
            $enableFtp = $value['enableftp'];
            $enableLocal = $value['enablelocal'];
            $ftpFolder = $value['folderftp'];
            $ftpHost = $value['hostname'];
            $ftpUsername = $value['username'];
            $ftpPassword = $value['password'];
            $ftpLocalFolder = $value['localfolderftp'];
            $ftpDoneFolder = $value['donefolderftp'];
            $doneFolder = $value['done'];
            $isLocalFolder = $value['localfolder'];
            $delimiter = $value['delimiter'];
            $profiler = $value;
        }
        if ($enableFtp == 1) {

            $params = array(
                'localhost' => $ftpHost,
                'username' => $ftpUsername,
                'password' => $ftpPassword,
            );
            $localFolder = $root . $ftpLocalFolder;
            $this->ftp->readAndMove($params, $ftpFolder, $localFolder, $ftpDoneFolder);
        }
        /**
         * local
         */
        if($enableLocal == 1){
            $doneFolder = $root . $doneFolder;
            $localFolder = $root . $isLocalFolder;
        }

        $interator = new \FilesystemIterator($localFolder);
        /**
         * Check exist file
         * exist: get filePath and fileName
         * not exist: stop cron
         */
        if($interator->getPathName() == ''){
        	$messages = 'File XML/CSV does not exist in '.$ftpHost.' ,the path is '.$ftpFolder;
            /**
             * write log
             */
            $this->logger->info($messages);
            /**
            * Send email
            */
            $this->_messages[] = $messages;
           $this->email->sendEmail($this->_messages);
            
        }else{
        	foreach($interator as $value){
        		if(is_dir($value))
        			continue;
        		$filePath[] = $value->getPathname();
        		$fileName[] = $value->getFilename();
        	}
        	for($i = 0 ; $i < count($filePath); $i++){
        		$importService = $this->getImportService($profiler)->execute($filePath[$i], $fileName[$i], $profiler, $doneFolder,$delimiter);
        	}
        }
        
        
        
    }
    protected function getImportService($profiler) {
    	if ($profiler['format'] == 'csv') {
    		return $this->csv;
    	}

    	if ($profiler['format'] == 'xml') {
    		return $this->xml;
    	}
    }
    /**
     * 
     * @param type $ftp
     */
    protected function connectFtpServer($ftp){
    	
    	return [$ftpLocalFolder,$doneFolder,$ftpHost,$ftpFolder];
    }
    
    
}