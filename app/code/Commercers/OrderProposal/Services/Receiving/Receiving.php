<?php
/**
 * Commercers VietNam
 * Chuc VB
 */
namespace Commercers\OrderProposal\Services\Receiving;

class Receiving implements \Magento\Framework\Event\ObserverInterface{

    protected $_stockLogFactory;
    protected $_orderProposalFactory;
    /**
     * 
     * @param \Commercers\StockLog\Model\StockLogFactory $stockLogFactory
     * @param \Commercers\OrderProposal\Model\OrderProposalFactory $orderProposalFactory
     */
    public function __construct(
    \Commercers\StockLog\Model\StockLogFactory $stockLogFactory, \Commercers\OrderProposal\Model\OrderProposalFactory $orderProposalFactory
    ) {
        $this->_stockLogFactory = $stockLogFactory;
        $this->_orderProposalFactory = $orderProposalFactory;
    }
    /**
     * 
     * @return boolean
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $difference = $observer->getDiff(); 
		$productId = $observer->getProduct()->getId();
		$sku = $observer->getSku();
		$orderNumber = $observer->getOrdernumber();
		$posex = $observer->getPosex();
		$this->saveDataInOrderProposal($sku,$difference,$orderNumber,$posex);
        
    }
    /**
     * 
     * @param type $productId
     * @param type $deliveredQty
     * @param type $orderNumber
     * @return boolean
     */
    public function saveDataInOrderProposal($sku, $deliveredQty, $orderNumber,$posex) {
        //Find field need to save data
        
        $orderProposal = $this->_orderProposalFactory->create()->getCollection();
        
        $orderProposal	->addFieldToFilter('order_number', ['eq' => $orderNumber])
						->addFieldToFilter('sku', ['eq' => $sku])
                        ->addFieldToFilter('posex', ['eq' => $posex]);
        $orderProposal = $orderProposal->getData();

        if ($orderProposal != NULL) {
            
            $orderProposal = $orderProposal[0];
            $oldDeliveredQty = $orderProposal['delivered_qty'];
            $newDeliveredQty = $oldDeliveredQty + $deliveredQty;
            $openQty = $orderProposal['orderqty'] - $newDeliveredQty;
            if($openQty < 0){
                $openQty = 0;
            }
            $data = array(
                'orderproposal_id' => $orderProposal['orderproposal_id'],
                'product_id' => $orderProposal['product_id'],
                'order_number' => $orderProposal['order_number'],
                'sku' => $orderProposal['sku'],
                'name' => $orderProposal['name'],
                'order_date' => $orderProposal['order_date'],
                'orderqty' => $orderProposal['orderqty'],
                'delivered_qty' => $newDeliveredQty,
                'open_qty' => $openQty
            );
            //echo '<pre>'; print_r($data);exit;
            $this->_orderProposalFactory->create()->setData($data)->save();
            
        }
    }
    

}
