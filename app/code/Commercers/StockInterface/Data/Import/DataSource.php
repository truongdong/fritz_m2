<?php

namespace Commercers\StockInterface\Data\Import;

class DataSource {

    const ORDER_STATUS_STOCK = 'section_stockimport/group_stockimport/order_status_stock';
    const ENABLE_COUNTING_ORDERED_QTY = 'section_stockimport/group_stockimport/enable_counting_ordered_qty';

    public function __construct(

        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\Event\Manager $eventManager,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $orderItemCollection,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        //$this->_stockImportLogFactory = $stockImportLogFactory;
        $this->_productRepository = $productRepository;
        $this->_stockRegistry = $stockRegistry;
        $this->_productFactory = $productFactory;
        $this->eventManager = $eventManager;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_orderItemCollection = $orderItemCollection;
        $this->_resource = $resource;
        $this->_scopeConfig = $scopeConfig;
    }

    public function execute($data,$profiler,$file) {

        foreach ($data['items']['item'] as $item) {
            $this->updateStock($item); 
        }
    }

    protected function updateStock( $item ) {

        //echo '<pre>' ;print_r($item);
        //exit;
        $sku = $item['sku'];
        $qty = $item['qty'];
        try {
            $stockItem = $this->_stockRegistry->getStockItemBySku($sku);


        } catch (\Exception $ex) {
            return false;
            //$stockItem = $this->stockRegistry->getStockItem($productData['entity_id']);

        }
        $isEnableCountingOrderQty = $this->isEnableCountingOrderQty();
        if(isset($item['order_count']) && $item['order_count'] == 1){
            $isEnableCountingOrderQty = true;
        }

        if(strpos($item['qty'],"+") === 0 && strpos($item['qty'],"-") === 0 ){

            $qty = $stockItem->getQty() + intval($item['qty']);

        }


        if ($isEnableCountingOrderQty == 1) {

            $countOrders = $this->getCountOrderValidate($sku);
            $availableQty = $qty - $countOrders;

        } else{

            $availableQty = $qty;
        }

        $stockItem->setQty($availableQty);

        if ($availableQty > 0) {
            $stockItem->setIsInStock(1);
        }
        $this->_stockRegistry->updateStockItemBySku($sku, $stockItem);
    }

     public function getCountOrderValidate($sku){

        $getOrderStatus = $this->getOrderStatus();
        $orderStatus = explode(",", $getOrderStatus);
        $collection = $this->_orderCollectionFactory->create();
        $collection->getConnection();
        $salesOrderItemTable = $this->_resource->getTableName('sales_order_item');
        $collection->getSelect()->reset(\Zend_Db_Select::COLUMNS);
        $collection->getSelect()->join(array('sales_order_item' => $salesOrderItemTable),
            'main_table.entity_id = sales_order_item.order_id');
        $orderItems = $collection->addAttributeToFilter('status', $orderStatus)->addAttributeToFilter('sku', $sku);
        $orderItems->getSelect()->reset(\Zend_Db_Select::COLUMNS);
        $orderItems->getSelect()->columns(array('count' => 'SUM(qty_ordered)'));
        $countOrders = $orderItems->getFirstItem()->getData('count');
        return $countOrders;
    }
    public function getOrderStatus()
    {
        return $this->_scopeConfig->getValue(self::ORDER_STATUS_STOCK, \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
    }

    public function isEnableCountingOrderQty(){
        return $this->_scopeConfig->getValue(self::ENABLE_COUNTING_ORDERED_QTY, \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
    }

}
