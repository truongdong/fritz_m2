<?php
namespace Commercers\TrackingImport\Model\ResourceModel;
class TrackingImport extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }
    public function _construct()
    {
        $this->_init("trackingimport_log","id");
    }

}