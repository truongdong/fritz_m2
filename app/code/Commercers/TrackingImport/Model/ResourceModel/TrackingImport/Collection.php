<?php
namespace Commercers\TrackingImport\Model\ResourceModel\TrackingImport;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    public function _construct()
    {
        $this->_init("Commercers\TrackingImport\Model\TrackingImport","Commercers\TrackingImport\Model\ResourceModel\TrackingImport");
    }
}