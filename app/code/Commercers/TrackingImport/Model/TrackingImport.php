<?php
namespace Commercers\TrackingImport\Model;
class TrackingImport extends \Magento\Framework\Model\AbstractModel
{
    public function _construct()
    {
        $this->_init('Commercers\TrackingImport\Model\ResourceModel\TrackingImport');
    }
}