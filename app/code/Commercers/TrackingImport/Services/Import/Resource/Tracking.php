<?php

namespace Commercers\TrackingImport\Services\Import\Resource;

class Tracking {
    protected $orderModel;
    protected $trackFactory;
    protected $shipmentFactory;
    protected $logger;
    protected $trackingImportFactory;
    protected $emailHelper;
    protected $orderFactory;
    protected $arrayMessages = array();
    public function __construct(
            \Magento\Sales\Model\Convert\Order $orderModel,
            \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory,
            \Magento\Shipping\Model\ShipmentNotifier $shipmentFactory,
            \Commercers\TrackingImport\Services\Logger\Logger $logger,
            \Commercers\TrackingImport\Model\TrackingImportFactory $trackingImportFactory,
            \Commercers\TrackingImport\Helper\Email $emailHelper,
            \Magento\Sales\Api\Data\OrderInterface $orderFactory
            ) {
                $this->orderModel = $orderModel;
                $this->trackFactory = $trackFactory;
                $this->shipmentFactory = $shipmentFactory;
                $this->logger = $logger;
                $this->trackingImportFactory = $trackingImportFactory;
                $this->emailHelper = $emailHelper;
                $this->orderFactory = $orderFactory;
    }
    public function execute($cleanRow){
        $arrayMessages = array();
        foreach($cleanRow[0] as $value){
            $arrayMessages = $this->_addTrackingNumber($value['order_id'], $value['tracking_number'], $value['carrier'], $value['lot_number']);
        }

        if($arrayMessages){
            /**
            * Send email
            */
           $arrayMessage = \GuzzleHttp\json_encode($arrayMessages);
           $this->emailHelper->sendEmail($arrayMessage);
        }
    }
    protected function _addTrackingNumber($orderNumber,$trackNumber,$carrierCode,$lotNumber){

    /**
     * Load order
     */
   $order = $this->orderFactory->loadByIncrementId($orderNumber);
       /**
        * Check ship
        */
       if($order->canShip()){
           /**
            * Get all items and register shipment
            */
           $shipment = $this->orderModel->toShipment($order);
           foreach($order->getAllItems() as $orderItem){
               if(!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()){
                   continue;
               }
               $qtyShipped = $orderItem->getQtyToShip();
               $shipmentItem = $this->orderModel->itemToShipmentItem($orderItem)->setQty($qtyShipped);
               $shipment->addItem($shipmentItem);
           }
           $shipment->register();
           $shipment->getOrder()->setIsInProcess(true);

           try{
                /**
                * Add tracking information
                */
                $carrierCode = strtolower($carrierCode);
                $title = ucfirst($carrierCode);
                $data = array(
                    'carrier_code' => $carrierCode,
                    'title'=>$title,
                    'number' => $trackNumber,
                );
                //print_r($carrierCode); exit;
                $track = $this->trackFactory->create()->addData($data);
                $shipment->addTrack($track)->save();
                $shipment->getOrder()->save();

                $this->shipmentFactory->notify($shipment);
                $shipment->save();
                /**
                 * Logger success
                 */
                $message = 'Add track done ';
                $this->logger->info($message.$orderNumber);
                /**
                 * save carrier code and lot number to database
                 */
                $saveDataToDatabase = [
                    'order_number'=> $orderNumber,
                    'carrier_code' => $carrierCode,
                    'lot_number'=>$lotNumber
                ];
                $this->trackingImportFactory->create()->addData($saveDataToDatabase)->save();

           } catch (Exception $ex) {
               $message = 'Something error: '.$orderNumber;
               /**
                * Write log
                */
               $this->logger->info($message);
               /**
                * messages array for send email
                */
               $this->arrayMessages[] = $message;
           }
       }else{
           $message = 'You can\'t not create an shipment '.$orderNumber;
           /**
            * Write log
            */
           $this->logger->info($message);
           /**
            * messages array for send email
            */
           $this->arrayMessages[] = $message;
       }
       return $this->arrayMessages;
    }
}
