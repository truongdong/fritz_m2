<?php

namespace Commercers\TrackingImport\Services;

class Import {

	protected $selectProfilers;
	protected $ftp;
	protected $csv;
	protected $xml;
	protected $logger;
	protected $email;
	public function __construct(
		\Commercers\TrackingImport\Model\Config\Source\SelectProfilers  $selectProfilers,
		\Commercers\Ftp\Model\Service\Ftp                               $ftp,
		\Commercers\TrackingImport\Services\Import\Csv                  $csv,
		\Commercers\TrackingImport\Services\Import\Xml                  $xml,
		\Commercers\TrackingImport\Services\Logger\Logger               $logger,
		\Commercers\TrackingImport\Helper\Email           $email
	) {
		$this->selectProfilers          =           $selectProfilers;
		$this->ftp                      =           $ftp;
		$this->csv                      =           $csv;
		$this->xml                      =           $xml;
		$this->logger                   =           $logger;
		$this->email                    =           $email;
	}
	public function execute($profiler){
        //lay profilers nao dang su dung
		// $profilers = $this->selectProfilers->getProfilersSelected();
		// if($profilers){
		// 	foreach ($profilers as $profiler) {
		// 		$this->executeByProfiler($profiler);
		// 	}
		// }
		$this->executeByProfiler($profiler);
	}
	protected function executeByProfiler($profiler){
		$root = $this->ftp->getRoot().DIRECTORY_SEPARATOR.'var'.DIRECTORY_SEPARATOR;
		$listFiles = array(); 
        /**
         * ftp
         */
        $dataProfiler = $profiler->getData();
        foreach($dataProfiler as $value){
            $id = $value['id'];
            $enableFtp = $value['enableftp'];
            $enableLocal = $value['enablelocal'];
            $ftpFolder = $value['folderftp'];
            $ftpHost = $value['hostname'];
            $ftpUsername = $value['username'];
            $ftpPassword = $value['password'];
            $ftpLocalFolder = $value['localfolderftp'];
            $ftpDoneFolder = $value['donefolderftp'];
            $doneFolder = $value['done'];
            $isLocalFolder = $value['localfolder'];
            $delimiter = $value['delimiter'];
            $profiler = $value;
        }
        if ($enableFtp == 1) {

            $params = array(
                'localhost' => $ftpHost,
                'username' => $ftpUsername,
                'password' => $ftpPassword,
            );
            $localFolder = $root . $ftpLocalFolder;
            $this->ftp->readAndMove($params, $ftpFolder, $localFolder, $ftpDoneFolder);
        }
        /**
         * local
         */
        if($enableLocal == 1){
            $doneFolder = $root . $doneFolder;
            $localFolder = $root . $isLocalFolder;
        }

        $interator = new \FilesystemIterator($localFolder);
        /**
         * Check exist file
         * exist: get filePath and fileName
         * not exist: stop cron
         */
        if($interator->getPathName() == ''){
        	$messages = 'File XML/CSV does not exist in '.$ftpHost.' ,the path is '.$ftpFolder;
            /**
             * write log
             */
            $this->logger->info($messages);
            /**
            * Send email
            */
            $this->email->sendEmail($messages);
            
        }else{
        	foreach($interator as $value){
        		if(is_dir($value))
        			continue;
        		$filePath[] = $value->getPathname();
        		$fileName[] = $value->getFilename();
        	}
        	for($i = 0 ; $i < count($filePath); $i++){
        		$importService = $this->getImportService($profiler)->execute($filePath[$i], $fileName[$i], $profiler, $doneFolder,$delimiter);
        	}
        }
        
        
        
    }
    protected function getImportService($profiler) {
    	if ($profiler['format'] == 'csv') {
    		return $this->csv;
    	}

    	if ($profiler['format'] == 'xml') {
    		return $this->xml;
    	}
    }
    /**
     * 
     * @param type $ftp
     */
    protected function connectFtpServer($ftp){
    	
    	return [$ftpLocalFolder,$doneFolder,$ftpHost,$ftpFolder];
    }
    
    
}