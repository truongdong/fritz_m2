<?php

namespace Commercers\TrackingImport\Helper;

class Email extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_ERROR_NOTIFICATION = 'section_tracking_import/group_commercers_import/enabled_commercers_import_error_notification';

    protected $transportBuilder;
    protected $inlineTranslation;
    protected $escaper;
    protected $logger;
    protected $scopeConfig;
    protected $arrayMessages = array();
    public function __construct(
            \Magento\Framework\App\Helper\Context $context,
            \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
            \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
            \Magento\Framework\Escaper $escaper,
            \Commercers\TrackingImport\Services\Logger\Logger $logger,
            \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig

    ) {
        parent::__construct($context);
        $this->inlineTranslation = $inlineTranslation;
        $this->escaper = $escaper;
        $this->transportBuilder = $transportBuilder;
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
    }
    public function sendEmail($messages){
        try{
            $this->inlineTranslation->suspend();

            $senderEmail = $this->scopeConfig->getValue('trans_email/ident_custom1/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
            $senderName = $this->scopeConfig->getValue('trans_email/ident_custom1/name', \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
            $sender = [
                'email'=>$senderEmail,
                'name'=>$senderName

            ];

            $senderEmail = $this->scopeConfig->getValue('trans_email/ident_custom1/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
            $senderName = $this->scopeConfig->getValue('trans_email/ident_custom1/name', \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
            $sender = [
                'email'=>$senderEmail,
                'name'=>$senderName

            ];
            $transport = $this->transportBuilder
                    ->setTemplateIdentifier('commercers_tracking_import_cron_error')
                    ->setTemplateOptions([
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store'=> \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ])
                    ->setTemplateVars([
                        'messages' => $messages,
                        'error' => 'Commercers Tracking Import'
                    ])
                    ->addTo($senderEmail)
                    ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (Exception $ex) {
            $this->logger->info('Can\'t send an email');
        }
    }
}
