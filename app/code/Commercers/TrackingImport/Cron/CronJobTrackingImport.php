<?php

namespace Commercers\TrackingImport\Cron;

class CronJobTrackingImport {

    protected $importServices;
    protected $_profilerFactory;

    public function __construct(
        \Commercers\TrackingImport\Services\Import $importServices,
        \Commercers\Profilers\Model\ProfilersFactory $profilerFactory
    ) {
        $this->importServices = $importServices;
        $this->_profilerFactory = $profilerFactory;
    }

    public function execute($schedule){
        return;
        $jobCode = $schedule->getJobCode();
        $profiler = $this->_profilerFactory->create()->getCollection()->addFieldToFilter('code',array('eq' => $jobCode));
        $this->importServices->execute($profiler);
    }
}