var config = {
    map: {
        '*': {
            'Magento_Checkout/js/view/shipping-address/address-renderer/default': 'Commercers_OnepageCheckout/js/view/shipping-address/address-renderer/default-override',
            'Magento_Checkout/js/view/shipping-information/address-renderer/default': 'Commercers_OnepageCheckout/js/view/shipping-information/address-renderer/default-override',

            'Magento_CheckoutAgreements/js/model/agreements-assigner' : 'Commercers_OnepageCheckout/js/model/agreements-assigner-override',
            'Magento_CheckoutAgreements/js/model/agreement-validator' : 'Commercers_OnepageCheckout/js/model/agreement-validator-override',

            'Magento_Checkout/js/model/shipping-save-processor/default' : 'Commercers_OnepageCheckout/js/model/shipping-save-processor/default-override',
            'Magento_Checkout/js/view/shipping-address/list' : 'Commercers_OnepageCheckout/js/view/shipping-address/list-override',
            'Magento_Checkout/js/action/set-billing-address' : 'Commercers_OnepageCheckout/js/action/set-billing-address-override',

            'Magento_Checkout/js/view/registration' : 'Commercers_OnepageCheckout/js/view/registration-override'
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/model/resource-url-manager': {
                'Commercers_OnepageCheckout/js/mixins/model/resource-url-manager-mixin': true
            },
            'Magento_Checkout/js/model/shipping-save-processor': {
                'Commercers_OnepageCheckout/js/mixins/model/shipping-save-processor-mixin': true
            }
        }
    }
};
