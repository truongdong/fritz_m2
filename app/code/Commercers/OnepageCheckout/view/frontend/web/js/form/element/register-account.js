define([
        'jquery',
        'ko',
        'uiComponent',
        'Magento_Customer/js/model/customer'
    ],
    function (
        $,
        ko,
        Component,
        customer
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Commercers_OnepageCheckout/form/element/register-account',
                isCreateAccount: ko.observable(false),
                isEmailExist: ko.observable(false),
                isCustomerLoggedIn: customer.isLoggedIn
            },

            /**
             * Init component
             */
            initialize: function () {
                this._super();
            },

            showPassword: function () {
                return true;
            },
            passCurrentVisible: function () {
                var pass_id = document.getElementById("account-password");
                if (pass_id.type == "password") {
                    pass_id.type = "text";
                    document.getElementById("eye-icon-current").classList.add("active");
                }
                else {
                    pass_id.type = "password";
                    document.getElementById("eye-icon-current").classList.remove("active");
                }
            }
        });
    });
