define([

], function () {
    'use strict';

    return function (target) {
        target.getUrlForSetShippingMethod = function(quote) {
            var params = this.getCheckoutMethod() == 'guest' ?
                {
                    cartId: quote.getQuoteId()
                } : {},
                urls = {
                    'guest': '/guest-carts/:cartId/shipping-method',
                    'customer': '/carts/mine/shipping-method'
                };

            return this.getUrl(urls, params);
        };

        return target;
    };
});
