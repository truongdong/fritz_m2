define([
    'Magento_Checkout/js/model/shipping-save-processor/default'
], function (defaultProcessor) {
    'use strict';

    var processors = [];

    processors['default'] =  defaultProcessor;

    return function (target) {

        target.saveShippingMethod = function(type) {
            var rates = [];

            if (processors[type]) {
                rates = processors[type].saveShippingMethod();
            } else {
                rates = processors['default'].saveShippingMethod();
            }

            return rates;
        };

        return target;
    };
});
