/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Checkout/js/checkout-data'
], function ($, Component, quote, stepNavigator, checkoutData) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Commercers_OnepageCheckout/review/shipping-method-payment'
        },
        quoteIsVirtual: quote.isVirtual(),

        /**
         * @return {Boolean}
         */
        isVisible: function () {
            return stepNavigator.isProcessed('payment');
        },

        /**
         * @return {String}
         */
        getShippingMethodTitle: function () {
            var shippingMethod = quote.shippingMethod();

            return shippingMethod ? shippingMethod['carrier_title'] + ' - ' + shippingMethod['method_title'] : '';
        },

        getPaymentMethodTitle: function () {
            var paymentMethod = checkoutData.getSelectedPaymentMethod();

            var paymentTitle = '';
            if(paymentMethod) {
                $.each(window.checkoutConfig.paymentMethodTitles, function(data,index){
                    if(index.method == paymentMethod){
                        paymentTitle = index.title;
                    }
                })
//                 paymentTitle = window.checkoutConfig.paymentMethodTitles[paymentMethod];

            }
            if(paymentMethod == 'banktransfer') {
                paymentTitle = 'Vorkasse';
            }

            return paymentTitle;
        },

        /**
         * Back step.
         */
        back: function () {
            stepNavigator.navigateTo('payment');
        }
    });
});
