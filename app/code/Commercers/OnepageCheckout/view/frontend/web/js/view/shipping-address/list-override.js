/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'underscore',
    'ko',
    'mageUtils',
    'uiComponent',
    'uiLayout',
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/checkout-data',
    'mage/url',
    'Magento_Ui/js/modal/confirm',
    'mage/translate',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/model/address-converter'
], function (_, ko, utils, Component, layout, addressList,  selectShippingAddressAction, checkoutData, url, confirmation, $t, formPopUpState, addressConverter) {
    'use strict';

    /* custom RendererTemplate for Dropdown*/
    var dropdownRendererTemplate = {
        parent: '${ $.$data.parentName }',
        name: '${ $.$data.name }',
        component: 'Commercers_OnepageCheckout/js/view/shipping-address/address-renderer/dropdown'
    };

    return Component.extend({
        defaults: {
            template: 'Commercers_OnepageCheckout/shipping-address/list',
            visible: addressList().length > 0,
            rendererTemplates: [],
            dropdownAddressList: [],
            dropdownComponentRendererIndex: 10
        },

        /** @inheritdoc */
        initialize: function () {
            this._super()
                .initChildren();

            this.createRendererDropdownComponent();

            return this;
        },

        /** @inheritdoc */
        initConfig: function () {
            this._super();
            // the list of child components that are responsible for address rendering
            this.rendererComponents = [];

            return this;
        },

        /** @inheritdoc */
        initChildren: function () {
            var self = this;

            _.each(addressList(), this.createAddressListData, this);

            // addressList.subscribe(function (changes) {
            //         changes.forEach(function (change) {
            //             if (change.status === 'added') {
            //                 self.dropdownAddressList.push(change.value);
            //                 formPopUpState.isVisible(true);
            //             }
            //         });
            //     },
            //     this,
            //     'arrayChange'
            // );

            var newAddressOption = {
                getAddressInline: function () {
                    return $t('New Address');
                },
                customerAddressId: null
            };

            this.dropdownAddressList.push(newAddressOption);

            return this;
        },

        createAddressListData: function(address, index){
            this.dropdownAddressList.push(address);
        },

        createRendererDropdownComponent: function() {
            var rendererTemplate, templateData, rendererComponent;

            rendererTemplate = utils.extend({}, dropdownRendererTemplate);

            templateData = {
                parentName: this.name,
                name: 'shipping-addressall-dropdown'
            };

            rendererComponent = utils.template(rendererTemplate, templateData);
            this.selectedItem = ko.observable();

            this.selectedItem.subscribe(function(latest){
                if(latest.customerAddressId) {
                    formPopUpState.isVisible(false);
                    selectShippingAddressAction(latest);
                    checkoutData.setSelectedShippingAddress(latest,latest.getKey());
                } else {
                    formPopUpState.isVisible(true);
                }
            },this);

            utils.extend(rendererComponent, {
                allAddress: ko.observableArray( this.dropdownAddressList),
                selectedItem: this.selectedItem
            });

            layout([rendererComponent]);
            this.rendererComponents[this.dropdownComponentRendererIndex] = rendererComponent;
        }

    });
});