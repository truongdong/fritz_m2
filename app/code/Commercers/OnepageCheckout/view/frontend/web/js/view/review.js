define(
    [
        'ko',
        'uiComponent',
        'underscore',
        'Magento_Checkout/js/model/step-navigator',
        'mage/translate',
        'Magento_Checkout/js/action/get-payment-information',
        'Magento_Checkout/js/checkout-data'
    ],
    function (
        ko,
        Component,
        _,
        stepNavigator,
        $t,
        getPaymentInformation,
        checkoutData
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Commercers_OnepageCheckout/review'
            },

            isVisible: ko.observable(false),

            /**
             *
             * @returns {*}
             */
            initialize: function () {
                this._super();
                // register your step
                stepNavigator.registerStep(
                    'review-step',
                    //step alias
                    null,
                    //step title value
                    $t('Order Review '),
                    //observable property with logic when display step or hide step
                    this.isVisible,

                    _.bind(this.navigate, this),

                    /**
                     * sort order value
                     * 'sort order value' < 10: step displays before shipping step;
                     * 10 < 'sort order value' < 20 : step displays between shipping and payment step
                     * 'sort order value' > 20 : step displays after payment step
                     */
                    30
                );

                return this;
            },

            /**
             * The navigate() method is responsible for navigation between checkout step
             * during checkout. You can add custom logic, for example some conditions
             * for switching to your custom step
             */
            navigate: function () {
                var self = this;
                var hasShippingMethod = window.checkoutConfig.selectedShippingMethod;
                var paymentMethod = checkoutData.getSelectedPaymentMethod();

                if (!hasShippingMethod || !paymentMethod) {
                    self.isVisible(false);
                    stepNavigator.setHash('payment');
                } else {
                    self.isVisible(true);
                }
            },

            /**
             * @returns void
             */
            navigateToNextStep: function () {
                stepNavigator.next();
            }
        });
    }
);
