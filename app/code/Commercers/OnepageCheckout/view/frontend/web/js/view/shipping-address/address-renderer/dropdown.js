/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-address/form-popup-state',
    'Magento_Checkout/js/checkout-data',
    'Magento_Customer/js/customer-data',
    'mage/translate'
], function ($, ko, Component, selectShippingAddressAction, quote, formPopUpState, checkoutData, customerData, $t) {
    'use strict';

    var countryData = customerData.get('directory-data');

    return Component.extend({
        defaults: {
            template: 'Commercers_OnepageCheckout/shipping-address/address-renderer/dropdown'
        },

        /** @inheritdoc */
        initObservable: function () {
            this._super();
            return this;
        },

        /**
         * @param {String} countryId
         * @return {String}
         */
        getCountryName: function (countryId) {
            return countryData()[countryId] != undefined ? countryData()[countryId].name : ''; //eslint-disable-line
        },
        addressOptionsText: function (address) {
            if(address.customerAddressId){
                if(address.company != null){
                    return address.company+', '+address.getAddressInline();
                }
                return address.getAddressInline();
            } else {
                return $t('New Address');
            }

        }
    });
});