<?php

namespace Commercers\OnepageCheckout\Plugin\Catalog\Product\Helper;

use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\App\ObjectManager;


class Configuration
{
    protected $_productFactory;
    public function __construct(
        Json $serializer = null,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {
        $this->_productFactory = $productFactory;
        $this->serializer = $serializer ?: ObjectManager::getInstance()->get(Json::class);
    }

    public function afterGetCustomOptions(
        \Magento\Catalog\Helper\Product\Configuration $configuration,
        $result,
        \Magento\Catalog\Model\Product\Configuration\Item\ItemInterface $item
    ) {
        $product = $item->getProduct();
        if (is_array($result)) {
            $addOptions = $item->getOptionByCode('additional_options');
            if ($addOptions) {
                $result = array_merge($result, $this->serializer->unserialize($addOptions->getValue()));
            }

            // Add additional information
            $result = array_merge($result,$this->getProductAdditionalInformation($product));
        }

        return $result;
    }

    protected function getProductAdditionalInformation($product){
        $options = [];

        // Add Delivery Time
        $productModel = $this->_productFactory->create();
        $_product = $productModel->load($product->getEntityId());
        $options[] = [
            'label' => __('Delivery time'),
            'value' => $_product->getDeliveryTime()
        ];

        return $options;
    }
}
