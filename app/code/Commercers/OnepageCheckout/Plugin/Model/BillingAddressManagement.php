<?php

namespace Commercers\OnepageCheckout\Plugin\Model;

use Magento\Checkout\Model\Session;

class BillingAddressManagement
{
    protected $checkoutSession;

    public function __construct(
        Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }

    public function beforeAssign(
        \Magento\Quote\Model\BillingAddressManagement $subject,
        $cartId,
        \Magento\Quote\Api\Data\AddressInterface $address,
        $useForShipping = false
    )
    {
        if (!$extAttributes = $address->getExtensionAttributes()) {
            return;
        }

        if($password = $extAttributes->getPassword()) {
            $this->checkoutSession->setPassword($password);
        }
    }
}
