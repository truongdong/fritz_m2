<?php

namespace Commercers\OnepageCheckout\Plugin\Model;

use Magento\Checkout\Model\Session;

class ShippingInformationManagement
{
    protected $checkoutSession;

    public function __construct(
        Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }

    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    )
    {
        if (!$extAttributes = $addressInformation->getExtensionAttributes()) {
            return;
        }
        $password = $extAttributes->getPassword();
        $this->checkoutSession->setPassword($password);
    }
}
