<?php

namespace Commercers\OnepageCheckout\Observer;

use Magento\Framework\Event\ObserverInterface;
use Commercers\OnepageCheckout\Logger\Logger;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Event\Observer;
use Magento\Checkout\Model\Session;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\Message\MessageInterface;
use Magento\Customer\Model\AddressFactory;

class CreateCustomer implements ObserverInterface
{
    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    protected $_checkoutSession;
    protected $_customerInterfaceFactory;

    protected $_storeManager;

    /**
     * @var AccountManagementInterface
     */
    protected $accountManagement;

    /**
     * @var CustomerUrl
     */
    protected $customerUrl;

    private $addressFactory;

    public function __construct(
        Logger $logger,
        ManagerInterface $messageManager,
        Session $checkoutSession,
        CustomerInterfaceFactory $customerInterfaceFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        AccountManagementInterface $accountManagement,
        CustomerUrl $customerUrl,
        AddressFactory $addressFactory
    ) {
        $this->_logger = $logger;
        $this->messageManager = $messageManager;
        $this->_checkoutSession = $checkoutSession;
        $this->_customerInterfaceFactory = $customerInterfaceFactory;
        $this->_storeManager = $storeManager;
        $this->accountManagement = $accountManagement;
        $this->customerUrl = $customerUrl;
        $this->addressFactory = $addressFactory;
    }

    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $password = $this->_checkoutSession->getPassword();

        if($password) {
            try {
                if($order->getIsVirtual()) {
                    $address = $order->getBillingAddress();
                } else {
                    $address = $order->getShippingAddress();
                }

                $email = $address->getEmail();
                $firstName = $address->getFirstname();
                $lastName = $address->getLastname();

                $store = $this->_storeManager->getStore();
                $websiteId = $this->_storeManager->getWebsite()->getWebsiteId();

                /** @var $customerData \Magento\Customer\Api\Data\CustomerInterface */
                $customerData = $this->_customerInterfaceFactory->create();
                $customerData->setWebsiteId($websiteId);
                $customerData->setStoreId($store->getId());
                $customerData->setEmail($email);
                $customerData->setFirstname($firstName);
                $customerData->setLastname($lastName);

                $customer = $this->accountManagement->createAccount($customerData, $password);

                $addressCustomer = $this->addressFactory->create();
                $addressCustomer->setData($order->getBillingAddress()->getData());
                $addressCustomer->setCustomerId($customer->getId())
                    ->setIsDefaultBilling('1')
                    ->setIsDefaultShipping('0')
                    ->setSaveInAddressBook('1');
                $addressCustomer->save();

                if(!$order->getIsVirtual()){
                    $addressCustomer = $this->addressFactory->create();
                    $addressCustomer->setData($order->getShippingAddress()->getData());
                    $addressCustomer->setCustomerId($customer->getId())
                        ->setIsDefaultBilling('0')
                        ->setIsDefaultShipping('1')
                        ->setSaveInAddressBook('1');
                    $addressCustomer->save();
                }


                $confirmationStatus = $this->accountManagement->getConfirmationStatus($customer->getId());
                if ($confirmationStatus === AccountManagementInterface::ACCOUNT_CONFIRMATION_REQUIRED) {
                    $this->messageManager->addComplexSuccessMessage(
                        'confirmAccountSuccessMessage',
                        [
                            'url' => $this->customerUrl->getEmailConfirmationUrl($customer->getEmail()),
                        ]
                    );
                } else {
                    $message = $this->messageManager
                        ->createMessage(MessageInterface::TYPE_SUCCESS)
                        ->setText(
                            __('Thank you for registering with %1.', $this->_storeManager->getStore()->getFrontendName())
                        );
                    $this->messageManager->addMessage($message);
                }
            } catch (\Exception $e) {
                $this->_logger->info($e->getMessage());
            }

            $this->_checkoutSession->unsPassword();
        }

        return $this;
    }
}
