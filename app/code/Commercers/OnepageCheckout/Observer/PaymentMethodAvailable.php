<?php

namespace Commercers\OnepageCheckout\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Commercers\OnepageCheckout\Helper\Data as OnepageCheckoutHelper;

class PaymentMethodAvailable implements ObserverInterface
{
    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * @var OnepageCheckoutHelper
     */
    protected $_helper;

    public function __construct(
        CheckoutSession $checkoutSession,
        OnepageCheckoutHelper $helper
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_helper = $helper;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $this->_checkoutSession->getQuote();
        $isPaymentDisable = false;

        foreach ($quote->getAllItems() as $item) {
            $auctionType = $item->getProduct()->getAuctionType();
            if($auctionType) {
                $isPaymentDisable = true;
            }
        }

        if($isPaymentDisable && $observer->getEvent()->getMethodInstance()->getCode()== "mollie_methods_klarnapaylater" ){
            $checkResult = $observer->getEvent()->getResult();
            $checkResult->setData('is_available', false);
        }
    }
}
