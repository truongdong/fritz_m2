<?php

namespace Commercers\OnepageCheckout\Model;

class GuestShippingMethodManagement implements \Commercers\OnepageCheckout\Api\GuestShippingMethodManagementInterface
{
    /**
     * @var \Magento\Quote\Model\QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;

    /**
     * @var \Commercers\OnepageCheckout\Api\ShippingMethodManagementInterface
     */
    protected $shippingMethodManagement;

    public function __construct(
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory,
        \Commercers\OnepageCheckout\Api\ShippingMethodManagementInterface $shippingMethodManagement
    ) {
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->shippingMethodManagement = $shippingMethodManagement;
    }

    /**
     * {@inheritDoc}
     */
    public function saveShippingMethodInformation(
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        /** @var $quoteIdMask \Magento\Quote\Model\QuoteIdMask */
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        return $this->shippingMethodManagement->saveShippingMethodInformation(
            $quoteIdMask->getQuoteId(),
            $addressInformation
        );
    }
}
