<?php

namespace Commercers\OnepageCheckout\Model\Checkout;

use Magento\Checkout\Model\ConfigProviderInterface;

class ConfigProvider implements ConfigProviderInterface{

    /**
     * Order Payment
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\Payment\Collection
     */
    protected $_orderPayment;

    /**
     * Payment Helper Data
     *
     * @var \Magento\Payment\Helper\Data
     */
    protected $_paymentHelper;

    /**
     * Payment Model Config
     *
     * @var \Magento\Payment\Model\Config
     */
    protected $_paymentConfig;

    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Payment\Collection $orderPayment,
        \Magento\Payment\Helper\Data $paymentHelper,
        \Magento\Payment\Model\Config $paymentConfig
    ) {
        $this->_orderPayment = $orderPayment;
        $this->_paymentHelper = $paymentHelper;
        $this->_paymentConfig = $paymentConfig;
    }

    public function getConfig()
    {
        $collection = $this->_orderPayment;
        $collection->getSelect()->group('method');
        $paymentMethodsTitle = [];
        foreach ($collection as $col) {
            $paymentMethodsTitle[$col->getMethod()] = $col->getAdditionalInformation()['method_title'];
        }

        return [
            'paymentMethodTitles' => $paymentMethodsTitle
        ];
    }
}
