<?php

namespace Commercers\ExportOrder\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{

    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        //Order table
        $installer->getConnection()
            ->addColumn(
                $setup->getTable('sales_order'),
                'exported_date',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                    'default' => null,
                    'nullable' => true,
                    'comment' => 'Order Export Date'
                ]
            );

        $installer->getConnection()->dropColumn('sales_order', 'is_exported');

        $installer->endSetup();
    }
}
