<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Commercers\ExportOrder\Model\Condition;

/**
 * Combination of product conditions
 */
class Combine extends \Magento\Rule\Model\Condition\Combine
{
    /**
     * @var \Magento\CatalogWidget\Model\Rule\Condition\ProductFactory
     */
    protected $productFactory;

    /**
     * {@inheritdoc}
     */
    protected $elementName = 'rule';

    /**
     * @var array
     */
    private $excludedAttributes;

    /**
     * @param \Magento\Rule\Model\Condition\Context $context
     * @param \Magento\CatalogWidget\Model\Rule\Condition\ProductFactory $conditionFactory
     * @param array $data
     * @param array $excludedAttributes
     */
    public function __construct(
        \Magento\Rule\Model\Condition\Context $context,
         \Commercers\ExportOrder\Model\Condition\Order $orderCondition,
         \Magento\SalesRule\Model\Rule\Condition\Product $ruleConditionProduct,
            
        array $data = [],
        array $excludedAttributes = []
    ) {
        $this->orderCondition = $orderCondition;
        $this->_ruleConditionProd = $ruleConditionProduct;
        parent::__construct($context, $data);
       
        $this->setType(\Commercers\ExportOrder\Model\Condition\Combine::class);
        $this->excludedAttributes = $excludedAttributes;
    }

    /**
     * Get new child select options
     *
     * @return array
     */
    public function getNewChildSelectOptions()
    {
        $addressAttributes = $this->orderCondition->loadAttributeOptions()->getAttributeOption();
        $attributes = [];
        foreach ($addressAttributes as $code => $label) {
            $attributes[] = [
                'value' => 'Commercers\ExportOrder\Model\Condition\Order|' . $code,
                'label' => $label,
            ];
        }
        
        $conditions = parent::getNewChildSelectOptions();
        $conditions = array_merge_recursive(
            $conditions,
            [
                /*
                [
                    'value' => \Magento\SalesRule\Model\Rule\Condition\Product\Found::class,
                    'label' => __('Product attribute combination'),
                ],
                [
                    'value' => \Magento\SalesRule\Model\Rule\Condition\Product\Subselect::class,
                    'label' => __('Products subselection')
                ],
                [
                    'value' => \Magento\SalesRule\Model\Rule\Condition\Combine::class,
                    'label' => __('Conditions combination')
                ],
                 */
                ['label' => __('Order Attribute'), 'value' => $attributes]
            ]
        );
         

        $additional = new \Magento\Framework\DataObject();
        //$this->_eventManager->dispatch('salesrule_rule_condition_combine', ['additional' => $additional]);
        $additionalConditions = $additional->getConditions();
        if ($additionalConditions) {
            $conditions = array_merge_recursive($conditions, $additionalConditions);
        }

        return $conditions;
    }

    /**
     * Collect validated attributes for Product Collection
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection
     * @return $this
     */
    public function collectValidatedAttributes($productCollection)
    {
        foreach ($this->getConditions() as $condition) {
            $condition->collectValidatedAttributes($productCollection);
        }
        return $this;
    }
}
