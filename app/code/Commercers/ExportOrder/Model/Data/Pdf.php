<?php

namespace Commercers\ExportOrder\Model\Data;

use Magento\Framework\App\Filesystem\DirectoryList;


use Commercers\Profilers\Service\DataSource\Factory as DataSourceFactory;

class Pdf implements \Commercers\Profilers\Service\Data\PdfCreatorInterface {

    public function __construct(
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Sales\Model\Order\Pdf\Shipment $pdfShipment,
        \Magento\Sales\Model\Order\Pdf\Invoice $pdfInvoice    
    ) {
        $this->orderFactory = $orderFactory;
        $this->filesystem = $filesystem;
        $this->fileFactory = $fileFactory;
        $this->pdfShipment = $pdfShipment;
        $this->pdfInvoice = $pdfInvoice;
    }

    public function create($profiler, $data) {

        $xslDoc = $profiler->getData('export_output_template');
        $xml = new \SimpleXMLElement($xslDoc);
        
        $files = $xml->xpath("/files/file");
        $exportFiles = array();
        //shipment
        if($files && count($files)){
            
            foreach($files as $file){
                
                $fileNameTemplate = (string)$file['filename'];
                foreach($data['orders']['order'] as $order){
                   
                    $fileName = $fileNameTemplate;
                    foreach($order as $key => $value){
                        
                        $fileName = str_replace("%".$key."%", $value, $fileName);
                    }
                    
                    $orderObject = $this->orderFactory->create();
                    $orderObject->load($order['id']);
                    
                    $dataSource = (string)$file->pdf_output['datasource'];
                    
                    if($dataSource == 'shipment'){
                        
                        $shipmentCollection = $orderObject->getShipmentsCollection();
                        if ($shipmentCollection->getSize() > 0) {

                            $pdf = $this->pdfShipment->getPdf($shipmentCollection);
                            $path = $this->filesystem->getDirectoryRead(DirectoryList::TMP)->getAbsolutePath($fileName);
                            $pdf->save($path);
                            $exportFiles[$fileName] = $path;
                        }
                        
                    }
                    
                    if($dataSource == 'invoice'){
                        
                        $invoiceCollection = $orderObject->getInvoiceCollection();
                        if ($invoiceCollection->getSize() > 0) {

                            //$pdf = $this->pdfShipment->getPdf($shipmentCollection);
                            $pdf = $this->pdfInvoice->getPdf($invoiceCollection);
                            $path = $this->filesystem->getDirectoryRead(DirectoryList::TMP)->getAbsolutePath($fileName);
                            $pdf->save($path);
                            $exportFiles[$fileName] = $path;
                        }
                        
                    }
                    
                    
                    
                }
                
            }
            
        }
        
        return $exportFiles;
        
    }

}
