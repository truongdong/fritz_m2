<?php
/*
 * Commercers Vietnam
 */
namespace Commercers\ExportOrder\Observer;
 
use Magento\Framework\Event\ObserverInterface;
 
class Event implements ObserverInterface
{
    
    public function __construct(
            \Commercers\Profilers\Model\ResourceModel\Profilers\CollectionFactory $profilerCollectionFactory
            ) {
     
        $this->profilerCollectionFactory = $profilerCollectionFactory;
    }



    public function execute(\Magento\Framework\Event\Observer $observer)     
    {
        /*
        echo '<pre>';
        var_dump($observer->getEventName());
        var_dump($observer->getName());
        echo $observer->getEvent()->getName(); 
        
        exit;
         * 
         */
        $profilers = $this->getProfilers();
        foreach($profilers as $profiler){
            $actionsSerialized = $profiler->getActionsSerialized();
            $actions = unserialize($actionsSerialized);
            if(isset($actions['events'])){
                foreach($actions['events'] as $eventName => $eventValue){
                    
                    if($eventName == $observer->getEvent()->getName() && $eventValue == 1){
                        $type = $profiler->getData("type");
                        //print_r($observer->getOrder()->getData('id'));exit;
                        if($type == \Commercers\Profilers\Model\Constant::EXPORT_PROFILER){
                            $process = \Magento\Framework\App\ObjectManager::getInstance()->get("\Commercers\Profilers\Service\Profiler\Process\Export");
                            $process->executeForOneItem($profiler, $observer->getOrder()->getId() );
                        }
                        /*
                        if($type == \Commercers\Profilers\Model\Constant::IMPORT_PROFILER){
                            $process = \Magento\Framework\App\ObjectManager::getInstance()->get("\Commercers\Profilers\Service\Profiler\Process\Import");
                            $process->executeOneItem($profiler);
                        }
                         * 
                         */
                    }
                }
            }
        }
         
        

    }
    
    protected function getProfilers(){
        $profilers = $this->profilerCollectionFactory->create();
        $profilers->addFieldToFilter('data_source', array('in' => \Commercers\ExportOrder\Model\Constant::showForDataSources));
        return $profilers;
        
    }
    
}