<?php
namespace Commercers\ExportOrder\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;

class Email extends AbstractHelper{
    CONST FILE_NAME  = 'orderExport.xls' ;
    protected $_transportBuilder;
    protected $_inlineTranslation;
    protected $_template;
    protected $_messageManager;
    protected $reader;
    protected $fileSystem;
    protected $_storeManager;

    public function __construct(
        Context $context,
        StateInterface $inlineTranslation,
        \Commercers\ExportOrder\Model\Mail\TransportBuilder $transportBuilder,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem\Driver\File $reader,
        \Magento\Framework\Filesystem $fileSystem
    ) {
        parent::__construct($context);
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->_messageManager = $messageManager;
        $this->reader = $reader;
        $this->fileSystem = $fileSystem;
    }

    public function getConfigValue($path)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getStore()
    {
        return $this->_storeManager->getStore();
    }

    public function getTemplateId($xmlPath)
    {
        return $this->getConfigValue($xmlPath, $this->getStore()->getStoreId());
    }

    public function sendEmail($templateId, $emailTemplateVariables, $senderInfo, $receiverInfo){
        $this->_inlineTranslation->suspend();
        $this->generateTemplate($templateId, $emailTemplateVariables, $senderInfo, $receiverInfo);
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }

        $this->_inlineTranslation->resume();
    }

    public function generateTemplate($templateId, $emailTemplateVariables, $senderInfo, $receiverInfo)
    {
        $senderEmail = $senderInfo['email'];
        $template = $this->_transportBuilder->setTemplateIdentifier($templateId)
            ->setTemplateOptions([
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $this->_storeManager->getStore()->getId(),
            ])
            ->setTemplateVars($emailTemplateVariables)
            ->setFrom($senderInfo)
            ->addTo($receiverInfo['email'], $receiverInfo['name'])
            ->setReplyTo($senderEmail, $senderInfo['name']);

        $mediaPath = $this->fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)
            ->getAbsolutePath();
        $orderExport = $mediaPath.self::FILE_NAME;
        $template->addAttachment($this->reader->fileGetContents($orderExport), self::FILE_NAME, '');

        return $this;
    }
}