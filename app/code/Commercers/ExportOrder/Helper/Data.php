<?php
namespace Commercers\ExportOrder\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper {
	public function isEnabled()
	{
		return $this->getConfigValue('export_order/general/enabled');
	}
}