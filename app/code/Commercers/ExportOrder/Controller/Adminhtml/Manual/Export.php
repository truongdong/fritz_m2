<?php

namespace Commercers\ExportOrder\Controller\Adminhtml\Manual;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Commercers\Profilers\Service\DataSource\Factory as DataSourceFactory;

class Export extends \Magento\Backend\App\Action {

    protected $_profilerFactory;

    public function __construct(
        Action\Context $context,
        \Commercers\Profilers\Model\ProfilersFactory $profilerFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Commercers\ExportOrder\Service\DataSource\Order $dataSourceOrder,
        DataSourceFactory $dataSourceFactory
    ) {
        $this->_profilerFactory = $profilerFactory;
        $this->_resultPageFactory = $resultPageFactory;
        $this->resultRawFactory      = $resultRawFactory;
        $this->fileFactory           = $fileFactory;
        $this->orderFactory = $orderFactory;
        $this->dataSourceFactory = $dataSourceFactory;
        $this->dataSourceOrder = $dataSourceOrder;
        parent::__construct($context);
    }

    public function execute() {
        $params = $this->getRequest()->getParams();
        //echo "<pre>";print_r($params);exit;
        $profilerId = $params['profiler_id'];
        $profiler = $this->_profilerFactory->create()->load($profilerId);
//        echo "<pre>";print_r($profiler->getData());exit;
        $dataInTextarea = $profiler->getExportOutputTemplate();

//        $dataSource = $this->dataSourceFactory->get($profiler->getDataSource());
        $process = \Magento\Framework\App\ObjectManager::getInstance()->get("\Commercers\Profilers\Service\Profiler\Process\Export");
        $incrementFrom = (string)$params['increment_from'];
        if($incrementFrom == 1)
            $incrementFrom = $this->orderFactory->create()->getCollection()
                ->getFirstItem()
                ->getIncrementId();
        $incrementTo = (string)$params['increment_to'];

        if(!$incrementTo){
            $incrementTo = $this->orderFactory->create()->getCollection()
                ->setOrder('entity_id','DESC')
                ->getFirstItem()
                ->getIncrementId();
        }
        $dataForm = array(
            'store_id'=>  $params['store_id'],
            'increment_from'=>  $incrementFrom,
            'increment_to'=>    $incrementTo,
            'daterange_from'=>  $params['daterange_from'],
            'daterange_to'=>    $params['daterange_to'],
            'last_x_days'=>  $params['last_x_days'],
        );
        $data = $this->dataSourceOrder->getDataManual($dataForm, $profiler);
        if(!count($data['items']['item'])){
            $this->messageManager->addError('Cannot find orders can export');
            $this->_redirect('*/*/index');
        }
        if($data == false) {
            $output = false;

        }else{
            $output = $process->getOutput($dataInTextarea,$data);

            $date = new \DateTime();
            $fileName = "export_" . $profiler->getData('data_source') . "_" . $date->format('dmYHis') . "." . $profiler->getData('format');
            $process->writeFileFromOutputFromat($profiler, $fileName,$output);
            if(isset($params['start_download'])){
                $localFolder = $profiler->getData('local_folder');
                $file = array('fileName'=>$fileName,'path'=>$localFolder);
                $this->downloadCsv($file);
                return;
            }
            $this->messageManager->addSuccess('Export Done');
            $this->_redirect('*/*/index');
        }


    }
    public function downloadCsv($file){
        return $this->fileFactory->create(
            $file['fileName'],
            [
                'type' => "filename",
                'value' => $file['path']. DIRECTORY_SEPARATOR .$file['fileName'],
                'rm' => true,
            ],
            \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR,
            'application/octet-stream'
        );
    }

}
