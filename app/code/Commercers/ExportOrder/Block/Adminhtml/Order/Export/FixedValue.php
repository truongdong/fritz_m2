<?php
namespace Commercers\ExportOrder\Block\Adminhtml\Order\Export;

use YourPackage\YourModule\Model\StorePriceFactory;

class FixedValue extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    public function render(\Magento\Framework\DataObject $row)
    {
        return $this->getColumn()->getValue();
    }
}