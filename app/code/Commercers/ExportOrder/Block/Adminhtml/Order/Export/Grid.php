<?php

namespace Commercers\ExportOrder\Block\Adminhtml\Order\Export;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magento\Sales\Api\ShipmentRepositoryInterface;

class Grid extends Extended
{
    protected $_shipmentCollectionFactory;

    public function __construct(
        Context $context,
        Data $backendHelper,
        ShipmentRepositoryInterface $shipmentCollectionFactory,
        array $data = []
    ) {
        $this->_shipmentCollectionFactory = $shipmentCollectionFactory;

        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();

        $this->setId('order_export_grid');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);

    }

    protected function _prepareCollection()
    {
        $collection = $this->_shipmentCollectionFactory->create()->getCollection()
            ->addFieldToSelect('created_at')
            ->addFieldToSelect('order_id');

        $collection->getSelect()
            ->join(
                array('order' =>'sales_order'), 'main_table.order_id = order.entity_id',
                array('exported_date','shippingaddresses_account_id','egfra_division')
            )
            ->join(
                array('shipment_item' =>'sales_shipment_item'), 'main_table.entity_id = shipment_item.parent_id',
                array('shipment_item_name' => 'shipment_item.name',
                    'shipment_item_sku' => 'shipment_item.sku',
                    'shipment_item_qty' => 'FORMAT(shipment_item.qty, 0)',
                    'shipment_item_price' => 'shipment_item.price'
                )
            )
            ->where('exported_date IS NULL')
            ->group('shipment_item.sku')
            ->group('shipment_item.parent_id');

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {


        $this->addColumn(
            'supplier_key',
            [
                'header' => __('Lieferantenschlüssel'),
                'type' => 'text',
                'value' => '0000',
                'renderer' => 'Commercers\ExportOrder\Block\Adminhtml\Order\Export\FixedValue'
            ]
        );

        $this->addColumn(
            'supplier',
            [
                'header' => __('Lieferant'),
                'index' => 'supplier',
                'value' => 'Nicht zugeordnet',
                'renderer' => 'Commercers\ExportOrder\Block\Adminhtml\Order\Export\FixedValue'
            ]
        );

        $this->addColumn(
            'product_hierarchy',
            [
                'header' => __('Produkthirearchie'),
                'value' => '#',
                'renderer' => 'Commercers\ExportOrder\Block\Adminhtml\Order\Export\FixedValue'
            ]
        );

        $this->addColumn(
            'shipment_item_sku',
            [
                'header' => __('Material'),
                'index' => 'shipment_item_sku'
            ]
        );
        $this->addColumn(
            'shipment_item_name',
            [
                'header' => __('Materialbezeichnung'),
                'index' => 'shipment_item_name',
                'header_css_class' => 'col-shipment_item_name',
                'column_css_class' => 'col-shipment_item_name'
            ]
        );
        $this->addColumn(
            'materialart',
            [
                'header' => __('Materialart'),
                'value' => 'WERB',
                'renderer' => 'Commercers\ExportOrder\Block\Adminhtml\Order\Export\FixedValue',
            ]
        );
        $this->addColumn(
            'order_id_1',
            [
                'header' => __('Auftrag 1'),
                'index' => 'order_id',
                'sortable' => false,
                'header_css_class' => 'col-order_id_1',
                'column_css_class' => 'col-order_id_1'
            ]
        );
        $this->addColumn(
            'shippingaddresses_account_id',
            [
                'header' => __('Kundennr.'),
                'index' => 'shippingaddresses_account_id',
                'sortable' => false,
                'header_css_class' => 'col-account_number',
                'column_css_class' => 'col-account_number'
            ]
        );

        $this->addColumn(
            'order_id',
            [
                'header' => __('Auftrag'),
                'index' => 'order_id',
                'sortable' => false,
                'header_css_class' => 'col-order_id',
                'column_css_class' => 'col-order_id'
            ]
        );
        $this->addColumn(
            'created_at',
            [
                'header' => __('WA-Datum'),
                'index' => 'created_at',
                'sortable' => false,
                'header_css_class' => 'col-created_at',
                'column_css_class' => 'col-created_at',
                'renderer' => 'Commercers\ExportOrder\Block\Adminhtml\Order\Export\FormatDateTime'
            ]
        );
        $this->addColumn(
            'ag',
            [
                'header' => __('AG'),
                'value' => 'M01',
                'renderer' => 'Commercers\ExportOrder\Block\Adminhtml\Order\Export\FixedValue'
            ]
        );
        $this->addColumn(
            'auftragsgrund',
            [
                'header' => __('Auftragsgrund'),
                'value' => 'Kunden-/Vertretermuster',
                'renderer' => 'Commercers\ExportOrder\Block\Adminhtml\Order\Export\FixedValue'
            ]
        );
        $this->addColumn(
            'total_lp_ek',
            [
                'header' => __('Menge Fl.'),
                'value' => ''
            ]
        );
        $this->addColumn(
            'shipment_item_qty',
            [
                'header' => __('Menge Stück'),
                'index' => 'shipment_item_qty',
                'header_css_class' => 'col-shipment_item_qty',
                'column_css_class' => 'col-shipment_item_qty'
            ]
        );
        $this->addColumn(
            'shipment_item_price',
            [
                'header' => __('Mat.eins.ohne SEK/VT'),
                'index' => 'shipment_item_price',
                'type' => "text",
                'header_css_class' => 'col-shipment_item_price',
                'column_css_class' => 'col-shipment_item_price',
                'renderer'  => 'Commercers\ExportOrder\Block\Adminhtml\Order\Export\TotalPrice'
            ]
        );
        $this->addColumn(
            'taxes',
            [
                'header' => __('Abgaben Zoll, Steuer'),
                'value' => '0,00',
                'renderer' => 'Commercers\ExportOrder\Block\Adminhtml\Order\Export\FixedValue'
            ]
        );
        $this->addColumn(
            'total_price',
            [
                'header' => __('Ver.Preis'),
                'index' => 'total_price',
                'type' => "text",
                'header_css_class' => 'col-total_price',
                'column_css_class' => 'col-total_price',
                'renderer'  => 'Commercers\ExportOrder\Block\Adminhtml\Order\Export\TotalPrice'
            ]
        );

        $this->addExportType('*/*/export/report/classification', __('CSV'));

        return parent::_prepareColumns();
    }
}