<?php
namespace Commercers\ExportOrder\Block\Adminhtml\Order\Export;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Backend\Block\Context;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class FormatDateTime extends AbstractRenderer
{
    protected $_dateTime;

    public function __construct(
    Context $context,
    TimezoneInterface $dateTime,
    array $data = []
    ){
        parent::__construct($context, $data);

        $this->_dateTime = $dateTime;
    }

    public function render(\Magento\Framework\DataObject $row)
    {
        $dateTime = $row->getData($this->getColumn()->getIndex());

        return $this->_dateTime->date($dateTime)->format('d.m.Y H:i:s');
    }
}