<?php
namespace Commercers\ExportOrder\Block\Adminhtml\Order\Export;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Backend\Block\Context;
use Magento\Framework\Pricing\Helper\Data;

class TotalPrice extends AbstractRenderer
{
    protected $_priceFormat;

    public function __construct(
        Context $context,
        Data $priceFormat,
        array $data = []
    ){
        parent::__construct($context, $data);

        $this->_priceFormat = $priceFormat;
    }

    public function render(\Magento\Framework\DataObject $row)
    {
        $price = $row->getData($this->getColumn()->getIndex());
        if($this->getColumn()->getIndex() === 'total_price'){
            $price = $row->getData('shipment_item_qty') * $row->getData('shipment_item_price');
        }
        $price = $this->_priceFormat->currency($price, true, false);

        return $price;
    }
}