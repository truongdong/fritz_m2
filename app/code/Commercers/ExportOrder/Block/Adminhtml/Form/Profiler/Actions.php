<?php
namespace Commercers\ExportOrder\Block\Adminhtml\Form\Profiler;

class Actions extends \Magento\Backend\Block\Template
{
    
    protected $_template = 'Commercers_ExportOrder::form/actions.phtml';
    
    public function __construct(
            \Magento\Backend\Block\Template\Context $context,
            \Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $statusCollectionFactory,
            \Commercers\Profilers\Model\ProfilersFactory $profilerFactory
            ) {
        
        $this->statusCollectionFactory = $statusCollectionFactory; 
        $this->profilerFactory = $profilerFactory;
        
        parent::__construct($context);
    }
    
    protected function getProfiler(){
        $profilerId = $this->_request->getParam('id');
        
        if(!$profilerId)
            return false;
        
        $profiler = $this->profilerFactory->create()->load($profilerId);
        return $profiler;
    }

    public function getActionValues(){
        $profiler = $this->getProfiler();
        $actionsSerialized = $profiler->getActionsSerialized();
        $actions = unserialize($actionsSerialized);
        //print_r($actions);exit;
        return isset($actions['actions']) ? $actions['actions'] : array();
    }


    public function getStatusOptions()
    {       
        $options = $this->statusCollectionFactory->create()->toOptionArray();    
        
        return $options;
    }  
    
}