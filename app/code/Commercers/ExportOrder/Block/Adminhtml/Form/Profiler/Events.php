<?php
namespace Commercers\ExportOrder\Block\Adminhtml\Form\Profiler;

class Events extends \Magento\Backend\Block\Template
{
    
    protected $_template = 'Commercers_ExportOrder::form/automatically_events.phtml';
    
    public function __construct(
            \Magento\Backend\Block\Template\Context $context,
            \Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $statusCollectionFactory,
            \Commercers\Profilers\Model\ProfilersFactory $profilerFactory
            ) {
        
        $this->statusCollectionFactory = $statusCollectionFactory; 
        $this->profilerFactory = $profilerFactory;
        
        parent::__construct($context);
    }
    
    public function getEvents()
    {       
        return [
            'sales_order_save_after'  => array('title' => "After place an order"),
            'order_cancel_after' => array('title' => "After cancel an order"),
            
        ];
    }  
    
    protected function getProfiler(){
        $profilerId = $this->_request->getParam('id');
        
        if(!$profilerId)
            return false;
        
        $profiler = $this->profilerFactory->create()->load($profilerId);
        return $profiler;
    }

    public function getEventValues(){
        $profiler = $this->getProfiler();
        $actionsSerialized = $profiler->getActionsSerialized();
        $actions = unserialize($actionsSerialized);
        return isset($actions['events']) ? $actions['events'] : array();
    }
    
}