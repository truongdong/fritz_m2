<?php

namespace Commercers\ExportOrder\Block\Adminhtml\Form\Profiler\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Rule\Model\Condition\AbstractCondition;

class Actions extends Generic implements TabInterface
{
    
    const showForDataSources = ['order_item', 'order', 'order_pdf','invoice_pdf'];
    
    protected $_template = 'Commercers_ExportOrder::form/profiler/actions.phtml';
    /**
     * Core registry
     *
     * @var \Magento\Backend\Block\Widget\Form\Renderer\Fieldset
     */
    protected $rendererFieldset;

    /**
     * @var \Magento\Rule\Block\Conditions
     */
    protected $conditions;
    

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Rule\Block\Conditions $conditions
     * @param \Magento\Backend\Block\Widget\Form\Renderer\Fieldset $rendererFieldset
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Backend\Block\Widget\Form\Renderer\Fieldset $rendererFieldset,
        \Commercers\Profilers\Model\ProfilersFactory $profilerFactory,    
        array $data = []
    ) {
        $this->rendererFieldset = $rendererFieldset;
        $this->_rendererFieldset = $rendererFieldset;
        
        $this->profilerFactory  = $profilerFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Filters');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Filters');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        
        if(!$this->getProfiler() ||  !in_array($this->getProfiler()->getDataSource(), array('order', 'order_item', 'order_pdf','invoice_pdf')) ){
            
            return false;
        }
            
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        if(!$this->getProfiler() || !in_array($this->getProfiler()->getDataSource(), array('order', 'order_item', 'order_pdf','invoice_pdf')) )
            return true;
        return false;
    }
    
    /**
     * Tab should be loaded trough Ajax call
     *
     * @return bool
     * @codeCoverageIgnore
     */
    public function isAjaxLoaded()
    {
        return false;
    }
    
}