<?php


namespace Commercers\ExportOrder\Block\Adminhtml;

class Manual extends \Magento\Backend\Block\Template
{
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Store\Model\System\Store $systemStore,
        \Commercers\Profilers\Model\ProfilersFactory $profileSource,
        \Magento\Framework\View\Element\Html\Date $dateElement,
        \Magento\Framework\Stdlib\DateTime\DateTimeFormatterInterface $dateTimeFormatter,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->profileSource = $profileSource;
        $this->systemStore = $systemStore;
        $this->dateElement = $dateElement;
        $this->dateTimeFormatter = $dateTimeFormatter;
        $this->orderCollectionFactory = $orderCollectionFactory;
    }

    public function optionProfilers()
    {
        $profilersCollection = $this->profileSource->create()->getCollection();
        $profilers = $profilersCollection->addFieldToFilter('data_source', array('eq' => 'order'));
        $arrayProfilers = array();
        $html = null;
        foreach ($profilers as $profile) {
            $html .= '<option value="' . $profile['id'] . '">' . $profile['name'] . ' (' . __(
                    'ID: %1',
                    $profile['id']
                ) . ')</option>';

        }
        return $html;
    }
    public function getStoreViewSelectorHtml()
    {
        $websiteCollection = $this->systemStore->getWebsiteCollection();
        $groupCollection = $this->systemStore->getGroupCollection();
        $storeCollection = $this->systemStore->getStoreCollection();

        $html = '<select multiple="multiple" id="store_id" name="store_id[]" style="width: 320px; height: 130px; margin-bottom: 10px;">';

        $html .= '<option value="0" selected="selected">' . __(
                'All Store Views'
            ) . '</option>';

        foreach ($websiteCollection as $website) {
            $websiteShow = false;
            foreach ($groupCollection as $group) {
                if ($group->getWebsiteId() != $website->getId()) {
                    continue;
                }
                $groupShow = false;
                foreach ($storeCollection as $store) {
                    if ($store->getGroupId() != $group->getId()) {
                        continue;
                    }
                    if (!$websiteShow) {
                        $websiteShow = true;
                        $html .= '<optgroup label="' . $website->getName() . '"></optgroup>';
                    }
                    if (!$groupShow) {
                        $groupShow = true;
                        $html .= '<optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;' . $group->getName() . '">';
                    }
                    $html .= '<option value="' . $store->getId() . '">&nbsp;&nbsp;&nbsp;&nbsp;' . $store->getName(
                        ) . '</option>';
                }
                if ($groupShow) {
                    $html .= '</optgroup>';
                }
            }
        }
        $html .= '</select>';
        return $html;
    }
    public function getCalendarHtml($id)
    {
        $this->dateElement->setData(
            [
                'name' => $id,
                'id' => $id,
                'class' => '',
                'value' => '',
                'date_format' => $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT),
                'image' => $this->getViewFileUrl('Magento_Theme::calendar.png'),
            ]
        );
        return $this->dateElement->getHtml();
    }
    public function getOrderIncrement(){
        $orderCollection = $this->orderCollectionFactory->create();
        $firstOrder = $orderCollection->getFirstItem();
        $orderCollection = $this->orderCollectionFactory->create()->setOrder('increment_id','DESC');
        $lastOrder = $orderCollection->getFirstItem();
        $increment = array('first'=>$firstOrder->getIncrementId(),'last'=>$lastOrder->getIncrementId());
        return $increment;
    }
}
