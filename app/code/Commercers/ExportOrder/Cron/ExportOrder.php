<?php
namespace Commercers\ExportOrder\Cron;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Commercers\ExportOrder\Block\Adminhtml\Order\Export\Grid;
use Commercers\ExportOrder\Helper\Email;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;

class ExportOrder
{
    protected $_view;
    protected $_fileFactory;
    protected $_email;
    protected $_orderCollectionFactory;
    protected $date;

	public function __construct(
        FileFactory $fileFactory,
        Context $context,
        Email $email,
        CollectionFactory $orderCollectionFactory,
        DateTime $date
	){
        $this->_fileFactory = $fileFactory;
        $this->_view = $context->getView();
        $this->_email  = $email;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->date = $date;
	}

	public function execute(){
	    $isEnable = $this->_email->getConfigValue('export_order/general/enabled');
	    if(!$isEnable) return $this;

        $blockInstance = $this->_view->getLayout()->createBlock(Grid::class);
        $this->_fileFactory->create(Email::FILE_NAME, $blockInstance->getExcel(), DirectoryList::MEDIA);

        $emailVariables = array();
        $templateId = 'email_order_export_template';
        $senderInfor = [
            'email' => 'huyphan@commercers.com',
            'name' => 'Admin'
        ];
        $receiveInfor = [
            'email' => $this->_email->getConfigValue('export_order/general/email'),
            'name' => 'Phan Huy'
        ];

        $this->_email->sendEmail($templateId, $emailVariables, $senderInfor, $receiveInfor);
        $this->updateOrderData();
	}

	protected function updateOrderData(){
        $dateTime = $this->date->gmtDate();
        $collection = $this->_orderCollectionFactory->create();
        $collection->getSelect()
            ->join(
                array('shipment' =>'sales_shipment'), 'shipment.order_id = main_table.entity_id',
                ''
            )
            ->where('exported_date IS NULL');
        foreach($collection as $order){
            $order->setExportedDate($dateTime);
        }
        $collection->save();
    }
}
