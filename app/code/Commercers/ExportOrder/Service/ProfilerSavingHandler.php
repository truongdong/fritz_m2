<?php
namespace Commercers\ExportOrder\Service;

class ProfilerSavingHandler {
    
    public function __construct(
        \Commercers\ExportOrder\Model\RuleFactory $ruleFactory
            ) {
        $this->ruleFactory = $ruleFactory;
    }
    
    public function execute($profiler, $data){
        
        
        if (isset($data['rule'])) {
            
            $rules = $this->ruleFactory->create()->getCollection();
            
            if( in_array($profiler->getDataSource(), \Commercers\ExportOrder\Model\Constant::showForDataSources) ){
                
                $rules->addFieldToFilter('data_source', $profiler->getDataSource());
                $rules->addFieldToFilter('profiler_id', $profiler->getId());

                if($rules->getSize()){
                    $rule = $rules->getFirstItem();
                }else{
                    $rule = $this->ruleFactory->create();
                }


                $rule->addData([
                    'profiler_id' => $profiler->getId(),
                    'data_source' => $profiler->getDataSource()
                ]);

                $rule->loadPost($data['rule'])->save();
            }
            
        }
        $actions = array();
        if(isset($data['actions'])){
            $actions['actions'] = $data['actions'];
            
        }
        if(isset($data['events'])){
            $actions['events'] = $data['events'];            
        }
        $profiler->setData('actions_serialized', serialize($actions))->save();
          
    }
}