<?php

namespace Commercers\ExportOrder\Service\DataSource;

class Pdf extends \Commercers\ExportOrder\Service\DataSource\Order
{

    
    
    protected function exportCollectionToData($orderCollection){
        
        $data['orders']['order'] = array();
        //$orderCollection->addFieldToFilter('entity_id', 1);
        foreach ($orderCollection as $order) {
            
            $data['orders']['order'][] = [
                'id' => $order->getId(),
                'increment_id' => $order->getIncrementId()
            ];
            $this->_proceededIds[] = $order->getId();
           
        }

        return $data;
    }
    
    protected function extendData(&$itemData){
        if(!count($this->dataExtensions)){
            return false;
        }
        foreach($this->dataExtensions as $dataExtension){
            $extension = $this->objectManager->create($dataExtension);
            $extension->execute($itemData);
        }
    }


    public function getItemById($id, $profiler, $isIncrement = false)
    {
        $orderCollection = $this->_getCollection($profiler);
        $orderCollection->addFieldToFilter('entity_id', $id);
        
        return $this->exportCollectionToData($orderCollection);
    }

    
    
    
}
