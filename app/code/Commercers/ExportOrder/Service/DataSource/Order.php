<?php

namespace Commercers\ExportOrder\Service\DataSource;

class Order implements \Commercers\Profilers\Service\DataSource
{
    protected $_proceededIds = array();




    public function __construct(
        \Magento\Framework\Api\ExtensibleDataObjectConverter $dataArrayConvert,
        \Magento\Sales\Model\Order $order,
        \Commercers\ExportOrder\Model\RuleFactory $ruleFactory,
        \Commercers\Profilers\Model\ObjectLogFactory $objectFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Commercers\ExportOrder\Model\Condition\Sql\Builder $conditionSqlBuilder,
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Framework\DB\Transaction $transaction,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Sales\Model\Order\Config $orderStatuses,
        \Magento\Framework\Stdlib\DateTime\Timezone $timeZone,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $attributeFactory,
        \Magento\Catalog\Model\Product\OptionFactory $customProductOptions,
        $dataExtensions = array()
    )
    {

        $this->_order = $order;
        $this->dataArrayConvert = $dataArrayConvert;
        $this->conditionSqlBuilder = $conditionSqlBuilder;
        $this->date = $date;
        $this->ruleFactory = $ruleFactory;
        $this->dataExtensions = $dataExtensions;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->objectFactory = $objectFactory;
        $this->_invoiceService = $invoiceService;
        $this->_transaction = $transaction;
        $this->_resource = $resource;
        $this->timeZone = $timeZone;
        $this->orderStatuses = $orderStatuses;
        $this->productFactory = $productFactory;
        $this->attributeFactory = $attributeFactory;
        $this->customProductOptions = $customProductOptions;
    }

    public function getProceededIds(){
        return $this->_proceededIds;
    }


    public function _getCollection($profiler = null,$isGetItemById = false){
        $collection = $this->_order->getCollection();
        $collection->addAttributeToSelect('*');
//        $collection->addFieldToFilter('entity_id',array('in'=> array(212,213,214,215)));
//
//        return $collection;
        if($profiler){

            $rules = $this->ruleFactory->create()->getCollection();
            $rules->addFieldToFilter('data_source', $profiler->getDataSource());
            $rules->addFieldToFilter('profiler_id', $profiler->getId());

            if($rules->getSize() AND !$isGetItemById){
                $rule = $rules->getFirstItem();

                if($rule->getConditions()){
                    $this->conditionSqlBuilder->attachConditionToCollection($collection, $rule->getConditions());

                }

            }
            //echo $collection->getSelect() ."<br>";
            if($profiler->getData('object_log_status') AND !$isGetItemById){

                //if($profiler->getDataSource() == 'invoice_pdf'){
                    $collection->getSelect()->where('main_table.entity_id NOT IN (SELECT object_id FROM commercers_profilers_object_log WHERE profiler_id = '.$profiler->getId().')');
                //}else{
                    //$collection->getSelect()->where('entity_id NOT IN (SELECT object_id FROM commercers_profilers_object_log WHERE profiler_id = '.$profiler->getId().')');
                //}

                //echo $collection->getSelect() ."<br>";exit;
            }
        }
        //exit;
        //echo "<pre>"; print_r($collection->getFirstItem()->getData());exit;
        return $collection;
    }

    public function getCount($profiler){
        $orderCollection = $this->_getCollection($profiler);
        if($orderCollection->getSize()){
            return $orderCollection->count();
        }
        return false;
    }



    public function getData($profiler, $limits = array() ) {

        $orderCollection = $this->_getCollection($profiler);
        if($orderCollection->getSize()){
            if($limits && isset($limits['limit']) && isset($limits['offset'])){
                $orderCollection->getSelect()->limit($limits['limit'], $limits['offset']);
                //echo $orderCollection->getSelect();exit;
            }
            return $this->exportCollectionToData($orderCollection);
        }

    }

    protected function exportCollectionToData($orderCollection){


        $data['items']['item'] = array();
        foreach ($orderCollection as $order) {

            $orderData = $this->dataArrayConvert->toFlatArray($order, [], \Magento\Sales\Api\Data\OrderInterface::class);

            foreach ($order->getAllItems() as $item) {
                $itemData = $this->dataArrayConvert->toFlatArray($item, [], \Magento\Sales\Api\Data\OrderItemInterface::class);
                $itemData['order'] = $orderData;
                /**
                 * Here we allow some plug-ins working here
                 */
                $this->extendData($itemData);
                $data['items']['item'][]   = $itemData;
            }
            $this->_proceededIds[] = $order->getId();

        }
        return $data;
    }




    protected function extendData(&$itemData){
        if(!count($this->dataExtensions)){
            return false;
        }
        foreach($this->dataExtensions as $dataExtension){
            $extension = $this->objectManager->create($dataExtension);
            $extension->execute($itemData);
        }
    }


    public function getItemById($id, $profiler, $isIncrement = false)
    {

        $orderCollection = $this->_getCollection($profiler,true);

        if($isIncrement){
            $orderCollection->addFieldToFilter('increment_id', $id);
        }else{
            $orderCollection->addFieldToFilter('entity_id', $id);
        }

        return $this->exportCollectionToData($orderCollection);
    }

    public function log($profiler, $processLog){

        if(count($this->_proceededIds)){
            $actionsSerialized = $profiler->getActionsSerialized();
            $actions = unserialize($actionsSerialized);
            if(is_array($actions)) {
                if (count($actions) && isset($actions['actions'])) {
                    foreach ($this->_proceededIds as $proceededId) {
                        $this->doAction($actions['actions'], $proceededId);
                    }
                }
            }


            //print_r($actions);exit;
        }


        if(count($this->_proceededIds)){
            foreach($this->_proceededIds as $proceededId){
                $objectLog = $this->objectFactory->create();
                $objectLog->addData(array(
                    'profiler_id' => $profiler->getId(),
                    'datasource' => 'order',
                    'process_log_id' => $processLog->getId(),
                    'object_id' => $proceededId
                ));
                $objectLog->save();
            }
        }

    }

    protected function doAction($actions, $objectId){

        $order = $this->_order->load($objectId);
        $isUpdated = false;
        foreach($actions as $actionName => $actionValue){
            if($actionName == 'set_order_status' && !empty($actionValue)) {

                $newOrderStatus = $actionValue;

                foreach ($this->orderStatuses->getStates() as $state => $label) {
                    $stateStatuses = $this->orderStatuses->getStateStatuses($state, false);
                    foreach ($stateStatuses as $status) {
                        if ($status == $newOrderStatus) {
                            $order->setData('state', $state);
                            $order->setStatus($newOrderStatus);
                        }
                    }
                }
                $isUpdated = true;
            }
            if($actionName == 'set_comment_history' && !empty($actionValue)) {
                $order->addStatusHistoryComment($actionValue);
                $isUpdated = true;
            }
            if($actionName == 'create_invoice' && !empty($actionValue)) {
                if($order->canInvoice()) {
                    $invoice = $this->_invoiceService->prepareInvoice($order);
                    $invoice->register();
                    $invoice->save();
                    $transactionSave = $this->_transaction->addObject(
                        $invoice
                    )->addObject(
                        $invoice->getOrder()
                    );
                    $transactionSave->save();
                    //$this->invoiceSender->send($invoice);
                    //send notification code
                    $order->addStatusHistoryComment(
                        __('Profiler export create an invoice #%1.', $invoice->getId())
                    )
                    ->setIsCustomerNotified(false)
                    ->save();
                }
            }
        }
        if($isUpdated){

            $order->save();
        }

    }

    public function getDataManual($dataForm, $profiler)
    {
        $today =  date("Y-m-d");
        $orderCollection = $this->_getCollection($profiler);
        if(!in_array(0,$dataForm['store_id']))
        $orderCollection->addFieldToFilter('store_id', array(
            "in" => $dataForm['store_id'],
        ));

        $orderCollection->addFieldToFilter('increment_id', array(
                                                "from" => $dataForm['increment_from'],
                                                "to" => $dataForm['increment_to']
                                            ));

        if($dataForm['daterange_from'] AND  !$dataForm['daterange_to']){
            $daterangeFrom = date("Y-m-d 00:00:00",strtotime($dataForm['daterange_from'])) ;
            $orderCollection->addFieldToFilter('created_at', ['gteq' => $daterangeFrom]);
            //echo $orderCollection->getSelect();exit;
        }elseif(!$dataForm['daterange_from'] AND  $dataForm['daterange_to']){
            $daterangeTo = date("Y-m-d 23:59:59",strtotime($dataForm['daterange_to']));
            $orderCollection->addFieldToFilter('created_at', array(
                "lteq" =>$daterangeTo
            ));

        }elseif($dataForm['daterange_from'] AND  $dataForm['daterange_to']){
            $daterangeFrom = date("Y-m-d 00:00:00",strtotime($dataForm['daterange_from'])) ;
            $daterangeTo = date("Y-m-d 23:59:59",strtotime($dataForm['daterange_to']));
            $orderCollection->addFieldToFilter('created_at', array(
                "from" => $daterangeFrom,
                "to" => $daterangeTo
            ));

        }
        if($dataForm['last_x_days']){
            $createdDays = date("Y-m-d 00:00:00", strtotime(date("Y-m-d 00:00:00", strtotime($today)) . " - " . $dataForm['last_x_days'] . " day"));
            $orderCollection->addFieldToFilter('created_at', array(
                "gteq" => $createdDays
            ));
        }

        return $this->exportCollectionToData($orderCollection);
    }



}
