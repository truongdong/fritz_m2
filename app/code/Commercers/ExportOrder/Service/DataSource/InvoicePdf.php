<?php

namespace Commercers\ExportOrder\Service\DataSource;

class InvoicePdf extends \Commercers\ExportOrder\Service\DataSource\Order
{

    
    
    protected function exportCollectionToData($orderCollection){
        
        $data['orders']['order'] = array();
        foreach ($orderCollection as $order) {

            //$shipmentCollection = $order->getShipmentsCollection();
            //print_r($order->getShipmentsCollection()->getSize());exit;
            // if ($shipmentCollection->getSize() > 0) {
            //     $data['orders']['order'][] = [
            //     'id' => $order->getId(),
            //         'increment_id' => $order->getIncrementId()
            //     ];
            //     $this->_proceededIds[] = $order->getId();
            // }
            $data['orders']['order'][] = [
            'id' => $order->getId(),
                'increment_id' => $order->getIncrementId()
            ];
            $this->_proceededIds[] = $order->getId();
        }
        //print_r($data);exit;
        return $data;
    }
    
    protected function extendData(&$itemData){
        if(!count($this->dataExtensions)){
            return false;
        }
        foreach($this->dataExtensions as $dataExtension){
            $extension = $this->objectManager->create($dataExtension);
            $extension->execute($itemData);
        }
    }

    /*
    public function getItemById($id, $profiler, $isInrement = false)
    {
        $orderCollection = $this->_getCollection($profiler);
        $orderCollection->addFieldToFilter('entity_id', $id);
        
        return $this->exportCollectionToData($orderCollection);
    }
     * 
     */
    
    
}
