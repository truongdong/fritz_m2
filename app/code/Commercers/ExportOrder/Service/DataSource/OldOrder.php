<?php

namespace Commercers\ExportOrder\Service\DataSource;

class OldOrder extends \Commercers\ExportOrder\Service\DataSource\Order
{



  public function _getCollection($profiler = null,$isGetItemById = false){
      $collection = $this->_order->getCollection();
      $collection->addAttributeToSelect('*');
      if($profiler){

          $rules = $this->ruleFactory->create()->getCollection();
          $rules->addFieldToFilter('data_source', $profiler->getDataSource());
          $rules->addFieldToFilter('profiler_id', $profiler->getId());

          if($rules->getSize() AND !$isGetItemById){
              $rule = $rules->getFirstItem();

              if($rule->getConditions()){
                  $this->conditionSqlBuilder->attachConditionToCollection($collection, $rule->getConditions());

              }

          }


          if(!$isGetItemById)
          $this->_buildConditionForFiltersCustomField($profiler,$collection);

          if($profiler->getData('object_log_status') AND !$isGetItemById){
                  $collection->getSelect()->where('main_table.entity_id NOT IN (SELECT object_id FROM commercers_profilers_object_log WHERE profiler_id = '.$profiler->getId().')');

          }
      }
      //exit;
      //echo "<pre>"; echo $collection->getSelect();exit;
      return $collection;
  }
  protected function exportCollectionToData($orderCollection){

      $data['orders']['order'] = array();
      if($orderCollection->getSize()){
          foreach ($orderCollection as $order) {

              $orderData = $this->dataArrayConvert->toFlatArray($order, [], \Magento\Sales\Api\Data\OrderInterface::class);
              $orderData['updated_at_timestamp'] = strtotime($orderData['updated_at']);
              $orderId = $order->getId();
              foreach ($order->getAllItems() as $item) {
                  /**
                   * Do not export bundle product
                   */
                  if($item->getData('product_type') == 'bundle') continue;
                  $itemId = $item->getItemId();

                  if($this->isChildProduct($itemId,$orderId)){
                      $itemData = $this->getChildItemData($itemId,$orderId);
                      unset($itemData[0]['product_options']);
                  }else{
                      $itemData = $this->dataArrayConvert->toFlatArray($item, [], \Magento\Sales\Api\Data\OrderItemInterface::class);
                  }
                  $orderData = $orderData;
                  $orderData['shipping'] = $order->getShippingAddress()->getData();
                  /**
                   * Here we allow some plug-ins working here
                   */
                  $this->extendData($itemData);
                  if(isset($itemData['qty_ordered']))
                      $itemData['qty'] = $itemData['qty_ordered'];
                  $orderData['items']['item'][]   = $itemData;
              }
              if($order->getInvoiceCollection()->getSize()){
                  $orderData['invoice'] = $order->getInvoiceCollection()->getData();
              }
              $data['orders']['order'][] = $orderData;
              $this->_proceededIds[] = $order->getId();

          }
      }
      return $data;
  }

  protected function extendData(&$itemData){
      if(!count($this->dataExtensions)){
          return false;
      }
      foreach($this->dataExtensions as $dataExtension){
          $extension = $this->objectManager->create($dataExtension);
          $extension->execute($itemData);
      }
  }
  protected function isChildProduct($itemId,$orderId){
      $connection = $this->_resource->getConnection();
      $tableName = $this->_resource->getTableName('sales_order_item');
      $sql = "SELECT `parent_item_id` FROM {$tableName} WHERE {$tableName}.order_id = {$orderId} AND {$tableName}.item_id = {$itemId}";
      $result = $connection->fetchOne($sql);
      if((int)$result){
          return true;
      }
      return false;
  }
  protected function getChildItemData($itemId,$orderId){
      $connection = $this->_resource->getConnection();
      $tableName = $this->_resource->getTableName('sales_order_item');
      $sql = "SELECT *  FROM {$tableName} WHERE {$tableName}.order_id = {$orderId} AND {$tableName}.item_id = {$itemId}";
      $result = $connection->fetchAll($sql);
      return $result;
  }
  protected function _buildConditionForFiltersCustomField($profiler, $collection){
      $filtersCustomEncode = $profiler->getFilterCustomEncode();
      $filtersCustom = json_decode($filtersCustomEncode,true);
      if(isset($filtersCustom['create_at_x_hour']) AND $filtersCustom['create_at_x_hour'] > 0){
          $timeZone = $this->timeZone->getConfigTimezone();
          $now = new \DateTime('now', new \DateTimeZone($timeZone));
          $toDate = $now->format('Y-m-d H:i:s');
          $fromDate = $now->modify('-'.$filtersCustom['create_at_x_hour'].' hour')->format('Y-m-d H:i:s');

          $collection->getSelect()
                  ->join(array('order_invoice' => 'sales_invoice'),'main_table.entity_id= order_invoice.order_id',array(
                      'invoice_created_at' => 'order_invoice.created_at'
                  ));

          $collection->addFieldToFilter('order_invoice.created_at',array(
              'from' => $fromDate,
              'to' => $toDate,
              'date' => true,
          ));

      }
  }

}
