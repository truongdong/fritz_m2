<?php

namespace Commercers\Theme\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Catalog\Model\ProductFactory;

class Data extends AbstractHelper {



	/**
	 * @var ProductFactory
	 */
	protected $_productFactory;

	protected $_filesystem ;

	protected $_imageFactory;

	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $_storeManager;

	public function __construct(
		Context $context,
		ProductFactory $productFactory,
		\Magento\Framework\Filesystem $filesystem,
		\Magento\Framework\Image\AdapterFactory $imageFactory,
		\Magento\Store\Model\StoreManagerInterface $storeManager
	) {
		$this->_productFactory = $productFactory;
		$this->_filesystem = $filesystem;
		$this->_imageFactory = $imageFactory;
		$this->_storeManager = $storeManager;

		parent::__construct($context);
	}

	public function getCategory(){
        $objectManager = $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $categoryFactory = $objectManager->create('Magento\Catalog\Model\ResourceModel\Category\CollectionFactory');
        $attrCategories = $categoryFactory->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('is_pro_layout', '1');
        return $attrCategories;
    }

	public function getFlipImage($productId, $width = 400, $height = 400){
		$product = $this->_productFactory->create()->load($productId);
		$imageFlipUrl = '';
		$images = $product->getMediaGalleryImages();

		foreach ($images as $key => $image) {
			if($image->getFile() != $product->getImage()) {
				$imageResizedUrl = $this->resize($image->getFile(), $width, $height);
				$imageFlipUrl = $imageResizedUrl;
				break;
			}
		}

		return $imageFlipUrl;
	}

	public function resize($image, $width = null, $height = null)
	{
		$absolutePath = $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath('catalog/product').$image;

		if (!file_exists($absolutePath)) return false;

		$imageResized = $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath('flip_image/'.$width).$image;

		if (!file_exists($imageResized)) {
			$imageResize = $this->_imageFactory->create();
			$imageResize->open($absolutePath);
			$imageResize->constrainOnly(true);
			$imageResize->keepTransparency(false);
			$imageResize->keepFrame(true);
			$imageResize->keepAspectRatio(true);
			$imageResize->resize($width,$height);
			$destination = $imageResized ;

			$imageResize->save($destination);
		}
		$resizedURL = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'flip_image/'.$width.$image;

		return $resizedURL;
	}
}
