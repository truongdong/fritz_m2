<?php

namespace Commercers\DeepTracking\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Commercers\DeepTracking\Model\Config\Source\Dpd\Status as DpdStatus;

class InstallSchema implements InstallSchemaInterface {

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (!$installer->tableExists('commercers_deeptracking_dpd_log')) {
            $data[] = ['status' => DpdStatus::DPD_ACCEPTED, 'label' => 'COMPLETE - Parcel handed to DPD'];
            $data[] = ['status' => DpdStatus::DPD_AT_SENDING_DEPOT, 'label' => 'COMPLETE - Arrived at DPD'];
            $data[] = ['status' => DpdStatus::DPD_ON_THE_ROAD, 'label' => 'COMPLETE - On the way to Destination'];
            $data[] = ['status' => DpdStatus::DPD_AT_DELIVERY_DEPOT, 'label' => 'COMPLETE - Arrived at Delivery Depot'];
            $data[] = ['status' => DpdStatus::DPD_DELIVERED, 'label' => 'COMPLETE - Delivered'];

            $setup->getConnection()->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], $data);

            $setup->getConnection()->insertArray(
                $setup->getTable('sales_order_status_state'),
                ['status', 'state', 'is_default','visible_on_front'],
                [
                    [DpdStatus::DPD_ACCEPTED,'complete', '0', '1'],
                    [DpdStatus::DPD_AT_SENDING_DEPOT, 'complete', '0', '1'],
                    [DpdStatus::DPD_ON_THE_ROAD, 'complete', '0', '1'],
                    [DpdStatus::DPD_AT_DELIVERY_DEPOT, 'complete', '0', '1'],
                    [DpdStatus::DPD_DELIVERED, 'complete', '0', '1']
                ]
            );
            $table = $installer
                ->getConnection()
                ->newTable($installer->getTable('commercers_deeptracking_dpd_log') )
                ->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'unsigned' => true,
                    ],
                    'ID'
                )
                ->addColumn(
                    'order_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [ 'nullable' => false],
                    'Order increment ID'
                )
                ->addColumn(
                    'increment_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [ 'nullable' => false],
                    'Order increment ID'
                )
                ->addColumn(
                    'tracking_number',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [],
                    'Shipment Tracking number'
                )
                ->addColumn(
                    'status',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [],
                    'Current Status'
                )
                ->addColumn(
                    'created_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false,
                        'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                    ],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false,
                        'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE
                    ],
                    'Updated At'
                )
            ;
            $installer->getConnection()->createTable($table);

            $installer->getConnection()->addIndex(
                $installer->getTable('commercers_deeptracking_dpd_log'),
                $setup->getIdxName(
                    $installer->getTable('commercers_deeptracking_dpd_log'),
                    ['order_id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['order_id'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
            );
        }

        $installer->endSetup();
    }
}
