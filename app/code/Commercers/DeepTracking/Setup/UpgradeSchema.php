<?php

namespace Commercers\DeepTracking\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Commercers\DeepTracking\Model\Config\Source\Dhl\Status as DhlStatus;
use Commercers\DeepTracking\Model\Config\Source\Hellmann\Status as HellmannStatus;
use Commercers\DeepTracking\Model\Config\Source\Ups\Status as UpsStatus;

class UpgradeSchema implements UpgradeSchemaInterface
{
	public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
		$installer = $setup;

		$installer->startSetup();

        // Version 1.0.2 - DHL Tracking
        if(version_compare($context->getVersion(), '1.0.2', '<')) {
            if (!$installer->tableExists('commercers_deeptracking_dhl_log')) {
                $data[] = ['status' => DhlStatus::DHL_DELIVERED, 'label' => 'COMPLETE - Delivered'];
                $data[] = ['status' => DhlStatus::DHL_IN_DELIVERY, 'label' => 'COMPLETE - In Delivery'];
                $data[] = ['status' => DhlStatus::DHL_DESTINATION_PARCEL_CENTER, 'label' => 'COMPLETE - Destination parcel center'];
                $data[] = ['status' => DhlStatus::DHL_COLLECTION_SUCCESSFUL, 'label' => 'COMPLETE - Collection successful'];
                $data[] = ['status' => DhlStatus::DHL_START_PARCEL_CENTER, 'label' => 'COMPLETE - Transport to the start parcel center'];
                $data[] = ['status' => DhlStatus::DHL_DELIVERY_TO_PACKSTATION, 'label' => 'COMPLETE - Delivery to PACKSTATION'];

                $setup->getConnection()->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], $data);

                $setup->getConnection()->insertArray(
                    $setup->getTable('sales_order_status_state'),
                    ['status', 'state', 'is_default','visible_on_front'],
                    [
                        [DhlStatus::DHL_DELIVERED,'complete', '0', '1'],
                        [DhlStatus::DHL_IN_DELIVERY, 'complete', '0', '1'],
                        [DhlStatus::DHL_DESTINATION_PARCEL_CENTER, 'complete', '0', '1'],
                        [DhlStatus::DHL_COLLECTION_SUCCESSFUL, 'complete', '0', '1'],
                        [DhlStatus::DHL_START_PARCEL_CENTER, 'complete', '0', '1'],
                        [DhlStatus::DHL_DELIVERY_TO_PACKSTATION, 'complete', '0', '1']
                    ]
                );

                $table = $installer
                    ->getConnection()
                    ->newTable($installer->getTable('commercers_deeptracking_dhl_log') )
                    ->addColumn(
                        'id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'nullable' => false,
                            'primary'  => true,
                            'unsigned' => true,
                        ],
                        'ID'
                    )
                    ->addColumn(
                        'order_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [ 'nullable' => false],
                        'Order increment ID'
                    )
                    ->addColumn(
                        'increment_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [ 'nullable' => false],
                        'Order increment ID'
                    )
                    ->addColumn(
                        'tracking_number',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'Shipment Tracking number'
                    )
                    ->addColumn(
                        'status',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'Current Status'
                    )
                    ->addColumn(
                        'created_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        [
                            'nullable' => false,
                            'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                        ],
                        'Created At'
                    )
                    ->addColumn(
                        'updated_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        [
                            'nullable' => false,
                            'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE
                        ],
                        'Updated At'
                    )
                ;
                $installer->getConnection()->createTable($table);

                $installer->getConnection()->addIndex(
                    $installer->getTable('commercers_deeptracking_dhl_log'),
                    $setup->getIdxName(
                        $installer->getTable('commercers_deeptracking_dhl_log'),
                        ['order_id'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                    ),
                    ['order_id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                );
            }
        }

        // Version 1.0.3 - HELLMANN Tracking
        if(version_compare($context->getVersion(), '1.0.3', '<')) {
            if (!$installer->tableExists('commercers_deeptracking_hellmann_log')) {
                $data[] = ['status' => HellmannStatus::HELLMANN_IN_TRANSFER, 'label' => 'COMPLETE - In transfer / On road to consignee'];
                $data[] = ['status' => HellmannStatus::HELLMANN_ACTION_NECESSARY, 'label' => 'COMPLETE - Action necessary'];
                $data[] = ['status' => HellmannStatus::HELLMANN_DELIVERED_WITH_DAMAGES, 'label' => 'COMPLETE - Delivered with damages / shortages'];
                $data[] = ['status' => HellmannStatus::HELLMANN_DELIVERED_SUCCESSFULLY, 'label' => 'COMPLETE - Delivered successfully'];

                $setup->getConnection()->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], $data);

                $setup->getConnection()->insertArray(
                    $setup->getTable('sales_order_status_state'),
                    ['status', 'state', 'is_default','visible_on_front'],
                    [
                        [HellmannStatus::HELLMANN_IN_TRANSFER,'complete', '0', '1'],
                        [HellmannStatus::HELLMANN_ACTION_NECESSARY, 'complete', '0', '1'],
                        [HellmannStatus::HELLMANN_DELIVERED_WITH_DAMAGES, 'complete', '0', '1'],
                        [HellmannStatus::HELLMANN_DELIVERED_SUCCESSFULLY, 'complete', '0', '1'],
                    ]
                );

                $table = $installer
                    ->getConnection()
                    ->newTable($installer->getTable('commercers_deeptracking_hellmann_log') )
                    ->addColumn(
                        'id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'nullable' => false,
                            'primary'  => true,
                            'unsigned' => true,
                        ],
                        'ID'
                    )
                    ->addColumn(
                        'order_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [ 'nullable' => false],
                        'Order increment ID'
                    )
                    ->addColumn(
                        'increment_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [ 'nullable' => false],
                        'Order increment ID'
                    )
                    ->addColumn(
                        'tracking_number',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'Shipment Tracking number'
                    )
                    ->addColumn(
                        'status',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'Current Status'
                    )
                    ->addColumn(
                        'created_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        [
                            'nullable' => false,
                            'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                        ],
                        'Created At'
                    )
                    ->addColumn(
                        'updated_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        [
                            'nullable' => false,
                            'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE
                        ],
                        'Updated At'
                    )
                ;
                $installer->getConnection()->createTable($table);

                $installer->getConnection()->addIndex(
                    $installer->getTable('commercers_deeptracking_hellmann_log'),
                    $setup->getIdxName(
                        $installer->getTable('commercers_deeptracking_hellmann_log'),
                        ['order_id'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                    ),
                    ['order_id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                );
            }
        }

        // Version 1.0.4 - UPS Tracking
        if(version_compare($context->getVersion(), '1.0.4', '<')) {
            if (!$installer->tableExists('commercers_deeptracking_ups_log')) {
                $data[] = ['status' => UpsStatus::UPS_DELIVERED, 'label' => 'COMPLETE - UPS Delivery'];
                $data[] = ['status' => UpsStatus::UPS_EXCEPTION, 'label' => 'COMPLETE - UPS Exception'];
                $data[] = ['status' => UpsStatus::UPS_IN_TRANSIT, 'label' => 'COMPLETE - UPS In transit'];
                $data[] = ['status' => UpsStatus::UPS_MANIFEST_PICKUP, 'label' => 'COMPLETE - UPS Manifest pickup'];
                $data[] = ['status' => UpsStatus::UPS_PICKUP, 'label' => 'COMPLETE - UPS Pickup'];

                $setup->getConnection()->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], $data);

                $setup->getConnection()->insertArray(
                    $setup->getTable('sales_order_status_state'),
                    ['status', 'state', 'is_default','visible_on_front'],
                    [
                        [UpsStatus::UPS_DELIVERED,'complete', '0', '1'],
                        [UpsStatus::UPS_EXCEPTION, 'complete', '0', '1'],
                        [UpsStatus::UPS_IN_TRANSIT, 'complete', '0', '1'],
                        [UpsStatus::UPS_MANIFEST_PICKUP, 'complete', '0', '1'],
                        [UpsStatus::UPS_PICKUP, 'complete', '0', '1']
                    ]
                );

                $table = $installer
                    ->getConnection()
                    ->newTable($installer->getTable('commercers_deeptracking_ups_log') )
                    ->addColumn(
                        'id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'nullable' => false,
                            'primary'  => true,
                            'unsigned' => true,
                        ],
                        'ID'
                    )
                    ->addColumn(
                        'order_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [ 'nullable' => false],
                        'Order increment ID'
                    )
                    ->addColumn(
                        'increment_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [ 'nullable' => false],
                        'Order increment ID'
                    )
                    ->addColumn(
                        'tracking_number',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'Shipment Tracking number'
                    )
                    ->addColumn(
                        'status',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'Current Status'
                    )
                    ->addColumn(
                        'created_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        [
                            'nullable' => false,
                            'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                        ],
                        'Created At'
                    )
                    ->addColumn(
                        'updated_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        [
                            'nullable' => false,
                            'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE
                        ],
                        'Updated At'
                    )
                ;
                $installer->getConnection()->createTable($table);

                $installer->getConnection()->addIndex(
                    $installer->getTable('commercers_deeptracking_ups_log'),
                    $setup->getIdxName(
                        $installer->getTable('commercers_deeptracking_ups_log'),
                        ['order_id'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                    ),
                    ['order_id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                );
            }
        }

		$installer->endSetup();
	}
}
