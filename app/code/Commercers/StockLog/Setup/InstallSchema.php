<?php
/**
 *  Commercers Vietnam
 *  Toan Dao 
 */
namespace Commercers\StockLog\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use \Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{

    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();
        $context->getVersion();

        $table = $setup->getConnection()->newTable(
            $setup->getTable('stocklog')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true],
            'ID'
        )->addColumn(
            'product_id',
            Table::TYPE_INTEGER,
            11,
            ['nullable' => true],
            'product_id'
        )->addColumn(
            'sku',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'SKU'
        )->addColumn(
            'name',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'NameProduct'
        )->addColumn(
            'user',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'User'
        )->addColumn(
            'stockbefore',
            Table::TYPE_INTEGER,
            11,
            ['nullable' => true],
            'stockbefore'
        )->addColumn(
            'stockafterwards',
            Table::TYPE_INTEGER,
            11,
            ['nullable' => true],
            'stockafterwards'
        )->addColumn(
            'difference',
            Table::TYPE_INTEGER,
            11,
            ['nullable' => true],
            'difference'
        )->addColumn(
            'date',
            Table::TYPE_DATETIME,
            255,
            ['nullable' => true],
            'Date'
         )->addColumn(
            'is_imported',
            Table::TYPE_INTEGER,
            11,
            ['nullable' => true],
            'is_imported'
         )->addColumn(
            'type',
            Table::TYPE_INTEGER,
            11,
            ['nullable' => true],
            'type'
        )->addColumn(
            'ordernumber',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'ordernumber'
        )->addColumn(
            'deliverynoteid',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'deliverynoteid'
        )->addIndex(
            $setup->getIdxName('stocklog', ['id']),
            ['id']
        )->setComment(
            'table related comments'
        );
        $setup->getConnection()->createTable($table);
        $setup->endSetup();
    }
}