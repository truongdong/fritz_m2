<?php

namespace Commercers\StockLog\Ui\Component\Listing\Column;

class Import implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 0, 'label' => __('No')],
                ['value' => 1, 'label' => __('Yes')]
               ];
    }
}