<?php

namespace Commercers\StockLog\Ui\Component\Listing\Column;

class Type implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 1, 'label' => __('Receiving')],
                ['value' => 2, 'label' => __('StockTaking')],
                ['value' => 3, 'label' => __('Orders')],
                ['value' => 4, 'label' => __('OrdersCancel')],
                ['value' => 5, 'label' => __('CreditMemo')],
                ['value' => 6, 'label' => __('ChangeManually')],
                ['value' => 7, 'label' => __('StockImport')]
               ];
    }
}