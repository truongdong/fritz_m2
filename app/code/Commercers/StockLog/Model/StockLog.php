<?php
/**
 *  Commercers Vietnam
 *  Toan Dao 
 */
namespace Commercers\StockLog\Model;

class StockLog extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Commercers\StockLog\Model\ResourceModel\StockLog'); /*định nghĩa lớp ResourceModel liên kết*/
    }
}
