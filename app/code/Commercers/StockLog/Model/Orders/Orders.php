<?php
/**
 *  Commercers Vietnam
 *  Toan Dao
 */

namespace Commercers\StockLog\Model\Orders;

use Magento\Framework\Event\ObserverInterface;

class Orders implements ObserverInterface
{
        protected $_productFactory;
        protected $_stockRegistry;
        private $eventManager;
        
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Framework\Event\Manager $eventManager
    )
    {
        $this->_productFactory = $productFactory;
        $this->eventManager = $eventManager;
        $this->_stockRegistry = $stockRegistry;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $order = $observer->getEvent()->getOrder();
        if($order) {
            foreach ($order->getAllItems() as $item) {
                $sku = $item->getSku();
                $idProduct = $item->getProductId();
                $qtyOrdered = $item->getQtyOrdered();
            }
            $product = $this->_productFactory->create()->load($idProduct);
            $stockItem = $this->_stockRegistry->getStockItemBySku($sku);
            $stockQty = $stockItem->getQty() + $qtyOrdered;
            $orderQty = $stockItem->getQty();
            $diff = -$qtyOrdered;
            //Send data Events
            $eventData = array(
                'product' => $product,
                'oldcount' => $stockQty,
                'newcount' => $orderQty,
                'sku' => $sku,
                'diff' => $diff,
                'ordernumber' => $order->getIncrementId(),
                'delivernoteid' => '',
                'type' => 3,
                'is_import' => 0
            );
            $this->eventManager->dispatch('commercers_stocklog_updated', $eventData);
        }
        
    }

}
