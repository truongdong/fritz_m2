<?php
/**
 *  Commercers Vietnam
 *  Toan Dao
 */

namespace Commercers\StockLog\Model\Orders;

use Magento\Framework\Event\ObserverInterface;

class CreditMemo implements ObserverInterface
{
        protected $_productFactory;
        protected $_stockRegistry;
        private $eventManager;
        
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Framework\Event\Manager $eventManager
    )
    {
        $this->_productFactory = $productFactory;
        $this->eventManager = $eventManager;
        $this->_stockRegistry = $stockRegistry;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $sku = null;
        $idProduct = null;
        foreach ($creditmemo->getAllItems() as $item) {
            $sku = $item->getSku();
            $idProduct = $item->getProductId();
            $qtyRefund = $item->getQty();
        }
        if($idProduct){
            $product = $this->_productFactory->create()->load($idProduct);
        }else {
            $product = null;
        }

        $stockItem = $this->_stockRegistry->getStockItemBySku($sku);
        $stockQty = $stockItem->getQty();
        $orderQty = $stockQty + $qtyRefund;
        $diff = $qtyRefund;
        //Send data Events
        $eventData = array(
            'product' => $product,
            'oldcount' => $stockQty,
            'newcount' => $orderQty,
            'sku' => $sku,
            'diff' => $diff,
            'ordernumber' => '',
            'delivernoteid' => '',
            'type' => 5,
            'is_import' => 0
        );
        $this->eventManager->dispatch('commercers_stocklog_updated', $eventData);
    }

}
