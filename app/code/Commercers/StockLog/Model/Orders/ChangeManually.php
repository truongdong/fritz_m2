<?php

/**
 *  Commercers Vietnam
 *  Toan Dao
 */

namespace Commercers\StockLog\Model\Orders;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
class ChangeManually implements ObserverInterface {

    protected $_productFactory;
    private $eventManager;

    public function __construct(
    \Magento\Catalog\Model\ProductFactory $productFactory,
    \Magento\Framework\Event\Manager $eventManager,
            \Magento\Framework\Registry $registry
    ) {
        $this->_productFactory = $productFactory;
        $this->registry     = $registry;
        $this->eventManager = $eventManager;
    }

    public function execute(Observer $observer) {
        if($this->registry->registry('stop_log_event') == 1){
            return;
        }
            $eventItem = $observer->getEvent()->getItem();

        if ((int) $eventItem->getData('qty') != (int) $eventItem->getOrigData('qty')){
            $idProduct = $eventItem->getProductId(); 
            $stockQty = $eventItem->getOrigData('qty');
            $qtyChange = $eventItem->getQty();
            if ($stockQty > $qtyChange) {
                $diff = -($stockQty - $qtyChange);
            } else {
                $diff = $qtyChange - $stockQty;
            }
            $product = $this->_productFactory->create()->load($idProduct);
            $sku = $product->getSku();
            //Send data Events
            $eventData = array(
                'product' => $product,
                'oldcount' => $stockQty,
                'newcount' => $qtyChange,
                'sku' => $sku,
                'diff' => $diff,
                'ordernumber' => '',
                'delivernoteid' => '',
                'type' => 6,
                'is_import' => 0
            );
            $this->eventManager->dispatch('commercers_stocklog_updated', $eventData);
        
        }
         
    }

}