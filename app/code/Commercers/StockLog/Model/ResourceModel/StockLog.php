<?php
namespace Commercers\StockLog\Model\ResourceModel;

class StockLog extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function __construct(
    \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
    parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('stocklog', 'id'); /* tên bảng , Id của bảng*/
    }

}