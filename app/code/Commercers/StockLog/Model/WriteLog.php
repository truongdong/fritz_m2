<?php
/**
 *  Commercers Vietnam
 *  Toan Dao
 */

namespace Commercers\StockLog\Model;

use Magento\Framework\Event\ObserverInterface;

class WriteLog implements ObserverInterface
{
    protected $date;
    protected $StockLogFactory;

    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Commercers\StockLog\Model\StockLogFactory $StockLogFactory
    )
    {
        $this->date = $date;
        $this->StockLogFactory = $StockLogFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        //echo 1; exit;
        $product = $observer->getProduct();
        $id = $product->getId();
        $nameProduct = $product->getName();
        $stockBefore = $observer->getOldcount();
        $stockAfterwards = $observer->getNewcount();
        $sku = $observer->getSku();
        $user = $observer->getUser();
        //get Time Now
        $date = $this->date->gmtDate();
        $isImport = $observer->getIs_import();
        $type = $observer->getType();
        $orderNumber = $observer->getOrdernumber();
        $deliveryNoteId = $observer->getDelivernoteid();
        //Get Database Log
        $StockLog = $this->StockLogFactory->create();
        //get diff
        $diff = $observer->getDiff();
        $StockLog->addData([
            "product_id" => $id,
            "sku" => $sku,
            "name" => $nameProduct,
            "user" => $user,
            "stockbefore" => $stockBefore,
            "stockafterwards" => $stockAfterwards,
            "difference" => $diff,
            "date" => $date,
            "is_imported" => $isImport,
            "type" => $type,
            "ordernumber" => $orderNumber,
            "deliverynoteid" => $deliveryNoteId,
            "posex"=> $observer->getPosex() ? $observer->getPosex() : ''
        ])->save();
    }
}