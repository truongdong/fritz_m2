<?php

namespace Commercers\PriceTax\Block\Price;

use Magento\Customer\Model\ResourceModel\GroupRepository;

class Tax extends \Magento\Framework\View\Element\Template
{
    protected $_storeManager;
    protected $_registry;
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_product;

    protected $_taxCalculationInterface;
    public function __construct(
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculationInterface,
        \Magento\Catalog\Model\Product $product,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_taxCalculationInterface = $taxCalculationInterface;
        $this->_product = $product;
        $this->_registry = $registry;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

    public function getDeliveryTime()
    {
        $dataProduct = $this->getCurrentProduct();
        return $dataProduct->getDeliveryTime();
    }

    /**
     * Retrieve current product
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        return $this->_product;
    }

    public function getTaxPercent()
    {
        $dataProduct = $this->getCurrentProduct();
        $percent = 0;
        $productTaxClassId = $dataProduct->getTaxClassId();
        if ($productTaxClassId) {
            $percent = $this->_taxCalculationInterface->getCalculatedRate($productTaxClassId,null,null);
        }
        return $percent;
    }

}
