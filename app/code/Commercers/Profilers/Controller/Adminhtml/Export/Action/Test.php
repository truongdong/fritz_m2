<?php

namespace Commercers\Profilers\Controller\Adminhtml\Export\Action;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Commercers\Profilers\Service\DataSource\Factory as DataSourceFactory;

class Test extends \Magento\Backend\App\Action {

    protected $_profilerFactory;
    
    public function __construct(
            Action\Context $context,
             \Commercers\Profilers\Model\ProfilersFactory $profilerFactory,
            \Magento\Framework\View\Result\PageFactory $resultPageFactory,
            DataSourceFactory $dataSourceFactory
    ) {
        $this->_profilerFactory = $profilerFactory;
        $this->_resultPageFactory = $resultPageFactory;
        $this->dataSourceFactory = $dataSourceFactory;
        parent::__construct($context);
    }

    public function execute() {
        $dataInTextarea = $this->getRequest()->getParam('dataInTextarea');

        $profilerId = $this->getRequest()->getParam('id');
        
        $id = $this->getRequest()->getParam('test_id');

        $profiler = $this->_profilerFactory->create()->load($profilerId);

        $dataSource = $this->dataSourceFactory->get($profiler->getDataSource());
        //print_r($dataSource);exit;
        $process = \Magento\Framework\App\ObjectManager::getInstance()->get("\Commercers\Profilers\Service\Profiler\Process\Export");
        //echo get_class($dataSource);exit;
        $data = $dataSource->getItemById($id, $profiler, true);
        
        $resultPage = $this->_resultPageFactory->create();
        
        $xml = new \SimpleXMLElement($dataInTextarea);
        
        $files = $xml->xpath("/files/file");
        if($files && count($files)){
            $xsltTemplate = current($files[0]->xpath('*'))->asXML();
        }else{
            $xsltTemplate = $dataInTextarea;
        }
        
        if ($data == false) {
            
            $output = false;
            
        } else {
            
            //print_r($data);exit;
            $output = $process->getOutput($profiler, $data);
            $output = current($output['files']);
            
        }        
        if($profiler->getFormat() == 'xml'){
            $doc = new \DOMDocument;
            $doc->loadXML($output);
            $doc->preserveWhiteSpace = false;
            $doc->formatOutput = true;
            $output = $doc->saveXML();
        }
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_RAW);

        return $resultJson->setContents($output);
    }

}
