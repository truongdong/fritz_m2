<?php

namespace Commercers\Profilers\Controller\Adminhtml\Index;

use Magento\Framework\Controller\ResultFactory;

class EditAction extends \Magento\Backend\App\Action
{
	
	

	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
		\Magento\Framework\Session\SessionManagerInterface $coreSession,
                \Magento\Framework\Registry $registry,
                \Commercers\Profilers\Model\ProfilersFactory $profilerFactory
	){
		$this->_pageFactory = $pageFactory;
		$this->_coreSession = $coreSession;
                $this->_registry = $registry;
                $this->_profilerFactory = $profilerFactory;
		return parent::__construct($context);
	}

    public function execute()
    {
    	$id = $this->getRequest()->getParam('id');
    	
        $profiler = $this->_profilerFactory->create();
        $profiler = $profiler->load($id);
        
        $this->_registry->register('current_profiler', $profiler);
        
        $this->_getSession()->setProfilerId($id);
        
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
