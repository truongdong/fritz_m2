<?php

namespace Commercers\Profilers\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Framework\App\Cache\Manager as CacheManager;
class DeleteHistory  extends \Magento\Backend\App\Action {

    private $_profilersFactory;

    public function __construct(
    \Commercers\Profilers\Model\ProfilersFactory $profilersFactory,
            \Magento\Framework\App\Config\ValueFactory $configValueFactory,
            \Commercers\Profilers\Model\RuleFactory $ruleFactory,
            CacheManager $cacheManager,
            \Commercers\Profilers\Model\ProfilerSavingPool $savingPool,
             \Magento\Framework\Message\ManagerInterface $messageManager,
            Action\Context $context
    ) {
        $this->_profilersFactory = $profilersFactory;
        $this->_configValueFactory = $configValueFactory;
        $this->ruleFactory = $ruleFactory;
        $this->_cacheManager = $cacheManager;
        $this->savingPool = $savingPool;
        $this->messageManager = $messageManager;
        parent::__construct($context);
    }

    public function execute() {

        if ($id = $this->getRequest()->getParam('id', false)) {
            $profiler = $this->_profilersFactory->create();
            $profiler = $profiler->load($id);
            
            try {
              $process = \Magento\Framework\App\ObjectManager::getInstance()->get("\Commercers\Profilers\Service\Profiler\Process\DeleteHistory");
              $process->execute($profiler);
                //echo "sucesss";
                $this->messageManager->addSuccess("A delete history is executed successfully");
            } catch (Exception $ex) {
                $this->messageManager->addError($ex->getMessage());
            }

            $this->_redirect('*/*/editaction', ['id' => $profiler->getId(), '_current' => true]);
            return;
        }
        $this->_redirect('*/*/listing');
    }

}
