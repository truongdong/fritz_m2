<?php

namespace Commercers\Profilers\Block\Adminhtml\Profilers\Button;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class DeleteHistory extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     * @codeCoverageIgnore
     */
    public function getButtonData()
    {
        $data = [
                'label' => __('Delete 0 Results History'),
                'class' => 'delete_history',
                'on_click' => sprintf("location.href = '%s';", $this->getUrl('profilers/index/deletehistory', array('_current' => true ))),
                'sort_order' => 100,
            ];
        return $data;
    }
}
