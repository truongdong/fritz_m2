<?php

namespace Commercers\Profilers\Block\Adminhtml\Form;

class Actions extends \Magento\Backend\Block\Template {
    protected $_template = 'Commercers_Profilers::form/action_process.phtml';
    protected $_profilerFactory;
    protected $_emailTemplateCollection;
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Commercers\Profilers\Model\ProfilersFactory $profilerFactory,
        \Magento\Email\Model\ResourceModel\Template\CollectionFactory $emailTemplateCollection
    ) {
        $this->_profilerFactory = $profilerFactory;
        $this->_emailTemplateCollection = $emailTemplateCollection;
        parent::__construct($context);
    }
    protected function getProfiler(){
        $profilerId = $this->_request->getParam('id');

        if(!$profilerId)
            return false;

        $profiler = $this->_profilerFactory->create()->load($profilerId);
        return $profiler;
    }
    public function getActionProcessValues(){
        $profiler = $this->getProfiler();
        $actionProcessEncode = $profiler->getActionProcessEncode();
        $actionProcess = json_decode($actionProcessEncode,true);
        return $actionProcess;
    }
    public function getEmailTemplate(){
        return $this->_emailTemplateCollection->create()->toOptionArray();
    }
}