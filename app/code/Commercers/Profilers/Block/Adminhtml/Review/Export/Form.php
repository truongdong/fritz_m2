<?php 

namespace Commercers\Profilers\Block\Adminhtml\Review\Export;

class Form extends  \Magento\Backend\Block\Template {
    
    
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    public function getProfilerJsonFormConfig(){

        $configs = array(

          'action_get_product_attribute' => $this->getUrl('*/export/action_get_attributes', array('_current' => true)),
           'show_fields_available' => $this->getUrl('*/export/showfieldsavailable',  array('_current' => true)),
            'action_get_profiler_data' => $this->getUrl('*/export/action_get_dataprofiler',  array('_current' => true))
        );


        return json_encode($configs);

    }
}