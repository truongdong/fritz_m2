<?php
namespace Commercers\Profilers\Model\ResourceModel;

class ObjectLog extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('commercers_profilers_object_log', 'object_log_id'); 
    }

}