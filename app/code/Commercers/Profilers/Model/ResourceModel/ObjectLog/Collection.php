<?php
namespace Commercers\Profilers\Model\ResourceModel\ObjectLog;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    public function _construct(){
    $this->_init("Commercers\Profilers\Model\ObjectLog","Commercers\Profilers\Model\ResourceModel\ObjectLog");
       
    }
}