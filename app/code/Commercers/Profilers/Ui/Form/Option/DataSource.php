<?php

namespace Commercers\Profilers\Ui\Form\Option;

class DataSource implements \Magento\Framework\Data\OptionSourceInterface
{
    
    public function __construct(array $dataSources = array() ) {
        $this->dataSources = $dataSources;
    }


    public function toOptionArray()
    {
       
        $options = array(); 
        foreach($this->dataSources as $key => $value){
            $options[] = [
                'value' => $key,
                'label' => $value
            ];
        }
        return $options;
    }
    
}