<?php

namespace Commercers\Profilers\Ui\Form\Option;

class FormatType implements \Magento\Framework\Data\OptionSourceInterface
{
    public function toOptionArray()
    {
        $arr = [
            'csv' => 'CSV',
            'xml' => 'XML',
            'edi' => 'EDI',
            'json' => 'JSON',
            'pdf' => 'PDF'
        ];
        foreach($arr as $key => $value){
            $options[] = [
                'value' => $key,
                'label' => $value
            ];
        }
        return $options;
    }
    
}