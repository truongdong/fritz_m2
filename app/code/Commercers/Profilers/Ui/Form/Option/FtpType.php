<?php

namespace Commercers\Profilers\Ui\Form\Option;

class FtpType implements \Magento\Framework\Data\OptionSourceInterface
{
    public function toOptionArray()
    {
        $arr = [
            'ftp' => 'fpt',
            'sftp' => 'sftp'
        ];
        foreach($arr as $key => $value){
            $options[] = [
                'value' => $key,
                'label' => $value
            ];
        }
        return $options;
    }
    
}