<?php

namespace Commercers\Profilers\Ui\Component\Profilers\Form;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\ComponentVisibilityInterface;
use Magento\Ui\Component\Form\Fieldset;


class ActionProcessEncode extends Fieldset implements ComponentVisibilityInterface
{
    /**
     * CustomFieldset constructor.
     * @param ContextInterface $context
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        \Magento\Framework\Registry $registry,
        array $components = [],
        array $data = []
    ) {
        $this->context = $context;
        $this->registry = $registry;
        parent::__construct($context, $components, $data);
    }

    /**
     * @return bool
     */
    public function isComponentVisible(): bool
    {

        $profiler = $this->registry->registry('current_profiler');
        //echo '<pre>'; print_r($profiler->getData());exit;
        $visible = false;
        if($profiler && $profiler->getType() == 0 && in_array($profiler->getDataSource(), array('order', 'order_item', 'order_pdf','invoice_pdf','old_order','product')) ){

            $visible = true;
        }

        return (bool)$visible;
    }
}