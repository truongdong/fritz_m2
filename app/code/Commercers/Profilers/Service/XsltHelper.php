<?php

namespace Commercers\Profilers\Service;

class XsltHelper {
    
    
    public static function convertDate($value, $format = 'Ymj'){
       //print_r($value);exit; 
        
       $date = \DateTime::createFromFormat($format, $value[0]->nodeValue);
       return $date->format('Y-m-d');
    }
    
    public static function convertDateToString($value, $format = 'Ymd'){
       
        $date = strtotime($value[0]->nodeValue);
        
        return date($format);
    }
    public static function getCurrentDate(){
        date_default_timezone_set('Europe/Berlin');
        return date("Ymd");
    }
    public static function getCurrentTime(){
        date_default_timezone_set('Europe/Berlin');
        return date("His");
    }
    public static function numberFormat($value,$decimals,$dec_point,$thousands_sep){
        
        return number_format((float)$value,$decimals,$dec_point,$thousands_sep);
    }
}