<?php

namespace Commercers\Profilers\Service\Data\Parser;

class Xml {

    protected function isAssoc(array $arr) {
        if (array() === $arr)
            return false;
        //return array_keys($arr) !== range(0, count($arr) - 1);
        //$keyFirst = \array_key_first($arr);
        if(!is_array($arr)) return false;
        reset($arr);
        $keyFirst = key($arr);

        if(is_numeric($keyFirst)){
            return false;
        }
        return true;
    }

    public function arrayToXml($data) {

        if(empty($data)) return false;
        $xml = null;
        $this->parseXml($data, $xml);
        //echo "<pre>"; print_r($xml->asXML());exit;
        return $xml->asXML();
    }

    function xmlsafe($s, $intoQuotes = 0) {
        if ($intoQuotes)
            return str_replace(array('&', '>', '<', '"'), array('&amp;', '&gt;', '&lt;', '&quot;'), $s);
        // SAME AS htmlspecialchars($s)
        else
            return str_replace(array('&', '>', '<'), array('&amp;', '&gt;', '&lt;'), $s);
        // SAME AS htmlspecialchars($s,ENT_NOQUOTES)
    }



    protected function parseXml($data, &$xml) {

        
        foreach ($data as $key => $value) { 
            
            if (!is_array($value)) {
                if (!is_numeric($key)) {
                    if (is_string($value)) {
                        $value = $this->xmlsafe($value);
                        if($xml == null){
                            if(empty($rootTagName)) $rootTagName = 'data';
                                $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?>' . "<" . $key . "/>");
                            //$child = $xml;
                        }else{
                            $child = $xml->addChild($key);
                            $node = dom_import_simplexml($child);
                            $node->appendChild($node->ownerDocument->createCDATASection($value));
                        }
                        // $node = dom_import_simplexml($child);
                        // $node->appendChild($node->ownerDocument->createCDATASection($value));
                        
                    } else {
                         if (!is_object($value)) {
                            
                            $xml->addChild($key, $value);
                        }
                    }
                }
            } else {

                
                if ($this->isAssoc($value)) {
                    
                    
                    if (!is_numeric($key)) {
                            $parentAssociatedTag = $this->createXmlNode($xml, $key);
                    }else{
                        $parentAssociatedTag = $xml;
                    }
                    
                    $this->parseXml($value, $parentAssociatedTag);
                } else {

                    foreach ($value as $valueItem) {

                        if (!is_numeric($key)) {
                            $parentAssociatedTag = $this->createXmlNode($xml, $key);
                        }
                        
                        if(!is_scalar($valueItem)){
                            $this->parseXml($valueItem, $parentAssociatedTag);
                        }else{
                            

                            $xml->addChild('el_'.$key, $valueItem);
                        }
                        
                        

                    }
                }
            }
        }
    }

    protected function createXmlNode(&$xml, $name) {
        if ($xml === null) {


            $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?>' . "<" . $name . "/>");
            //echo $name; exit;
            return $xml;
        } else {

            return $xml->addChild($name);
        }
        //return $xml;
    }




    public function xmlToArray($xmlData){

        $xml = new \SimpleXMLElement($xmlData);

        $data = array('items' => array('item' => array()));

        $items = $xml->xpath("/items/item");
        foreach($items as $item){
            $itemData = (array)$item;
            foreach($itemData as $key => &$value){
                $value = (string)$value;
            }
            $data['items']['item'][] = $itemData;
        }
        return $data;
    }

}
