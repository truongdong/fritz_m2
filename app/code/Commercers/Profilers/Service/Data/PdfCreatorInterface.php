<?php

namespace Commercers\Profilers\Service\Data;

interface  PdfCreatorInterface {
    
     public function create($profiler, $data);
}
