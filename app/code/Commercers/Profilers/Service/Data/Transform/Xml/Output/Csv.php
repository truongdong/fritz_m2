<?php

namespace Commercers\Profilers\Service\Data\Transform\Xml\Output;

class Csv {
    
    
    public function __construct(
       
        \Commercers\Profilers\Service\Data\Parser\Xml $xml    
    ) {
        
        $this->xmlService = $xml;
    }
    
    public function execute($fileName, $profiler){
        
        $data = $this->toArray($fileName, $profiler);
       
        return $this->xmlService->arrayToXml($data);
    }
    
    public function toArray($fileName, $profiler, $numberOfItemsToShow = false){
        
        $h = fopen($fileName, "r");
        $delimeter = $profiler->getData('delimiter');
        //echo '<pre>';print_r($profiler->getData());exit;
        
        $firstRow = true;
        $header = array();
        $items = array('items' => array('item' => []));
        while (($row = fgetcsv($h, false, $delimeter)) !== FALSE) 
        {
          
            if($firstRow == true){
                foreach($row as $postion => $cell){
                    $header[$this->underscore($cell)] = $postion;  
                }
                $firstRow = false;
            }else{
                $item = array();
                //print_r($header);exit;
                foreach($header as $headerCell  => $postion){
                    $item[$headerCell] = $row[$postion];
                }
                $items['items']['item'][] = $item;
            }
        }
        //echo '<pre>';print_r($items);exit;
        fclose($h);
        return $items;
        
    }
    
    public function underscore($str, array $noStrip = array())
    {
        // non-alpha and non-numeric characters become spaces
        $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
        $str = trim($str);
        //$str = str_replace(" ", "_", $str);
        //$str = str_replace(" ", "_", $str);
        $str = preg_replace('/\s+/', '_', $str);
        $str = strtolower($str);

        return $str;
    }
    
    
}