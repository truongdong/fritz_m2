<?php

namespace Commercers\Profilers\Service\Data\Transform\Xml\Output;

class Edi {

    public function __construct(
       
        \Commercers\Profilers\Service\Data\Parser\Xml $xml    
    ) {
        
        $this->xmlService = $xml;
    }
    
    public function execute($fileName, $profiler, $numberOfItemsToShow = false) {
        
        $data = $this->toArray($fileName);
        //print_r($data);
        //var_dump($this->xmlService->arrayToXml($data));exit;
        return $this->xmlService->arrayToXml($data);
    }
    
    public function toArray($fileName, $profiler, $numberOfItemsToShow = false){
        
        $message = \Metroplex\Edifact\Message::fromFile($fileName);
        //echo $fileName; exit;
        $allowedSegmentCodes = array("LIN", 'PRI', 'QTY');
        $itemSegmentCode = 'LIN';
        $segmentIndex = 0;
        
        foreach ($message->getAllSegments() as $segment) {

            if (!in_array($segment->getSegmentCode(), $allowedSegmentCodes)) {
                continue;
            }
            $segmentData = array();
           
            if ($segment->getSegmentCode() == $itemSegmentCode) {
                $segmentIndex = $segment->getElement(0);
            }
            
            if($numberOfItemsToShow !== false){
                if($segmentIndex > $numberOfItemsToShow){
                    break;
                }
            }
            
            $firstValue = '';
            foreach ($segment->getAllElements() as $key => $element) {

                if (is_array($element)) {
                    foreach ($element as $subKey => $subElement) {
                        if (is_integer($subKey)) {
                            $element['val_' . $subKey] = $subElement;
                            unset($element[$subKey]);
                        }

                        if (!$firstValue)
                            $firstValue = $subElement;
                    }
                }else {
                    if (!$firstValue)
                        $firstValue = $element;
                }
                $segmentData['e_' . $key] = $element;
            }
            if ($segment->getSegmentCode() != $itemSegmentCode && !is_numeric($firstValue) ) {
                
                  $tagCode = $segment->getSegmentCode() . '_' . $firstValue;
            } else {
                
                $tagCode = $segment->getSegmentCode();
            }
            
            $data['segments']['segment'][$segmentIndex - 1][$tagCode] = $segmentData;
            //if($segmentIndex > 10)                break;
        }
        //echo '<pre>';print_r($data);exit;
        return $data;
    }

}
