<?php

namespace Commercers\Profilers\Service\Data\Transform\Xml;

class Output {

    public function __construct(
        array $processors,
        \Commercers\Profilers\Service\Data\Parser\Xml $xml    
    ) {
        $this->_processors = $processors;
        $this->_xmlHelper = $xml;
    }

    public function execute($filename, $profiler, $numberOfItemsToShow = false) {
        //echo $filename; exit;
        $type = $profiler->getFormat() ? $profiler->getFormat() : 'xml';
        
        if (file_exists($filename)) {
            if($processor = $this->getProceesor($type)){
                return $processor->execute($filename, $profiler, $numberOfItemsToShow);
            }
            return file_get_contents($filename);
        }
        return false;
    }

    public function toArray($filename, $profiler , $numberOfItemsToShow = false) {
        
        $type = $profiler->getFormat() ? $profiler->getFormat() : 'xml';
        if (file_exists($filename)) {
            //print_r($type);exit;
            if($processor = $this->getProceesor($type)){
                return $processor->toArray($filename, $profiler, $numberOfItemsToShow);
            }
            return $this->XML2Array(simplexml_load_string(file_get_contents($filename))) ;
        }
        return false;
    }

    protected function getProceesor($type) {
       
        if (isset($this->_processors[$type])) {
            $processor = \Magento\Framework\App\ObjectManager::getInstance()->get($this->_processors[$type]);
            return $processor;
        }
    }
    
    public function XML2Array(\SimpleXMLElement $parent)
    {
        $array = array();

        foreach ($parent as $name => $element) {
            ($node = & $array[$name])
                && (1 === count($node) ? $node = array($node) : 1)
                && $node = & $node[];

            $node = $element->count() ? $this->XML2Array($element) : trim($element);
        }

        return $array;
    }


}
