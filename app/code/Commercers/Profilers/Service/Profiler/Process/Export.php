<?php

namespace Commercers\Profilers\Service\Profiler\Process;

use Commercers\Profilers\Service\Data\Parser\Xml;
use Commercers\Profilers\Service\DataSource\Factory as DataSourceFactory;
use Commercers\Profilers\Service\Log;
use Commercers\Profilers\Service\Mail\Template\TransportBuilderFactory;
use Commercers\Profilers\Service\Profiler\Process;
use Commercers\Profilers\Service\Profiler\Process\Helper\Factory;
use DateTime;
use Exception;
use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DomDocument\DomDocumentFactory;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;

class Export extends Process
{

    const SEPARATED_LIMIT = 10000;
    protected $currentFilePath = false;

    public function __construct(
        DataSourceFactory $dataSourceFactory,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        DomDocumentFactory $domFactory,
        Log $processLog,
        Xml $xmlService,
        Factory $outputHelper,
        \Commercers\Io\File $fileManagement,
        StoreManagerInterface $storeManager,
        TransportBuilderFactory $transportBuilder,
        ScopeConfigInterface $scopeConfig,
        File $ioFile,
        DirectoryList $dir,
        StateInterface $inlineTranslation
    )
    {
        $this->dataSourceFactory = $dataSourceFactory;
        $this->_domFactory = $domFactory;
        $this->fileManagement = $fileManagement;
        $this->_processLog = $processLog;
        $this->xmlService = $xmlService;
        $this->outputHelper = $outputHelper;
        $this->inlineTranslation = $inlineTranslation;
        $this->_storeManager = $storeManager;
        $this->_transportBuilder = $transportBuilder;
        $this->_scopeConfig = $scopeConfig;
        $this->ioFile = $ioFile;
        $this->dir = $dir;
    }


    public function getOutput($profiler, $data)
    {
        $outHelper = $this->outputHelper->create($profiler);
        return $outHelper->getOutPut($profiler, $data);
    }

    public function execute($profiler)
    {

        $dataProcessLog = $this->_processLog->execute($profiler->getId());
        $dataSource = $this->dataSourceFactory->get($profiler->getDataSource());
        $collection = $dataSource->_getCollection($profiler);

        $count = $dataSource->getCount($profiler);
        // var_dump($count);exit;
        if ($count == false) return;
        $pages = $count / static::SEPARATED_LIMIT + 1;
        for ($page = 1; $page <= $pages; $page++) {
            $limits = array(
                'limit' => static::SEPARATED_LIMIT,
                'offset' => static::SEPARATED_LIMIT * ($page - 1)
            );
            $data = $dataSource->getData($profiler, $limits);
            //echo "<pre>";print_r($data);exit;
            try {
                $profilerDataSource = $profiler->getDataSource();

                if ($profilerDataSource == 'product'
                    || $profilerDataSource == 'order'
                    || $profilerDataSource == 'order_pdf'
                    and (int)$profiler->getData('export_for_each_item') == 1) {
                     //echo 1;exit;

                    $dataExport = isset($data['items']['item']) ? $data['items']['item'] : $data['orders']['order'];

                    foreach ($dataExport as $value) {
                        if (isset($value['shipping']['street'])) {
                            $no = explode(" ", $value['shipping']['street'])[0];
                            $value['shipping']['no'] = $no;
                        }

                        if ($profilerDataSource == 'product') {
                            $valueExport['items']['item'] = array();
                            $valueExport['items']['item'][] = $value;

                            $itemName = $value['sku'];
                        } else if ($profilerDataSource == 'order') {
                            $valueExport['orders']['order'] = array();
                            $valueExport['orders']['order'][] = $value;
                            $itemName = $value['increment_id'];
                        }else if ($profilerDataSource == 'to_soller_sales_orders_export') {
                            $valueExport['orders']['order'] = array();
                            $valueExport['orders']['order'][] = $value;
                            $itemName = $value['increment_id'];
                        }else if ($profilerDataSource == 'order_pdf') {
                            $valueExport['orders']['order'] = array();
                            $valueExport['orders']['order'][] = $value;
                            $itemName = '';
                        }

                        $fileName = $this->_createExportForEachItem($profiler, $valueExport, $dataSource, $itemName);
                        if ($fileName) {
                            $fileNames[] = $fileName;
                        }
                    }
                } else {
                    $fileName = $this->_createExportFiles($profiler, $data, $dataSource);
                    if ($fileName) {
                        $fileNames[] = $fileName;
                    }
                }
                $dataSource->log($profiler, $dataProcessLog);
                $status = 1;
                $message = "Success!";

                if (!empty($fileNames))
                    $this->_sendEmail($profiler, $fileNames);
            } catch (Exception $e) {
                $status = 0;
                $message = $e->getMessage();
            }
        }

        $this->_processLog->executeAfter($dataProcessLog['process_id'], $status, $message);
        if ($profiler->getData('ftp_enable')) {
            if (!empty($fileNames)){
                foreach ($fileNames as $fileName) {
                    $filePath = 'var/tmp_folder' . DIRECTORY_SEPARATOR . $fileName;
                    //$this->ioFile->rm($filePath);
                    if (file_exists($filePath)) unlink($filePath);

                }
            }
        }
    }

    public function executeForOneItem($profiler, $itemId)
    {
        $dataProcessLog = $this->_processLog->execute($profiler->getId());
        $dataSource = $this->dataSourceFactory->get($profiler->getDataSource());
        try {
            $data = $dataSource->getItemById($itemId, $profiler, true);
            $this->_createExportFiles($profiler, $data, $dataSource);
            $dataSource->log($profiler, $dataProcessLog);
            $status = 1;
            $message = "Success!";
        } catch (Exception $ex) {
            $status = 0;
            $message = $ex->getMessage();
        }
        $this->_processLog->executeAfter($dataProcessLog['process_id'], $status, $message);
    }

    protected function _createExportFiles($profiler, $data, $dataSource)
    {
        //  $out = $this->getOutput($profiler, $data, $dataSource);
        $date = new DateTime();
        $out = $this->getOutput($profiler, $data);

        $actionProcessEncode = $profiler->getActionProcessEncode();
        $actionProcess = json_decode($actionProcessEncode, true);

        if ($out['type'] == 'file') {

            $exportedFiles = $out['files'];
            if ($profiler->getData('ftp_enable')) {
                foreach ($exportedFiles as $fileName => $filePath) {
                    $fileNames = explode('.', $fileName);
                    $typeFile = $fileNames[1];
                    if ($profiler->getDataSource() == 'invoice_pdf') {
                        $fileName = $fileNames[0] . '.' . $typeFile;
                    } else {
                        $fileName = $fileNames[0] . '-' . $date->format('Ymd-His') . '.' . $typeFile;
                    }

                    $this->fileManagement->transferFilesToFtp($profiler->getData(), $fileName, $filePath);
                    if (trim($profiler->getActionProcessEncode()) != '' and trim($actionProcess['email']) != '') {
                        $this->_createTmpFile($profiler->getData(), $fileName, $filePath);
                    }
                }
            }
            if ($profiler->getData('local_enable')) {
                foreach ($exportedFiles as $fileName => $filePath) {
                    $fileNames = explode('.', $fileName);
                    $typeFile = $fileNames[1];
                    if ($profiler->getDataSource() == 'invoice_pdf') {
                        $fileName = $fileNames[0] . '.' . $typeFile;
                    } else {
                        $fileName = $fileNames[0] . '-' . $date->format('Ymd-His') . '.' . $typeFile;
                    }
                    $this->fileManagement->writeFilesToLocal($profiler->getData(), $fileName, $filePath);
                }
            }
        } elseif ($out['type'] == 'content') {

            /**
             * For easy to make adjustments later, I did a duplicated code here
             */
            $exportedFiles = $out['files'];

            if ($profiler->getData('ftp_enable')) {

                foreach ($exportedFiles as $fileName => $content) {
                    $fileNames = explode('.', $fileName);
                    $typeFile = $fileNames[1];
                    if ($profiler->getDataSource() == 'invoice_pdf') {
                        $fileName = $fileNames[0] . '.' . $typeFile;
                    } else {
                        $fileName = $fileNames[0] . '-' . $date->format('Ymd-His') . '.' . $typeFile;
                    }
                    $this->fileManagement->transferFilesToFtp($profiler->getData(), $fileName, $content);
                    if (trim($profiler->getActionProcessEncode()) != '' and trim($actionProcess['email']) != '') {
                        $this->_createTmpFile($profiler->getData(), $fileName, $content);
                    }
                }
            }
            if ($profiler->getData('local_enable')) {

                foreach ($exportedFiles as $fileName => $content) {
                    $fileNames = explode('.', $fileName);
                    $typeFile = $fileNames[1];
                    if ($profiler->getDataSource() == 'invoice_pdf') {
                        $fileName = $fileNames[0] . '.' . $typeFile;
                    } else {
                        $fileName = $fileNames[0] . '-' . $date->format('Ymd-His') . '.' . $typeFile;
                    }
                    $this->fileManagement->writeFilesToLocal($profiler->getData(), $fileName, $content);
                }
            } else {
                foreach ($exportedFiles as $fileName => $content) {
                    $fileNames = explode('.', $fileName);
                    $typeFile = $fileNames[1];
                    if ($profiler->getDataSource() == 'invoice_pdf') {
                        $fileName = $fileNames[0] . '.' . $typeFile;
                    } else {
                        $fileName = $fileNames[0] . '-' . $date->format('Ymd-His') . '.' . $typeFile;
                    }
                }
            }

        }
        $fileName = $this->_checkFileEmpty($profiler, $fileName);
        return $fileName;
    }

    protected function _createExportForEachItem($profiler, $data, $dataSource, $itemName)
    {
        $out = $this->getOutput($profiler, $data);
        $actionProcessEncode = $profiler->getActionProcessEncode();
        $actionProcess = json_decode($actionProcessEncode, true);
        if ($out['type'] == 'file') {

            $exportedFiles = $out['files'];
            if ($profiler->getData('ftp_enable')) {
                foreach ($exportedFiles as $fileName => $filePath) {
                    $fileNames = explode('.', $fileName);
                    $typeFile = $fileNames[1];
                    $fileName = $fileNames[0] . '-' . $itemName . '-' . time() . '.' . $typeFile;
                    $this->fileManagement->transferFilesToFtp($profiler->getData(), $fileName, $filePath);
                    if (trim($profiler->getActionProcessEncode()) != '' and trim($actionProcess['email']) != '') {
                        $this->_createTmpFile($profiler->getData(), $fileName, $filePath);
                    }
                }
            }
            if ($profiler->getData('local_enable')) {
                foreach ($exportedFiles as $fileName => $filePath) {
                    $fileNames = explode('.', $fileName);
                    $typeFile = $fileNames[1];
                    if($itemName == ''){
                        $fileName = $fileNames[0] . '.' . $typeFile;
                    }else{
                        $fileName = $fileNames[0] . '-' . $itemName . '-' . time() . '.' . $typeFile;
                    }

                    $this->fileManagement->writeFilesToLocal($profiler->getData(), $fileName, $filePath);
                }
            }
        } elseif ($out['type'] == 'content') {

            /**
             * For easy to make adjustments later, I did a duplicated code here
             */
            $exportedFiles = $out['files'];
            $fileName = '';
            if ($profiler->getData('ftp_enable')) {

                foreach ($exportedFiles as $fileName => $content) {
                    $fileNames = explode('.', $fileName);
                    $typeFile = $fileNames[1];

                    $fileName = $fileNames[0] . '-' . $itemName . '-' . time() . '.' . $typeFile;

                    $this->fileManagement->transferFilesToFtp($profiler->getData(), $fileName, $content);
                    if (trim($profiler->getActionProcessEncode()) != '' and trim($actionProcess['email']) != '') {
                        $this->_createTmpFile($profiler->getData(), $fileName, $content);
                    }
                }
            }
            if ($profiler->getData('local_enable')) {
                foreach ($exportedFiles as $fileName => $content) {

                    $fileNames = explode('.', $fileName);
                    $typeFile = $fileNames[1];
                    $fileName = $fileNames[0] . '-' . $itemName . '-' . time() . '.' . $typeFile;

                    //echo $fileName;exit;
                    $this->fileManagement->writeFilesToLocal($profiler->getData(), $fileName, $content);
                }
            }
        }
        $fileName = $this->_checkFileEmpty($profiler, $fileName);
        //var_dump($fileName);exit;
        return $fileName;
    }

    protected function _sendEmail($profiler, $fileNames)
    {
        $from = [
            'name' => $this->_scopeConfig->getValue(
                'trans_email/ident_general/name',
                ScopeInterface::SCOPE_STORE
            ),
            'email' => $this->_scopeConfig->getValue(
                'trans_email/ident_general/email',
                ScopeInterface::SCOPE_STORE
            )
        ];

        if ((boolean)$profiler->getActionProcessEncode()) {

            $actionProcessEncode = $profiler->getActionProcessEncode();
            $actionProcess = json_decode($actionProcessEncode, true);

            if ((boolean)$actionProcess['email']) {

                $emails = preg_replace('/\s/', '', $actionProcess['email']);
                $emails = explode(',', $emails);
                foreach ($emails as $email) {

                    if ($profiler->getData('ftp_enable')) {
                        $localFolder = 'var/tmp_folder';
                    } else {
                        $localFolder = $profiler->getData('local_done_folder');
                    }
                    $transport = $this->_transportBuilder->create();
                    //print_r($fileNames);exit;
                    foreach ($fileNames as $fileName) {
                        $filePath = $this->dir->getRoot().DIRECTORY_SEPARATOR.$localFolder . DIRECTORY_SEPARATOR . $fileName;
                        $transport->addAttachment(file_get_contents($filePath), $fileName, '');
                    }
                    $defaultEmailTemplate = $this->_getDefaultEmailTemplate($profiler);

                    $emailTemplate = (isset($actionProcess['email_template']) and $actionProcess['email_template'] != '') ? $actionProcess['email_template'] : $defaultEmailTemplate;
                    // var_dump($emailTemplate);exit;
                    if (!$emailTemplate) return;
                    $transport->setTemplateIdentifier($emailTemplate)->setTemplateOptions(
                        [
                            'area' => Area::AREA_FRONTEND,
                            'store' => 1,
                        ]
                    )
                        ->setTemplateVars([])
                        ->setFrom($from)
                        ->addTo($email, 'Admin');
                    // print_r($transport->getTemplate());exit;
                    try{
                        $transport->getTransport()->sendMessage();
                    }catch (\Exception $exception){

                    }
                }
            }
        }
    }

    protected function _createTmpFile($profiler, $fileName, $src)
    {
        $tmpFolder = 'var/tmp_folder';
        $rootDir = $this->dir->getRoot();
        $localFolder = $rootDir . DIRECTORY_SEPARATOR . $tmpFolder;
        $this->ioFile->mkdir($tmpFolder, 0775);

        if (is_string($src) && is_readable($src)) {
            rename($src, $localFolder . DIRECTORY_SEPARATOR . basename($fileName));
        } else {
            file_put_contents($localFolder . DIRECTORY_SEPARATOR . basename($fileName), $src);
        }
    }

    protected function _getDefaultEmailTemplate($profiler)
    {
        switch ($profiler->getDataSource()) {
            case 'profiler_min_stock_csv_export':
                return 'profiler_min_stock_csv_export_default_email_template';
            default:
                return 'profiler_default_email_template';
        }
    }

    protected function _checkFileEmpty($profiler, $fileName)
    {
        if ($profiler->getData('ftp_enable')) {
            $localFolder = 'var/tmp_folder';
        } else {
            $localFolder = $profiler->getData('local_done_folder');
        }

        $filePath = $this->dir->getRoot().DIRECTORY_SEPARATOR.$localFolder . DIRECTORY_SEPARATOR . $fileName;
        if ($profiler->getFormat() == 'csv') {
            $data = $this->_readCsvData($profiler, $filePath);
            if ($data == false) {
                unlink($filePath);
                return false;
            }
        }
        return $fileName;
    }

    protected function _readCsvData($profiler, $fileName)
    {
        $file = fopen($fileName, "r");
        while (!feof($file)) {
            $csv[] = fgetcsv($file, 0, $profiler->getDelimiter());
        }

        $keys = array_shift($csv);
        foreach ($csv as $data) {
            if (is_array($data)) {
                $returnValue[] = array_combine($keys, $data);
            }
        }
        if (isset($returnValue))
            return $returnValue;
        return false;
    }
}
