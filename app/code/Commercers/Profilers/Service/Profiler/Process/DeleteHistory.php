<?php

namespace Commercers\Profilers\Service\Profiler\Process;


class DeleteHistory extends \Commercers\Profilers\Service\Profiler\Process
{

    public function __construct(
        \Commercers\Profilers\Model\ProfilersFactory $profilersFactory,
        \Commercers\Profilers\Model\ProfilersProcessLogFactory $processLog
    )
    {
        $this->profilersFactory = $profilersFactory;
        $this->_processLog = $processLog;
    }


    public function execute($profiler) {
      $collection = $this->_processLog->create()->getCollection();
      $collection->addFieldToFilter('id_profiler',['eq' => $profiler->getId()]);
      $collection->addFieldToFilter('status',['eq' => 0]);
      $records = array();
      foreach ($collection as $data){
        $records[] = $data->getData('process_id');
      }
      foreach ($records as $record){
        $process = $this->_processLog->create()->load($record);
        $process->delete();
      }
    }


}
