<?php

namespace Commercers\Profilers\Service\Profiler\Process\Helper;



class Factory 
{
 
    public function __construct(
             $helpers
            
            ) {
        
        $this->helpers = $helpers;
    }


    public function create($profiler){
        
        if($profiler->getFormat() == 'pdf')
            return \Magento\Framework\App\ObjectManager::getInstance()->get($this->helpers['pdf']);
        
        if($profiler->getFormat() == 'json')
            return \Magento\Framework\App\ObjectManager::getInstance()->get($this->helpers['json']);
        
        return \Magento\Framework\App\ObjectManager::getInstance()->get($this->helpers['xslt']);
        //return $this->xsltHelper;
         
    }
}