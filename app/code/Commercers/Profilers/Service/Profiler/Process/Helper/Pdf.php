<?php

namespace Commercers\Profilers\Service\Profiler\Process\Helper;

use Commercers\Profilers\Service\DataSource\Factory as DataSourceFactory;

class Pdf 
{
    
    
    public function __construct(
            
            \Commercers\Profilers\Service\Data\PdfCreatorInterface $pdfCreator

            ) {
        
        $this->pdfCreator = $pdfCreator;
        
    }


    public function getOutput($profiler, $data)
    {
       
        $exportedFiles = $this->pdfCreator->create($profiler, $data);

        $files = array(
            'type' => 'file',
            'files' => $exportedFiles
        );
        return $files;
              
    }
    
    
}