<?php

namespace Commercers\Profilers\Service\Profiler\Process\Helper;

use Commercers\Profilers\Service\DataSource\Factory as DataSourceFactory;

class Xslt
{


    public function __construct(
            \Commercers\Profilers\Service\Data\Parser\Xml $xmlService,
            \Magento\Framework\DomDocument\DomDocumentFactory $domFactory
            ) {

        $this->xmlService = $xmlService;
        $this->domFactory = $domFactory;
    }


    public function getOutput($profiler, $data)
    {

        ///change file name
      //  $fileName = $this->_getFileName($profiler);
        $date = new \DateTime();
        $xslDoc = $profiler->getData('export_output_template');
        $xml = new \SimpleXMLElement($xslDoc);
        $files = $xml->xpath("/files/file");
        $fileName = $this->_getFileName($files,$data);



        if($files && count($files)){
            $xsltTemplate = current($files[0]->xpath('*'))->asXML();

        }else{
            $xsltTemplate = $xslDoc;
        }

        $dom = new \DOMDocument();
        $xsl = new \XSLTProcessor();
        $dom->loadXML($xsltTemplate);
//        var_dump($dom);exit();
        $xsl->importStyleSheet($dom);
        $xsl->registerPHPFunctions();
        $xmlSource = $this->domFactory->create();
        if($this->xmlService->arrayToXml($data) != false){
            $xmlSource->loadXML($this->xmlService->arrayToXml($data), LIBXML_PARSEHUGE|LIBXML_NSCLEAN);

            $out = $xsl->transformToXML($xmlSource);

            $result = array(
                'type' => 'content',
                'files' => array(
                    $fileName => $out
                )
            );
            return $result;
        }

        return false;


    }
    protected function _getFileName($files,$data){

        foreach ( (array) $files[0] as $index => $node )
          $out[] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;
        $fileName = $out[0]['filename'];


        return $fileName;
    }

}
