<?php

namespace Commercers\ShipmentInventory\Plugin;

class SourceDeductionService
{
    public function aroundExecute(
        \Magento\InventorySourceDeductionApi\Model\SourceDeductionService $subject,
        \Closure $proceed,
        \Magento\InventorySourceDeductionApi\Model\SourceDeductionRequestInterface $sourceDeductionRequest
    )
    {
        try {
            $proceed($sourceDeductionRequest);
        } catch (\Exception $e) {

        }
    }
}
