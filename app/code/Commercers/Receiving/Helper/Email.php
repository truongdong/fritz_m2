<?php

namespace Commercers\Receiving\Helper;

class Email extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_ERROR_NOTIFICATION_TO = 'section_receiving/group_receiving/error_notification_to';
    protected $transportBuilder;
    protected $inlineTranslation;
    protected $escaper;
    protected $logger;
    protected $scopeConfig;
    protected $arrayMessages = array();
    public function __construct(
            \Magento\Framework\App\Helper\Context $context,
            \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
            \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
            \Magento\Framework\Escaper $escaper,
            \Commercers\Receiving\Logger\Logger $logger,
            \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
            
    ) {
        parent::__construct($context);
        $this->inlineTranslation = $inlineTranslation;
        $this->escaper = $escaper;
        $this->transportBuilder = $transportBuilder;
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
    }
    public function sendEmail($message){
        try{
            $this->inlineTranslation->suspend();           
            $email = $this->scopeConfig->getValue('receiving_import/group_receiving/error_notification_to', \Magento\Store\Model\ScopeInterface::SCOPE_STORES);
            $transport = $this->transportBuilder
                    ->setTemplateIdentifier('commercers_receiving_cron_error')
                    ->setTemplateOptions([
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store'=> \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ])
                    ->setTemplateVars([
                        'messages' => $message
                    ])
                    
                    ->addTo($email)
                    ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (Exception $ex) {
            $this->logger->info('Can\'t send an email');
        }
    }
}