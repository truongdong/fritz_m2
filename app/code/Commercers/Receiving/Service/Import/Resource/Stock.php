<?php

namespace Commercers\Receiving\Service\Import\Resource;

class Stock {

    protected $_productRepository;
    protected $logger;
    protected $_stockRegistry;
    protected $email;
    private $_productFactory;
    private $eventManager;
    protected $orderProposalCollection;
    public function __construct(
            \Magento\Catalog\Model\ProductRepository $productRepository,
            \Magento\Backend\Model\Auth\Session $authSession, 
            \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
            \Commercers\Receiving\Logger\Logger $logger,
            \Commercers\Receiving\Helper\Email $email,
            \Magento\Catalog\Model\ProductFactory $productFactory,
            \Magento\Framework\Event\Manager $eventManager,
            \Commercers\OrderProposal\Model\ResourceModel\OrderProposal\CollectionFactory $orderProposalCollection
    ) {
        $this->_productRepository = $productRepository;
        $this->authSession = $authSession;
        $this->_stockRegistry = $stockRegistry;
        $this->email = $email;
        $this->_productFactory = $productFactory;
        $this->eventManager = $eventManager;
        $this->orderProposalCollection = $orderProposalCollection;
        $this->logger = $logger;
    }

    public function execute($dataImport) {
       echo "<pre>";print_r($dataImport);exit;
        //import 
        $error = array();
        foreach ($dataImport[0] as $key => $data) {
            /**
             * import data
             */
            $key = $key + 2;
            $sku = isset($data['sku']) ? $data['sku'] : '';
            $qty = trim($data['qty']);
            $orderNumber = trim($data['number']);
            $deliOrderNumber = trim($data['delivery']);
            $diff = $qty;
            try {
                $product = $this->_productRepository->get($sku);
            } catch (\Exception $e) {
                 $message = "Line: ".$key."-Invalid product SKU: " . $sku;
                 $this->logger->info($message);               
                $error[] = $message;
                continue;
            }
            // You can set other product data with $product->setAttributeName() if you want to update more data if you set price => use getPrice() and setPrice()
            try {
                $stockItem = $this->_stockRegistry->getStockItemBySku($sku);
            } catch (\Exception $e) {
                 $message = "Line: ".$key."-Invalid stock for product SKU: " . $sku;
                 $this->logger->info($message);
                 $error[] = $message;
                continue;
            }
            /**
             * check qty other new updates. note : if qty = $stockItem->getQty() (example : 5 = 5) do not update else (example : 6 vs 5) then update
             */
            //check qty
            $orderProposalCollection = $this->orderProposalCollection->create();
            $orderProposalCollection->addFieldToFilter('order_number', array('eq' => $data['number']))
                    ->addFieldToFilter('sku', array('eq' => $data['sku']));
            if ($orderProposalCollection->getSize() == 0) {
                 $message = "Line: ".$key."-SKU doesn't belong to this order.";
                 $this->logger->info($message);
                 $error[] = $message;
                continue;
            }
            $orderProposal = $orderProposalCollection->getFirstItem();
//            print_r($orderProposal->getData());exit;
            //check count
            if ($orderProposal['orderqty'] != NULL) {
                if ($orderProposal['open_qty'] == 0 && $orderProposal['open_qty'] != NULL) {
                    $message = "Line: ".$key."-This order has received enough.";
                    $this->logger->info($message);
                    $error[] = $message;
                    continue;
                }
                $checkQty = $qty + $orderProposal['delivered_qty'];
                if ($checkQty > $orderProposal['orderqty']) {
                    $message = "Line: ".$key."-Error: The delivered quantity is greater than the Ordered quantity.";
                    $this->logger->info($message);
                    $error[] = $message;
                    continue;
                }
            }
            $stockQty = $stockItem->getQty();
            $orderQty = $stockQty + $qty;
            if ($orderQty < 0) {
                 $message = "Line: ".$key."-Not to go stock qty go below 0";
                 $this->logger->info($message);
                $error[] = $message;
            } else {
                $stockItem->setQty($orderQty);
                $stockItem->save();
                //Send data Events
                $eventData = array(
                    'product' => $product,
                    'oldcount' => $stockQty,
                    'newcount' => $orderQty,
                    'sku' => $sku,
                    'qty' => $qty,
                    'diff' => $diff,
                    'ordernumber' => $orderNumber,
                    'delivernoteid' => $deliOrderNumber,
                    'user'=> $this->authSession->getUser()->getUsername(),
                    'type' => 1,
                    'is_import' => 1,
                    'posex' => '00010'
                );
                $this->eventManager->dispatch('commercers_stocklog_updated', $eventData);
            }
        }
        if($error){
            $message = implode('----',$error);
            $this->email->sendEmail($message);
        }
    }

}
