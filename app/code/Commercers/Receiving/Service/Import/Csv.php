<?php
namespace Commercers\Receiving\Service\Import;

class Csv {

    protected $_importStock;
    protected $_ioFile;
    protected $_scopeConfig;

    public function __construct(
        \Commercers\Receiving\Service\Import\Resource\Stock $importStock,
        \Magento\Framework\Filesystem\Io\File $ioFile,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) 
    {
        $this->_importStock = $importStock;
        $this->_ioFile = $ioFile;
        $this->_scopeConfig = $scopeConfig;
    }
    
    public function execute($filePath, $fileName, $profiler, $doneFolder,$delimiter) {
        //process
        $dataCsv = $this->readCsvFile($filePath, $delimiter);
        foreach ($dataCsv[0] as $valueCsv) {
            $headerCsv = $valueCsv[0];
            $rows[] = $valueCsv[1];
        }
        $mapping = $this->getMappings($profiler['mapping']);
        foreach ($mapping as $key => $values) {
            $result[] = array($key, $values);
        }
        $params = [
            'header' => $headerCsv,
            'rows' => $rows,
            'mapping' => $result
        ];
        
        $cleanRow = null;//$this->_mappingCsv->convert($params); //mapping
//        echo "<pre>";print_r($cleanRow);exit;
        // echo "<pre>";print_r($cleanRow);exit;
        //goi ham cap nhat du lieu vao database
        $this->_importStock->execute($cleanRow);
        //sau khi xu ly xong thi di chuyen file vao folder done
        $this->moveFile($filePath,$fileName,$doneFolder);
//        echo 123;exit;
    }

    public function readCsvFile($pathFile, $delimiter) {
        /**
         * Read CSV file
         */
        $file = fopen($pathFile, 'r', '"');
        $header = fgetcsv($file, 3000, $delimiter);
        while ($row = fgetcsv($file, 3000, $delimiter)) {
            $dataCount = count($row);
            if ($dataCount < 1) {
                continue;
            }
            if($row[0] != ''){
                $dataCsv[] = array($header, $row);
            }
        }

        return [$dataCsv, $dataCount];
    }

    public function moveFile($filePath, $fileName, $doneFolder) {
        if($doneFolder != ''){
            $pathNew = $doneFolder .DIRECTORY_SEPARATOR. $fileName;
//            echo $filePath;exit;
            $this->_ioFile->mv($filePath, $pathNew);
        }
    }

    public function getMappings($mapping){
        if(!is_null($mapping)){
           $mapping = preg_split('/\\r\\n|\\r|\\n/',$mapping);
          
            $value = array();
            foreach ($mapping as $_mapping){
                $attribute = explode('=',trim($_mapping));
                $value[$attribute[0]] = $attribute[1];
            }

            return $value;
       }
       return false;
    }

}