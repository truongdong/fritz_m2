<?php
namespace Commercers\Receiving\Service;

class Export 
{
	protected $_stockLogFactory;
	protected $_domFactory;
	protected $_dir;
	protected $_profilerExport;
	protected $_file;
	protected $_processLog;

	public function __Construct(
		\Commercers\StockLog\Model\ResourceModel\StockLog\CollectionFactory $stockLogFactory,
		\Magento\Framework\DomDocument\DomDocumentFactory $domFactory,
		\Commercers\Ftp\Model\Service\Ftp $dir,
		\Commercers\Receiving\Model\Config\Source\SelectProfilersExport $profilerExport,
		\Magento\Framework\Filesystem\Io\File $file,
		\Magento\Framework\App\Filesystem\DirectoryList $directoryList,
		\Commercers\Profilers\Service\Log $processLog
	){
		$this->_stockLogFactory = $stockLogFactory;
		$this->_domFactory = $domFactory;
		$this->_dir = $dir;
		$this->_profilerExport = $profilerExport;
		$this->_file = $file;
		$this->_directoryList = $directoryList;
		$this->_processLog = $processLog;
	}

	public function execute($profiler){
		$this->getStockLog($profiler);
	}

	public function getStockLog($profiler){
		//get data receiving in stock log
		$collection = $this->_stockLogFactory->create()->addFieldToFilter('type','1');
		$stockLogData = $collection->getData();
		//get xslt
		//$profilerExport = $this->_profilerExport->getProfilersSelected();
		$dataProfiler = $profiler->getData();
		foreach($dataProfiler as $value){
			$xslDoc = $value['outputformat'];
			$idProfiler = $value['id_profiler'];
			$localFolder = $value['localfolder'];
			$localFolderFtp = $value['localfolderftp'];
			$enableLocal = $value['enablelocal'];
			$enableFtp = $value['enableftp'];
			$idPrimaryProfiler = $value['id'];
			$format = $value['format'];
		}

		//write log to start export
		$dataProcessLog = $this->_processLog->execute($idPrimaryProfiler);

		try {
			$dom = new \DOMDocument();
			$xsl = new \XSLTProcessor;
			$dom->loadXML($xslDoc);
			$xsl->importStyleSheet($dom);
			$xmlSource = $this->_domFactory->create();
			// $xsl->loadXML($xslDoc);
			$xmlSource->loadXML($this->arrayToXml($stockLogData));

			$out = $xsl->transformToXML( $xmlSource );
			//echo "<pre>";echo $out;
			$fileName = $idProfiler . "." . $format;
			if($enableLocal == 1){
				$filePathLocal = DIRECTORY_SEPARATOR . $localFolder;
				$this->writeFileFromOutputFromat($filePathLocal,$out,$fileName);
			}
			if($enableFtp == 1){
				$filePathFtp = DIRECTORY_SEPARATOR . $localFolderFtp;
				$this->writeFileFromOutputFromat($filePathFtp,$out,$fileName);
			}
			$status = 1;
			$message = "EXPORT RECEIVING TO FILE SUCCESS!";
		} catch (Exception $e) {
			$status = 1;
			$message = "EXPORT RECEIVING TO FILE FAILED!";
		}
		//write log to end export
		$this->_processLog->executeAfter($dataProcessLog['process_id'],$status,$message);	

		//print_r($this->arrayToXml($stockLogData));
		//exit;
	}

	public function writeFileFromOutputFromat($filePath,$out,$fileName){
		$path = $this->_directoryList->getPath('var').$filePath;
		if (!is_dir($path)) {
			$this->_file->mkdir($path, 0775);
		}
		$this->_file->open(array('path'=>$path));
		$this->_file->write($fileName, $out, 0666);
	}

	public function arrayToXml($array, $rootElement = null, $xml = null) { 
		$_xml = $xml; 

		    // If there is no Root Element then insert root 
		if ($_xml === null) { 
			$_xml = new \SimpleXMLElement($rootElement !== null ? $rootElement : '<items/>'); 
		} 

		    // Visit all key value pair 
		foreach ($array as $k => $v) { 

		        // If there is nested array then 
			if (is_array($v)) {  

		            // Call function for nested array 
				$this->arrayToXml($v, '<item/>', $_xml->addChild('item')); 
			} 

			else { 

		            // Simply add child element.  
				//echo $k;
				$_xml->addChild($k, $v); 
			} 
		} 

		return $_xml->asXML(); 
	} 

}