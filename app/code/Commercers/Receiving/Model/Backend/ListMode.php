<?php

/* 
 * Commercers Viet Nam
 * Nhan Dep Trai
 */

namespace Commercers\Receiving\Model\Backend;

class ListMode implements \Magento\Framework\Option\ArrayInterface
{
 public function toOptionArray()
 {
  return [
    ['value' => 'standard', 'label' => __('Standard')],
    ['value' => 'h1', 'label' => __('H1')],
    ['value' => 'h2', 'label' => __('H2')]
  ];
 }
}