<?php
/**
 *  Commercers Vietnam
 *  Toan Dao 
 */
namespace Commercers\Receiving\Controller\Adminhtml\Receiving;

use Magento\Backend\App\Action;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;
class Save extends \Magento\Backend\App\Action{
    private $eventManager;
    protected $helperData;
	protected $orderProposalCollection;
	protected $messageManager;
    public function __construct(
        Action\Context $context,
        \Magento\Framework\Event\Manager $eventManager,
        \Commercers\Receiving\Helper\Data $helperData,
        CollectionFactory $collectionFactory,
		\Commercers\OrderProposal\Model\ResourceModel\OrderProposal\CollectionFactory $orderProposalCollection,
		\Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->eventManager = $eventManager;
        $this->helperData = $helperData;
        $this->_collectionFactory = $collectionFactory;
		$this->orderProposalCollection = $orderProposalCollection;
		$this->messageManager = $messageManager;
        parent::__construct($context);
    }
    public function execute() {
        $data = $this->getRequest()->getParam('general');
        //order proposal collection
        $orderProposalCollection = $this->orderProposalCollection->create();
        $orderProposalCollection->addFieldToFilter('order_number', array('eq' => $data['number']));
        //check order number
        if ($orderProposalCollection->getSize() == 0) {
            $this->messageManager->addErrorMessage(__('Order Number doesn\'t exist.'));
            return $this->_redirect('*/*/index');
        }
        //get attribute 
        $valueAttribute = $this->helperData->getOption();
        $attribites = $this->_collectionFactory->create();
        $nameAttribute = '';
        foreach ($attribites as $item) {
            if ($item->getData()['attribute_code'] === $valueAttribute) {
                $nameAttribute = $item->getData()['frontend_label'];
            }
        }
        //get qty and attribute code
        $qty = (int) str_replace(",", ".", $data['qty']);
        $attr = strtolower($nameAttribute);
        //check qty
        $orderProposalCollection = $this->orderProposalCollection->create();
        $orderProposalCollection->addFieldToFilter('order_number', array('eq' => $data['number']))
                ->addFieldToFilter('sku', array('eq' => $data['sku']));
        if ($orderProposalCollection->getSize() == 0) {
            $this->messageManager->addErrorMessage(__('SKU doesn\'t belong to this order.'));
            return $this->_redirect('*/*/index');
        }
        $orderProposal = $orderProposalCollection->getData()[0];
        //check count
        if ($orderProposal['open_qty'] != NULL) {
            if ($orderProposal['open_qty'] == 0) {
                $this->messageManager->addErrorMessage(__('This order has received enough.'));
                return $this->_redirect('*/*/index');
            }
        }
        if ($qty + $orderProposal['delivered_qty'] > $orderProposal['orderqty']) {
            $this->messageManager->addErrorMessage(__('Error: The delivered quantity is greater than the Ordered quantity.'));
            return $this->_redirect('*/*/index');
        }

        $eventData = array(
            'attrselect' => $attr,
            'attr' => $data[$attr],
            'qty' => $qty,
            'ordernumber' => $data['number'],
            'deliordernumber' => $data['delivery']
        );
        $this->eventManager->dispatch('receiving_data_load_after', $eventData);
        $this->_redirect('*/*/index');
    }

}
