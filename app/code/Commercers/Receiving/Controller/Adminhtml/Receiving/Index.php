<?php
/**
 *  Commercers Vietnam
 *  Toan Dao 
 */
namespace Commercers\Receiving\Controller\Adminhtml\Receiving;

class Index extends \Magento\Framework\App\Action\Action
{
/** @var \Magento\Framework\View\Result\PageFactory  */
protected $resultPageFactory;
public function __construct(
     \Magento\Framework\App\Action\Context $context,
        \Commercers\Receiving\Helper\Data $helperData,
     \Magento\Framework\View\Result\PageFactory $resultPageFactory
) {
     $this->resultPageFactory = $resultPageFactory;
     $this->helperData = $helperData;
     parent::__construct($context);
}
     public function execute()
     {
     //echo $this->getRequest()->getFullActionName();exit;
         //$valueAttribute = $this->helperData->getProductAttributeShowOnSuccess();exit;
          return $this->resultPageFactory->create();
     }
}