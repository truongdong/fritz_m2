<?php

namespace Commercers\Receiving\Cron;

class CronJobReceiving 
{ 
    
    protected $_import;
    protected $_profilerFactory;

    public function __construct(
        \Commercers\Receiving\Service\Import $import,
        \Commercers\Profilers\Model\ProfilersFactory $profilerFactory,
        \Magento\Framework\Registry $registry
    )
    {
        $this->_profilerFactory = $profilerFactory;
        $this->_import = $import;
        $this->registry = $registry;
    }
    
    public function execute($schedule){
        $jobCode = $schedule->getJobCode();
        $profiler = $this->_profilerFactory->create()->getCollection()->addFieldToFilter('code',array('eq' => $jobCode));
        $this->_import->execute($profiler);
    }
}