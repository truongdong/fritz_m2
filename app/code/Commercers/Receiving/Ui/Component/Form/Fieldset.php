<?php  
 namespace Commercers\Receiving\Ui\Component\Form;


 use Magento\Framework\View\Element\UiComponent\ContextInterface;  
 use Magento\Framework\View\Element\UiComponentInterface;  
 use Magento\Ui\Component\Form\FieldFactory;  
 use Magento\Ui\Component\Form\Fieldset as BaseFieldset;
 use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;

 class Fieldset extends BaseFieldset  
 {
/**
 * @var FieldFactory
 */
    protected $helperData;
    private $fieldFactory;

public function __construct(
    ContextInterface $context,
    \Commercers\Receiving\Helper\Data $helperData,
    CollectionFactory $collectionFactory,
    array $components = [],
    array $data = [],
    FieldFactory $fieldFactory
    )
{
    parent::__construct($context, $components, $data);
    $this->fieldFactory = $fieldFactory;
    $this->helperData = $helperData;
    $this->_collectionFactory = $collectionFactory;
}

/**
 * Get components
 *
 * @return UiComponentInterface[]
 */
public function getChildComponents()
{
        $valueAttribute = $this->helperData->getOption();
 
        $attribites = $this->_collectionFactory->create();
        $nameAttribute = '';
        foreach ($attribites as $item) {
            if($item->getData()['attribute_code'] === $valueAttribute){
                $nameAttribute = $item->getData()['frontend_label'];
            }
        }
    $fields = [
        [
            'label' => __('Attribute: '.$nameAttribute),
            'formElement' => 'input',
            'source'=>'general',
            'sortOrder' => 0,
            'validation'=> ['required-entry'=> true]
        ]
    ];

    foreach ($fields as $k => $fieldConfig) {
        $fieldInstance = $this->fieldFactory->create();
        $name = strtolower($nameAttribute);

        $fieldInstance->setData(
            [
                'config' => $fieldConfig,
                'name' => $name
            ]
        );

        $fieldInstance->prepare();
        $this->addComponent($name, $fieldInstance);
    }

    return parent::getChildComponents();
}
}