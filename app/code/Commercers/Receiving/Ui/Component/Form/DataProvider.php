<?php
/**
 *  Commercers Vietnam
 *  Toan Dao 
 */
namespace Commercers\Receiving\Ui\Component\Form;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider {

    /**
     * DataProvider constructor.
     * @param $name
     * @param $primaryFieldName
     * @param $requestFieldName
     * @param array $meta
     * @param array $data
     */
    protected $helperData;
    public function __construct(
        \Commercers\Receiving\Helper\Data $helperData,
        CollectionFactory $collectionFactory,
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
       
        $this->helperData = $helperData;
        $this->_collectionFactory = $collectionFactory;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->collection = $objectManager->create("Commercers\StockLog\Model\StockLog")->getCollection();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        
    }
    
    

    public function getData()
    { 
        $data = [];
        foreach($this->getCollection()->getItems() as $item){
            $data[$item->getId()] = array('general' => $item->getData());
        }
        return $data;
    }
    
    public function getMeta()
    {  
        
        $defaultItem = $this->collection->addFieldToFilter('type',array('eq'=>1))->setOrder('id','desc')->getFirstItem();
        $defaulOrderNumber= $defaultItem->getOrdernumber();
        $meta = parent::getMeta();
        $meta['general']['children']['number']['arguments']['data']['config']['default'] = $defaulOrderNumber;
        $valueAllowNegative = $this->helperData->getAllowNegative();
        if($valueAllowNegative == 1){
            $active = false;
        }else{
            $active = true;
        }
            $meta['general']['children']['qty']['arguments']['data']['config']['validation']['validate-greater-than-zero'] = $active;
        return $meta;
    }
    
}