<?php

namespace Commercers\Receiving\Ui\Component\Form;

class InputQty extends \Magento\Ui\Component\Form\Element\Input {

    /**
     * Prepare component configuration
     *
     * @return void
     */
    public function prepare() {
        parent::prepare();
        $config = $this->getData('config');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $showprice = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('receiving/general/predefined_quantity');

        if (isset($config['dataScope']) && $config['dataScope'] == 'qty') {
            $config['default'] = $showprice;
            $this->setData('config', (array) $config);
        }
    }

}
