<?php

namespace Commercers\TrackingInterface\Data\Import;

use Commercers\Profilers\Service\Mail\Template\TransportBuilderFactory;
use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class DataSource
{
    public function __construct(
        \Magento\Sales\Model\Convert\Order $orderConverter,
        \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory,
        \Magento\Shipping\Model\ShipmentNotifier $shipmentNotifier,
        \Magento\Sales\Api\Data\OrderInterface $orderFactory,
        \Magento\Sales\Model\Order\ShipmentFactory $shipmentFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        TransportBuilderFactory $transportBuilder,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->orderConverter = $orderConverter;
        $this->trackFactory = $trackFactory;
        $this->shipmentFactory = $shipmentFactory;
        $this->shipmentNotifier = $shipmentNotifier;
        $this->orderFactory = $orderFactory;
        $this->productFactory = $productFactory;
        $this->_transportBuilder = $transportBuilder;
        $this->_scopeConfig = $scopeConfig;
        $this->dir = $dir;
    }

    public function execute($data, $profiler,$file)
    {
        $importActionEncode = $profiler->getImportActionEncode();
        $importActions = json_decode($importActionEncode, true);
        $data = $data['items']['item'];
//        echo "<pre>";
//        print_r($data);exit;
        $productsData = array();
        $tracks = array();
        foreach ($data as $item) {
            if ($item['tracking_number']) {
                $productsData[$item['order_increment']][] = array(
                    'sku' => $item['sku'],
                    'qty' => $item['qty']
                );

                $trackData = array(
                    'carrier_code' => strtolower($item['carrier_code']),
                    'title' => $item['carrier_code'],
                    'number' => $item['tracking_number']
                );
                $tracks[$item['order_increment']][$item['tracking_number']] = $trackData;
            }
        }
        if (empty($tracks)) return "ERROR";
        // Combine Qty
        foreach ($productsData as $orderIncrement => $productData) {
            $flag_1 = array();
            foreach ($productData as $data) {
                $flag_1[$data['sku']][] = $data['qty'];
            }
            $flag_2 = array();
            foreach ($flag_1 as $sku => $value) {
                $flag_2[$sku][] = array_sum($value);
            }
            $productsData_2[$orderIncrement][] = $flag_2;
        }

        $productsData = array();
        foreach ($productsData_2 as $orderIncrement => $productData) {
            $productData = $productData[0];
            foreach ($productData as $sku => $qty) {
                $qty = $qty[0];
                $productsData[$orderIncrement][] = array(
                    'sku' => $sku,
                    'qty' => $qty
                );
            }
        }
        // Combine Qty
        foreach ($productsData as $orderNumber => $productData) {
            $order = $this->orderFactory->loadByIncrementId($orderNumber);
            if (!$order->canShip()) {
                continue;
            }
            $items = array();
            foreach ($order->getAllItems() as $orderItem) {

                foreach ($productData as $data) {
                    if ($orderItem->getSku() == $data['sku']) {
                        if ($orderItem->getIsVirtual() || ($orderItem->getParentItemId() && !$orderItem->isShipSeparately())) {
                            $items[$orderItem->getParentItemId()] = $data['qty'];
                        } else {
                            $items[$orderItem->getId()] = $data['qty'];
                        }
                    }
                }
            }
//            echo "<pre>";
//            print_r($items);exit;
            try {
                $shipment = $this->shipmentFactory->create($order, $items, $tracks[$orderNumber]);
                $shipment->register();
                $shipment->getOrder()->setIsInProcess(true);
                $shipment->save();
                $shipment->getOrder()->save();
            }catch (\Exception $exception){
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/profiler_tracking_import_error.log');
                $logger = new \Zend\Log\Logger();
                $logger->addWriter($writer);
                $errorMessage = $exception->getMessage() . " - " . "Order #". $orderNumber;
                $logger->info($errorMessage);
                $this->_sendNotificationToAdmin($profiler,$errorMessage,$file);
                return "ERROR";
            }
            //Import Action

            if (isset($importActions['send_email'])) {
                if ((int)$importActions['send_email'] == 1) {
                    $this->shipmentNotifier->notify($shipment);
                    $shipment->getOrder()->addStatusHistoryComment('Die Versand E-Mail wurde.')->save();
                }
            }

        }


    }

    protected function _sendNotificationToAdmin($profiler,$errorMessage,$file){
        $importActionEncode = $profiler->getImportActionEncode();
        $importActions = json_decode($importActionEncode, true);
        if ((boolean)$importActions['error_email']) {

            $from = [
                'name' => $this->_scopeConfig->getValue(
                    'trans_email/ident_general/name',
                    ScopeInterface::SCOPE_STORE
                ),
                'email' => $this->_scopeConfig->getValue(
                    'trans_email/ident_general/email',
                    ScopeInterface::SCOPE_STORE
                )
            ];

            $emails = preg_replace('/\s/', '', $importActions['error_email']);
            $emails = explode(',', $emails);

            //file path in error folder
            $localRootPath = $this->dir->getRoot();
            $filePath = $localRootPath . DIRECTORY_SEPARATOR . $profiler->getLocalFolder() . DIRECTORY_SEPARATOR . 'error'. DIRECTORY_SEPARATOR . basename($file);

            foreach ($emails as $email){

                $transport = $this->_transportBuilder->create();

                $transport->addAttachment(file_get_contents($file), basename($file), '');

                $defaultEmailTemplate = 'profiler_import_process_error_email_template';

                $emailTemplate = (isset($importActions['error_email_template']) and $importActions['error_email_template'] != '' and $importActions['error_email_template'] != 0) ? $importActions['error_email_template'] : $defaultEmailTemplate;
                //var_dump($emailTemplate);exit;
                if (!$emailTemplate) return;
                $transport->setTemplateIdentifier($emailTemplate)->setTemplateOptions(
                    [
                        'area' => Area::AREA_FRONTEND,
                        'store' => 1,
                    ]
                )
                    ->setTemplateVars([
                        'error_message' => $errorMessage,
                        'file_name' => basename($file),
                        'file_path' => $filePath
                    ])
                    ->setFrom($from)
                    ->addTo($email, 'Admin');

                try{
                    $transport->getTransport()->sendMessage();
                }catch (\Exception $exception){
                    $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/profiler_import_log_error.log');
                    $logger = new \Zend\Log\Logger();
                    $logger->addWriter($writer);
                    $logger->info("Profiler " . $profiler->getName() . " has the problem when send the email: " . $exception->getMessage());
                }
            } //end foreach email
        } //end check should do send email
    }

}
