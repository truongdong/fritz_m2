<?php

namespace Commercers\TrackingInterface\Block\Adminhtml\Form;

class Action extends \Magento\Backend\Block\Template
{

    protected $_template = 'Commercers_TrackingInterface::form/import_action.phtml';
    protected $_emailTemplateCollection;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Commercers\Profilers\Model\ProfilersFactory $profilerFactory,
        \Magento\Email\Model\ResourceModel\Template\CollectionFactory $emailTemplateCollection
    )
    {
        $this->profilerFactory = $profilerFactory;
        $this->_emailTemplateCollection = $emailTemplateCollection;
        parent::__construct($context);
    }


    protected function getProfiler()
    {
        $profilerId = $this->_request->getParam('id');

        if (!$profilerId)
            return false;

        $profiler = $this->profilerFactory->create()->load($profilerId);
        return $profiler;
    }

    public function getTrackingImportValues()
    {
        $profiler = $this->getProfiler();
        $importActionEncode = $profiler->getImportActionEncode();
        $importAction = json_decode($importActionEncode, true);
        return $importAction;
    }

    public function getEmailTemplate()
    {
        return $this->_emailTemplateCollection->create()->toOptionArray();
    }
}
