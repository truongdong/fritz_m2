<?php

namespace Commercers\ProductAttribute\Controller\Adminhtml\Image;
use Magento\Framework\Controller\ResultFactory;
class Upload extends \Magento\Backend\App\Action
{
    protected $fileUploaderFactory;

    protected $fileSystem;

    protected $_storeManager;

    protected $coreSession;

    public function __construct(
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Framework\Filesystem $fileSystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Backend\App\Action\Context $context
    )
    {
        $this->coreSession = $coreSession;
        $this->_storeManager = $storeManagerInterface;
        $this->fileUploaderFactory = $fileUploaderFactory;
        $this->fileSystem = $fileSystem;
        parent::__construct($context);
    }

    public function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Catalog::products');
    }

    public function execute()
    {
        try {
            $files = $this->getRequest()->getFiles();
            $data = $this->getRequest()->getParams();
            if ($files['product']['information_labels']) {
                foreach ($files['product']['information_labels'] as $data) {
                    $filesData = $data['image'];
                }
            }
            if ($filesData['name']) {
                $uploader = $this->fileUploaderFactory->create(['fileId' => $filesData]);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $uploader->setAllowCreateFolders(true);
                $uploader->setAllowedExtensions(['jpg', 'png', 'jpeg']);
                $mediaPath = $this->fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath();
                $path = $mediaPath . 'label_image/';
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $result = $uploader->save($path);
                $upload_document = 'label_image' . $uploader->getUploadedFilename();
                $filePath = 'pub/media/label_image' . $result['file'];
                $fileName = $result['name'];
                $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
                $filePathFull = $baseUrl . $filePath;
                $fileFull = array('fileName' => $fileName, 'filePath' => $filePathFull);
                $uploadFile = $this->coreSession->getPathUploadFiles();
                $uploadFile[] = $fileFull;
                $this->coreSession->setPathUploadFiles($uploadFile);
                $mediaUrl = $this->_storeManager->getStore()->getBaseUrl().'pub/media/';
                $result['url'] = $mediaUrl . 'label_image' . $result['file'];
            } else {
                $filePath = '';
                $this->coreSession->setPathUploadFiles($filePath);
            }
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}
