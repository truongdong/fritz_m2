<?php

namespace Commercers\ProductAttribute\Controller\Adminhtml\Index;
use Magento\Framework\Controller\ResultFactory;
class Grid extends \Magento\Backend\App\Action
{
    protected $fileUploaderFactory;

    protected $fileSystem;

    protected $_storeManager;

    protected $coreSession;

    public function __construct(
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Framework\Filesystem $fileSystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Backend\App\Action\Context $context
    )
    {
        $this->coreSession = $coreSession;
        $this->_storeManager = $storeManagerInterface;
        $this->fileUploaderFactory = $fileUploaderFactory;
        $this->fileSystem = $fileSystem;
        parent::__construct($context);
    }

    public function _isAllowed()
    {
        return $this->_authorization->isAllowed('Commercers_ProductAttribute::Faq');
    }

    public function execute()
    {

    }
}
