<?php
/**
 * Copyright © 2017 BORN . All rights reserved.
 */
namespace Commercers\ProductAttribute\Observer;

use Commercers\Backend\Ui\DataProvider\Product\Form\Modifier\InformationLabels;
use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;

class SerializedInformationLabels implements ObserverInterface
{
    const ATTR_INFORMATION_LABELS_CODE = 'information_labels';
    /**
     * @var  \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * Constructor
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->request = $request;
    }

    public function execute(Observer $observer)
    {
        /** @var $product \Magento\Catalog\Model\Product */
        $product = $observer->getEvent()->getDataObject();
        $post = $this->request->getPost();
        $post = $post['product'];
        $informationLabels = isset($post[self::ATTR_INFORMATION_LABELS_CODE]) ? $post[self::ATTR_INFORMATION_LABELS_CODE] : '';
        $product -> setInformationLabels($informationLabels);
        $requiredParams = ['label','position'];
        if (is_array($informationLabels)) {
//            $informationLabels = $this -> removeEmptyArray($informationLabels, $requiredParams);
            $product -> setInformationLabels(json_encode($informationLabels));
        }
    }

    /**
     * Function to remove empty array from the multi dimensional array
     *
     * @return Array
     */
    private function removeEmptyArray($attractionData, $requiredParams){

        $requiredParams = array_combine($requiredParams, $requiredParams);
        $reqCount = count($requiredParams);

        foreach ($attractionData as $key => $values) {
            $values = array_filter($values);
            $inersectCount = count(array_intersect_key($values, $requiredParams));
            if ($reqCount != $inersectCount) {
                unset($attractionData[$key]);
            }
        }
        return $attractionData;
    }
}
