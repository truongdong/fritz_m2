<?php

namespace Commercers\ProductAttribute\Ui\DataProvider\Product\Form\Modifier;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Catalog\Controller\Adminhtml\Product\Initialization\StockDataFilter;
use Magento\Catalog\Model\Locator\LocatorInterface;

use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;

use Magento\Ui\Component\Container;
use Magento\Ui\Component\Form\Element\DataType\Number;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\Textarea;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Modal;

use Magento\Ui\Component\Form\Element\DataType\Media\Image;

/**
 * Data provider for attraction highlights field
 */
class InformationLabels extends AbstractModifier
{
    const INFORMATION_LABELS_FIELD = 'information_labels';

    /**
     * @var LocatorInterface
     */
    private $locator;

    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var array
     */
    private $meta = [];

    /**
     * @var string
     */
    protected $scopeName;

    /**
     * @param LocatorInterface $locator
     * @param ArrayManager $arrayManager
     */
    public function __construct(
        LocatorInterface $locator,
        ArrayManager $arrayManager,
        $scopeName = ''
    ) {
        $this->locator = $locator;
        $this->arrayManager = $arrayManager;
        $this->scopeName = $scopeName;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        $fieldCode = self::INFORMATION_LABELS_FIELD;

        $model = $this->locator->getProduct();
        $modelId = $model->getId();

        $highlightsData = $model->getInformationLabels();

        if ($highlightsData) {
            $highlightsData = json_decode($highlightsData, true);
            $path = $modelId . '/' . self::DATA_SOURCE_DEFAULT . '/'. self::INFORMATION_LABELS_FIELD;
            $data = $this->arrayManager->set($path, $data, $highlightsData);
        }
        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;
        $this -> initInformationLabelsFields();
//        echo "<pre>";
//        print_r($this->meta);exit;
        return $this->meta;
    }


    /**
     * Customize attraction highlights field
     *
     * @return $this
     */
    protected function initInformationLabelsFields()
    {
        $informationLabelsPath = $this->arrayManager->findPath(
            self::INFORMATION_LABELS_FIELD,
            $this->meta,
            null,
            'children'
        );

        if ($informationLabelsPath) {
            $this->meta = $this->arrayManager->merge(
                $informationLabelsPath,
                $this->meta,
                $this->initInformationLabelsFieldStructure($informationLabelsPath)
            );
            $this->meta = $this->arrayManager->set(
                $this->arrayManager->slicePath($informationLabelsPath, 0, -3)
                . '/' . self::INFORMATION_LABELS_FIELD,
                $this->meta,
                $this->arrayManager->get($informationLabelsPath, $this->meta)
            );
            $this->meta = $this->arrayManager->remove(
                $this->arrayManager->slicePath($informationLabelsPath, 0, -2),
                $this->meta
            );
        }

        return $this;
    }


    /**
     * Get information labels dynamic rows structure
     *
     * @param string $informationLabelsPath
     * @return array
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function initInformationLabelsFieldStructure($informationLabelsPath)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => 'dynamicRows',
                        'label' => __('Information Labels'),
                        'renderDefaultRecord' => false,
                        'recordTemplate' => 'record',
                        'dataScope' => '',
                        'dndConfig' => [
                            'enabled' => false,
                        ],
                        'disabled' => false,
                        'sortOrder' =>
                            $this->arrayManager->get($informationLabelsPath . '/arguments/data/config/sortOrder', $this->meta),
                    ],
                ],
            ],
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'isTemplate' => true,
                                'is_collection' => true,
                                'component' => 'Magento_Ui/js/dynamic-rows/record',
                                'dataScope' => '',
                            ],
                        ],
                    ],
                    'children' => [
                        'label' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'formElement' => Input::NAME,
                                        'componentType' => Field::NAME,
                                        'dataType' => Text::NAME,
                                        'label' => __('Label'),
                                        'dataScope' => 'label',
                                        'additionalClasses' => 'data-information-labels-label',
                                        'fit' => false
                                    ],
                                ],
                            ],
                        ],
                        'position' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'formElement' => Input::NAME,
                                        'componentType' => Field::NAME,
                                        'dataType' => Text::NAME,
                                        'label' => __('Position'),
                                        'dataScope' => 'position',
                                    ],
                                ],
                            ],
                        ],
                        'link' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'formElement' => Input::NAME,
                                        'componentType' => Field::NAME,
                                        'dataType' => Text::NAME,
                                        'label' => __('Link'),
                                        'dataScope' => 'link',
                                    ],
                                ],
                            ],
                        ],
                        'imageUploader' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'visible'=>true,
                                        'dataType'=>'string',
                                        'formElement' => [
                                            'imageUploader' => [
                                                'setting' =>[
                                                    'allowedExtensions'=>'jpg jpeg png',
                                                    'maxFileSize'=>'2097152'
                                                ]
                                            ]
                                        ],
                                        'componentType' => 'imageUploader',
                                        'component' => 'Magento_Ui/js/form/element/image-uploader',
                                        'label' => __('Upload Image'),
                                        'previewTmpl'=>'Magento_Catalog/image-preview',
                                        'elementTmpl'=>'ui/form/element/uploader/uploader',
                                        'dataScope' => 'image',
//                                        'isMultipleFiles'=>true,
                                        'uploaderConfig'=>[
                                            'url'=>'productattribute/image/upload'
                                        ]
                                    ],
                                ],
                            ],
                        ],
                        'actionDelete' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'componentType' => 'actionDelete',
                                        'dataType' => Text::NAME,
                                        'label' => __('Delete'),
                                        'fit' => 'false',
                                        'template' => 'Magento_Backend/dynamic-rows/cells/action-delete'
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
