<?php

namespace Commercers\Ftp\Model\Service;

class Ftp 
{
    protected $ioFtp;
    protected $_dir;
    
    public function __construct(
        \Magento\Framework\Filesystem\Io\Ftp $ioFtp,
        \Magento\Framework\Filesystem\DirectoryList $dir
    ) 
    {
        $this->ioFtp = $ioFtp;
        $this->_dir = $dir;
    }
    
    public function getRoot(){
        $root = $this->_dir->getRoot();
        return $root;
    }
    
    public function readAndMove($params,$ftpFolder,$ftpLocalFolder,$ftpDoneFolder){
        $open = $this->ioFtp->open(
                array(
                    'host' => $params['localhost'],
                    'user' => $params['username'],
                    'password' => $params['password'],
                    'file_mode' => FTP_BINARY,
                    'ssl' => false,
                    'passive' => true
                )
            );
        $this->ioFtp->cd($ftpFolder); 
        $files = $this->ioFtp->ls();
        for($i = 2; $i < count($files); $i++){ 
            /**
            * read and copy file from folder ftp to local folder
            */ 
            $this->ioFtp->read($files[$i]['id'],DIRECTORY_SEPARATOR.$ftpLocalFolder.DIRECTORY_SEPARATOR.$files[$i]['text']);
            /**
            * move file on ftp
            */
            $this->ioFtp->mv($files[$i]['id'],DIRECTORY_SEPARATOR.$ftpDoneFolder.DIRECTORY_SEPARATOR.$files[$i]['text']);
        }
    }
}
