<?php
namespace Commercers\Io;

use phpseclib\Crypt\RSA;


class Sftp extends \Magento\Framework\Filesystem\Io\Sftp {

    
    public function open(array $args = []) {
        
        
        if (!isset($args['timeout'])) {
            $args['timeout'] = self::REMOTE_TIMEOUT;
        }
        if (strpos($args['host'], ':') !== false) {
            list($host, $port) = explode(':', $args['host'], 2);
        } else {
            $host = $args['host'];
            $port = self::SSH2_PORT;
        }
        $this->_connection = new \phpseclib\Net\SFTP($host, $port, $args['timeout']);
        if($args['key_file']){
            $rsa = new RSA();
            $rsa->setPassword($args['password']);
            $rsa->loadKey(file_get_contents($args['key_file']));
            //echo '<pre>';print_r($rsa);exit;
            try{
                if (!$this->_connection->login($args['username'], $rsa  )) {
                    throw new \Exception(
                    sprintf("Unable to open SFTP connection as %s@%s", $args['username'], $args['host'])
                    );
                }
            } catch (Exception $ex) {
                print_r($ex->getTraceAsString());exit;
            }
            
             
             
        }else{
            if (!$this->_connection->login($args['username'], $args['password'])) {
                throw new \Exception(
                sprintf("Unable to open SFTP connection as %s@%s", $args['username'], $args['host'])
                );
            }
        }
        
        
    }

}
