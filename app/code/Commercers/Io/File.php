<?php

namespace Commercers\Io;

class File {

    protected $ftp;
    protected $dir;
    protected $sftp;
    protected $ioFile;
    protected $filesName;

    public function __construct(
    \Magento\Framework\Filesystem\Io\Ftp $ioFtp,
    \Commercers\Io\Sftp $sftp,
    \Magento\Framework\Filesystem\DirectoryList $dir,
    \Magento\Framework\Filesystem\Io\File $ioFile,
    \Magento\Framework\Filesystem $filesystem

    ) {
        $this->ftp = $ioFtp;
        $this->sftp = $sftp;
        $this->dir = $dir;
        $this->ioFile = $ioFile;
        $this->filesystem = $filesystem;
    }

    protected function connect($profiler) {
        $localRootPath = $this->dir->getRoot();
        if ($profiler['ftp_type'] == 'sftp') {
            $this->sftp->open(
                    array(
                        'host' => $profiler['ftp_hostname'],
                        'username' => $profiler['ftp_username'],
                        'password' => $profiler['ftp_password'],
                        'file_mode' => FTP_BINARY,
                        'ssl' => $profiler['ftp_type'],
                        'passive' => true,
                        'key_file' => $localRootPath . DIRECTORY_SEPARATOR . $profiler['ftp_key_file']
                    )
            );
            $this->ftp = $this->sftp;
            return $this->ftp;
        }
      //  echo "<pre>"; print_r($profiler);
        $this->ftp->open(
                array(
                    'host' => $profiler['ftp_hostname'],
                    'user' => $profiler['ftp_username'],
                    'password' => $profiler['ftp_password'],
                    'file_mode' => FTP_BINARY,
                    //'ssl' => $profiler['ftp_type'],
                    'passive' => true
                )
        );

        return $this->ftp;
    }

    public function getFiles($profiler) {

        $fileList = array(
            'ftp' => array(),
            'local' => array()
        );

        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $localRootPath = $this->dir->getRoot();

        if ($profiler['ftp_enable']) {

            $this->connect($profiler);

            $this->ftp->cd($profiler['ftp_folder']);

            //exit;
            $files = $this->ftp->ls();
            //echo '<pre>'; print_r($files);
            $ftpTempFolder = $localRootPath . DIRECTORY_SEPARATOR . $profiler['ftp_local_tmp'];
            if(!file_exists($ftpTempFolder)){
                mkdir($ftpTempFolder);
            }
            foreach ($files as $file) {
                if (!(isset($profiler['file_prefix']) && substr($file['text'], 0, mb_strlen($profiler['file_prefix'])) === $profiler['file_prefix'])) {
                    continue;
                }

                $filePath = $file['id'];
                @$this->ftp->read($filePath, $ftpTempFolder . DIRECTORY_SEPARATOR . $file['text']);
                if (file_exists($ftpTempFolder . DIRECTORY_SEPARATOR . $file['text']) && !is_dir($ftpTempFolder . DIRECTORY_SEPARATOR . $file['text'])) {
                    //$_ftpProcessedFiles[] = $file;
                    $fileList['ftp'][] = array(
                        'file' => $ftpTempFolder . DIRECTORY_SEPARATOR . $file['text'],
                        'ftp_file' => $file
                    );
                }
            }
        }


        if ($profiler['local_enable']) {

            $localFolder = $localRootPath . DIRECTORY_SEPARATOR . $profiler['local_folder'];

            $fileIterators = new \FilesystemIterator($localFolder);

            if ($fileIterators->getPathName() != '') {
                /** @var \FilesystemIterator $file */
                foreach ($fileIterators as $file) {
                    if (!(isset($profiler['file_prefix']) && substr($file->getFilename(), 0, mb_strlen($profiler['file_prefix'])) === $profiler['file_prefix'])) {

                        continue;
                    }
                    //$file->getP
                    if (is_dir($file))
                        continue;
                    $fileList['local'][] = $file;
                }
            }
        }
        $this->ftp->close();

        return $fileList;
    }

    public function mvFilesOnFtp($ftpFile, $profiler) {
        if (isset($profiler['ftp_done_folder'])) {

            $this->connect($profiler);
            $proceededFolder = $profiler['ftp_done_folder'];
            $this->ftp->mkdir($proceededFolder);
            $this->ftp->mv($ftpFile['id'], $proceededFolder . DIRECTORY_SEPARATOR . $ftpFile['text']);
            $this->ftp->close();
        }
    }

     public function mvFilesOnLocal($localFile, $profiler, $isMoveFileToErrorFolder = false) {
        $toFolder = $profiler['local_done_folder'];
        if($isMoveFileToErrorFolder){
            $toFolder = $profiler['local_folder'] . DIRECTORY_SEPARATOR . 'error';
        }
        $localRootPath = $this->dir->getRoot();
        $proceededFolder = $localRootPath . DIRECTORY_SEPARATOR. $toFolder;
        if(!file_exists($proceededFolder)){
            mkdir($proceededFolder, 0755, true);
        }

        rename($localFile, $proceededFolder . DIRECTORY_SEPARATOR . basename($localFile));
    }

    public function moveTmpFile($file, $profiler) {

        if (isset($profiler['ftp_local_tmp'])) {
            $localRootPath = $this->dir->getRoot();
            $proceededFolder = $localRootPath . DIRECTORY_SEPARATOR . $profiler['ftp_local_tmp'] . DIRECTORY_SEPARATOR . 'proceeded';
            if (!file_exists($proceededFolder)) {
                //echo 1; exit;
                mkdir($proceededFolder);
            }
            rename($file, $proceededFolder . DIRECTORY_SEPARATOR . basename($file));
        }
    }

    public function transferFilesToFtp($profiler, $fileName, $file) {
        if (isset($profiler['ftp_folder'])) {
            $this->connect($profiler);
            $proceededFolder = $profiler['ftp_folder'];
            $this->ftp->mkdir($proceededFolder);
            $this->ftp->write($proceededFolder . DIRECTORY_SEPARATOR . $fileName, $file);
            $this->ftp->close();
        }
    }

    public function writeFilesToLocal($profiler, $fileName, $src) {
        if (isset($profiler['local_done_folder'])) {

            $localFolder = $profiler['local_done_folder'];
            $rootDir = $this->dir->getRoot();
            $localFolder = $rootDir . DIRECTORY_SEPARATOR . $localFolder;
            $this->ioFile->mkdir($localFolder, 0775);
            //var_dump($localFolder);exit;
            if (is_string($src) && is_readable($src)) {
                rename($src, $localFolder . DIRECTORY_SEPARATOR . basename($fileName));
            } else {
                /*
                $writer = $this->filesystem->getDirectoryWrite('root');

                $file = $writer->openFile($localFolder . DIRECTORY_SEPARATOR .$fileName, 'a');
                try {
                    $file->lock();
                    try {
                        $file->write($src);
                    } finally {
                        $file->unlock();
                    }
                } finally {
                    $file->close();
                }
                 *
                 */
                // echo $src;
                // echo $localFolder . DIRECTORY_SEPARATOR . basename($fileName); exit;
                file_put_contents($localFolder . DIRECTORY_SEPARATOR . basename($fileName), $src);
            }

        }
    }

}
