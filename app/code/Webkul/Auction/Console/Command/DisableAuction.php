<?php
/**
 * Webkul Software
 *
 * @category Magento
 * @package  Webkul_Auction
 * @author   Webkul
 * @license  https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;

/**
 * Class DisableMpAuction
 */
class DisableAuction extends Command
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $_moduleManager;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute
     */
    protected $_eavAttribute;

    /**
     * @var \Magento\Framework\Module\Status
     */
    protected $_modStatus;
    /**
     * @param \Magento\Eav\Setup\EavSetupFactory        $eavSetupFactory
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Framework\Module\Manager         $moduleManager
     * @param \Magento\Framework\Module\Status          $modStatus
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Eav\Model\Entity\Attribute $entityAttribute,
        \Magento\Framework\Module\Status $modStatus,
        ModuleDataSetupInterface $setup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->_resource = $resource;
        $this->setup = $setup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->_moduleManager = $moduleManager;
        $this->_eavAttribute = $entityAttribute;
        $this->_modStatus = $modStatus;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('auction:disable')
            ->setDescription('Auction Disable Command');
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->_moduleManager->isEnabled('Webkul_Auction')) {
            $connection = $this->_resource
                ->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);

            //drop foreign key
            $connection->dropForeignKey(
                $connection->getTableName('wk_auction_amount'),
                $connection->getForeignKeyName(
                    'wk_auction_amount',
                    'auction_id',
                    'wk_auction_product',
                    'entity_id'
                )
            );
            $connection->dropForeignKey(
                $connection->getTableName('wk_auto_auction'),
                $connection->getForeignKeyName(
                    'wk_auto_auction',
                    'auction_id',
                    'wk_auction_product',
                    'entity_id'
                )
            );
            $connection->dropForeignKey(
                $connection->getTableName('wk_auction_winner_data'),
                $connection->getForeignKeyName(
                    'wk_auction_winner_data',
                    'auction_id',
                    'wk_auction_product',
                    'entity_id'
                )
            );
            $connection->dropForeignKey(
                $connection->getTableName('wk_auction_fee_record'),
                $connection->getForeignKeyName(
                    'wk_auction_fee_record',
                    'customer_id',
                    'customer_entity',
                    'entity_id'
                )
            );
            $connection->dropForeignKey(
                $connection->getTableName('wk_auction_fee_transaction'),
                $connection->getForeignKeyName(
                    'wk_auction_fee_transaction',
                    'customer_id',
                    'customer_entity',
                    'entity_id'
                )
            );

            // drop custom tables
            $connection->dropTable($connection->getTableName('wk_auction_product'));
            $connection->dropTable($connection->getTableName('wk_auction_amount'));
            $connection->dropTable($connection->getTableName('wk_auction_incremental_price'));
            $connection->dropTable($connection->getTableName('wk_auto_auction'));
            $connection->dropTable($connection->getTableName('wk_auction_winner_data'));
            $connection->dropTable($connection->getTableName('wk_au_wallet_record'));
            $connection->dropTable($connection->getTableName('wk_au_wallet_transaction'));
            $connection->dropTable($connection->getTableName('wk_customer_wallet_transfer_details'));
            $connection->dropTable($connection->getTableName('wk_auction_fee_record'));
            $connection->dropTable($connection->getTableName('wk_auction_fee_transaction'));

            // delete auction_type product attribute
            $eavSetup = $this->eavSetupFactory->create(['setup' => $this->setup]);
            $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'auction_type');
           
            // disable Auction
            $this->_modStatus->setIsEnabled(false, ['Webkul_Auction']);
            // delete entry from setup_module table
            $setupModuleTable = $this->_resource->getTableName('setup_module');
            $patchList = $this->_resource->getTableName('patch_list');
          //  $connection->query("DELETE FROM " . $tableName . " WHERE module = 'Webkul_Auction'");
            $connection->delete($setupModuleTable, "module = 'Webkul_Auction'");
            $connection->delete($patchList, "patch_name = 'Webkul\Auction\Setup\Patch\Data\CreateAttributes'");

            $output->writeln('<info>Auction module has been disabled successfully.</info>');
        }
    }
}
