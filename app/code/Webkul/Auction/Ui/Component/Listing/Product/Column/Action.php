<?php
/**
 * Webkul Auction Action UI.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Ui\Component\Listing\Product\Column;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class Action extends Column
{
    /** Url path */
    const AUCTION_AUCTION_ADD_URL = 'auction/auction/addauction';
    /** @var UrlInterface */
    protected $_urlBuilder;

    /**
     * @var string
     */
    private $_editUrl;

    /**
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface       $urlBuilder
     * @param array              $components
     * @param array              $data
     * @param string             $editUrl
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        TimezoneInterface $localeDate,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = [],
        $editUrl = self::AUCTION_AUCTION_ADD_URL
    ) {
        $this->localeDate = $localeDate;
        $this->_urlBuilder = $urlBuilder;
        $this->_editUrl = $editUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $name = $this->getData('name');
                $today = $this->localeDate->date()->format('Y-m-d H:i:s');
                $auctionStartTime = $item['start_auction_time'];
                $diff = strtotime($auctionStartTime) - strtotime($today);
                if (isset($item['entity_id'])) {
                    $actionLabel = ($item['auction_status'] == 1) ? __('Edit / Detail '): __('Detail ');
                    $item[$name]['edit'] = [
                        'href' => $this->_urlBuilder->getUrl(
                            $this->_editUrl,
                            ['id' => $item['entity_id'],
                            'auction_id'=>$item['entity_id']]
                        ),
                        'label' => $actionLabel,
                    ];
                }
            }
        }
        return $dataSource;
    }
}
