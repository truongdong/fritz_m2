<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Observer;

use Magento\Framework\Event\ObserverInterface;

class SalesOrderInvoiceSaveAfterObserver implements ObserverInterface
{
    /**
     * @var \Webkul\Auction\Helper\Data
     */
    protected $_auctionHelper;

    /**
     * @param \Webkul\Auction\Helper\Data            $auctionHelper
     */

    public function __construct(
        \Webkul\Auction\Helper\Data $auctionHelper,
        \Webkul\Auction\Helper\Email $emailHelper
    ) {
        $this->_auctionHelper = $auctionHelper;
        $this->_emailHelper = $emailHelper;
    }

    /**
     * Invoice save after
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        //this is for auction discount which we want to show on invoice
        $order = $observer->getEvent()->getOrder();
        $invoice = $observer->getEvent()->getInvoice();
        $invoice->setAuctionDiscount($order->getAuctionDiscount());
        $invoice->setBaseAuctionDiscount($order->getBaseAuctionDiscount());
        $invoice->save();
       
        $emailParams = $this->_auctionHelper->checkAndUpdateWalletAmount($order);
        if (!empty($emailParams)) {
            $this->_emailHelper->sendMailForTransaction(
                $emailParams['customer_id'],
                $emailParams,
                $emailParams['store']
            );
        }
    }
}
