<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Observer;

use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Webkul\Auction\Helper\Data;
use Webkul\Auction\Logger\Logger;
use Webkul\Auction\Model\ProductFactory;
use Webkul\Auction\Observer\CatalogControllerProductView;
use Webkul\Auction\Model\AuctionProductMShipMapFactory;

class SalesOrderCancelAfter implements ObserverInterface
{

    /**
     * @var EventManager
     */
    private $_eventManager;

    /**
     * @var Url
     */

    public $_url;

    /**
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;
    /**
     * @var Logger
     */
    public $logger;

    /**
     * @var Data
     */
    public $helper;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var CatalogControllerProductView
     */
    protected $catalogControllerProductView;

    /**
     * @var AuctionProductMShipMapFactory
     */
    protected $auctionProductMShipMapFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoader $creditmemoLoader
     * @param CreditmemoSender $creditmemoSender
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param ProductFactory $productFactory
     * @param CatalogControllerProductView $catalogControllerProductView
     * @param \Magento\Framework\UrlInterface $url
     * @param \Webkul\Auction\Model\Wallettransaction $wallettransaction
     * @param AuctionProductMShipMapFactory                        $auctionProductMShipMapFactory
     */
    public function __construct(
        Logger $logger,
        Data $helper,
        \Magento\Framework\Webapi\Rest\Request $request,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Sales\Model\Order\ItemFactory $itemFactory,
        ProductFactory $productFactory,
        \Webkul\Auction\Model\WallettransactionFactory $walletTransactionFactory,
        CatalogControllerProductView $catalogControllerProductView,
        \Webkul\Auction\Model\ResourceModel\Feerecord\Collection $feerecordCollection,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Webkul\Auction\Model\ProductFactory $auctionProductFactory,
        \Magento\Framework\App\ResponseInterface $response,
        EventManager $eventManager,
        AuctionProductMShipMapFactory $auctionProductMShipMapFactory
    ) {

        $this->helper = $helper;
        $this->request = $request;
        $this->orderFactory = $orderFactory;
        $this->_auctionProductFactory = $auctionProductFactory;
        $this->messageManager = $messageManager;
        $this->itemFactory = $itemFactory;
        $this->productFactory = $productFactory;
        $this->catalogControllerProductView = $catalogControllerProductView;
        $this->feerecordCollection = $feerecordCollection;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->_url = $url;
        $this->_response = $response;
        $this->walletTransactionFactory = $walletTransactionFactory;
        $this->_eventManager = $eventManager;
        $this->auctionProductMShipMapFactory = $auctionProductMShipMapFactory;
    }

    public function execute(Observer $observer)
    {
        if ($this->isAuctionEnable() && $this->isWalletEnable() && $this->isRefundable()) {
            $order = $observer->getEvent()->getOrder();
            $itemId = $this->getItemId($order);
            $orderId = $order->getId();
            $auctionDiscount = abs($order->getAuctionDiscount());
            $getIsMultishipping = $order->getIsMultishipping();

            $getItem = $this->itemFactory->create()->getCollection()
                ->addFieldToFilter('item_id', $itemId)->getFirstItem();

            $getOrder = $this->orderFactory->create()
                ->getCollection()
                ->addFieldToFilter('entity_id', $orderId)->getFirstItem();
            $customerId = $getOrder->getCustomerId();
            $productId = $getItem->getProductId();
            if ($getIsMultishipping) {
                $auctionProductMShipMapFactory = $this->auctionProductMShipMapFactory->create()
                                                ->load($orderId, 'order_id');
                $auctionActPro = $this->_auctionProductFactory->create()
                                ->load($auctionProductMShipMapFactory->getAuctionId());
            } else {
                $auctionActPro = $this->_auctionProductFactory->create()->getCollection()
                        ->addFieldToFilter('product_id', ['eq' => $productId])
                        ->addFieldToFilter('auction_status', 4)
                        ->addFieldToFilter('order_id', $orderId)
                        ->addFieldToFilter('status', 1)->getFirstItem();
               
            }
            
            if ($auctionActPro->getData()) {
                $walletTransaction = $this->walletTransactionFactory->create()->getCollection()
                                            ->addFieldToFilter('auction_id', $auctionActPro->getId())
                                            ->addFieldToFilter('action', 'credit')
                                            ->getFirstItem();
                if (empty($walletTransaction->getData())) {
                    $getAuction = $this->feerecordCollection
                    ->addFieldToFilter('product_id', $productId)
                    ->addFieldToFilter('customer_id', $customerId)
                    ->addFieldToFilter('start_auction_time', $auctionActPro->getStartAuctionTime())
                    ->addFieldToFilter('stop_auction_time', $auctionActPro->getStopAuctionTime())
                    ->getFirstItem();
                    $entity_id = $getAuction->getId();
                    $status = 1;

                    $this->catalogControllerProductView->refundAmount(
                        $customerId,
                        $entity_id,
                        $productId,
                        $status,
                        $orderId,
                        $auctionDiscount
                    );
                }
               
            }
        }
    }

    public function getItemId($order)
    {
        $items = $order->getAllVisibleItems();
        $getItemId = 0;
        foreach ($items as $item) {
            $getItemId = $item->getItemId();
        }
        return $getItemId;
    }

    /**
     * get Auction Configuration
     *
     * @return array
     */
    public function getAuctionConfiguration()
    {
        return $this->helper->getAuctionConfiguration();
    }

    /**
     * is AuctionEnable
     *
     * @return boolean
     */
    public function isAuctionEnable()
    {
        $getAuctionConfiguration = $this->getAuctionConfiguration();
        return $getAuctionConfiguration['enable'];
    }

    /**
     * is Wallet Enable
     *
     * @return boolean
     */
    public function isWalletEnable()
    {
        $getAuctionConfiguration = $this->getAuctionConfiguration();
        return $getAuctionConfiguration['enable_wallet'];
    }
    /**
     * get Refund Status
     *
     * @return boolean
     */
    public function isRefundable()
    {
        return $this->helper->getRefundStatus();
    }
}
