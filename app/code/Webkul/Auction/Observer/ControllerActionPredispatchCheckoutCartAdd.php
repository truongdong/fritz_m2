<?php
/**
 * Webkul Auction ControllerActionPredispatchCheckoutCartAdd Observer Model.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Customer\Model\SessionFactory as CustomerSession;

class ControllerActionPredispatchCheckoutCartAdd implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Webkul\Auction\Model\ProductFactory
     */
    protected $_auctionProductFactory;

    /**
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Checkout\Model\Session             $checkoutSession
     * @param \Webkul\Auction\Model\ProductFactory        $auctionProductFactory
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Webkul\Auction\Model\ProductFactory $auctionProductFactory,
        \Webkul\Auction\Helper\Data $helper,
        CustomerSession $customerSession,
        ProductFactory $product
    ) {

        $this->_messageManager = $messageManager;
        $this->_checkoutSession = $checkoutSession;
        $this->_auctionProductFactory = $auctionProductFactory;
        $this->product = $product;
        $this->helper = $helper;
        $this->customerSession = $customerSession->create();
    }

    /**
     * add to cart event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $cartItems = $this->_checkoutSession->getQuote()->getAllItems();
        $auctionProductStatus = false;
        $auctionProductId = 0;
        $proId = $observer->getRequest()->getParam('product');
        $auctionOpt = $this->product->create()->load($proId)->getAuctionType();
        $auctionOpt = explode(',', $auctionOpt);
        $auctionProductData = $this->_auctionProductFactory->create()->getCollection()
            ->addFieldToFilter('product_id', ['eq' => $proId])
            ->addFieldToFilter('auction_status', ['in' => [1,0]])->setPageSize(1)->getLastItem();
        $data = $observer->getRequest()->getParams();
        if (!in_array(1, $auctionOpt) && $auctionProductData->getEntityId()) {
                $auctionProductDataTemp = $this->_auctionProductFactory->create()
                                                        ->getCollection()
                                                        ->addFieldToFilter('product_id', ['eq' => $proId])
                                                        ->addFieldToFilter('auction_status', ['in' => [0]])
                                                        ->addFieldToFilter('status', ['in' => [0]])
                                                        ->setPageSize(1)->getLastItem();
                $tempId = $auctionProductDataTemp ? $auctionProductDataTemp->getId() : 0;
                $winnerBidDetail = $this->helper->getWinnerBidDetail($tempId);
            if (!($winnerBidDetail && $this->customerSession->getCustomerId() == $winnerBidDetail->getCustomerId())) {
                $this->_messageManager->addError(__('You can not add auction product to cart.'));
                $observer->getRequest()->setParam('product', false);
                return $this;
            }
        }
        $qty = 0;
        $maxQty = $auctionProductData->getMaxQty();
        $nonAuctionProduct = false;
        foreach ($cartItems as $item) {
            $auctionProduct = $this->_auctionProductFactory->create()->getCollection()
                    ->addFieldToFilter('product_id', ['eq' => $item->getProductId()])
                    ->addFieldToFilter('status', ['eq' => 0])->getLastItem();
            if ($auctionProduct->getEntityId()) {
                $auctionProductStatus = true;
                $auctionProductId = $auctionProduct->getProductId();
            }
            if ($item->getProductId() == $proId) {
                $qty = $qty + $item->getQty();
            } else {
                $nonAuctionProduct = true;
            }
        }

        if ($auctionProductStatus && ($auctionProductId != $proId)) {
            $msg = 'You can not add another product with auction Product.';
            $this->_messageManager->addError(__($msg));
            $observer->getRequest()->setParam('product', false);
            return $this;
        }

        if ($auctionProductData->getEntityId()) {
            if ($nonAuctionProduct) {
                $msg = 'You can not add auction Product with another product.';
                $this->_messageManager->addError(__($msg));
                $observer->getRequest()->setParam('product', false);
                return $this;
            }
            if ($qty == $maxQty) {
                $msg = 'Maximum '. $maxQty.' quantities are allowed to purchase this item.';
                $this->_messageManager->addNotice(__($msg));
                $observer->getRequest()->setParam('product', false);
                return $this;
            } elseif ($qty < $maxQty && $observer->getRequest()->getParam('qty') > $maxQty-$qty) {
                $msg = 'Maximum '. $maxQty.' quantities are allowed to purchase this item.';
                $this->_messageManager->addNotice(__($msg));
                $observer->getRequest()->setParam('qty', ($maxQty-$qty));
                return $this;
            }
        }
        return $this;
    }
}
