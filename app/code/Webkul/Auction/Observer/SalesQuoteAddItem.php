<?php
/**
 * Webkul Auction SalesQuoteAddItem Observer.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Observer;

use Magento\Framework\Event\ObserverInterface;

class SalesQuoteAddItem implements ObserverInterface
{

    /**
     * @var \Webkul\Auction\Model\ProductFactory
     */

    protected $_messageManager;

    /**
     * @var \Webkul\Auction\Model\ProductFactory
     */

    protected $_customerSession;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Webkul\Auction\Model\ProductFactory
     */
    protected $_auctionProductFactory;

    /**
     * @var \Webkul\Auction\Helper\Data
     */

    protected $_dataHelper;

    /**
     * @var \Webkul\Auction\Model\WinnerDataFactory
     */

    protected $_winnerData;
    protected $_orderFactory;
    /**
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Customer\Model\Session             $customerSession
     * @param \Magento\Framework\App\RequestInterface     $request
     * @param \Webkul\Auction\Model\ProductFactory        $auctionProductFactory
     * @param \Webkul\Auction\Helper\Data                 $dataHelper
     * @param \Webkul\Auction\Model\WinnerDataFactory     $winnerData
     */
    public function __construct(
        \Magento\Sales\Model\OrderFactory $order,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\RequestInterface $request,
        \Webkul\Auction\Model\ProductFactory $auctionProductFactory,
        \Webkul\Auction\Helper\Data $dataHelper,
        \Webkul\Auction\Model\WinnerDataFactory $winnerData
    ) {
        $this->_orderFactory = $order;
        $this->_messageManager = $messageManager;
        $this->_customerSession = $customerSession;
        $this->_request = $request;
        $this->_auctionProductFactory = $auctionProductFactory;
        $this->_dataHelper = $dataHelper;
        $this->_winnerData = $winnerData;
    }

    /**
     * Sales quote add item event handler.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->_customerSession->isLoggedIn()) {
            $customerId = $this->_customerSession->getCustomerId();
            $item = $observer->getQuoteItem();
            $product = $item->getProduct();
            $productId = $product->getId();
           
            if ($product->getTypeId()=='configurable') {
                $superAttribute = $this->_request->getParam('super_attribute');
                if (($superAttribute) && (!empty($superAttribute))) {
                    $pro = $product->getTypeInstance(true)->getProductByAttributes($superAttribute, $product);
                    $productId = $pro->getId();
                }
            }
            $biddingProductCollection = $this->_winnerData->create()->getCollection()
                                                    ->addFieldToFilter('product_id', ['eq' => $productId])
                                                    ->addFieldToFilter('status', ['eq' => 1])
                                                    ->addFieldToFilter('customer_id', ['eq' => $customerId])
                                                    ->addFieldToFilter('complete', ['eq' => 0])
                                                    ->setOrder('auction_id')->getFirstItem();
            if ($biddingProductCollection->getEntityId()) {
                $auctionId = $biddingProductCollection->getAuctionId();
                $auctionPro = $this->_auctionProductFactory->create()->load($auctionId);
                if ($auctionPro->getEntityId() && $auctionPro->getAuctionStatus() == 0) {
                    $bidWinPrice = $biddingProductCollection->getWinAmount();
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $storeManager = $objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);
                    $priceCurrency = $objectManager->get(\Magento\Framework\Pricing\PriceCurrencyInterface::class);
        
                    $currency = null;
                    $store = $storeManager->getStore()->getStoreId();
                    $bidWinPrice = $priceCurrency->convert($bidWinPrice, $store, $currency);

                    $item->setOriginalCustomPrice($bidWinPrice);
                    $item->getProduct()->setIsSuperMode(true);
                }
            }
        }
        $helper = $this->_dataHelper;
        $walletProductId = $helper->getWalletProductId();
        $amount = 0;
        $params = $this->_request->getParams();
        $minimumAmount = $helper->getMinimumAmount();
        $maximumAmount = $helper->getMaximumAmount();
        $currencySymbol = $helper->getCurrencySymbol($helper->getCurrentCurrencyCode());
        $event = $observer->getQuoteItem();
        $product = $event->getProduct();
        $productId = $product->getId();
        if ($minimumAmount > $maximumAmount) {
            $temp = $minimumAmount;
            $minimumAmount = $maximumAmount;
            $maximumAmount = $temp;
        }
        $currentCurrenyCode = $helper->getCurrentCurrencyCode();
        $baseCurrenyCode = $helper->getBaseCurrencyCode();

        $finalminimumAmount = $helper->getwkconvertCurrency(
            $baseCurrenyCode,
            $currentCurrenyCode,
            $minimumAmount
        );
        $finalmaximumAmount = $helper->getwkconvertCurrency(
            $baseCurrenyCode,
            $currentCurrenyCode,
            $maximumAmount
        );
        $controllerAction = $this->_request->getActionName();
        $walletAmountExists = $this->getExistsWalletPrice();
        if ($controllerAction == 'reorder') {
            $orderId = $params['order_id'];
            $order = $this->_orderFactory->create()->load($orderId);
            $orderItems = $order->getAllItems();
            foreach ($orderItems as $orderItem) {
                if ($orderItem->getProductId() == $productId) {
                    if ($productId == $walletProductId) {
                        $amount = $orderItem->getPrice();
                        $this->updateWalletProductAmount(
                            $amount,
                            $finalminimumAmount,
                            $finalmaximumAmount,
                            $event,
                            $walletAmountExists
                        );
                    }
                }
            }
        } else {
            if (array_key_exists('price', $params)) {
                $amount = $params['price'];
            }
            if ($productId == $walletProductId) {
                $this->updateWalletProductAmount(
                    $amount,
                    $finalminimumAmount,
                    $finalmaximumAmount,
                    $event,
                    $walletAmountExists
                );
            }
        }
        return $this;
    }

    public function updateWalletProductAmount(
        $amount,
        $finalminimumAmount,
        $finalmaximumAmount,
        $event,
        $walletAmountExists
    ) {
        $currencySymbol = $this->_dataHelper->getCurrencySymbol(
            $this->_dataHelper->getCurrentCurrencyCode()
        );
        $finalAmount = $walletAmountExists + $amount;
        $setAmount = $amount;
        if ($finalAmount > $finalmaximumAmount) {
            $setAmount = $finalmaximumAmount-$walletAmountExists;
            $this->_messageManager->addNotice(
                __(
                    'You can not add more than %1 amount to your wallet.',
                    $currencySymbol.$finalmaximumAmount
                )
            );
        } elseif ($finalAmount < $finalminimumAmount) {
            $setAmount = $finalminimumAmount+$walletAmountExists;
            $this->_messageManager->addNotice(
                __(
                    'You can not add less than %1 amount to your wallet.',
                    $currencySymbol.$finalminimumAmount
                )
            );
        }
        $event->setOriginalCustomPrice($setAmount);
        $event->setCustomPrice($setAmount);
        $event->getProduct()->setIsSuperMode(true);
    }

    public function getExistsWalletPrice()
    {
        $price = 0;
        return $price;
    }
}
