<?php
/**
 * Webkul Auction ControllerActionPostdispatchCheckoutCartAdd Observer.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Observer;

use Magento\Framework\Event\ObserverInterface;

class ControllerActionPostdispatchCheckoutCartAdd implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Checkout\Helper\Cart
     */
    protected $_cartHelper;
    /**
     * @var \Webkul\Auction\Model\ProductFactory
     */
    protected $_auctionProductFactory;

    /**
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Checkout\Model\Session             $checkoutSession
     * @param \Webkul\Auction\Model\ProductFactory        $cartHelper
     * @param \Webkul\Auction\Model\ProductFactory        $auctionProductFactory
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Helper\Cart $cartHelper,
        \Webkul\Auction\Model\ProductFactory $auctionProductFactory
    ) {

        $this->_messageManager = $messageManager;
        $this->_checkoutSession = $checkoutSession;
        $this->_cartHelper = $cartHelper;
        $this->_auctionProductFactory = $auctionProductFactory;
    }

    /**
     * add to cart event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $controller = $observer->getControllerAction();
        $cartItems = $this->_checkoutSession->getQuote()->getAllItems();
        $idArray = [];
        $proTypeArray = ['configurable','bundle'];
        $flag = false;
        foreach ($cartItems as $item) {
            if (!in_array($item->getProduct()->getTypeId(), $proTypeArray)) {
                $auctionProduct = $this->_auctionProductFactory->create()
                                            ->getCollection()
                                            ->addFieldToFilter('product_id', ['eq' => $item->getProductId()])
                                            ->addFieldToFilter('status', ['eq' => 0])->getFirstItem();
                //is auction avilable
                if ($auctionProduct->getEntityId()) {
                    $cartAddedItems = $this->_cartHelper->getCart()->getItems();

                    /*remove product from cart if that product
                     *is not auction product*/
                    foreach ($cartAddedItems as $cartItem) {
                        if ($item->getItemId() != $cartItem->getItemId()) {
                            $this->_cartHelper->getCart()->removeItem($cartItem->getItemId())->save();
                            $flag = true;
                        }
                    }
                }
            }
        }
        if ($flag) {
            $msg='Only Auction product can be added to cart!.';
            $this->_messageManager->addError(__($msg));
        }
        return $this;
    }
}
