<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Sales\Model\OrderFactory;
use Webkul\Auction\Model\WallettransactionFactory;
use Webkul\Auction\Model\WalletrecordFactory;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Framework\DB\Transaction;
use Webkul\Auction\Model\WalletUpdateData;
use Magento\Quote\Model\QuoteRepository;
use Magento\Framework\Session\SessionManager;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Webkul\Auction\Model\AuctionProductMShipMapFactory;

class AuctionSalesOrderPlaceAfter implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;
    /**
     * @var \Webkul\Auction\Helper\Data
     */
    protected $_helper;
    /**
     * @var \Webkul\Auction\Helper\Email
     */
    protected $_mailHelper;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;
    /**
     * @var Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $_stockRegistry;
    /**
     * @var InvoiceSender
     */
    protected $_invoiceSender;
    /**
     * @var Magento\Sales\Model\OrderFactory;
     */
    protected $_orderModel;
    /**
     * @var Webkul\Auction\Model\WallettransactionFactory
     */
    protected $_walletTransaction;
    /**
     * @var WalletrecordFactory
     */
    protected $_walletrecordFactory;
    /**
     * @var Magento\Sales\Model\Service\InvoiceService
     */
    protected $_invoiceService;
    /**
     * @var Magento\Framework\DB\Transaction
     */
    protected $_dbTransaction;
    /**
     * @var Webkul\Auction\Model\WalletUpdateData
     */
    protected $walletUpdateData;
    /**
     * @var QuoteRepository
     */
    protected $_quoteRepository;
    /**
     * @var SessionManager
     */
    protected $_coreSession;
    /**
     * @var OrderRepositoryInterface
     */
    protected $_orderRepository;
   /**
    * @var CustomerSession
    */
    protected $_customerSession;

    /**
     * @var AuctionProductMShipMapFactory
     */
    protected $auctionProductMShipMapFactory;
    /**
     * @param \Magento\Framework\Stdlib\DateTime\DateTime          $date
     * @param \Webkul\Auction\Helper\Data                          $helper
     * @param \Webkul\Auction\Helper\Email                          $mailHelper
     * @param \Magento\Checkout\Model\Session                      $checkoutSession
     * @param \Magento\Catalog\Model\Product                       $productFactory
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param InvoiceSender                                        $invoiceSender
     * @param OrderFactory                                         $orderModel
     * @param WallettransactionFactory                             $walletTransaction
     * @param WalletrecordFactory                                  $walletRecordModel
     * @param InvoiceService                                       $invoiceService
     * @param Transaction                                          $dbTransaction
     * @param AuctionProductMShipMapFactory                        $auctionProductMShipMapFactory
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\Auction\Helper\Data $helper,
        \Webkul\Auction\Helper\Email $mailHelper,
        \Webkul\Auction\Model\WinnerDataFactory $winnerData,
        \Webkul\Auction\Model\Product $auctionProduct,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\Product $productFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        InvoiceSender $invoiceSender,
        OrderFactory $orderModel,
        WallettransactionFactory $walletTransaction,
        WalletrecordFactory $walletRecordModel,
        InvoiceService $invoiceService,
        Transaction $dbTransaction,
        WalletUpdateData $walletUpdateData,
        QuoteRepository $quoteRepository,
        SessionManager $coreSession,
        CustomerSession $customerSession,
        OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\ResourceModel\Order $orderResourceModel,
        AuctionProductMShipMapFactory $auctionProductMShipMapFactory
    ) {
        $this->_date = $date;
        $this->_helper = $helper;
        $this->_mailHelper = $mailHelper;
        $this->_winnerData = $winnerData;
        $this->_auctionProduct = $auctionProduct;
        $this->_productFactory = $productFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_stockRegistry = $stockRegistry;
        $this->_invoiceSender = $invoiceSender;
        $this->_orderModel = $orderModel;
        $this->_walletTransaction = $walletTransaction;
        $this->_walletrecordFactory = $walletRecordModel;
        $this->_invoiceService = $invoiceService;
        $this->_dbTransaction = $dbTransaction;
        $this->walletUpdateData = $walletUpdateData;
        $this->_quoteRepository = $quoteRepository;
        $this->_coreSession = $coreSession;
        $this->_customerSession = $customerSession;
        $this->_orderRepository = $orderRepository;
        $this->orderResourceModel = $orderResourceModel;
        $this->auctionProductMShipMapFactory = $auctionProductMShipMapFactory;
    }

    /**
     * sales order place after.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        
        $isMultiShipping = $this->_checkoutSession->getQuote()->getIsMultiShipping();
        $customerId = $this->_customerSession->getCustomerId();
        if (!$isMultiShipping) {
            $auctionProductId = $this->_helper->getWalletProductId();
            $orderId = $observer->getOrder()->getId();
            $order = $this->_orderRepository->get($orderId);
            if ($this->alreadyAddedInData($order)) {
                return;
            }
            $this->setDataInWalletTable($orderId, $order);
            foreach ($order->getAllItems() as $item) {
                $activeAucId = $this->_helper->getActiveAuctionId($item->getProductId());
                $auctionWinData = $this->_winnerData->create()->getCollection()
                                            ->addFieldToFilter('product_id', $item->getProductId())
                                            ->addFieldToFilter('status', 1)
                                            ->addFieldToFilter('complete', 0)
                                            ->addFieldToFilter('customer_id', $customerId)
                                            ->addFieldToFilter('auction_id', $activeAucId)
                                            ->getFirstItem();

                if ($auctionWinData->getEntityId()) {
                    $winnerBidDetail = $this->_helper->getWinnerBidDetail($auctionWinData->getAuctionId());
                    if ($winnerBidDetail) {
                        //bider bid row update
                        $winnerBidDetail->setShop(1);
                        $this->saveObj($winnerBidDetail);
                        //update winner Data
                        $auctionWinData->setComplete(1);
                        $this->saveObj($auctionWinData);

                        $aucPro = $this->_auctionProduct->load($auctionWinData->getAuctionId());
                        //here we set auction process completely done
                        $aucPro->setAuctionStatus(4);
                        $aucPro->setStatus(1);
                        $aucPro->setOrderId($orderId);
                        $this->saveObj($aucPro);
                    }
                }
            }
        } else {
            $quoteId = $this->_checkoutSession->getLastQuoteId();
            $quote = $this->_quoteRepository->get($quoteId);
            $getAllShippingAddresses = $quote->getAllShippingAddresses();
            if ($quote->getIsMultiShipping() == 1 || $isMultiShipping == 1) {
                $orderIds = $this->_coreSession->getOrderIds();
                $getOrderCount = count($orderIds);
                $count = 0;
                $discountAmount = 0;
                $baseDiscountAmount = 0;
                foreach ($orderIds as $orderId => $orderIncId) {
                    $lastOrderId = $orderId;
                   
                    $order = $this->_orderRepository->get($lastOrderId);
                    foreach ($getAllShippingAddresses as $key => $address) {
                        if ($count == $key) {
                            $discountAmount = $address->getAuctionDiscount();
                            $baseDiscountAmount = $address->getBaseAuctionDiscount();
                            $count++;
                            break;
                        }
                    }
                    $order->setAuctionDiscount($discountAmount);
                    $order->setIsMultishipping(1);
                    $order->setBaseAuctionDiscount($baseDiscountAmount);
                    $this->orderResourceModel->save($order);
                   
                    if ($this->alreadyAddedInData($order)) {
                        continue;
                    }
                    $this->setDataInWalletTable($lastOrderId, $order);
                   
                    foreach ($order->getAllItems() as $item) {
                        
                        $activeAucId = $this->_helper->getActiveAuctionId($item->getProductId());
                        /*this is multishipping  order mapping with auction product id
                        so we can refund amount in wallet for multishipping as well*/
                       
                        $auctionProductMShipMapFactory = $this->auctionProductMShipMapFactory->create();
                        $auctionProductMShipMapFactory->addData([
                            "auction_id" => $activeAucId,
                            "order_id" => $orderId
                            ]);
                        $saveData = $auctionProductMShipMapFactory->save();
                       
                        $auctionWinData = $this->_winnerData->create()->getCollection()
                                                    ->addFieldToFilter('product_id', $item->getProductId())
                                                    ->addFieldToFilter('status', 1)
                                                    ->addFieldToFilter('complete', 0)
                                                    ->addFieldToFilter('customer_id', $customerId)
                                                    ->addFieldToFilter('auction_id', $activeAucId)
                                                    ->getFirstItem();
                        $winnerBidDetail = $this->_helper->getWinnerBidDetail($auctionWinData->getAuctionId());
                        if ($auctionWinData->getEntityId() && $winnerBidDetail && $getOrderCount == $count) {
                                //bider bid row update
                                $winnerBidDetail->setShop(1);
                                $this->saveObj($winnerBidDetail);
                                //update winner Data
                                $auctionWinData->setComplete(1);
                                $this->saveObj($auctionWinData);

                                $aucPro = $this->_auctionProduct->load($auctionWinData->getAuctionId());
                                //here we set auction process completely done
                                $aucPro->setAuctionStatus(4);
                                $aucPro->setStatus(1);
                                $this->saveObj($aucPro);
                                
                        }
                      
                    }
                }
                
            }
        }
    }
    public function setDataInWalletTable($orderId, $order)
    {
        $auctionProductId = $this->_helper->getWalletProductId();
        $customerId = $order->getCustomerId();
        $currencyCode = $order->getOrderCurrencyCode();
        $incrementId = $order->getIncrementId();
        $flag = 0;
        if ($orderId) {
            foreach ($order->getAllItems() as $item) {
                $productId = $item->getProductId();
                if ($productId == $auctionProductId) {
                    $price = number_format($item->getBasePrice(), 2, '.', '');
                    $currPrice = number_format($item->getPrice(), 2, '.', '');
                    $flag = 1;
                }
            }
        }
        $totalAmount = 0;
        $usedAmount = 0;
        $remainingAmount = 0;
        if ($flag == 1) {
            $transferAmountData = [
                'customerid' => $customerId,
                'walletamount' => $price,
                'walletactiontype' => 'credit',
                'curr_code' => $currencyCode,
                'curr_amount' => $currPrice,
                'walletnote' => __('Order id : %1 credited amount', $incrementId),
                'order_id' => $orderId,
                'status' => 0,
                'increment_id' => $incrementId
            ];
            $this->walletUpdateData->creditAmount($customerId, $transferAmountData);
        }
        $this->updateWaletProductQuantity($auctionProductId);
    }

    public function alreadyAddedInData($order)
    {
        $status = false;
        $transactionCollection = $this->_walletTransaction
            ->create()
            ->getCollection()
            ->addFieldToFilter('order_id', $order->getId());

        if ($transactionCollection->getSize()) {
            $status = true;
        }
        return $status;
    }
    public function updateWaletProductQuantity($auctionProductId)
    {
        $product = $this->_productFactory->load($auctionProductId); //load product which you want to update stock
        $stockItem = $this->_stockRegistry->getStockItem($auctionProductId); // load stock of that product
        $stockItem->setData('manage_stock', 0);
        $stockItem->setData('use_config_notify_stock_qty', 0);
        $stockItem->save(); //save stock of item
        $this->_stockRegistry->updateStockItemBySku($this->_helper::AUCTION_PRODUCT_SKU, $stockItem);
        $product->setStockData(
            [
                'use_config_manage_stock' => 0,
                'manage_stock' => 0
            ]
        )->save(); //  also save product
    }
    
    /**
     * saveObj
     * @param Object
     * @return void
     */
    private function saveObj($object)
    {
        $object->save();
    }
}
