<?php
/**
 * Webkul Auction CheckoutCartProductAddAfter Observer.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\CatalogInventory\Api\StockStateInterface;

class CheckoutCartProductAddAfter implements ObserverInterface
{
    /**
     * @var \Magento\Customer\Model\Session
     */

    protected $_customerSession;

    /**
     * @var \Magento\Checkout\Model\Session
     */

    protected $_checkoutSession;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */

    protected $_request;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */

    protected $_messageManager;

    /**
     * @var \Magento\CatalogInventory\Api\StockStateInterface
     */

    protected $_stockStateInterface;

    /**
     * @var \Webkul\Auction\Model\WinnerDataFactory
     */

    protected $_winnerData;

    /**
     * @var \Webkul\Auction\Model\ProductFactory
     */
    protected $_auctionProductFactory;
    /**
     * @param \Magento\Customer\Model\Session         $customerSession
     * @param \Magento\Checkout\Model\Session         $checkoutSession
     * @param RequestInterface                        $request
     * @param ManagerInterface                        $messageManager
     * @param StockStateInterface                     $stockStateInterface
     * @param \Webkul\Auction\Model\WinnerDataFactory $winnerData
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        RequestInterface $request,
        ManagerInterface $messageManager,
        StockStateInterface $stockStateInterface,
        \Webkul\Auction\Model\WinnerDataFactory $winnerData,
        \Webkul\Auction\Helper\Data $helperData,
        \Webkul\Auction\Model\ProductFactory $auctionProductFactory,
        \Magento\Catalog\Model\Product $productModel
    ) {
        $this->_auctionProductFactory = $auctionProductFactory;
        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_request = $request;
        $this->_messageManager = $messageManager;
        $this->_stockStateInterface = $stockStateInterface;
        $this->_winnerData = $winnerData;
        $this->_helperData = $helperData;
        $this->_productModel = $productModel;
    }

    /**
     * Sales quote add item event handler.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $maxqty=0;
        if ($this->_customerSession->isLoggedIn()) {
            $customerId = $this->_customerSession->getCustomerId();
            $event = $observer->getQuoteItem();
            $product = $event->getProduct();
            $productId = $product->getId();
            $params = $this->_request->getParams();
            $cartData = $this->_checkoutSession->getQuote()->getAllItems();
            $this->getBiddingCustomer($cartData, $params, $product, $customerId, $maxqty);
        }
        return $this;
    }

    public function getBiddingCustomer($cartData, $params, $product, $customerId, $maxqty)
    {
        $mainArray=[];
        $productId = $product->getId();
        $totalQty = 0;
        foreach ($cartData as $cart) {
            if (isset($params['product']) && $cart->getProduct()->getId() == $params['product']) {
                $totalQty = $totalQty + $cart->getQty();
            }
        }
        foreach ($cartData as $cart) {
            if (isset($params['product']) && $cart->getProduct()->getId() == $params['product']) {
                if (($product->getTypeId() == 'configurable') && (isset($params['super_attribute'])
                 && (!empty($params['super_attribute']) > 0))) {
                            $pro = $product->getTypeInstance(true)
                                            ->getProductByAttributes($params['super_attribute'], $product);
                            $productId = $pro->getId();
                }
                // To Do -need to updated logic
                $auctionId = $this->_helperData->getActiveAuctionId($productId);
                $bidProCol = $this->_winnerData->create()->getCollection()
                                        ->addFieldToFilter('product_id', $productId)
                                        ->addFieldToFilter('complete', 0)->addFieldToFilter('status', 1)
                                        ->addFieldToFilter('customer_id', $customerId)
                                        ->addFieldToFilter('auction_id', $auctionId);
                $biddingProductCollection =
                $bidProCol->setOrder('auction_id')->getFirstItem();
                if ($biddingProductCollection->getEntityId()) {
                    $auctionId = $biddingProductCollection->getAuctionId();
                    $auctionPro = $this->_auctionProductFactory->create()->load($auctionId);
                    if ($auctionPro->getEntityId() && $auctionPro->getAuctionStatus() == 0) {
                        $bidWinPrice = $biddingProductCollection->getWinAmount();
                        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                        $storeManager = $objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);
                        $priceCurrency = $objectManager->get(\Magento\Framework\Pricing\PriceCurrencyInterface::class);
                        $currency = null;
                        $store = $storeManager->getStore()->getStoreId();
                        $bidWinPrice = $priceCurrency->convert($bidWinPrice, $store, $currency);
                        $cart->setOriginalCustomPrice($bidWinPrice);
                        $cart->getProduct()->setIsSuperMode(true);
                    }
                }
                if (!empty($bidProCol)) {
                    foreach ($bidProCol as $winner) {
                        $maxqty+=$winner->getMaxQty();
                        $customerArray['max']=$winner->getMaxQty();
                        $customerArray['min']=$winner->getMinQty();
                        $mainArray[$winner->getCustomerId()]=$customerArray;
                    }
                }
                $this->productAddToCart($customerId, $mainArray, $product, $params, $totalQty, $cart, $maxqty);
            }
        }
    }

    public function productAddToCart($customerId, $mainArray, $product, $params, $totalQty, $cart, $maxqty)
    {
        
        $productId = $product->getId();
        $products=$this->_productModel->load($productId);
        $sku = $products->getSku();
        if (array_key_exists($customerId, $mainArray) && $product->getTypeId() != 'downloadable') {
            $itemQty = $totalQty;
            $customerMaxQty = $mainArray[$customerId]['max'];
            $customerMinQty = $mainArray[$customerId]['min'];
            if ($itemQty > $customerMaxQty) {
                $msg='Maximum '. $customerMaxQty.' quantities are allowed to purchase this item.';
                $this->_messageManager->addNotice(__($msg));
                $cart->setQty($customerMaxQty);
                $this->saveObj($cart);
            } elseif ($itemQty < $customerMinQty) {
                $this->_messageManager
                        ->addNotice(
                            __('Minimum '. $customerMinQty .' quantities are allowed to purchase this item.')
                        );
                $cart->setQty($customerMinQty);
                $this->saveObj($cart);
            }
        } elseif ($product->getTypeId() != 'downloadable') {
            if ($this->_helperData->getWalletProductId() != $productId) {
                if (($product->getTypeId() == 'configurable') && (isset($params['super_attribute'])
                && (!empty($params['super_attribute']) > 0))) {
                           $pro = $product->getTypeInstance(true)
                                           ->getProductByAttributes($params['super_attribute'], $product);
                           $productId = $pro->getId();
                }
        
                $stockQty = $this->_stockStateInterface->getStockQty($productId);
                $availqty = $stockQty - $maxqty;
                if ($availqty > 0) {
                    if ($cart->getQty() > $availqty) {
                        $this->_messageManager
                            ->addNotice(
                                __('You can add Only '. $availqty .' quantities to purchase this item.')
                            );
                        $cart->setQty($availqty);
                        $this->saveObj($cart);
                    }
                }
            } elseif ($sku!='wk_auction_amount') {
                $this->_messageManager->addNotice(__('You can not add this quantities for purchase.'));
                $cart->setQty($cart->getQty()-$params['qty']);
                $this->saveObj($cart);
            }
        }
    
        return $this;
    }

    /**
     * saveObj
     * @param Object
     * @return void
     */
    private function saveObj($object)
    {
        $object->save();
    }
}
