<?php

namespace Webkul\Auction\Observer;

/**
 * Webkul_Auction Product View Observer.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

use Magento\Framework\Event\ObserverInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Customer\Model\CustomerFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Customer\Model\Session as CustomerSession;

/**
 * Reports Event observer model.
 */
class CatalogControllerProductView implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $_dateTime;

    /**
     * @var Configurable
     */
    protected $_configurableProTypeModel;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $_customer;

    /**
     * @var Product
     */
    protected $_product;

    /**
     * @var \Webkul\Auction\Helper\Data
     */
    protected $_helperData;

    /**
     * @var \Webkul\Auction\Helper\Email
     */
    protected $_helperEmail;

    /**
     * @var \Webkul\Auction\Model\ProductFactory
     */
    protected $_auctionProductFactory;

    /**
     * @var \Webkul\Auction\Model\Amount
     */
    protected $_auctionAmount;

    /**
     * @var \Webkul\Auction\Model\AutoAuction
     */
    protected $_autoAuction;

    /**
     * @var \Webkul\Auction\Model\WinnerData
     */
    protected $_winnerData;
    protected $catalogsession;
    /**
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param Configurable                                $configProTypeModel
     * @param Customer                                    $customer
     * @param Product                                     $product
     * @param \Webkul\Auction\Helper\Data                 $helperData
     * @param \Webkul\Auction\Helper\Email                $helperEmail
     * @param \Webkul\Auction\Model\ProductFactory        $auctionProductFactory
     * @param \Webkul\Auction\Model\AmountFactory         $auctionAmount
     * @param \Webkul\Auction\Model\AutoAuctionFactory    $autoAuction
     * @param \Webkul\Auction\Model\WinnerDataFactory     $winnerData
     */
    protected $logger;
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        Configurable $configProTypeModel,
        CustomerSession $customerSession,
        CustomerFactory $customer,
        ProductFactory $product,
        \Webkul\Auction\Helper\Data $helperData,
        \Webkul\Auction\Helper\Email $helperEmail,
        \Webkul\Auction\Model\ProductFactory $auctionProductFactory,
        \Webkul\Auction\Model\FeerecordFactory $feerecordFactory,
        \Webkul\Auction\Model\FeetransactionFactory $feetransactionFactory,
        \Webkul\Auction\Model\WalletUpdateData $walletUpdateData,
        \Webkul\Auction\Model\AmountFactory $auctionAmount,
        \Webkul\Auction\Model\AutoAuctionFactory $autoAuction,
        \Webkul\Auction\Model\WinnerDataFactory $winnerData,
        \Magento\Framework\Session\SessionManagerInterface $catalogSession,
        \Psr\Log\LoggerInterface $logger,
        \Webkul\Auction\Helper\Data $Helper
    ) {
        $this->catalogsession = $catalogSession;
        $this->_dateTime = $dateTime;
        $this->_configurableProTypeModel = $configProTypeModel;
        $this->_customer = $customer;
        $this->_customerSession = $customerSession;
        $this->_product = $product;
        $this->_helperData = $helperData;
        $this->_helperEmail = $helperEmail;
        $this->_auctionProductFactory = $auctionProductFactory;
        $this->_feerecordFactory = $feerecordFactory;
        $this->_feetransactionFactory = $feetransactionFactory;
        $this->_walletUpdateData = $walletUpdateData;
        $this->_auctionAmount = $auctionAmount;
        $this->_autoAuction = $autoAuction;
        $this->_winnerData = $winnerData;
        $this->logger = $logger;
        $this->_Helper= $Helper;
    }

    /**
     * View Catalog Product View observer.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        $product = $observer->getEvent()->getProduct();
        $productId = $product->getId();
        $this->catalogsession->setProductId($productId);
        
        if ($product && $auctionConfig['enable']) {
            $productId = $product->getId();
            if ($product->getTypeId() == 'configurable') {
                $childPro = $this->_configurableProTypeModel->getChildrenIds($productId);
                $childProIds = isset($childPro[0]) ? $childPro[0]:[0];
                $auctionActPro = $this->_auctionProductFactory->create()->getCollection()
                                            ->addFieldToFilter('product_id', ['in' => $childProIds])
                                            ->addFieldToFilter('auction_status', 1)
                                            ->addFieldToFilter('status', 1);
                if (!empty($auctionActPro)) {
                    foreach ($auctionActPro as $value) {
                        $this->biddingOperation($value->getProductId());
                    }
                }
            } else {
                $this->biddingOperation($productId);
            }
        }
        return $this;
    }

    /**
     * biddingOperation
     * @param int $productId on which auction apply
     * @return void
     */
    public function biddingOperation($productId)
    {
       
        $winnerCustomerId = '';
        $bidDay = '';
        $stop = '';
        $addToWin = 0;
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        $auctionActPro = $this->_auctionProductFactory->create()->getCollection()
                                ->addFieldToFilter('product_id', ['eq' => $productId])
                                ->addFieldToFilter('auction_status', 1)
                                ->addFieldToFilter('status', 0)->getFirstItem();
        if ($auctionActPro->getEntityId()) {
            $resPrice = 0;
            $bidDay = $auctionActPro->getDays();
            $bidId = $auctionActPro->getEntityId();

            $incPrice = $auctionActPro->getIncrementPrice();
            $resPrice = $auctionActPro->getReservePrice();
            $datestop = strtotime($auctionActPro->getStopAuctionTime());
            $datestart = strtotime($auctionActPro->getStartAuctionTime());

            if ($datestop >= $datestart) {
                $winDataTemp['auction_id'] = $auctionActPro->getEntityId();
                $today = $this->_dateTime->date('Y-m-d H:i:s');
                $current = strtotime($today);
                $difference = $datestop - $current;
                if ($difference <= 0) {
                    if (!$auctionActPro->getIsProcessing()) {
                        $auctionActPro->setIsProcessing(1)->save();
                        $this->productBidWinner($productId, $bidId, $auctionConfig, $auctionActPro, $resPrice);
                        $auctionActPro->setIsProcessing(0)->save();
                    }
                } else {
                    $winnerDataModel = $this->_winnerData->create()->getCollection()
                                                    ->addFieldToFilter('product_id', ['eq' => $productId])
                                                    ->addFieldToFilter('status', ['eq'=>1])
                                                    ->getFirstItem();
                    if ($winnerDataModel->getEntityId()) {
                        $bidDay = $winnerDataModel->getDays() ? 1: $winnerDataModel->getDays();
                        $current = strtotime($this->_dateTime->date('Y-m-d H:i:s'));
                        $day = strtotime($winnerDataModel->getStopBiddingTime().' + '.$bidDay.' days');
                        $difference = $day - $current;
                        if ($difference < 0) {
                            $winnerDataModel->setStatus(0);
                            $winnerDataModel->setId($winnerDataModel->getEntityId());
                            $this->saveObj($winnerDataModel);
                        }
                    }
                }
            }
        }
    }
    
    public function productBidWinner($productId, $bidId, $auctionConfig, $auctionActPro, $resPrice)
    {
        $winDataTemp=[];
        $bidArray = [];
        $autoBidAmount = '';
        $bidAmountList = $this->_auctionAmount->create()->getCollection()
                                ->addFieldToFilter('product_id', ['eq' => $productId])
                                ->addFieldToFilter('auction_id', ['eq' => $bidId])
                                ->addFieldToFilter('status', ['eq' => 1])
                                ->setOrder('auction_amount', 'ASC');
        foreach ($bidAmountList as $bidAmt) {
            $bidArray[$bidAmt->getCustomerId()] = $bidAmt->getAuctionAmount();
        }
        $autoBidArray = [];
        $autoBidAmountArray = [];
        $status = true;
        if (!empty($bidArray)) {
            $customerIds = array_keys($bidArray, max($bidArray));
            $winDataTemp['customer_id'] = $customerIds[0];
            $winDataTemp['amount'] = max($bidArray);
            $winDataTemp['type'] = 'normal';
        }
        $autoBidFlag = false;
        if ($auctionConfig['auto_enable'] && $auctionActPro->getAutoAuctionOpt()) {
            $autoBidList = $this->_autoAuction->create()->getCollection()
                                    ->addFieldToFilter('auction_id', ['eq' => $bidId])
                                    ->addFieldToFilter('status', ['eq' => 1]);
            if (!empty($bidArray)) {
                $autoBidList->addFieldToFilter('amount', ['gteq'=> max($bidArray)]);
                $winDataTemp['amount'] = max($bidArray);
            } else {
                $resPrice = $auctionActPro->getReservePrice();
                $starPrice = $auctionActPro->getStartingPrice();
                if (!empty($autoBidList->getData())) {
                    foreach ($autoBidList as $autoBid) {
                        $autoBidAmountArray[] = $autoBid->getAmount();
                    }
                }
                if (!empty($autoBidAmountArray) && (max($autoBidAmountArray) >= $resPrice)) {
                    $winDataTemp['amount'] = max($autoBidAmountArray);
                } else {
                    $status = false;
                }
            }
            if (!empty($autoBidList->getData()) && $status) {
                foreach ($autoBidList as $autoBid) {
                    $custId = $autoBid->getCustomerId();
                    $autoBidArray[$custId] = $autoBid->getAmount();
                }
                $customerIds = array_keys($autoBidArray, max($autoBidArray));
                $winDataTemp['customer_id'] = $customerIds[0];
                $winDataTemp['type'] = 'auto';
                $autoBidAmount = $autoBidList->setOrder('amount', 'DESC')->getFirstItem();
            }
        }
        $this->saveWinnerRecord(
            $bidAmountList,
            $winDataTemp,
            $resPrice,
            $auctionActPro,
            $auctionConfig,
            $autoBidAmount,
            $bidId,
            $productId
        );
        $auctionActPro->setId($auctionActPro->getEntityId());
        $this->saveObj($auctionActPro);
    }

    public function saveWinnerRecord(
        $bidAmountList,
        $winDataTemp,
        $resPrice,
        $auctionActPro,
        $auctionConfig,
        $autoBidAmount,
        $bidId,
        $productId
    ) {
        if (isset($winDataTemp['customer_id']) && $resPrice <= $winDataTemp['amount']) {
            
            //save winner record in winner data table
            $feeAmount = $this->getBidFeeAmount($winDataTemp['customer_id'], $auctionActPro);
            $winningAmount = $winDataTemp['amount'] - $feeAmount;
            if ($winningAmount < 0) {
               
                if ($this->_helperData->getWalletStatus()) {
                    $this->refundAmount(
                        $winDataTemp['customer_id'],
                        $bidId,
                        $productId,
                        $status = 0,
                        $orderId = 0,
                        $feeAmount
                    );
                }
            } else {
                $this->sendWinnerNotification($winDataTemp, $productId, $bidId, $auctionConfig);
                $winnerDataModel = $this->_winnerData->create();
                $auctionModel = $this->_auctionProductFactory->create()->load($bidId)->getData();
                $auctionModel['customer_id'] = $winDataTemp['customer_id'];
                $auctionModel['status'] = 1;
                $auctionModel['auction_id'] = $auctionModel['entity_id'];
                $auctionModel['win_amount'] = $auctionModel['min_amount'];
                $auctionModel['show_amount'] = $winningAmount;
                unset($auctionModel['entity_id']);
                $currentCustomerId = $this->_customerSession->getCustomerId();
                if ($this->checkWinnerDetailSaved($auctionModel)) {
                    $winnerDataModel->setData($auctionModel);
                    $this->saveObj($winnerDataModel);
                }
                
                $auctionActPro->setAuctionStatus(0);
                if ($this->_helperData->getWalletStatus()) {
                    $this->refundFeeAmountInWallet($winDataTemp['customer_id'], $auctionActPro);
                }
            }
        } elseif (isset($winDataTemp['type']) && $auctionConfig['auto_enable'] && $winDataTemp['type'] == 'auto'
         && $resPrice <= $autoBidAmount->getAmount()) {
            $autoBiddList = $this->_autoAuction->create()->getCollection()
            ->addFieldToFilter('auction_id', ['eq' => $bidId])
            ->addFieldToFilter('status', 1);
            $winnerAmount = $auctionActPro->getReservePrice();
            $this->sendNotificationt(
                $auctionActPro,
                $autoBiddList,
                $winDataTemp,
                $productId,
                $winnerAmount,
                $auctionConfig
            );
            //save winner record in winner data table
            $feeAmount = $this->getBidFeeAmount($winDataTemp['customer_id'], $auctionActPro);
            $winningAmount = $winnerAmount - $feeAmount;
            $winnerDataModel = $this->_winnerData->create();
            $auctionModel = $this->_auctionProductFactory->create()->load($bidId)->getData();
            $auctionModel['customer_id'] = $winDataTemp['customer_id'];
            $auctionModel['status'] = 1;
            $auctionModel['auction_id'] = $auctionModel['entity_id'];
            $auctionModel['win_amount'] = $winnerAmount;
            $auctionModel['show_amount'] = $winningAmount;
            unset($auctionModel['entity_id']);
            if ($this->checkWinnerDetailSaved($auctionModel)) {
                $winnerDataModel->setData($auctionModel);
                $this->saveObj($winnerDataModel);
            }
            $auctionActPro->setAuctionStatus(0);
            if ($this->_helperData->getWalletStatus()) {
                $this->refundFeeAmountInWallet($winDataTemp['customer_id'], $auctionActPro);
            }
        } else {
            $auctionActPro->setAuctionStatus(2);
            if ($this->_helperData->getWalletStatus()) {
                $this->refundFeeAmountInWallet(0, $auctionActPro);
            }
            foreach ($bidAmountList as $bidAmt) {
                $bidAmt->setStatus(0)->save();
            }
        }
    }

    public function sendNotificationt(
        $auctionActPro,
        $autoBiddList,
        $winDataTemp,
        $productId,
        $winnerAmount,
        $auctionConfig
    ) {
    
        if ($auctionActPro->getReservePrice() < $auctionActPro->getMinAmount()) {
            $winnerAmount = $auctionActPro->getMinAmount();
        }
        foreach ($autoBiddList as $autoBid) {
            if ($autoBid->getCustomerId() == $winDataTemp['customer_id']) {
                $autoBid->setFlag(1);
                $autoBid->setWinningPrice($winnerAmount);
                if ($auctionConfig['enable_winner_notify_email']) {
                    $this->_helperEmail->sendWinnerMail(
                        $winDataTemp['customer_id'],
                        $productId,
                        $winnerAmount
                    );
                }
            }
            $autoBid->setStatus(0);
            $autoBid->setId($autoBid->getEntityId());
            $this->saveObj($autoBid);
        }
    }

    public function sendWinnerNotification($winDataTemp, $productId, $bidId, $auctionConfig)
    {
        $currentCustomerId = $this->_customerSession->getCustomerId();
        if ($winDataTemp['type']=='auto') {
            $normalBidList = $this->_auctionAmount->create()->getCollection()
                                  ->addFieldToFilter('product_id', ['eq' => $productId])
                                  ->addFieldToFilter('auction_id', ['eq' => $bidId]);
            foreach ($normalBidList as $normalBid) {
                $normalBid->setWinningStatus(2);
                $normalBid->setId($normalBid->getEntityId());
                $this->saveObj($normalBid);
            }
            $autoBiddList = $this->_autoAuction->create()->getCollection()
                                    ->addFieldToFilter('auction_id', ['eq' => $bidId])
                                    ->addFieldToFilter('status', 1);
            
            foreach ($autoBiddList as $autoBid) {
                if ($autoBid->getCustomerId() == $winDataTemp['customer_id']) {
                    $autoBid->setFlag(1);
                    $autoBid->setWinningPrice($winDataTemp['amount']);
                    /** send notification mail to winner */
                    if ($auctionConfig['enable_winner_notify_email']) {
                        $this->_helperEmail->sendWinnerMail(
                            $winDataTemp['customer_id'],
                            $productId,
                            $winDataTemp['amount']
                        );
                    }
                } elseif ($autoBid->getCustomerId() != $winDataTemp['customer_id']) {
                    $autoBid->setFlag(2);
                    $autoBid->setId($autoBid->getEntityId());
                }
                $autoBid->setStatus(0);
                $autoBid->setId($autoBid->getEntityId());
                $this->saveObj($autoBid);
            }
        } elseif ($winDataTemp['type']== 'normal') {
            $normalBidList = $this->_auctionAmount->create()->getCollection()
                                  ->addFieldToFilter('product_id', ['eq' => $productId])
                                  ->addFieldToFilter('auction_id', ['eq' => $bidId])
                                  ->addFieldToFilter('status', ['eq' => 1]);
            foreach ($normalBidList as $normalBid) {
                if ($normalBid->getCustomerId() == $winDataTemp['customer_id']) {
                    $normalBid->setWinningStatus(1);
                    $normalBid->setStatus(0);
                    $normalBid->setId($normalBid->getEntityId());
                    $this->saveObj($normalBid);
               
                    /** send notification mail to winner */
                    if ($auctionConfig['enable_winner_notify_email']) {
                        $this->_helperEmail->sendWinnerMail(
                            $winDataTemp['customer_id'],
                            $productId,
                            $winDataTemp['amount']
                        );
                    }
                } elseif ($winDataTemp['customer_id']!=$normalBid->getCustomerId()) {
                    $normalBid->setWinningStatus(2);
                    $normalBid->setStatus(0);
                    $normalBid->setId($normalBid->getEntityId());
                    $this->saveObj($normalBid);
                }
            }
        }
    }

    /**
     * saveObj
     * @param Object
     * @return void
     */
    private function saveObj($object)
    {
        $object->save();
    }

    public function getBidFeeAmount($customerId, $auctionProductModel)
    {
        $amount = 0;
        $feeRecordModel = $this->_feerecordFactory->create()->getCollection()
                        ->addFieldToFilter('customer_id', $customerId)
                        ->addFieldToFilter('product_id', $auctionProductModel->getProductId())
                        ->addFieldToFilter('start_auction_time', $auctionProductModel->getStartAuctionTime())
                        ->addFieldToFilter('stop_auction_time', $auctionProductModel->getStopAuctionTime())
                        ->getFirstItem()->getData();
        if (!empty($feeRecordModel)) {
            $transactionModel = $this->_feetransactionFactory->create()->getCollection()
                              ->addFieldToFilter('customer_id', $feeRecordModel['customer_id'])
                              ->addFieldToFilter('bidfee_id', $feeRecordModel['entity_id'])
                              ->addFieldToFilter('status', 0);
            if ($transactionModel->getSize()) {
                foreach ($transactionModel as $transactions) {
                    $amount = $amount + $transactions->getDeductedAmount();
                    $transactions->setStatus(1)->save();
                }
            }
        }
        return $amount;
    }

    public function refundFeeAmountInWallet($customerId, $auctionProductModel)
    {
        $feeRecordModel = $this->_feerecordFactory->create()->getCollection()
                          ->addFieldToFilter('customer_id', ['neq'=>$customerId])
                          ->addFieldToFilter('product_id', $auctionProductModel->getProductId())
                          ->addFieldToFilter('start_auction_time', $auctionProductModel->getStartAuctionTime())
                          ->addFieldToFilter('stop_auction_time', $auctionProductModel->getStopAuctionTime());
        if ($feeRecordModel->getSize()) {
            foreach ($feeRecordModel as $record) {
                $this->refundAmount(
                    $record->getCustomerId(),
                    $record->getEntityId(),
                    $auctionProductModel->getProductId()
                );
            }
        }
    }

    public function refundAmount($customerId, $bidId, $productId, $status = 0, $orderId = 0, $amount = 0)
    {
    
        $transactionModel = $this->_feetransactionFactory->create()->getCollection()
        ->addFieldToFilter('customer_id', $customerId)
        ->addFieldToFilter('bidfee_id', $bidId)
        ->addFieldToFilter('refund_status', 1)
        ->addFieldToFilter('status', $status);
        if ($orderId == 0) {
            if ($transactionModel->getSize()) {
                foreach ($transactionModel as $transactions) {
                    $amount = $amount + $transactions->getDeductedAmount();
                }
            }
        }
       
        if ($amount > 0) {
            $productName = $this->_product->create()->load($productId)->getName();
            $currentCurrencyCode = $this->_helperData->getCurrentCurrencyCode();
            $transferAmountData = [
                   'customerid' => $customerId,
                   'walletamount' => $amount,
                   'walletactiontype' => 'credit',
                   'curr_code' => $currentCurrencyCode,
                   'curr_amount' => $amount,
                   'walletnote' => __('Refund of Bidding on %1 : credited amount', $productName),
                   'order_id' => $orderId,
                   'status' => 1,
                   'auction_id' => $bidId
            ];
            $result = $this->_walletUpdateData->creditAmount($customerId, $transferAmountData);
            if ($result['success']) {
                foreach ($transactionModel as $transactions) {
                    $transactions->setStatus(5)->save();
                }
            }
        }
        return true;
    }

    public function checkWinnerDetailSaved($auctionModel)
    {
        $winnerDataModel = $this->_winnerData->create()->getCollection()
                         ->addFieldToFilter('auction_id', $auctionModel['auction_id'])
                         ->addFieldToFilter('product_id', $auctionModel['product_id'])
                         ->addFieldToFilter('customer_id', $auctionModel['customer_id'])
                         ->addFieldToFilter('start_auction_time', $auctionModel['start_auction_time'])
                         ->addFieldToFilter('stop_auction_time', $auctionModel['stop_auction_time'])
                         ->addFieldToFilter('complete', 0);
        if ($winnerDataModel->getSize()) {
            return false;
        }
        return true;
    }
}
