<?php
/**
 * Webkul Auction CatalogProductSaveAfter Observer.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Observer;

use Magento\Framework\Event\ObserverInterface;

class CatalogProductSaveAfter implements ObserverInterface
{
    /**
     * @var \Webkul\Auction\Model\ProductFactory
     */
    protected $_auctionProductFactory;

    /**
     * @param \Webkul\Auction\Model\ProductFactory $auctionProductFactory
     */
    public function __construct(
        \Webkul\Auction\Model\ProductFactory $auctionProductFactory
    ) {
        $this->_auctionProductFactory = $auctionProductFactory;
    }

    /**
     * product save event handler.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getDataObject();
        $auctionOpt = $product->getAuctionType();
        $auctionOpt = explode(',', $auctionOpt);
        $productId = $product->getId();
        $auctionProduct = $this->_auctionProductFactory
                                    ->create()->getCollection()
                                    ->addFieldToFilter(
                                        'product_id',
                                        ['eq' => $product->getId()]
                                    )->getFirstItem();
        return $this;
    }
}
