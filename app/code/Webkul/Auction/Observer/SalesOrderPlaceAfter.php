<?php
/**
 * Webkul Auction SalesOrderPlaceAfter Observer
 *
 * @category    Webkul
 *
 * @author      Webkul Software Pvt. Ltd.
 */
namespace Webkul\Auction\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session as CustomerSession;

class SalesOrderPlaceAfter implements ObserverInterface
{
    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * @var \Webkul\Auction\Model\WinnerDataFactory
     */
    protected $_winnerData;

    /**
     * @var \Webkul\Auction\Helper\Data
     */
    protected $_helperData;
    /**
     * @param CustomerSession $customerSession
     * @param Webkul\Auction\Model\WinnerDataFactory
     * @param Webkul\Auction\Model\Product
     * @param Webkul\Auction\Helper\Data
     */
    public function __construct(
        CustomerSession $customerSession,
        \Webkul\Auction\Model\WinnerDataFactory $winnerData,
        \Webkul\Auction\Model\Product $auctionProduct,
        \Webkul\Auction\Helper\Data $helperData
    ) {
    
        $this->_customerSession = $customerSession;
        $this->_winnerData = $winnerData;
        $this->_auctionProduct = $auctionProduct;
        $this->_helperData = $helperData;
    }

    /**
     * after place order event handler.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customerId = $this->_customerSession->getCustomerId();
        $order = $observer->getOrder();
        foreach ($order->getAllItems() as $item) {
            $auctionWinData = $this->_winnerData->create()
                                        ->getCollection()
                                        ->addFieldToFilter(
                                            'product_id',
                                            $item->getProductId()
                                        )->addFieldToFilter(
                                            'status',
                                            1
                                        )->addFieldToFilter(
                                            'complete',
                                            0
                                        )->addFieldToFilter(
                                            'customer_id',
                                            $customerId
                                        )->setOrder('auction_id')
                                        ->getFirstItem();
            
            if ($auctionWinData->getEntityId()) {
                $winnerBidDetail = $this->_helperData
                                        ->getWinnerBidDetail(
                                            $auctionWinData->getAuctionId()
                                        );
                if ($winnerBidDetail) {
                    //bider bid row update
                    $winnerBidDetail->setShop(1)->save();

                    //update winner Data
                    $auctionWinData->setComplete(1);
                    $auctionWinData->save();

                    $aucPro = $this->_auctionProduct->load($auctionWinData->getAuctionId());
                    //here we set auction process completely done
                    $aucPro->setStatus(0)->save();
                }
            }
        }
        return $this;
    }
}
