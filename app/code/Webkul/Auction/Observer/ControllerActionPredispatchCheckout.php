<?php
/**
 * Webkul Auction ControllerActionPostdispatchCheckout Observer.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\Auction\Model\WinnerDataFactory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProTypeModel;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Quote\Model\Quote\ItemFactory;

class ControllerActionPredispatchCheckout implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;

    /**
     * @var \Webkul\Auction\Model\WinnerDataFactory
     */
    private $winnerData;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    /**
     * @var \Magento\Checkout\Helper\Cart
     */
    private $cartHelper;

    /**
     * @var \Webkul\Auction\Model\ProductFactory
     */
    private $auctionProductFactory;

    /**
     * @var ConfigurableProTypeModel
     */
    protected $configurableProTypeModel;

    /**
     * @var TimezoneInterface
     */
    protected $_timezoneInterface;
    /**
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Checkout\Model\Session             $checkoutSession
     * @param \Webkul\Auction\Model\ProductFactory      $cartHelper
     * @param \Webkul\Auction\Model\ProductFactory      $auctionProductFactory
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Helper\Cart $cartHelper,
        \Webkul\Auction\Helper\Data $helper,
        \Webkul\Auction\Model\ProductFactory $auctionProductFactory,
        ConfigurableProTypeModel $configurableProTypeModel,
        WinnerDataFactory $winnerData,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        ItemFactory $itemFactory,
        \Magento\Framework\App\ResponseInterface $response,
        TimezoneInterface $timezoneInterface,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->messageManager = $messageManager;
        $this->checkoutSession = $checkoutSession;
        $this->cartHelper = $cartHelper;
        $this->_helper = $helper;
        $this->winnerData = $winnerData;
        $this->_storeManager = $storeManager;
        $this->_itemFactory = $itemFactory;
        $this->_response = $response;
        $this->configurableProTypeModel = $configurableProTypeModel;
        $this->auctionProductFactory = $auctionProductFactory;
        $this->_timezoneInterface = $timezoneInterface;
        $this->logger = $logger;
    }

    /**
     * add to cart event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $controller = $observer->getControllerAction();
        $cartItems = $this->checkoutSession->getQuote()->getAllItems();
        $idArray = [];
        $proTypeArray = ['configurable','bundle'];
        $flag = false;
        $auctionWalletProductId = $this->_helper->getWalletProductId();
        $minimumAmount = $this->_helper->getMinimumAmount();
        $maximumAmount = $this->_helper->getMaximumAmount();
        if ($minimumAmount > $maximumAmount) {
            $temp = $minimumAmount;
            $minimumAmount = $maximumAmount;
            $maximumAmount = $temp;
        }
        $currentCurrenyCode = $this->_helper->getCurrentCurrencyCode();
        $baseCurrenyCode = $this->_helper->getBaseCurrencyCode();

        $finalminimumAmount = $this->_helper->getwkconvertCurrency(
            $baseCurrenyCode,
            $currentCurrenyCode,
            $minimumAmount
        );
        $finalmaximumAmount = $this->_helper->getwkconvertCurrency(
            $baseCurrenyCode,
            $currentCurrenyCode,
            $maximumAmount
        );
      
        $currencySymbol = $this->_helper->getCurrencySymbol(
            $this->_helper->getCurrentCurrencyCode()
        );
        return $this;
    }
    public function addCartProduct(
        $cartItems,
        $finalmaximumAmount,
        $currencySymbol,
        $auctionWalletProductId,
        $finalminimumAmount
    ) {
    
        $amount = 0;
        $itemId = '';
        $itemIds = '';
        $price = '';
        $walletflag = 0;
        $walletItemId = 0;
        $walletInCart = 0;
        $otherInCart = 0;
        foreach ($cartItems as $item) {
            if ($item->getProduct()->getId() == $auctionWalletProductId) {
                $walletInCart = 1;
                $walletItemId = $item->getItemId();
                $amount = $item->getCustomPrice();
                if ($amount > $finalmaximumAmount) {
                    $amount = $finalmaximumAmount;
                    $walletflag = 1;
                    $this->messageManager->addNotice(
                        __(
                            'You can not add more than %1 amount to your wallet.',
                            $currencySymbol.$finalmaximumAmount
                        )
                    );
                } elseif ($amount < $finalminimumAmount) {
                    $amount = $finalminimumAmount;
                    $walletflag = 1;
                    $this->messageManager->addNotice(
                        __(
                            'You can not add less than %1 amount to your wallet.',
                            $currencySymbol.$finalminimumAmount
                        )
                    );
                }
                if ($walletflag == 1) {
                    $this->updateCartData($item, $amount);
                    $this->checkoutSession->getQuote()->setItemsQty(1);
                    $this->checkoutSession->getQuote()->setSubtotal($amount);
                    $this->checkoutSession->getQuote()->setGrandTotal($amount);
                    $storeManager = $this->_storeManager;
                    $currentStore = $storeManager->getStore();
                    $url = $currentStore->getBaseUrl().'checkout/cart/';
                    $this->_response->setRedirect($url)->sendResponse();
                }
            } else {
                $otherInCart = 1;
            }
            if ($walletInCart==1 && $otherInCart==1 && $walletItemId!=0) {
                $quote = $this->_itemFactory->create()->load($walletItemId);
                $quote->delete();
                $this->checkoutSession->getQuote()->save();
            }
            if ($item->getProduct()->getId() != $auctionWalletProductId) {
                $product = $item->getProduct();
                $productId = $this->getProductIdIfConfigurable($product);
                $this->winnerPriceSet($productId, $item);
            }
        }
    }

    public function winnerPriceSet($productId, $item)
    {
        $this->logger->info("auctionPeeeeero");
        $bidProCol = $this->winnerData->create()->getCollection()->addFieldToFilter('product_id', $productId)
        ->addFieldToFilter('status', 1)
        ->addFieldToFilter('complete', 0)
        ->setOrder('auction_id', 'ASC');
        $this->logger->info(json_encode($bidProCol));
        if (!empty($bidProCol)) {
            $this->logger->info("status");
            foreach ($bidProCol as $winner) {
                $day = strtotime($winner->getStopAuctionTime(). ' + '.$winner->getDays().' days');
                $difference = $day - strtotime($this->_timezoneInterface->date()->format('m/d/y H:i:s'));
                $this->logger->info(json_encode($difference));
                if ($difference > 0) {
                    $auctionPro = $this->auctionProductFactory->create()->getCollection()
                                    ->addFieldToFilter('entity_id', $winner->getAuctionId())
                                    ->addFieldToFilter('auction_status', 0)->setPageSize(1);

                    $auctionPro = $this->getFirstItemFromRecord($auctionPro);
                    if (!$auctionPro) {
                        $this->logger->info("auctionPro");
                        $customOptionPrice = $this->getCustomOptionPrice($item->getProduct());
                        $productPrice = $product->getPrice() + $customOptionPrice;
                        $item->setOriginalCustomPrice($productPrice);
                        $item->setCustomPrice($productPrice);
                        $item->getProduct()->setIsSuperMode(true);
                        $this->saveObj($item);
                    } else {
                        $bidWinPrice = $winner->getWinAmount();
                        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                        $storeManager = $objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);
                        $priceCurrency = $objectManager->get(\Magento\Framework\Pricing\PriceCurrencyInterface::class);
                        $currency = null;
                        $store = $storeManager->getStore()->getStoreId();
                        $bidWinPrice = $priceCurrency->convert($bidWinPrice, $store, $currency);
                        $item->setOriginalCustomPrice($bidWinPrice);
                        $item->getProduct()->setIsSuperMode(true);
                    }
                }
            }
            $this->saveObj($this->checkoutSession->getQuote()->collectTotals());
        }
    }
   
    /**
     * getFirstItemFromRecord
     * @param Collection Object
     * @return false | data
     */
    private function getFirstItemFromRecord($collection)
    {
        foreach ($collection as $row) {
            return $row;
        }
        return false;
    }

    /**
     * removeCartItem
     * @param int $itemId
     * @return void
     */
    private function removeCartItem($itemId)
    {
        $this->cartHelper->getCart()->removeItem($itemId)->save();
    }

    /**
     * getProductIdIfConfigurable
     * @param Magento\Catalog\Model\Product $product
     * @return int
     */
    private function getProductIdIfConfigurable($product)
    {
        $productId = $product->getEntityId();
        if ($product->getTypeId() == 'configurable') {
            $childPro = $this->configurableProTypeModel->getChildrenIds($product->getId());
            $childProIds = isset($childPro[0]) ? $childPro[0]:[0];

            $biddingCollection = $this->auctionProductFactory->create()->getCollection()
                                            ->addFieldToFilter('product_id', ['in'=>$childProIds])
                                            ->addFieldToFilter('status', 1);
            if (!empty($biddingCollection)) {
                foreach ($biddingCollection as $bidProduct) {
                    $proId=$bidProduct->getProductId();
                    if (in_array($proId, $proIds)) {
                        $productId = $bidProduct->getProductId();
                        break;
                    }
                }
            }
        }
        return $productId;
    }

    /**
     * saveObj
     * @param int $itemId
     * @return void
     */
    private function saveObj($object)
    {
        $object->save();
    }
    public function getCustomOptionPrice($product)
    {
        $finalPrice = 0;
        $optionIds = $product->getCustomOption('option_ids');
        if ($optionIds) {
            foreach (explode(',', $optionIds->getValue()) as $optionId) {
                if ($option = $product->getOptionById($optionId)) {
                    $confItemOption = $product->getCustomOption('option_' . $option->getId());

                    $group = $option->groupFactory($option->getType())
                        ->setOption($option)
                        ->setConfigurationItemOption($confItemOption);
                    $finalPrice += $group->getOptionPrice($confItemOption->getValue(), 0);
                }
            }
        }
        return $finalPrice;
    }

    public function updateCartData($cart, $amount)
    {
        $cart->setCustomPrice($amount);
        $cart->setOriginalCustomPrice($amount);
        $cart->setQty(1);
        $cart->getProduct()->setIsSuperMode(true);
        $cart->setRowTotal($amount);
        $cart->save();
    }
}
