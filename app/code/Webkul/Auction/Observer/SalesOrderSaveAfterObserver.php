<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Observer;

use Magento\Framework\Event\ObserverInterface;

class SalesOrderSaveAfterObserver implements ObserverInterface
{

    public function __construct(
        \Webkul\Auction\Helper\Data $Helper
    ) {
        $this->_Helper= $Helper;
    }
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $discountAmount = $observer->getQuote()->getAuctionDiscount();
        $baseDiscountAmount = $observer->getQuote()->getBaseAuctionDiscount();
        $order = $observer->getOrder();
        $order->setAuctionDiscount($discountAmount);
        $order->setBaseAuctionDiscount($baseDiscountAmount);
        $this->_Helper->cacheFlush();
    }
}
