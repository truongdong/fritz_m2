<?php
/**
 * Webkul Auction CheckoutCartUpdateItemsAfter Observer.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class CheckoutCartUpdateItemsAfter implements ObserverInterface
{
   /**
    * @var TimezoneInterface
    */
    private $_timezoneInterface;

    /**
     * @var \Magento\Customer\Model\Session
     */

    protected $_customerSession;

    /**
     * @var \Magento\Checkout\Model\Session
     */

    protected $_checkoutSession;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */

    protected $_request;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */

    protected $_messageManager;

    /**
     * @var \Magento\CatalogInventory\Api\StockStateInterface
     */

    protected $_stockStateInterface;

    /**
     * @var Webkul\Auction\Helper\Data
     */
    private $_auctionHelper;

    /**
     * @var \Webkul\Auction\Model\WinnerDataFactory
     */

    protected $_winnerData;

    /**
     * @var \Webkul\Auction\Model\ProductFactory
     */

    protected $_auctionProductFactory;

    /**
     * @param \Magento\Customer\Model\Session         $customerSession
     * @param \Magento\Checkout\Model\Session         $checkoutSession
     * @param RequestInterface                        $request
     * @param ManagerInterface                        $messageManager
     * @param StockStateInterface                     $stockStateInterface
     * @param Configurable                            $configurableProTypeModel
     * @param \Webkul\Auction\Helper\Data             $auctionHelper
     * @param \Webkul\Auction\Model\WinnerDataFactory $winnerData
     * @param \Webkul\Auction\Model\ProductFactory    $auctionProductFactory
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        RequestInterface $request,
        ManagerInterface $messageManager,
        StockStateInterface $stockStateInterface,
        Configurable $configurableProTypeModel,
        \Webkul\Auction\Helper\Data $auctionHelper,
        \Webkul\Auction\Model\WinnerDataFactory $winnerData,
        \Webkul\Auction\Model\ProductFactory $auctionProductFactory,
        \Psr\Log\LoggerInterface $logger,
        TimezoneInterface $timezoneInterface
    ) {

        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_request = $request;
        $this->_messageManager = $messageManager;
        $this->_stockStateInterface = $stockStateInterface;
        $this->_configurableProTypeModel = $configurableProTypeModel;
        $this->_auctionHelper = $auctionHelper;
        $this->_winnerData = $winnerData;
        $this->_auctionProductFactory = $auctionProductFactory;
        $this->logger = $logger;
        $this->_timezoneInterface = $timezoneInterface;
    }

    /**
     * Sales quote add item event handler.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $maxqty=0;
        $mainArray=[];
        if ($this->_customerSession->isLoggedIn()) {
            $cart = $this->_checkoutSession->getQuote()->getAllItems();
            $info = $observer->getInfo();
            $customerid = $this->_customerSession->getCustomerId();
            foreach ($cart as $item) {
                $proIds[]=$item->getProductId();
            }
            $totalQty = [];
            foreach ($cart as $item) {
                $totalQty[$item->getProductId()] = isset($totalQty[$item->getProductId()]) ?
                $totalQty[$item->getProductId()] + $item->getQty() : $item->getQty();
            }
            $this->cartQtyUpdate($cart, $info, $customerid, $mainArray, $totalQty, $maxqty, $proIds);
        }
        return $this;
    }

    public function cartQtyUpdate($cart, $info, $customerid, $mainArray, $totalQty, $maxqty, $proIds)
    {
        foreach ($cart as $item) {
            if (!isset($info[$item->getId()])) {
                continue;
            }
            $product = $item->getProduct();
            $productId = $item->getProductId();
            $this->configurableProductBid($product, $proIds);
           
            $bidProCol = $this->_winnerData->create()->getCollection()
                                                        ->addFieldToFilter('product_id', $productId)
                                                        ->addFieldToFilter('status', 1)
                                                        ->addFieldToFilter('complete', 0)
                                                        ->setOrder('auction_id', 'ASC');
            if (!empty($bidProCol)) {
                foreach ($bidProCol as $winner) {
                    $maxqty+=$winner->getMaxQty();
                    $customerArray['max']=$winner->getMaxQty();
                    $customerArray['min']=$winner->getMinQty();
                    $mainArray[$winner->getCustomerId()]=$customerArray;
                }
            }
            if (array_key_exists($customerid, $mainArray)) {
                $customerMaxQty=$mainArray[$customerid]['max'];
                $customerMinQty=$mainArray[$customerid]['min'];
                if ($totalQty[$item->getProductId()] <= $customerMaxQty &&
                 $totalQty[$item->getProductId()] >= $customerMinQty) {
                    $item->setQty($item->getQty());
                    $this->saveObj($item);
                } elseif ($totalQty[$item->getProductId()] > $customerMaxQty) {
                    $item->setQty($item->getQty() + $customerMaxQty - $totalQty[$item->getProductId()]);
                    $totalQty[$item->getProductId()] = $customerMaxQty;
                    $this->saveObj($item);
                    $this->_messageManager
                            ->addNotice(
                                __('Maximum '. $customerMaxQty .' quantities are allowed to purchase this item.')
                            );
                } elseif ($totalQty[$item->getProductId()]<$customerMinQty) {
                    $item->setQty($item->getQty() + $customerMinQty - $totalQty[$item->getProductId()]);
                    $totalQty[$item->getProductId()] = $customerMinQty;
                    $this->saveObj($item);
                    $this->_messageManager
                            ->addNotice(
                                __('Minimum '. $customerMinQty .' quantities are allowed to purchase this item.')
                            );
                }
            } else {
                if ($this->_auctionHelper->getWalletProductId() != $productId) {
                    $stockQty = $this->_stockStateInterface->getStockQty($productId);
                    $availqty =  (int) $stockQty - (int) $maxqty;
                    $this->productQtyAdd($availqty, $info, $item);
                } else {
                    $this->updateItemQty($item);
                    $this->_messageManager->addNotice(
                        __("You can not update auction wallet product's quantity.")
                    );
                }
            }
        }
    // }
    }

    public function configurableProductBid($product, $proIds)
    {
        if ($product->getTypeId() == 'configurable') {
            $childPro=$this->_configurableProTypeModel
                                    ->getChildrenIds($product->getId());

            $childProIds = isset($childPro[0]) ? $childPro[0]:[0];

            $biddingCollection=$this->_auctionProductFactory->create()->getCollection()
                                                ->addFieldToFilter('product_id', ['in'=>$childProIds])
                                                ->addFieldToFilter('status', 1);
            if (!empty($biddingCollection)) {
                foreach ($biddingCollection as $bidProduct) {
                    $proId=$bidProduct->getProductId();
                    if (in_array($proId, $proIds)) {
                        $productId = $bidProduct->getProductId();
                        break;
                    }
                }
            }
        }
    }
    public function productQtyAdd($availqty, $info, $item)
    {
        if ($availqty > 0) {
            if (array_key_exists('qty', $info[$item->getId()])) {
                if ($info[$item->getId()]['qty']>=$availqty) {
                    $item->setQty($availqty);
                    $this->_messageManager
                        ->addNotice(
                            __('Maximum '. $availqty .' quantities are allowed to purchase this item.')
                        );
                } else {
                    $item->setQty($info[$item->getId()]['qty']);
                }
                $this->saveObj($item);
            }
        } else {
            $item->setQty($item->getQty()-$info[$item->getId()]['qty']);
            $this->saveObj($item);
            $this->_messageManager->addNotice('you can not add this quantity for purchase.');
        }
    }

    /**
     * saveObj
     * @param Object
     * @return void
     */
    private function saveObj($object)
    {
        $object->save();
    }

    //set auction wallet product quantity 1
    public function updateItemQty($item)
    {
        $item->setQty(1);
        $item->save();
    }
}
