<?php
/**
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Block;

use Webkul\Auction\Model\ResourceModel\Amount\CollectionFactory;

/**
 * Auction detail block
 */
class Detail extends \Magento\Framework\View\Element\Template
{
    /**
     * @var string
     */
    protected $_template = 'auction/detail.phtml';

    /**
     * @var \Webkul\Auction\Model\ResourceModel\Amount\Collection
     */
    protected $_auctionAmtCollectionFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Webkul\Auction\Model\ResourceModel\Amount\Collection
     */
    protected $_auctionDetails;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param CollectionFactory $auctionAmtCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Model\ProductRepository $product,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        CollectionFactory $auctionAmtCollectionFactory,
        array $data = []
    ) {

        $this->_priceHelper = $priceHelper;
        $this->_auctionAmtCollectionFactory = $auctionAmtCollectionFactory;
        $this->_customerSession = $customerSession;
        $this->_product = $product;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('Auction Details'));
    }

    /**
     * @return bool|\Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public function getAuctionDetails()
    {
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            return false;
        }
        if (!$this->_auctionDetails) {
            $this->_auctionDetails = $this->_auctionAmtCollectionFactory->create()->addFieldToSelect(
                '*'
            )->addFieldToFilter(
                'customer_id',
                $customerId
            )->setOrder(
                'entity_id',
                'desc'
            );
        }
        return $this->_auctionDetails;
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getAuctionDetails()) {
            $pager = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager::class,
                'sales.order.history.pager'
            )->setCollection(
                $this->getAuctionDetails()
            );
            $this->setChild('pager', $pager);
            $this->getAuctionDetails()->load();
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @param int $id
     * @return string
     */
    public function getDeleteUrl($id)
    {
        return $this->getUrl('auction/account/deletebid', ['id' => $id]);
    }

    /**
     * @param int $productId
     * @return string
     */
    public function getProductDetail($productId)
    {
        $pro = $this->_product->getById($productId);
        return ['name'=> $pro->getName(), 'url' => $pro->getProductUrl()];
    }
    /**
     * get Formated price
     * @param $amount float
     * @return string
     *
     */
    public function formatPrice($amount)
    {
        return $this->_priceHelper->currency($amount, true, false);
    }

    /**
     * get Winning Status Label
     * @param $winningStatus int
     * @return string
     *
     */
    public function winningStatus($auctionData)
    {
        if ($auctionData->getStatus() == 0) {
            $status = $auctionData->getWinningStatus() == 1 ? __("Winner") : __("Lost");
        } else {
            $status = __("Pending");
        }
        return $status;
    }

    /**
     * get Auction Status Label
     * @param $status int
     * @return string
     *
     */
    public function status($status)
    {
        $label = [0 =>__('Complete'), 1 =>__('Pending')];
        return isset($label[$status]) ? $label[$status] : '--';
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }
}
