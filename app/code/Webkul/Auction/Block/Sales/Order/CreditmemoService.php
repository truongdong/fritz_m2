<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Webkul\Auction\Block\Sales\Order;

/**
 * Class CreditmemoService
 * override credit memo for admin
 *
 */

class CreditmemoService
{
    /**
     * @var \Magento\Sales\Api\CreditmemoRepositoryInterface
     */

    protected $creditmemosRepository;

    /**
     * @var \Magento\Sales\Api\CreditmemoCommentRepositoryInterface
     */
    protected $commentRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \Magento\Sales\Model\Order\CreditmemoNotifier
     */
    protected $creditmemoNotifier;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resource;

    /**
     * @var \Magento\Sales\Model\Order\RefundAdapterInterface
     */
    private $refundAdapter;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var \Magento\Sales\Api\InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * @param \Magento\Sales\Api\CreditmemoRepositoryInterface $creditmemosRepository
     * @param \Magento\Sales\Api\CreditmemoCommentRepositoryInterface $creditmemoCommentRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     * @param \Magento\Sales\Model\Order\CreditmemoNotifier $creditmemoNotifier
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     */
    public function __construct(
        \Magento\Sales\Api\CreditmemoRepositoryInterface $creditmemosRepository,
        \Magento\Sales\Api\CreditmemoCommentRepositoryInterface $creditmemoCommentRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Sales\Model\Order\CreditmemoNotifier $creditmemoNotifier,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\Event\ManagerInterface $eventManager
    ) {
        $this->creditmemosRepository = $creditmemosRepository;
        $this->commentRepository = $creditmemoCommentRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->creditmemoNotifier = $creditmemoNotifier;
        $this->priceCurrency = $priceCurrency;
        $this->eventManager = $eventManager;
    }

    /**
     * Prepare creditmemo to refund and save it.
     *
     * @param \Magento\Sales\Api\Data\CreditmemoInterface $creditmemo
     * @param bool $isOfflineRequested
     * @return \Magento\Sales\Api\Data\CreditmemoInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     *
     */

    public function refund(
        \Magento\Sales\Api\Data\CreditmemoInterface $creditmemo,
        $isOfflineRequested = false
    ) {
       
        $this->validateForRefund($creditmemo);
        $creditmemo->setState(\Magento\Sales\Model\Order\Creditmemo::STATE_REFUNDED);

        $connection = $this->getResource()->getConnection('sales');
        $connection->beginTransaction();
      
        try {
            $invoice = $creditmemo->getInvoice();
            
            if ($invoice && !$isOfflineRequested) {
                $invoice->setIsUsedForRefund(true);
                $invoice->setBaseTotalRefunded(
                    $invoice->getBaseTotalRefunded() + $creditmemo->getBaseGrandTotal()
                );
                $creditmemo->setInvoiceId($invoice->getId());
                $this->getInvoiceRepository()->save($creditmemo->getInvoice());
            }
            $order = $this->getCreditMemoRefundAdapter()->refund(
                $creditmemo,
                $creditmemo->getOrder(),
                !$isOfflineRequested
            );
            $this->creditmemosRepository->save($creditmemo);
            $this->getOrderRepository()->save($order);
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
        }

        return $creditmemo;
    }

   /**
    * Validates if credit memo is available for refund.
    *
    * @param \Magento\Sales\Api\Data\CreditmemoInterface $creditmemo
    * @return bool
    * @throws \Magento\Framework\Exception\LocalizedException
    */
    protected function validateForRefund(\Magento\Sales\Api\Data\CreditmemoInterface $creditmemo)
    {
       
        if ($creditmemo->getId() && $creditmemo->getState() != \Magento\Sales\Model\Order\Creditmemo::STATE_OPEN) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('We cannot register an existing credit memo.')
            );
        }

        $baseOrderRefundData = $this->getRoundPriceCurrency(
            $creditmemo->getOrder()->getBaseTotalRefunded()
            + $creditmemo->getBaseGrandTotal()
            + $creditmemo->getOrder()
            ->getAuctionDiscount()
        );
       
        if ($baseOrderRefundData
        > $this->getRoundPriceCurrency($creditmemo->getOrder()->getBaseTotalPaid())
        ) {
           
            $baseAvailableRefund = $creditmemo->getOrder()->getBaseTotalPaid()
                - $creditmemo->getOrder()->getBaseTotalRefunded();

            throw new \Magento\Framework\Exception\LocalizedException(
                __(
                    'The most money available to refund is %1.',
                    $creditmemo->getOrder()->getBaseCurrency()->formatTxt($baseAvailableRefund)
                )
            );
        }
        
        return true;
    }
    public function getRoundPriceCurrency($price)
    {
        return $this->priceCurrency->round($price);
    }

     /**
      * Initializes RefundAdapterInterface dependency.
      *
      * @return \Magento\Sales\Model\Order\RefundAdapterInterface
      * @deprecated 100.1.3
      */
    private function getCreditMemoRefundAdapter()
    {
         return $this->getRefundAdapter();
    }
    public function getRefundAdapter()
    {
        if ($this->refundAdapter === null) {
            $this->refundAdapter = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\Sales\Model\Order\RefundAdapterInterface::class);
        }
        return $this->refundAdapter;
    }
     /**
      * Initializes ResourceConnection dependency.
      *
      * @return \Magento\Framework\App\ResourceConnection|mixed
      * @deprecated 100.1.3
      */
    private function getResource()
    {
        return $this->getCreditmemoResource();
    }

    public function getCreditmemoResource()
    {
        if ($this->resource === null) {
            $this->resource = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\Framework\App\ResourceConnection::class);
        }
        return $this->resource;
    }

    /**
     * Initializes OrderRepositoryInterface dependency.
     *
     * @return \Magento\Sales\Api\OrderRepositoryInterface
     * @deprecated 100.1.3
     */
    private function getOrderRepository()
    {
        if ($this->orderRepository === null) {
            $this->orderRepository = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\Sales\Api\OrderRepositoryInterface::class);
        }
        return $this->orderRepository;
    }

   /**
    * Initializes InvoiceRepositoryInterface dependency.
    *
    * @return \Magento\Sales\Api\InvoiceRepositoryInterface
    * @deprecated 100.1.3
    */
    private function getInvoiceRepository()
    {
        return $this->invoiceRepository();
    }
    public function invoiceRepository()
    {
        if ($this->invoiceRepository === null) {
            $this->invoiceRepository = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\Sales\Api\InvoiceRepositoryInterface::class);
        }
        return $this->invoiceRepository;
    }
}
