<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Block\Sales\Order;

use Magento\Sales\Model\Order;

class AuctionDiscount extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Order
     */
    protected $_order;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_source;

    /**
     * need display full total info
     *
     * @return bool
     */
    public function displayFullSummary()
    {
        return true;
    }

    /**
     * Initialize order totals array
     *
     * @return $this
     */
    public function initTotals()
    {
       
        $parent = $this->getParentBlock();
        $this->_order = $parent->getOrder();
        $this->_source = $parent->getSource();
        $title = 'Wallet Deducted Amount';
        $store = $this->getStore();
        if ($this->_order->getAuctionDiscount()!=0) {
            $auctionDiscount = new \Magento\Framework\DataObject(
                [
                    'code' => 'auction_discount',
                    'strong' => false,
                    'value' => $this->_order->getAuctionDiscount(),
                    'label' => __($title),
                ]
            );
           
            $parent->addTotal($auctionDiscount, 'auction_discount');
        }
        return $this;
    }

    /**
     * Get order store object
     *
     * @return \Magento\Store\Model\Store
     */
    public function getStore()
    {
        return $this->_order->getStore();
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
       
        return $this->_order;
    }

    /**
     * @return array
     */
    public function getLabelProperties()
    {
        return $this->getParentBlock()->getLabelProperties();
    }

    /**
     * @return array
     */
    public function getValueProperties()
    {
        return $this->getParentBlock()->getValueProperties();
    }
}
