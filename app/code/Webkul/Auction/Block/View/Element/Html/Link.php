<?php
namespace Webkul\Auction\Block\View\Element\Html;

class Link extends \Magento\Framework\View\Element\Html\Link\Current
{
    public function _toHtml()
    {
        if (!$this->_scopeConfig->isSetFlag('wk_auction/auction_wallet/enable_wallet') ||
            !$this->_scopeConfig->isSetFlag('wk_auction/general_settings/enable')
        ) {
            return '';
        }
        return parent::_toHtml();
    }
}
