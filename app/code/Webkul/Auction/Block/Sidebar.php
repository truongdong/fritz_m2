<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Block;

class Sidebar extends \Magento\Wishlist\Block\Customer\Sidebar
{

    public function getHelper()
    {
        $ObjectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $wishlistHelper = $ObjectManager->create(\Magento\Wishlist\Helper\Data::class);
        return $wishlistHelper;
    }
}
