<?php
/**
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Block\Plugin;

use Magento\Catalog\Model\Product;
use Magento\Framework\Pricing\Render;

class Sidebar
{
     /**
     * @var buy it now
     */
    const BUY_IT_NOW = "1";
    /**
     * @var \Webkul\Auction\Helper\Data
     */
    private $helperData;

    public function __construct(\Webkul\Auction\Helper\Data $helperData)
    {
        $this->helperData = $helperData;
    }

    public function aroundGetProductPriceHtml(
        \Magento\Wishlist\Block\Customer\Sidebar $sidebar,
        $proceed,
        $product,
        $priceType,
        $renderZone,
        $arguments
    ) {
       
        $isEnable = $this->helperData->getAuctionConfiguration();
        $isAuctinAvail = $this->helperData->isAuctionAvail($product->getId());
        $isUserWinner = $this->helperData->isUserWinner($product->getId());
        $converttoArray = explode(',', $product->getAuctionType());
        $auctionDetail = $proceed(
            $product,
            $priceType,
            $renderZone,
            $arguments
        );
        if (($isEnable['enable'] && $isAuctinAvail)
        || in_array(self::BUY_IT_NOW, $converttoArray)
        || $isUserWinner
        ) {
            $auctionDetail = $auctionDetail.$this->helperData->getProductAuctionDetail($product);
        }
        return $auctionDetail;
    }
}
