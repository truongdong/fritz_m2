<?php
/**
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Block\Plugin;

class ProductListUpdateForAuction
{
    /**
     * @var buy it now
     */
    const BUY_IT_NOW = "1";
    /**
     * @var \Webkul\Auction\Helper\Data
     */
    protected $helperData;

    public function __construct(\Webkul\Auction\Helper\Data $helperData)
    {
        $this->helperData = $helperData;
    }

    public function aroundGetProductPrice(
        \Magento\Catalog\Block\Product\ListProduct $list,
        $proceed,
        $product
    ) {
        $isEnable = $this->helperData->getAuctionConfiguration();
        $isAuctinAvail = $this->helperData->isAuctionAvail($product->getId());
        $isUserWinner = $this->helperData->isUserWinner($product->getId());
        $converttoArray = explode(',', $product->getAuctionType());
        $auctionDetail = null;
        if (($isEnable['enable'] && $isAuctinAvail)
        || in_array(self::BUY_IT_NOW, $converttoArray)
        || $isUserWinner
        ) {
            $auctionDetail = $proceed($product).$this->helperData->getProductAuctionDetail($product);
        } else {
            $auctionDetail = $proceed($product);
        }
        return $auctionDetail;
    }
}
