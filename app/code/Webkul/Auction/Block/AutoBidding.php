<?php
/**
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Block;

use Webkul\Auction\Model\ResourceModel\AutoAuction\CollectionFactory;
use Webkul\Auction\Helper\Data as AuctionHelperData;

/**
 * Auction detail block
 */
class AutoBidding extends \Magento\Framework\View\Element\Template
{
    /**
     * @var string
     */
    protected $_template = 'auction/autobidding.phtml';

    /**
     * @var \Webkul\Auction\Model\ResourceModel\Amount\Collection
     */
    protected $_auctionAmtCollectionFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Webkul\Auction\Model\ResourceModel\Amount\Collection
     */
    protected $_autoAuctionDetails;
    /**
     * @var AuctionHelperData
     */
    protected $helperData;

    /**
     * Undocumented function
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Catalog\Model\ProductRepository $product
     * @param \Magento\Framework\Pricing\Helper\Data $priceHelper
     * @param CollectionFactory $autoAuctionCollectionFactory
     * @param Webkul\Auction\Helper\Data $helperData
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\SessionFactory $customerSession,
        \Magento\Catalog\Model\ProductRepository $product,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        CollectionFactory $autoAuctionCollectionFactory,
        AuctionHelperData $helperData,
        array $data = []
    ) {

        $this->_priceHelper = $priceHelper;
        $this->_autoAuctionCollectionFactory = $autoAuctionCollectionFactory;
        $this->_customerSession = $customerSession;
        $this->_product = $product;
        $this->helperData = $helperData;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('Auto Bidding Details'));
    }

    /**
     * @return bool|\Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public function getAutoAuctionDetails()
    {
        if (!($customerId = $this->_customerSession->create()->getCustomerId())) {
            return false;
        }
        if (!$this->_autoAuctionDetails) {
            $this->_autoAuctionDetails = $this->_autoAuctionCollectionFactory->create()->addFieldToSelect(
                '*'
            )->addFieldToFilter(
                'customer_id',
                $customerId
            )->setOrder(
                'entity_id',
                'desc'
            );
        }
        return $this->_autoAuctionDetails;
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getAutoAuctionDetails()) {
            $pager = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager::class,
                'auto.bidding.detail.pager'
            )->setCollection(
                $this->getAutoAuctionDetails()
            );
            $this->setChild('pager', $pager);
            $this->getAutoAuctionDetails()->load();
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @param int $id
     * @return string
     */
    public function getDeleteUrl($id)
    {
        return $this->getUrl('auction/account/deleteautobid', ['id' => $id]);
    }

    /**
     * @param int $productId
     * @return string
     */
    public function getProductDetail($productId)
    {
        $pro = $this->_product->getById($productId);
        return ['name'=> $pro->getName(), 'url' => $pro->getProductUrl()];
    }
    /**
     * get Formated price
     * @param $amount float
     * @return string
     *
     */
    public function formatPrice($amount)
    {
        return $this->_priceHelper->currency($amount, true, false);
    }

    /**
     * get Winning Status Label
     * @param $winningStatus int
     * @return string
     *
     */
    public function winningStatus($auctionData)
    {
        if ($auctionData->getStatus() == 0) {
            $status = $auctionData->getFlag() == 1 ? __("Winner") : __("Lost");
        } else {
            $status = __("Pending");
        }
        return $status;
    }

    /**
     * get Auction Status Label
     * @param $status int
     * @return string
     *
     */
    public function status($status)
    {
        $label = [0 =>__('Complete'), 1 =>__('Pending')];
        return isset($label[$status]) ? $label[$status] : '--';
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }
}
