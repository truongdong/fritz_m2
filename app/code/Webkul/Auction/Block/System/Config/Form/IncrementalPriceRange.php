<?php
/**
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Block\System\Config\Form;

class IncrementalPriceRange extends \Magento\Config\Block\System\Config\Form\Field
{
    const BUTTON_TEMPLATE = 'system/config/button/incrementalPriceRange.phtml';

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Webkul\Auction\Model\IncrementalPrice $incrementalPrice,
        array $data = []
    ) {

        $this->_incrementalPrice = $incrementalPrice;
        parent::__construct($context, $data);
    }
    /**
     * Set template to itself.
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate(static::BUTTON_TEMPLATE);
        }

        return $this;
    }
    /**
     * Render button.
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     *
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        // Remove scope label
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();

        return parent::render($element);
    }
    /**
     * Return ajax url for button.
     *
     * @return string
     */
    public function getAjaxCheckUrl()
    {
        return $this->getUrl('auction/config/incrementalpricesave');
    }

    /**
     * Get incremental Price Rules
     *
     * @return array
     */
    public function incrementalPriceRule()
    {
        $incrementalPriceRuleInArray = [];
        $incrementalPriceRule = '';
        $incrementalPriceCollection = $this->_incrementalPrice->getCollection();
        foreach ($incrementalPriceCollection as $incPriceData) {
            $incrementalPriceRule = $incPriceData->getIncval();
            break;
        }
        if ($incrementalPriceRule) {
            $incrementalPriceRule = json_decode($incrementalPriceRule, true);
            foreach ($incrementalPriceRule as $key => $price) {
                $key = explode('-', $key);
                $incrementalPriceRuleInArray[] = ['from' => $key[0],'to' => $key[1],'price' => $price];
            }
            return $incrementalPriceRuleInArray;
        }
        return [];
    }
    /**
     * Get the button and scripts contents.
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     *
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $this->addData(
            [
                'id' => 'wk_auction_incremental_price_button',
                'button_label' => __('Set Incremental Price Range'),
                'onclick' => 'javascript:check(); return false;',
            ]
        );

        return $this->_toHtml();
    }
}
