<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Block;

use Webkul\Auction\Model\ResourceModel\Wallettransaction;
use Webkul\Auction\Model\WallettransactionFactory;
use Webkul\Auction\Model\ResourceModel\Walletrecord;
use Magento\Sales\Model\Order;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollection;
use Magento\Customer\Model\Session as CustomerSession;

class Index extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Order
     */
    private $_order;
    /**
     * @var Webkul\Auction\Helper\Data
     */
    private $_auctionHelper;

    /**
     * @var _transactioncollection
     */
    private $_transactioncollection;
    /**
     * @var Magento\Framework\Pricing\Helper\Data
     */
    private $_pricingHelper;
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    public $jsonHelper;

    /**
     * @var Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    private $_customerCollection;
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    private $_customerFactory;
    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    private $_priceCurrency;

    /**
     * @param MagentoFrameworkViewElementTemplateContext    $context
     * @param Order                                         $order
     * @param WebkulAuctionHelperData                       $auctionHelper
     * @param MagentoFrameworkPricingHelperData             $pricingHelper
     * @param CustomerCollection                            $customerCollection
     * @param MagentoCustomerModelCustomerFactory           $customerFactory
     * @param MagentoFrameworkPricingPriceCurrencyInterface $priceCurrency
     * @param [type]                                        $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Order $order,
        Walletrecord\CollectionFactory $walletrecordModel,
        Wallettransaction\CollectionFactory $wallettransactionModel,
        WallettransactionFactory $wallettransactionFactory,
        \Webkul\Auction\Helper\Data $auctionHelper,
        \Magento\Framework\Pricing\Helper\Data $pricingHelper,
        CustomerCollection $customerCollection,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        CustomerSession $customerSession,
        \Magento\Framework\Json\Helper\Data  $jsonHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_order = $order;
        $this->_walletrecordModel = $walletrecordModel;
        $this->_wallettransactionModel = $wallettransactionModel;
        $this->_walletTransaction = $wallettransactionFactory;
        $this->_auctionHelper = $auctionHelper;
        $this->_customerSession = $customerSession;
        $this->_pricingHelper = $pricingHelper;
        $this->_customerCollection = $customerCollection;
        $this->_customerFactory = $customerFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_priceCurrency = $priceCurrency;
    }

    protected function _prepareLayout()
    {
         parent::_prepareLayout();
        if ($this->getwalletTransactionCollection()) {
            $pager = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager::class,
                'auction.wallettransaction.pager'
            )
            ->setCollection(
                $this->getwalletTransactionCollection()
            );
            $this->setChild('pager', $pager);
            $this->getwalletTransactionCollection()->load();
        }
        return $this;
    }

    // get remaining total of a customer
    public function getWalletRemainingTotal($customerId)
    {
        $remainingAmount = 0;
        $walletRecordCollection = $this->_walletrecordModel->create()
            ->addFieldToFilter('customer_id', ['eq' => $customerId]);
        if ($walletRecordCollection->getSize()) {
            foreach ($walletRecordCollection as $record) {
                $remainingAmount = $record->getRemainingAmount();
            }
        }
        return $this->_pricingHelper
            ->currency($remainingAmount, true, false);
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    // get transaction collection of a customer
    public function getwalletTransactionCollection()
    {
        if (!$this->_transactioncollection) {
            $customerId = $this->_auctionHelper
                ->getCustomerId();
            $walletRecordCollection = $this->_wallettransactionModel->create()
                ->addFieldToFilter('customer_id', $customerId)
                ->setOrder('transaction_at', 'DESC');
            $this->_transactioncollection = $walletRecordCollection;
        }
        return $this->_transactioncollection;
    }
    public function getLocaleDateTime($dateTime)
    {
        return $this->_auctionHelper->getLocaleDateTime($dateTime);
    }
    /**
     * To get minimumum amount
     *
     * @return integer
     */
    public function getMinimumAmount()
    {
        $helper=$this->_auctionHelper;
        $minimumAmount = $helper->getMinimumAmount();
        return $minimumAmount;
    }

    public function getHelperData()
    {
        return  $this->_auctionHelper;
    }
  
    public function getJsonHelper()
    {
        return  $this->jsonHelper;
    }
}
