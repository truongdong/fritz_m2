<?php
/**
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Block;

use Webkul\Auction\Model\ResourceModel\AutoAuction\CollectionFactory;
use Webkul\Auction\Model\Product as AuctionProduct;
use Webkul\Auction\Helper\Data as AuctionHelper;

/**
 * Auction detail block
 */
class AutoBidHistory extends \Magento\Framework\View\Element\Template
{
    /**
     * @var string
     */
    protected $_template = 'auction/autobidhistory.phtml';

    /**
     * @var \Webkul\Auction\Model\ResourceModel\AutoAuction\Collection
     */
    protected $_autoAuctionCollectionFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Webkul\Auction\Model\ResourceModel\Amount\Collection
     */
    protected $_auctionAmtDetails;

    /**
     * @var \Webkul\Auction\Model\Auction
     */
    protected $_auctionProduct;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $_customer;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_product;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param CollectionFactory $autoAuctionCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Catalog\Model\Product $product,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        CollectionFactory $autoAuctionCollectionFactory,
        AuctionProduct $auctionProduct,
        AuctionHelper $auctionHelper,
        array $data = []
    ) {
        $this->_priceHelper = $priceHelper;
        $this->_autoAuctionCollectionFactory = $autoAuctionCollectionFactory;
        $this->_customerSession = $customerSession;
        $this->_customer = $customer;
        $this->_product = $product;
        $this->_auctionProduct = $auctionProduct;
        $this->_auctionHelper = $auctionHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return bool|Webkul\Auction\Model\ResourceModel\AutoAuction\CollectionFactory
     */
    public function getAutoBidDetails()
    {
        $data = $this->getRequest()->getParams();
        $config = $this->getAuctionConfig();
        if (!isset($data['id']) || !$config['auto_enable']
            || !$config['show_auto_details']) {
            return false;
        }
        if ($this->getRequest()->getFullActionName() == 'catalog_product_view') {
            $data['id'] = (int)$this->_auctionHelper->getActiveAuctionId($data['id']);
        }
        if (!$this->_auctionAmtDetails) {
            $this->_auctionAmtDetails = $this->_autoAuctionCollectionFactory
                                                    ->create()
                                                    ->addFieldToFilter(
                                                        'auction_id',
                                                        $data['id']
                                                    )->setOrder(
                                                        'entity_id',
                                                        'DESC'
                                                    );
        }
        return $this->_auctionAmtDetails;
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getAutoBidDetails()) {
            $pager = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager::class,
                'auto.bidding.detail.pager'
            )->setCollection(
                $this->getAutoBidDetails()
            );
            $this->setChild('pager', $pager);
            $this->getAutoBidDetails()->load();
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @param int $customerId
     * @return string
     */
    public function getCustomerName($customerId)
    {
        $config = $this->getAuctionConfig();
        return $config['show_autobidder_name'] ?
                $this->getSrarredCustomerName($customerId) : '--';
    }

    /**
     * get starred Customer Name
     * @return string
     */
    public function getSrarredCustomerName($customerId)
    {
        $customer = $this->_customer->load($customerId);
        $firstName = $customer->getFirstname();
        $lastName = $customer->getLastname();
        return substr_replace($firstName, str_repeat("*", (strlen($firstName)-1)), 1).' '
            .substr_replace($lastName, str_repeat("*", (strlen($lastName)-1)), 1);
    }

    /**
     * get Formated price
     * @param $amount float
     * @return string
     *
     */
    public function formatPrice($amount)
    {
        $config = $this->getAuctionConfig();
        return $config['show_auto_bid_amount'] ?
                    $this->_priceHelper->currency($amount, true, false) : '--';
    }

    /**
     * get Auction Configuratin
     * @return array
     *
     */
    public function getAuctionConfig()
    {
        return $this->_auctionHelper->getAuctionConfiguration();
    }
}
