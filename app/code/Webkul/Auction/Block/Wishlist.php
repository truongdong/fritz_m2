<?php
/**
 * Webkul_Auction View On Product Block.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Block;

class Wishlist extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Wishlist\Helper\Data
     */

    public $helperData;
     /**
      * @var \MMagento\Wishlist\Model\WishlistFactory
      */

    public $wishlistFactory;
   /**
    * Undocumented function
    *
    * @param \Magento\Framework\View\Element\Template\Context $context
    * @param \Magento\Wishlist\Helper\Data $helperData
    * @param \Magento\Wishlist\Model\WishlistFactory $wishlistFactory
    * @param \Magento\Customer\Model\SessionFactory $customerSession
    * @param \Webkul\Auction\Helper\Data $helper
    * @param array $data
    */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Wishlist\Helper\Data $helperData,
        \Magento\Wishlist\Model\WishlistFactory $wishlistFactory,
        \Magento\Customer\Model\SessionFactory $customerSession,
        \Webkul\Auction\Helper\Data $helper,
        array $data = []
    ) {
        $this->wishlistFactory = $wishlistFactory;
        $this->_customerSession = $customerSession;
        $this->helper = $helper;
        parent::__construct($context, $data);
    }
   
    public function getHelperData()
    {
        return $this->helperData;
    }
     /**
      * get Auction Details Url
      *
      * @return url
      */
    public function getAuctionDetailsUrl()
    {
        return $this->getUrl('auction/account/auctiondetails');
    }

    public function getWishlistProductIds()
    {
        
        $wishlist = $this->getAuctionProductId();
        return $wishlist;
    }
    /**
     * Undocumented function
     *
     * @return array product ids
     */
    public function getAuctionProductId()
    {
        $customerId = $this->_customerSession->create()->getCustomerId();
        $wishlist =  $this->wishlistFactory->create()
        ->loadByCustomerId($customerId)->getItemCollection();
        $productId['productId'] = [];
        foreach ($wishlist as $wish) {
            $isAuctionAvail = $this->helper->isAuctionAvail($wish->getProductId());
            if ($isAuctionAvail) {
                array_push($productId, $wish->getProductId());
            }
        }
        $jsonEncodeProductIds = $this->helper->jsonEncode($productId);
        return $jsonEncodeProductIds;
    }
}
