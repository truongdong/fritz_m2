<?php
/**
 * Webkul_Auction Product Product Attribute Adminhtml Block.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Block\Adminhtml\Product;

class ProductSetAttribute extends \Magento\Backend\Block\Template
{
    /**
     * @var string
     */
    protected $_template = 'product/setattribute.phtml';

    /**
     * @param \Magento\Backend\Block\Template\Context   $context
     * @param \Magento\Framework\Registry               $coreRegistry
     * @param array                                     $data = []
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }

    /**
     * getAuctionType
     * @return false|string
     */
    public function getAuctionType()
    {
        $auctionType = $this->coreRegistry->registry('product')->getAuctionType();
        if ($auctionType != '') {
            return $auctionType;
        } else {
            return false;
        }
    }
}
