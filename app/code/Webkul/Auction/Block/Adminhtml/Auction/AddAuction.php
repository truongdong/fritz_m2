<?php
/**
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Block\Adminhtml\Auction;

class AddAuction extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * auction product id.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_proId = 0;

    /**
     * @var Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $localeDate;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry           $registry
     * @param array                                 $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        \Webkul\Auction\Model\ProductFactory $aucProductFactory,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->_aucProductFactory = $aucProductFactory;
        $this->localeDate = $context->getLocaleDate();
        parent::__construct($context, $data);
    }

    /**
     * Initialize Imagegallery Images Edit Block.
     */
    protected function _construct()
    {
        $this->_objectId = 'category_map_id';
        $this->_blockGroup = 'Webkul_Auction';
        $this->_controller = 'adminhtml_auction';
        parent::_construct();
        if ($this->_isAllowedAction('Webkul_Auction::add_auction')) {
            $auction = $this->_isReAuctionActive();
            if ($auction['reauction']) {
                $this->buttonList->remove('save');
                $this->buttonList->remove('reset');
                $this->addButton(
                    'reauction',
                    [
                        'label' => __('Re Auction'),
                        'onclick' => 'setLocation(\'' . $this->getReAuctionUrl() . '\')',
                        'class' => 'primary'
                    ],
                    -1
                );
            } elseif (!$auction['auction_status']) {
                $this->buttonList->remove('save');
                $this->buttonList->remove('reset');
            } else {
                $this->buttonList->update('save', 'label', __('Save'));
            }
        } else {
            $this->buttonList->remove('save');
        }
    }

    /**
     * Retrieve text for header element depending on loaded image.
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        return __('Add Auction');
    }

    /**
     * Check permission for passed action.
     *
     * @param string $resourceId
     *
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    /**
     * Check re auction active or not
     * @return bool
     */
    protected function _isReAuctionActive()
    {
        $aucId = $this->getRequest()->getParam('auction_id');
        $auction=[];
        $auction['reauction'] = false;
        $auction['auction_status'] = 1;
        $today = $this->localeDate->date()->format('Y-m-d H:i:s');
        if ($aucId) {
            $auctionPro = $this->_aucProductFactory->create()->load($aucId);
            if ($auctionPro->getEntityId()) {
                $auctionStartTime = $this->localeDate->date($auctionPro->getStartAuctionTime())->format('Y-m-d H:i:s');
                $diff = strtotime($auctionStartTime) - strtotime($today);
                $aucStatus = $auctionPro->getAuctionStatus();
                $auction['auction_status'] = $auctionPro->getAuctionStatus();
                $status = $auctionPro->getStatus();
                if (!$auction['auction_status'] && $status) {
                    $this->_proId = $auctionPro->getProductId();
                    $auction['reauction'] = true;
                    return true;
                }
            }
        }
        return $auction;
    }

    /**
     * Get form action URL.
     *
     * @return string
     */
    public function getReAuctionUrl()
    {
        return $this->getUrl('auction/auction/addauction', ['pro_id'=> $this->_proId]);
    }

    /**
     * Get form action URL.
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        if ($this->hasFormActionUrl()) {
            return $this->getData('form_action_url');
        }

        return $this->getUrl('*/*/save');
    }
}
