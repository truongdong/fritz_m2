<?php
/**
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Block\Adminhtml\Auction;

/**
 * Abstract items renderer
 */
class DetailContainer extends \Magento\Backend\Block\Template
{
    /**
     * @var AllProductsForAuction
     */
    protected $_product;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\Data\Form\Element\Factory $elementFactory
     * @param \Webkul\Auction\Helper\Data $auctionHelperData
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Data\Form\Element\Factory $elementFactory,
        \Webkul\Auction\Helper\Data $auctionHelperData,
        \Magento\Catalog\Model\ProductFactory $product,
        array $data = []
    ) {
        $this->_product = $product;
        $this->_jsonEncoder = $jsonEncoder;
        $this->_elementFactory = $elementFactory;
        $this->_auctionConfig = $auctionHelperData->getAuctionConfiguration();
        parent::__construct($context, $data);
    }

    protected function _toHtml()
    {
        $proData = [];
        $data = $this->_product->create()->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('type_id', ['neq' => 'bundle'])
            ->addFieldToFilter('type_id', ['neq' => 'grouped'])
            ->addFieldToFilter('auction_type', ['like' => '%'.'2'.'%']);
        foreach ($data as $product) {
            $proData['product'][$product->getEntityId()] = $product->getTypeId();
        }
       
        $proData['reserve_enable'] = $this->_auctionConfig['reserve_enable'];
        $proData = json_encode($proData);
        $script = '<script type="text/x-magento-init">
            {"body": {"forAutoAuction": '.$proData.'}}
            </script>';
        return '<div id="auction_product_section"></div>'.$script;
    }
}
