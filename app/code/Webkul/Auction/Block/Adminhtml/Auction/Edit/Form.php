<?php

/**
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Block\Adminhtml\Auction\Edit;

use Webkul\Auction\Model\ResourceModel\Product\Source\AllProductsForAuction;

/**
 * Adminhtml Add New Auction Form.
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $coreSession;

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var AllProductsForAuction
     */
    protected $_allProducts;

    /**
     * @var array configuration of Auction
     */
    protected $_auctionConfig;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_product;

    /**
     * @param   \Magento\Framework\Session\SessionManagerInterface $coreSession
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry             $registry
     * @param \Magento\Framework\Data\FormFactory     $formFactory
     * @param \Webkul\Auction\Helper\Data             $auctionHelperData
     * @param AllProductsForAuction                   $allProducts
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Webkul\Auction\Helper\Data $auctionHelperData,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        AllProductsForAuction $allProducts,
        \Magento\Catalog\Model\ProductFactory $product,
        array $data = []
    ) {
        $this->coreSession = $coreSession;
        $this->_product = $product;
        $this->_allProducts = $allProducts;
        $this->_auctionConfig = $auctionHelperData->getAuctionConfiguration();
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {
       
        $type = '';
        $dateFormat = $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);
        $this->coreSession->start();
        $model = $this->_coreRegistry->registry('auction_product');
         $form = $this->_formFactory->create(
             [
                'data' => [
                    'id' => 'edit_form',
                    'enctype' => 'multipart/form-data',
                    'action' => $this->getData('action'),
                    'method' => 'post'
                ]
             ]
         );
        $disabled = false;
        $form->setHtmlIdPrefix('wkauction_');
        if (isset($model["entity_id"])) {
            $model->setStartAuctionTime(
                $this->_localeDate
                    ->date(new \DateTime($model['start_auction_time']))
                    ->format('m/d/y H:i:s')
            );

            $model->setStopAuctionTime(
                $this->_localeDate
                    ->date(new \DateTime($model['stop_auction_time']))
                    ->format('m/d/y H:i:s')
            );
            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Edit Auction'), 'class' => 'fieldset-wide']
            );
            $fieldset->addField('entity_id', 'hidden', ['name' => 'entity_id']);
            $type = $this->_product->create()->load($model["product_id"])->getTypeId();
            $today = $this->_localeDate->date()->format('Y-m-d H:i:s');
            $auctionStartTime = $model->getStartAuctionTime();
            $diff = strtotime($auctionStartTime) - strtotime($today);
            if ($diff < 0) {
                $disabled = true;
            }
        } else {
            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Add New Auction'), 'class' => 'fieldset-wide']
            );
        }

        $fieldset->addField(
            'product_id',
            'select',
            [
                'name' => 'product_id',
                'label' => __('Product Name'),
                'id' => 'product_id',
                'title' => __('Product Name'),
                'values' => $this->_allProducts->productListForAuction($model["product_id"]),
                'class' => 'required-entry',
                'required' => true,
            ]
        );
        $fieldset->addField(
            'starting_price',
            'text',
            [
                'name' => 'starting_price',
                'label' => __('Starting Price'),
                'id' => 'starting_price',
                'title' => __('Starting Price'),
                'type' => 'price',
                'class' => 'required-entry validate-zero-or-greater',
                'required' => true,
                'disabled' => $disabled
            ]
        );

        if ($this->_auctionConfig['reserve_enable']) {
            $fieldset->addField(
                'reserve_price',
                'text',
                [
                    'name' => 'reserve_price',
                    'label' => __('Reserve Price'),
                    'id' => 'reserve_price',
                    'title' => __('Reserve Price'),
                    'values' => [],
                    'class' => 'validate-zero-or-greater'
                ]
            );
        }

        $fieldset->addField(
            'start_auction_time',
            'date',
            [
                'name' => 'start_auction_time',
                'label' => __('Start Auction Time'),
                'id' => 'start_auction_time',
                'title' => __('Start Auction Time'),
                'date_format' => $dateFormat,
                'time_format' => 'HH:mm:ss',
                'class' => 'required-entry admin__control-text',
                'style' => 'width:210px',
                'required' => true,
                'disabled' => $disabled
            ]
        );
        $fieldset->addField(
            'stop_auction_time',
            'date',
            [
                'name' => 'stop_auction_time',
                'label' => __('Stop Auction Time'),
                'id' => 'stop_auction_time',
                'title' => __('Stop Auction Time'),
                'date_format' => $dateFormat,
                'time_format' => 'HH:mm:ss',
                'class' => 'required-entry admin__control-text',
                'style' => 'width:210px',
                'required' => true,
                'disabled' => $disabled
            ]
        );

        $fieldset->addField(
            'days',
            'text',
            [
                'name' => 'days',
                'label' => __('Number of Days Till Winner Can Buy'),
                'id' => 'days',
                'title' => __('Number of Days Till Winner Can Buy'),
                'class' => 'required-entry integer validate-greater-than-zero',
                'required' => true,
            ]
        );
        if ($type != 'downloadable' || $type == '') {
            $fieldset->addField(
                'min_qty',
                'text',
                [
                    'name' => 'min_qty',
                    'label' => __('Minimum Quantity'),
                    'id' => 'min_qty',
                    'title' => __('Minimum Quantity'),
                    'class' => 'required-entry validate-zero-or-greater',
                    'required' => true,
                ]
            );

            $fieldset->addField(
                'max_qty',
                'text',
                [
                    'name' => 'max_qty',
                    'label' => __('Maximum Quantity'),
                    'id' => 'max_qty',
                    'title' => __('Maximum Quantity'),
                    'class' => 'required-entry validate-zero-or-greater',
                    'required' => true,
                ]
            )->setAfterElementHtml('
                <script>
                    require([
                        "jquery",
                        "jquery/ui",
                        "jquery/validate",
                        "mage/translate"
                    ], function($){
                        $(document).ready(function () {
                          $("#save").click(function(){
                                if(parseInt($("#wkauction_max_qty").attr("value"))
                                < parseInt($("#wkauction_min_qty").attr("value"))){
                                    $("#wkauction_max_qty").addClass("max-qty-required").removeClass("required");
                                    $.validator.addMethod(
                                    "max-qty-required", function (value) {
                                        return false;
                                    }, $.mage.__("Max qty must be greater than or equal to min qty"));
                                } else {
                                    $("#wkauction_max_qty").removeClass("max-qty-required"); 
                                    return true;   
                                }
                           })
                            
                        });
                    });
                </script>
            ');
        }

        if ($this->_auctionConfig['increment_auc_enable']) {
            $fieldset->addField(
                'increment_opt',
                'select',
                [
                    'name' => 'increment_opt',
                    'label' => __('Increment Option'),
                    'options' => ['1' => __('Enabled'), '0' => __('Disabled')],
                    'id' => 'increment_opt',
                    'title' => __('Increment Option'),
                    'class' => 'required-entry',
                    'required' => true,
                ]
            );
        }

        if ($this->_auctionConfig['auto_enable']) {
            $fieldset->addField(
                'auto_auction_opt',
                'select',
                [
                    'name' => 'auto_auction_opt',
                    'label' => __('Automatic Option'),
                    'options' => ['1' => __('Enabled'), '0' => __('Disabled')],
                    'id' => 'auto_auction_opt',
                    'title' => __('Automatic Option'),
                    'class' => 'required-entry',
                    'required' => true,
                ]
            );
        }

        $form->setValues($model);
        if ($this->_isReAuction()) {
            $form->setValues(['product_id' => $this->_isReAuction()]);
        }
        $this->coreSession->start();
        $this->coreSession->setAuctionValues([]);
        $form->setUseContainer(true);
        $this->setForm($form);
      
        return parent::_prepareForm();
    }

    /**
     * check auction for reorder
     * @return int | bool
     */

    protected function _isReAuction()
    {
        $proId = $this->getRequest()->getParam('pro_id');
        return $proId ? $proId : false;
    }
}
