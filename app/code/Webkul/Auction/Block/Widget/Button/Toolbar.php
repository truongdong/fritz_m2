<?php

/**
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Block\Widget\Button;

use Magento\Backend\Block\Widget\Button\Toolbar as ToolbarContext;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Backend\Block\Widget\Button\ButtonList;

class Toolbar
{
     /**
      * Initialize dependency
      *
      * @param \Magento\Framework\Registry $registry
      */
    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Registry $registry,
        \Webkul\Auction\Model\ProductFactory $productFactory,
        \Webkul\Auction\Model\WallettransactionFactory $walletTransactionFactory
    ) {
        $this->request =  $request;
        $this->coreRegistry = $registry;
        $this->productFactory = $productFactory;
        $this->walletTransactionFactory = $walletTransactionFactory;
    }

    public function beforePushButtons(
        ToolbarContext $toolbar,
        \Magento\Framework\View\Element\AbstractBlock $context,
        \Magento\Backend\Block\Widget\Button\ButtonList $buttonList
    ) {
        if (!$context instanceof \Magento\Sales\Block\Adminhtml\Order\View) {
            return [$context, $buttonList];
        }
        
        $order = $context->getOrder();
        $orderId = $order->getId();
        $checkWalletTransaction = $this->walletTransactionFactory
        ->create()->load($orderId, 'order_id');
        
        if (!empty($checkWalletTransaction->getData())) {
            $buttonList->remove('order_edit');
            $buttonList->remove('order_ship');
            $buttonList->remove('order_reorder');
        }
        if ($order->hasInvoices() && !empty($checkWalletTransaction->getData())) {
            $buttonList->remove('order_hold');
            $buttonList->remove('order_creditmemo');
        }
        return [$context, $buttonList];
    }
}
