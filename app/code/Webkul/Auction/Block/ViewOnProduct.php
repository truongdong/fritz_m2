<?php
/**
 * Webkul_Auction View On Product Block.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Block;

use Magento\Customer\Model\SessionFactory as CustomerSession;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class ViewOnProduct extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */

    public $jsonHelper;
    /**
     * @param Magento\Catalog\Block\Product\Context   $context
     * @param Magento\Catalog\Model\Product           $product
     * @param CustomerSession                          $customerSession
     * @param Magento\Framework\Pricing\Helper\Data   $priceHelper
     * @param Webkul\Auction\Model\ProductFactory     $auctionProFactory
     * @param Webkul\Auction\Model\AmountFactory      $aucAmountFactory
     * @param Webkul\Auction\Model\AutoAuctionFactory $autoAuctionFactory
     * @param Webkul\Auction\Model\WinnerDataFactory  $winnerDataFactory
     * @param Webkul\Auction\Helper\Data              $helperData
     * @param \Magento\Framework\Json\Helper\Data     $jsonHelper
     * @param array                                    $data
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\Product $product,
        CustomerSession $customerSession,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Webkul\Auction\Model\ProductFactory $auctionProFactory,
        \Webkul\Auction\Model\AmountFactory $aucAmountFactory,
        \Webkul\Auction\Model\AutoAuctionFactory $autoAuctionFactory,
        \Webkul\Auction\Model\WinnerDataFactory $winnerDataFactory,
        \Webkul\Auction\Helper\Data $helperData,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Webkul\Auction\Model\AmountRepositry $amountRepositry,
        array $data = []
    ) {
        $this->logger = $logger;
        $this->amountRepositry =  $amountRepositry;
        $this->_coreRegistry = $context->getRegistry();
        $this->_product = $product;
        $this->_customerSession = $customerSession;
        $this->_priceHelper = $priceHelper;
        $this->_auctionProFactory = $auctionProFactory;
        $this->_aucAmountFactory = $aucAmountFactory;
        $this->_autoAuctionFactory = $autoAuctionFactory;
        $this->_winnerDataFactory = $winnerDataFactory;
        $this->_helperData = $helperData;
        $this->httpContext = $httpContext;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context, $data);
    }
    /**
     * @var $productList Product list of current page
     * @return array of current category product in auction and buy it now
     */
    public function getAuctionDetail()
    {

        $auctionConfig = $this->getAuctionConfiguration();
        $auctionData = [];
        if ($auctionConfig['enable']) {
            $curPro = $this->_coreRegistry->registry('current_product');
            if ($curPro) {
                $currentProId = $curPro->getEntityId();

            } else {
                $auctionId = $this->getRequest()->getParam('id');
                $currentProId = $this->getAuctionProId($auctionId);
                $curPro = $this->_product->load($currentProId);
            }

            $auctionOpt = $curPro->getAuctionType();
            $auctionOpt = explode(',', $auctionOpt);
            /**
             * 2 : use for auction
             * 1 : use for Buy it now
             */
            if (in_array(2, $auctionOpt)) {

                $aucDataObj = $this->_auctionProFactory->create()->getCollection()
                                        ->addFieldToFilter('product_id', ['eq'=>$currentProId])
                                        ->setPageSize(1)->getFirstItem();
                    $auctionData = $aucDataObj->getData();
                if (isset($auctionData['entity_id'])) {
                    $aucEntityId = $auctionData['entity_id'];
                    $aucAmtData = $this->amountRepositry->getAmountDataByAuctionId($currentProId, $aucEntityId);

                    if ($aucAmtData->getEntityId()) {
                        $aucAmtData = $this->_autoAuctionFactory->create()->getCollection()
                                            ->addFieldToFilter('auction_id', ['eq'=> $aucEntityId])
                                            ->addFieldToFilter('flag', ['eq'=>1])
                                            ->addFieldToFilter('shop', ['neq'=>0])->setPageSize(1)->getFirstItem();
                    }
                    $currentAuctionPriceData = $this->amountRepositry
                    ->getCurrentAuctionPriceData($currentProId, $auctionData['entity_id']);

                    $auctionData['min_amount'] = $auctionData['starting_price'];
                    if ($currentAuctionPriceData->getAuctionAmount()) {
                        $auctionData['current_auction_amount'] = $currentAuctionPriceData->getAuctionAmount();
                        $auctionData['min_amount'] = $auctionData['current_auction_amount'];
                    } else {
                        $auctionData['current_auction_amount'] = 0.00;
                    }

                    if ($auctionData['increment_opt'] && $auctionConfig['increment_auc_enable']) {
                        $incVal = $this->_helperData->getIncrementPriceAsRange($aucDataObj);
                        $minAmt = $auctionData['min_amount'];
                        $auctionData['min_amount'] = $incVal ? $minAmt + $incVal : $minAmt;
                    }

                    $today = $this->_localeDate->date()->format('m/d/y H:i:s');
                    $stopDate = date_create($auctionData['stop_auction_time']);
                    $startDate = date_create($auctionData['start_auction_time']);
                    $auctionData['stop_auction_time'] = date_format($stopDate, 'Y-m-d H:i:s');
                    $auctionData['start_auction_time'] = date_format($startDate, 'Y-m-d H:i:s');

                    $auctionData['current_time_stamp'] = strtotime($today);
                    $auctionData['start_auction_time_stamp'] = strtotime($auctionData['start_auction_time']);
                    $auctionData['stop_auction_time_stamp'] = strtotime($auctionData['stop_auction_time']);

                    $auctionData['new_auction_start'] = $aucAmtData->getEntityId() ? true : false;
                    $auctionData['auction_title'] = __('Bid on ').$curPro->getName();

                    $auctionData['pro_url'] = $this->getUrl().$curPro->getUrlKey().'.html';
                    $auctionData['pro_name'] = $curPro->getName();
                    $auctionData['pro_buy_it_now'] = in_array(1, $auctionOpt) !== false ? 1:0;
                    if (!$auctionConfig['reserve_enable'] && $currentAuctionPriceData->getEntityId()) {
                        $auctionData['pro_buy_it_now'] = 0;
                    } elseif ($auctionConfig['reserve_enable']
                    && $currentAuctionPriceData->getEntityId() &&
                    $auctionData['reserve_price'] <= $currentAuctionPriceData->getAuctionAmount()) {
                        $auctionData['pro_buy_it_now'] = 0;
                    }
                    if ($auctionData['min_amount'] < $auctionData['starting_price']) {
                        $auctionData['min_amount'] = $auctionData['starting_price'];
                    }
                    $auctionData =  $this->productAuctionDetail($auctionOpt, $currentProId, $auctionConfig, $curPro);
                } else {
                    $auctionData = [];
                }
            } elseif (in_array(1, $auctionOpt)) {
                $auctionData['pro_buy_it_now'] = in_array(1, $auctionOpt) !== false ? 1:0;
            }
        }

        return $auctionData;
    }

    public function productAuctionDetail($auctionOpt, $currentProId, $auctionConfig, $curPro)
    {

            $auctionData = $this->_auctionProFactory->create()->getCollection()
                                    ->addFieldToFilter('product_id', ['eq'=>$currentProId])
                                    ->addFieldToFilter('auction_status', ['nin'=>[2,3]])
                                    ->addFieldToFilter('status', ['eq'=>0])
                                    ->getFirstItem()->getData();
            $aucShopStatus= $this->_aucAmountFactory->create()->getCollection()
                                    ->addFieldToFilter('product_id', ['eq' => $currentProId])
                                    ->addFieldToFilter('winning_status', ['eq'=>1])
                                    ->addFieldToFilter('shop', ['neq'=>0])->getSize();
            $autoShopStatus = $this->_autoAuctionFactory->create()->getCollection()
                                    ->addFieldToFilter('product_id', ['eq' => $currentProId])
                                    ->addFieldToFilter('flag', ['eq'=>1])
                                    ->addFieldToFilter('shop', ['neq'=>0])->getSize();
            $auctionWinStatus = $this->_auctionProFactory->create()->getCollection()
                                    ->addFieldToFilter('product_id', ['eq'=>$currentProId])
                                    ->addFieldToFilter('auction_status', ['neq'=>3])
                                     ->addFieldToFilter('status', ['eq'=>1])
                                    ->getFirstItem()->getData();
        if (isset($auctionData['entity_id'])) {

            $aucAmtData = $this->_aucAmountFactory->create()->getCollection()
                                ->addFieldToFilter('product_id', ['eq' => $currentProId])
                                ->addFieldToFilter('auction_id', ['eq'=> $auctionData['entity_id']])
                                ->addFieldToFilter('winning_status', ['eq'=>1])
                                ->addFieldToFilter('shop', ['neq'=>0])->getFirstItem();

            if ($aucAmtData->getEntityId()) {
                $aucAmtData = $this->_autoAuctionFactory->create()->getCollection()
                                        ->addFieldToFilter('auction_id', ['eq'=> $auctionData['entity_id']])
                                        ->addFieldToFilter('flag', ['eq'=>1])
                                        ->addFieldToFilter('shop', ['neq'=>0])->getFirstItem();
            }

            $today = $this->_localeDate->date()->format('m/d/y H:i:s');
            $auctionData['stop_auction_time'] = $this->_localeDate
                                                    ->date(
                                                        new \DateTime($auctionData['stop_auction_time'])
                                                    )->format('Y-m-d H:i:s');
            $auctionData['start_auction_time'] = $this->_localeDate
                                                    ->date(
                                                        new \DateTime($auctionData['start_auction_time'])
                                                    )->format('Y-m-d H:i:s');
            $currentAuctionPriceData = $this->_aucAmountFactory->create()->getCollection()
                                ->addFieldToFilter('product_id', ['eq' => $currentProId])
                                ->addFieldToFilter('auction_id', ['eq'=> $auctionData['entity_id']])
                                ->setOrder('auction_amount', 'DESC')
                                ->getFirstItem();
            if ($currentAuctionPriceData->getAuctionAmount()) {
                $auctionData['current_auction_amount'] = $currentAuctionPriceData->getAuctionAmount();
                $auctionData['customer_id'] = $currentAuctionPriceData->getCustomerId();
            } else {
                $auctionData['current_auction_amount'] = $auctionData['min_amount'];
            }
            $outbidStatus = false;
            if ($this->getAuctionConfiguration()['enable_auto_outbid_msg']) {
                $outbidStatus = $this->checkOutBidStatus($auctionData['entity_id']);
            }
            $auctionData['outbid_status'] = $outbidStatus;
            $auctionData['outbid_msg'] = $this->getAuctionConfiguration()['show_auto_outbid_msg'];
            $auctionData['current_time_stamp'] = strtotime($today);
            $auctionData['start_auction_time_stamp'] = strtotime($auctionData['start_auction_time']);
            $auctionData['stop_auction_time_stamp'] = strtotime($auctionData['stop_auction_time']);
            $auctionData['new_auction_start'] = $aucAmtData->getEntityId() ? true : false;
            $auctionData['auction_title'] = __('Bid on ').$curPro->getName();
            $auctionData['pro_url'] = $curPro->getProductUrl();
            $auctionData['pro_name'] = $curPro->getName();
            $auctionData['pro_buy_it_now'] = in_array(1, $auctionOpt) !== false ? 1:0;
            if (!$auctionConfig['reserve_enable'] && $currentAuctionPriceData->getEntityId()) {
                $auctionData['pro_buy_it_now'] = 0;
            } elseif ($auctionConfig['reserve_enable'] && $currentAuctionPriceData->getEntityId()
            && $auctionData['reserve_price'] <= $currentAuctionPriceData->getAuctionAmount()) {
                $auctionData['pro_buy_it_now'] = 0;
            }
            $auctionData['pro_auction'] = in_array(2, $auctionOpt) !== false ? 1:0;
            if ($auctionData['min_amount'] < $auctionData['starting_price']) {
                $auctionData['min_amount'] = $auctionData['starting_price'];
            }

            if ($auctionData['increment_opt']
            && $auctionConfig['increment_auc_enable']
            && $auctionConfig['auto_use_increment']
            ) {
                $incVal = $this->_helperData->getIncrementPriceAsRange($auctionData['min_amount']);
                $auctionData['min_amount'] = $incVal ? $auctionData['min_amount'] + $incVal
                                                                    : $auctionData['min_amount'];
            }
        } elseif ($aucShopStatus==1 || $autoShopStatus==1) {
            $productId=$currentProId;
            $today = $this->_localeDate->date()->format('m/d/y H:i:s');
            $auctionData = $this->_auctionProFactory->create()->getCollection()
            ->addFieldToFilter('auction_status', ['neq'=>3])
            ->addFieldToFilter('product_id', ['eq' => $currentProId])
            ->getFirstItem()->getData();
            $currentAuctionPriceData = $this->_aucAmountFactory->create()->getCollection()
                                            ->addFieldToFilter('product_id', ['eq' => $currentProId])
                                            ->addFieldToFilter('auction_id', ['eq'=> $auctionData['entity_id']])
                                            ->setOrder('auction_amount', 'DESC')
                                            ->getFirstItem();
            $aucAmtData = $this->_aucAmountFactory->create()->getCollection()
                                ->addFieldToFilter('product_id', ['eq' => $currentProId])
                                ->addFieldToFilter('winning_status', ['eq'=>1])
                                ->addFieldToFilter('shop', ['neq'=>0])->getFirstItem();
            $auctionData['current_time_stamp'] = strtotime($today);
            $auctionData['start_auction_time_stamp'] = strtotime($auctionData['start_auction_time']);
            $auctionData['stop_auction_time_stamp'] = strtotime($auctionData['stop_auction_time']);
            $auctionData['current_auction_amount'] = $currentAuctionPriceData->getAuctionAmount();
            $auctionData['auction_title'] = __('Bid on ').$curPro->getName();
        } else {
            $auctionData = false;
        }
            return $auctionData;
    }

    public function checkOutBidStatus($auctionId = 0)
    {
        $status = false;
        $auctionAmount = 0;
        $currentCustomerId = $this->httpContext->getValue('customer_id');
        $aucAmtData = $this->_autoAuctionFactory->create()->getCollection()
                              ->addFieldToFilter('auction_id', ['eq'=> $auctionId]);
        if ($aucAmtData->getSize() && ($aucAmtData->getSize() > 1)) {
            $aucAmtData->addFieldToFilter('customer_id', $currentCustomerId);
            if (!$aucAmtData->getSize()) {
                return false;
            }
            foreach ($aucAmtData as $aucAmount) {
                $auctionAmount = $aucAmount->getAmount();
            }
            $aucAuctionColl = $this->_autoAuctionFactory->create()->getCollection()
                                ->addFieldToFilter('auction_id', ['eq'=> $auctionId])
                                ->addFieldToFilter('customer_id', ['neq'=> $currentCustomerId]);
            foreach ($aucAuctionColl as $autAuctionData) {
                if ($autAuctionData->getAmount() > $auctionAmount) {
                    $status = true;
                    break;
                }
            }
        }
        return $status;
    }
    /**
     * @return array of Auction Configuration Settings
     */
    public function getAuctionConfiguration()
    {
        return $this->_helperData->getAuctionConfiguration();
    }

    /**
     * @return string url of auction form
     */

    public function getAuctionFormAction()
    {
        $curPro = $this->_coreRegistry->registry('current_product');
        if ($curPro) {
            $currentProId = $curPro->getEntityId();
        } else {
            $auctionId = $this->getRequest()->getParam('id');
            $currentProId = $this->getAuctionProId($auctionId);
        }
        return $this->_urlBuilder->getUrl('auction/account/loginbeforebid/', ['id'=>$currentProId]);
    }

    /**
     *
     */
    public function getAuctionDetailAftetEnd($auctionData)
    {
        $auctionStatus=[];
        $currentProId = $auctionData['product_id'];
        $auctionId = $auctionData['entity_id'];
        $customerId = 0;
        $shop = '';
        $price = 0;
        $winnerBidDetail = $this->_helperData->getWinnerBidDetail($auctionId);
        if ($winnerBidDetail) {
            $customerId = $winnerBidDetail->getCustomerId();
            $shop = $winnerBidDetail->getShop();
            $price = $winnerBidDetail->getBidType() == 'normal' ? $winnerBidDetail->getAuctionAmount():
                                                                $winnerBidDetail->getWinningPrice();
        }

        $waittingList = $this->_aucAmountFactory->create()->getCollection()
                                        ->addFieldToFilter('product_id', ['eq'=>$currentProId])
                                        ->addFieldToFilter('auction_id', ['eq'=>$auctionId])
                                        ->addFieldToFilter('winning_status', ['neq'=>1]);
        $autoWaittingList = $this->_autoAuctionFactory->create()->getCollection()
                                        ->addFieldToFilter('product_id', ['eq'=>$currentProId])
                                        ->addFieldToFilter('auction_id', ['eq'=> $auctionId])
                                        ->addFieldToFilter('flag', ['neq'=>1]);
        $winnderData = $this->_winnerDataFactory->create()->getCollection()
                                        ->addFieldToFilter('customer_id', $customerId)
                                        ->addFieldToFilter('product_id', ['eq'=>$currentProId])
                                        ->addFieldToFilter('auction_id', ['eq'=> $auctionId]);
        $winningAmount = 0;
        if ($winnderData->getSize()) {
            foreach ($winnderData as $windata) {
                $winningAmount = $windata->getShowAmount();
            }
        }
        $custList=[];

        foreach ($waittingList as $waitAuc) {
            if ($waitAuc->getCustomerId()!= $customerId && !in_array($waitAuc->getCustomerId(), $custList)) {
                array_push($custList, $waitAuc->getCustomerId());
            }
        }
        //get watting winner list from auto auction
        foreach ($autoWaittingList as $autoWaitAuc) {
            if ($autoWaitAuc->getCustomerId()!= $customerId && !in_array($autoWaitAuc->getCustomerId(), $custList)) {
                array_push($custList, $autoWaitAuc->getCustomerId());
            }
        }

        $currentUserWinner = false;
        $currentUserWaitingList = false;

        if ($this->httpContext->getValue('customer_id')) {
            $currentCustomerId = $this->httpContext->getValue('customer_id');
            $curPro = $this->_coreRegistry->registry('current_product');
            $currentProId = $curPro->getEntityId();
            $auctionStatus = $this->_auctionProFactory->create()->getCollection()
                                    ->addFieldToFilter('product_id', ['eq'=>$currentProId])
                                    ->addFieldToFilter('auction_status', ['eq'=>4])
                                    ->getSize();
            $auctionId = $auctionData['entity_id'];
            $winnerCustomerId = $this->_winnerDataFactory->create()->getCollection()
                                    ->addFieldToFilter('customer_id', $currentCustomerId)
                                    ->addFieldToFilter('product_id', ['eq'=>$currentProId])
                                    ->getFirstItem();
            if ($currentCustomerId == $winnerCustomerId['customer_id']) {
                $day = strtotime($auctionData['stop_auction_time']. ' + '.$auctionData['days'].' days');
                $difference = $day - $auctionData['current_time_stamp'];
                $currentUserWinner = ['shop' => $shop, 'price' => $price, 'amount_to_pay' =>  $winningAmount,
                 'time_for_buy' => $difference,'day'=> $day];
                if ($difference < 0) {
                    $this->_auctionProFactory->create()->load($auctionId)->setAuctionStatus(3)->save();
                }
            }
            if (in_array($currentCustomerId, $custList)) {
                $bidcount = count($custList);
                if ($currentUserWinner) {
                    $bidcount = count($custList)+1;
                }
                $currentUserWaitingList = [
                            'auc_list_url' => $this->_urlBuilder->getUrl('auction/index/history', ['id'=>$auctionId]),
                            'auc_url_lable' => $bidcount.__(' Bids'),
                            'msg_lable' => __('Bidding has been done for this product.'),
                        ];
            } elseif ($auctionStatus && $currentCustomerId != $winnerCustomerId['customer_id']) {
                $currentUserWaitingList = [
                        'auc_list_url' => $this->_urlBuilder->getUrl('auction/index/history', ['id'=>$auctionId]),
                        'msg_lable' => __('Bidding has been done for this product.'),
                    ];
            }

        } else {
            $currentUserWaitingList = [
                'auc_list_url' => $this->_urlBuilder->getUrl('auction/index/history', ['id'=>$auctionId]),
                'msg_lable' => __('Bidding has been done for this product.'),
            ];
        }
        return ['watting_user'=> $currentUserWaitingList,'winner' => $currentUserWinner ];
    }
    /**
     * For get Bidding history link
     * @param array $auctionDetail
     * @return string url
     */
    public function getHistoryUrl($auctionData)
    {
        return $this->_urlBuilder->getUrl('auction/index/history', ['id'=>$auctionData['entity_id']]);
    }

    /**
     * For get Bidding record count
     * @param int $auctionId
     * @return string
     */
    public function getNumberOfBid($auctionId)
    {
        $records = $this->_aucAmountFactory->create()->getCollection()
                                    ->addFieldToFilter('auction_id', ['eq'=>$auctionId]);
        return count($records).__(' bids');
    }

    /**
     * get currency in format
     * @param $amount float
     * @return string
     *
     */
    public function formatPrice($amount)
    {
        return $this->_priceHelper->currency($amount, true, false);
    }

    /**
     * get auction product id
     * @param $auctionId int
     * @return int
     *
     */
    public function getAuctionProId($auctionId)
    {
        return  $this->_auctionProFactory->create()->load($auctionId)->getProductId();
    }

    /**
     * getProAuctionType
     * @return string type of auction
     */
    public function getProAuctionType()
    {
        $curPro = $this->_coreRegistry->registry('current_product');
        $auctionType = "";
        if ($curPro) {
            $auctionOpt = explode(',', $curPro->getAuctionType());
            if ((in_array(2, $auctionOpt) && in_array(1, $auctionOpt)) || in_array(1, $auctionOpt)) {
                $auctionType = 'buy-it-now';
            } elseif (in_array(2, $auctionOpt)) {
                $auctionType = 'auction';
            }
        }
        return $auctionType;
    }

    public function getHelperData()
    {
        return $this->jsonHelper;
    }
    /**
     * get Auction Details Url
     *
     * @return url
     */
    public function getProudctCurrentTimeUrl()
    {
        return $this->getUrl('auction/account/productcurrenttime');
    }

    public function getCustomerSession(){
        return $this->_customerSession->create();
    }
}
