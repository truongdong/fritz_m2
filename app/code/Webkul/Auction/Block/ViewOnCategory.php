<?php
/**
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Block;

class ViewOnCategory extends \Magento\Catalog\Block\Product\ListProduct
{
    /**
     * get Auction Details Url
     *
     * @return url
     */
    public function getAuctionDetailsUrl()
    {
        return $this->getUrl('auction/account/auctiondetails');
    }
}
