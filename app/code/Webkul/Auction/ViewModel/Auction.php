<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\ViewModel;

use Magento\Framework\Json\Helper\Data as JsonHelper;

class Auction implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    /**
     * jsonHelper
     *
     * @var jsonHelper
     */
    public $jsonHelper;

    /**
     * Helper
     *
     * @var \Webkul\Auction\Helper\Data
     */
    public $helperData;
    
    /**
     * Undocumented function
     *
     * @param \Magento\Wishlist\Helper\Data $wishlistHelperData
     * @param \Webkul\Auction\Helper\Data $helperData
     * @param JsonHelper $jsonHelper
     */
    public function __construct(
        \Magento\Wishlist\Helper\Data $wishlistHelperData,
        \Webkul\Auction\Helper\Data $helperData,
        JsonHelper $jsonHelper
    ) {
        $this->wishlistHelperData = $wishlistHelperData;
        $this->jsonHelper = $jsonHelper;
        $this->helperData = $helperData;
    }
    
    /**
     * getJsonHelper
     *
     * @return josnHelper
     */
    public function getWishlistHelperData()
    {
        return $this->wishlistHelperData;
    }
     /**
      * get Helper
      *
      * @return josnHelper
      */
    public function getAuctionHelperData()
    {
        return $this->helperData;
    }
}
