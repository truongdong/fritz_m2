<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
 
namespace Webkul\Auction\Plugin\Backend\Block\Widget;

/**
 * Webkul Auction  Class
 */
class Container
{
    /**
     * Initialize dependency
     *
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Registry $registry,
        \Webkul\Auction\Model\ProductFactory $productFactory,
        \Webkul\Auction\Model\WallettransactionFactory $walletTransactionFactory
    ) {
        $this->request =  $request;
        $this->coreRegistry = $registry;
        $this->productFactory = $productFactory;
        $this->walletTransactionFactory = $walletTransactionFactory;
    }
    /**
     * Before plugin for addbutton function to change Invoice Button Label
     *
     * @param \Magento\Backend\Block\Widget\Container $subject
     * @param string $buttonId
     * @param array $data
     * @param integer $level
     * @param integer $sortOrder
     * @param string|null $region That button should be displayed in ('toolbar', 'header', 'footer', null)
     * @return $this
     */
    public function beforeAddButton(
        \Magento\Backend\Block\Widget\Container $subject,
        $buttonId,
        $data,
        $level = 0,
        $sortOrder = 0,
        $region = 'toolbar'
    ) {
        $order = $this->getOrder();
        if (!$order || $buttonId != "order_invoice") {
            return [$buttonId,$data,$level,$sortOrder,$region];
        }
        try {
            $orderId = $this->request->getParam('order_id');
            $checkWalletTransaction = $this->walletTransactionFactory
            ->create()
            ->load($orderId, 'order_id');
            if (!empty($checkWalletTransaction->getData())) {
                $label = __("Confirm Wallet Order");
                $data['label'] = $label;
            }
        } catch (\Exception $e) {
            return [$buttonId,$data,$level,$sortOrder,$region];
        }
        return [$buttonId,$data,$level,$sortOrder,$region];
    }

    /**
     * Retrieve order model object
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->coreRegistry->registry('sales_order');
    }
}
