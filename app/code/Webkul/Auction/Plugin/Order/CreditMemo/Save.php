<?php


/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
 

namespace Webkul\Auction\Plugin\Order\CreditMemo;

use Webkul\Auction\Logger\Logger;
use Webkul\Auction\Helper\Data;
use Webkul\Auction\Observer\CatalogControllerProductView;
use Webkul\Auction\Model\ProductFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Webkul\Auction\Model\AuctionProductMShipMapFactory;
 
class Save
{

    /**
     * @var EventManager
     */
    private $_eventManager;
 
    /**
     * @var Url
     */

    public $_url;

    /**
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;
    /**
     * @var Logger
     */
    public $logger;

    /**
     * @var Data
     */
    public $helper;

   /**
    * @var ProductFactory
    */
    protected $productFactory;

     /**
      * @var CatalogControllerProductView
      */
    protected $catalogControllerProductView;

    /**
     * @var AuctionProductMShipMapFactory
     */
    protected $auctionProductMShipMapFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoader $creditmemoLoader
     * @param CreditmemoSender $creditmemoSender
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param ProductFactory $productFactory
     * @param CatalogControllerProductView $catalogControllerProductView
     * @param \Magento\Framework\UrlInterface $url
     * @param \Webkul\Auction\Model\Wallettransaction $wallettransaction
     * @param AuctionProductMShipMapFactory                        $auctionProductMShipMapFactory
     */
    public function __construct(
        Logger $logger,
        Data $helper,
        \Magento\Framework\Webapi\Rest\Request $request,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Sales\Model\Order\ItemFactory $itemFactory,
        ProductFactory $productFactory,
        \Webkul\Auction\Model\WallettransactionFactory $walletTransactionFactory,
        CatalogControllerProductView $catalogControllerProductView,
        \Webkul\Auction\Model\ResourceModel\Feerecord\Collection $feerecordCollection,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Webkul\Auction\Model\ProductFactory $auctionProductFactory,
        \Magento\Framework\App\ResponseInterface $response,
        EventManager $eventManager,
        AuctionProductMShipMapFactory $auctionProductMShipMapFactory
    ) {
        
        $this->helper = $helper;
        $this->request =  $request;
        $this->orderFactory = $orderFactory;
        $this->_auctionProductFactory = $auctionProductFactory;
        $this->messageManager = $messageManager;
        $this->itemFactory = $itemFactory;
        $this->productFactory = $productFactory;
        $this->catalogControllerProductView = $catalogControllerProductView;
        $this->feerecordCollection = $feerecordCollection;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->_url =  $url;
        $this->_response = $response;
        $this->walletTransactionFactory = $walletTransactionFactory;
        $this->_eventManager = $eventManager;
        $this->auctionProductMShipMapFactory = $auctionProductMShipMapFactory;
    }

    /**
     * Save creditmemo
     * We can save only new creditmemo. Existing creditmemos are not editable
     *
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Backend\Model\View\Result\Forward
     *
     * @param Logger $logger
     * @param  \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\Webapi\Rest\Request $request,
     */
    public function afterExecute(
        \Magento\Sales\Controller\Adminhtml\Order\Creditmemo\Save $subject,
        $result
    ) {
       
            $resultRedirect = $this->resultRedirectFactory->create();
            $getData = $this->request->getPost('creditmemo');
            $getKeys = array_keys($getData['items']);
        
            $itemid = isset($getKeys) ? $getKeys[0] : 0 ;
            $getItem = $this->itemFactory->create()->getCollection()
            ->addFieldToFilter('item_id', $itemid)->getFirstItem();

            $orderId = $getItem->getOrderId();
            $getOrder = $this->orderFactory->create()
            ->getCollection()
            ->addFieldToFilter('entity_id', $orderId)->getFirstItem();
            $customerId = $getOrder->getCustomerId();
            $productId = $getItem->getProductId();
            $auctionDiscount = abs($getOrder->getAuctionDiscount());
            $getIsMultishipping = $getOrder->getIsMultishipping();
        if ($getIsMultishipping) {
            $auctionProductMShipMapFactory = $this->auctionProductMShipMapFactory->create()
                                            ->load($orderId, 'order_id');
            $auctionActPro = $this->_auctionProductFactory->create()
                            ->load($auctionProductMShipMapFactory->getAuctionId());
        } else {
            $auctionActPro =
            $this->_auctionProductFactory->create()->getCollection()
                                ->addFieldToFilter('product_id', ['eq' => $productId])
                                ->addFieldToFilter('auction_status', 4)
                                ->addFieldToFilter('order_id', $getOrder->getId())
                                ->addFieldToFilter('status', 1)->getFirstItem();
        }
        if ($auctionActPro->getData()) {
            $getAuction =  $this->feerecordCollection
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('customer_id', $customerId)
            ->addFieldToFilter('start_auction_time', $auctionActPro->getStartAuctionTime())
            ->addFieldToFilter('stop_auction_time', $auctionActPro->getStopAuctionTime())
            ->getFirstItem();
            $entity_id = $getAuction->getId();
            $status = 1;
            $walletTransaction = $this->walletTransactionFactory->create()->getCollection()
            ->addFieldToFilter('auction_id', $auctionActPro->getId())
            ->addFieldToFilter('action', 'credit')
            ->getFirstItem();
            if (empty($walletTransaction->getData())) {
                $this->catalogControllerProductView->refundAmount(
                    $customerId,
                    $entity_id,
                    $productId,
                    $status,
                    $orderId,
                    $auctionDiscount
                );
            }

        }
       
        $orderState = \Magento\Sales\Model\Order::STATE_CLOSED;
        $getOrder->setState($orderState)->setStatus($orderState);
        $getOrder->save();
        return $result;
    }
}
