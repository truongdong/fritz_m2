/**
 * Webkul_Auction DailyDeals.setAttribute
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

 /*jshint jquery:true*/
define(
    [
    "jquery",
    "jquery/ui"
    ],
    function ($) {
    "use strict";
    $.widget(
        'auction.setattr',
        {
        _create: function () {
            var aucType = this.options.auctionType;
            aucType = aucType.split(',');
            aucType.each(
                function (i) {
                $('select[name="product[auction_type]"]  option[value="'+i+'"]').attr('selected', 'selected');
                }
            );
        }
        }
    );
    return $.auction.setattr;
    }
);
