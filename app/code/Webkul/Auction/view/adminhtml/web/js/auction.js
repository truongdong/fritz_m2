require(
    [
    "jquery",
    "mage/mage"
    ],
    function ($) {
    $('#save').click(
        function (e) {
        setTimeout(
            function () {
               
                if (!$(".admin__field-control [id$='-error']").length || $(".admin__field-control [id$='-error'][style='display: none;']").length == $(".admin__field-control [id$='-error']").length) {
                    $('#save').attr('disabled', 'disabled');
                    $('#starting_price').removeAttr('disabled');
                    $('#start_auction_time').removeAttr('disabled');
                    $('#stop_auction_time').removeAttr('disabled');
                }
                
            },
            1
        );
        }
    )
    }
);
