/**
 * Webkul_Auction Incremental Price js
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

 /*jshint jquery:true*/
 define([
     "jquery",
     "Magento_Ui/js/modal/modal",
     'mage/translate',
     "jquery/ui"
 ], function ($, modal,$t) {
     "use strict";
     $.widget('console.js', {
         _create: function () {
             var saveIncPrice = this.options.saveurl;
             $('#html-body').mouseover(function () {
                 $('.delete-option').on('click',function () {
                     $(this).parents('tr').remove();
                 });
             });
             var options = {
                         type: 'popup',
                         title: $t('Set Incremental Price Range'),
                         autoOpen: true,
                         innerScroll:true,
                         buttons: [{
                             text:$t('Add Option'),
                             attr:{
                                 'data-action':'na'
                             },
                             'class': 'action-primary',
                             click: function () {
                                 $('#incremental_price_container')
                                     .append($('#increment_row').clone().html());
                                 $('.delete-option').on('click',function () {
                                     console.log("hiibb");
                                     $(this).parents('tr').remove();
                                 });
                             }
                         },{
                             text: $t('Save'),
                             attr: {
                                 'data-action': 'cancel',
                                 'id':'save-auc-data'
                             },
                             'class': 'action-primary',
                             click: function () {
                                 var priceContainerInput = $('#incremental_price_container input');
                                 priceContainerInput.removeClass('mage-error');
                                 priceContainerInput.each(function () {
                                     if (!$(this).val()) {
                                         $(this).addClass('mage-error');
                                     }
                                 });
                                 if (priceContainerInput.hasClass('mage-error')) {
                                     return false;
                                 }
                                 var loader = $('#wk-loader').clone().html();
                                 var modelFooter = $('#save-auc-data').parent('.modal-footer');
                                 modelFooter.find('img').remove();
                                 console.log("remove");
                                 modelFooter.prepend(loader);
                                 $.ajax({
                                     url: saveIncPrice,
                                     data: $('#price_range_info').serialize(),
                                     type: 'POST',
                                     form_key: FORM_KEY,
                                     dataType:'html',
                                     success: function (transport) {
                                         console.log("ajax")
                                         var response = $.parseJSON(transport);
                                         modelFooter.find('img').remove();
                                         if (response.msg) {
                                            if (jQuery(".message.message-success.success.msgShow").length == 0) {
                                                $(".modal-header").append('<div class="messages"><div class="message message-success success msgShow"><div data-ui-id="messages-message-success">'+response.msg+'</div></div></div>');
                                             }
                                         }
                                         location.reload();
                                     }
                                 });
                             }
                         }]
                     };
             $("#"+this.options.blockId).on("click", function () {
                 $('.modal-popup').remove();
                 console.log("delete");
                 var cont = $('<div />').html($('#datatemo').html());
                 modal(options, cont);
                 cont.modal('openModal');
             });
         }
     });
     return $.console.js;
 });
