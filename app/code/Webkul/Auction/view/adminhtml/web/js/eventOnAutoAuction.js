/**
 * Webkul_Auction autoauc.event
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

 /*jshint jquery:true*/
define(
    [
    "jquery",
    "jquery/ui"
    ],
    function ($) {
    "use strict";
    $.widget(
        'autoauc.event',
        {
        options: {
            autoAuctionOpt: $('#wkauction_auto_auction_opt'),
            reservePriceElm: $('#wkauction_reserve_price'),
            reservePriceParentElm: $('#wkauction_reserve_price').parents('.field-reserve_price')
        },
        _create: function () {
            var reservePriceElm = this.options.reservePriceElm,
            reservePriceParentElm = this.options.reservePriceParentElm;
            if (!this.options.reserve_enable) {
                if (this.options.autoAuctionOpt.val()==1) {
                    reservePriceElm.addClass('required-entry');
                    reservePriceParentElm.addClass('_required');
                }
                this.options.autoAuctionOpt.on(
                    'change',
                    function () {
                    if ($(this).val()==1) {
                    reservePriceElm.addClass('required-entry');
                    reservePriceParentElm.addClass('_required');
                    } else {
                        reservePriceElm.removeClass('required-entry');
                        reservePriceParentElm.removeClass('_required');
                    }
                    }
                );
            }
            var mainthis = this;
            $('#wkauction_product_id').change(
                function () {
                var id = $(this).val();
                if (mainthis.options.product[id] == 'downloadable') {
                    $('.field-min_qty').hide();
                    $('.field-max_qty').hide();
                    $("#wkauction_min_qty").prop('disabled', true);
                    $("#wkauction_max_qty").prop('disabled', true);
                } else {
                    $('.field-min_qty').show();
                    $('.field-max_qty').show();
                }
                }
            );
            $('#save').click(
                function () {
                setTimeout(
                    function () {
                    if (!$(".admin__field-control [id$='-error']").length || $(".admin__field-control [id$='-error'][style='display: none;']").length == $(".admin__field-control [id$='-error']").length) {
                        $('#save').attr('disabled', 'disabled');
                        $('#starting_price').removeAttr('disabled');
                        $('#start_auction_time').removeAttr('disabled');
                        $('#stop_auction_time').removeAttr('disabled');
                    }
                    },
                    1
                );
                }
            )
        }
        }
    );
    return $.autoauc.event;
    }
);
