/**
 * Webkul_Auction requirejs config
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

var config = {
    map: {
        '*': {
            incrementalPriceRange: 'Webkul_Auction/js/incrementalPriceRange',
            forAutoAuction:'Webkul_Auction/js/eventOnAutoAuction',
            setattribute:'Webkul_Auction/js/setattribute'
        }
    }
};
