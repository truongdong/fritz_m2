/**
 * Webkul_Auction requirejs config
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

var config = {
    map: {
        '*': {
            viewonwishlist: 'Webkul_Auction/js/viewonwishlist',
            viewoncategory: 'Webkul_Auction/js/viewoncategory',
            viewoncompare: 'Webkul_Auction/js/viewoncompare',
            viewonproduct: 'Webkul_Auction/js/viewonproduct',
            viewonhistory: 'Webkul_Auction/js/viewonhistory',
            auctionbiddetail: 'Webkul_Auction/js/auctionbiddetail',
            Wkauction: 'Webkul_Auction/js/Wkauction',
            catalogAddToCart:'Webkul_Auction/js/catalog-add-to-cart'
        }
    }
};
