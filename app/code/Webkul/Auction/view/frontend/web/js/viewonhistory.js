/**
 * Webkul_Auction history View Js
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

define(
    [
    "jquery",
    "jquery/ui"
    ],
    function ($) {
    "use strict";
    $.widget(
        'auction.productview',
        {
        _create: function () {
            $('#bidding_amount').keypress(
                function (e) {
                var key, keychar;
                if (window.event) {
                    key = window.event.keyCode;
                } else if (e) {
                    key = e.which;
                } else {
                    return true;
                }
                keychar = String.fromCharCode(key);
                if ((key==null) || (key==0) || (key==8) ||
                    (key==9) || (key==13) || (key==27) || (key==46)) {
                    return true;
                } else if ((("0123456789").indexOf(keychar) > -1)) {
                    return true;
                } else {
                    return false;
                }
                }
            );
            var viewProductOpt = this.options,
            days    = 24*60*60,
            hours   = 60*60,
            minutes = 60;
            $.fn.countdown = function (prop) {
                var options = $.extend(
                    {
                    callback    : function () {
                    alert("");},
                    timestamp   : 0
                    },
                    prop
                );
                var left, d, h, m, s, positions;
                positions = this.find('.position');
                var initialize =  setInterval(
                    function () {
                    left = Math.floor((options.timestamp - (new Date())) / 1000);
                    if (left < 0) {
                        left = 0;
                    }
                    d = Math.floor(left / days);
                    left -= d*days;
                    h = Math.floor(left / hours);
                    left -= h*hours;
                    m = Math.floor(left / minutes);
                    left -= m*minutes;
                    s = left;
                    options.callback(d, h, m, s);
                    if (d==0 && h==0 && m==0 && s==0) {
                        clearInterval(initialize);
                    }
                    },
                    1000
                );
                return this;
            };

            var clockOnPrice = viewProductOpt.auctionData.pro_auction,
                buyIdNow = viewProductOpt.auctionData.pro_buy_it_now,
                allowforbuy = $('#winner-data-container').hasClass('allow-for-buy');
            if (buyIdNow == 1 && !allowforbuy) {
                $('#product-addtocart-button span').text(viewProductOpt.buyItNow);
            } else if (allowforbuy) {
                var bidWinnerCart = $('#winner-data-container').attr('data-cart-label');
                $('#product-addtocart-button span').text(bidWinnerCart);
            } else if (viewProductOpt.auctionType!="") {
                $('.product-add-form').remove();
            }
            $('.product-add-form').show();
            if (clockOnPrice == 1) {
                $('.product-info-price').after($('<p />',{'class':'wk-auction-clock'}));
                clockOnPrice = $('.wk-auction-clock');
            }
            var note =  $('.wk_front_dd_countdownnew'),
                ts = new Date(2012, 0, 1),
                newYear = true;
            if (note.length) {
                if ((new Date()) > ts) {
                    var t = note.attr('data-diff-timestamp')*1000;
                    ts = (new Date()).getTime() + t;
                    newYear = false;
                }
                note.countdown(
                    {
                    timestamp : ts,
                    callback : function (days, hours, minutes, seconds) {
                        var message = "",
                            stopt = viewProductOpt.auctionData.stop_auction_time,
                            timez = "",
                            distr = stopt.split(' '),
                            tzones =  distr[0].split('-'),
                            months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                        if (hours < 10) {
                            hours = "0"+hours;
                        }
                        if (minutes < 10) {
                            minutes = "0"+minutes;
                        }
                        if (seconds < 10) {
                            seconds = "0"+seconds;
                        }
                        message += "<span class='wk_front_dd_set_time' title='Days'>"+days + "</span> Days ";
                        message += "<span class='wk_front_dd_set_time' title='Hours'>"+hours + "</span>:";
                        message += "<span class='wk_front_dd_set_time' title='Minutes'>"+minutes + "</span>:";
                        message += "<span class='wk_front_dd_set_time' title='Seconds'>"+seconds + "</span> ";
                        message += "("+ tzones[2]+' '+months[tzones[1]-1]+', '+tzones[0]+' '+ timez +")";
                        note.html(message);
                        if (clockOnPrice != 0) {
                            clockOnPrice.html(message);
                        }
                        if (hours == '00' && minutes == '00' && seconds == '00') {
                            location.reload();
                        }
                    }
                    }
                );
            }
            /*switch(viewProductOpt.auctionType){
                case 'auction':
                    $('.product-add-form').remove();
                    break;
                case 'buy-it-now':
                    $('#product-addtocart-button span').text(viewProductOpt.buyItNow);
                    $('.product-add-form').show();
                    break;
                default :
                    $('.product-add-form').show();
            }*/
        }
        }
    );
    return $.auction.productview;
    }
);
