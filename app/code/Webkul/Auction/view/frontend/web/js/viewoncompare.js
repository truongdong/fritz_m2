/**
 * Webkul_Auction Compare List Js
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
define(
    [
    "jquery",
    "mage/translate",
    "jquery/ui"
    ],
    function ($, $t) {
    "use strict";
    $.widget(
        'auction.compare',
        {
        _create: function () {
            var viewCategoryOpt = this.options;
            var days    = 24*60*60,
            hours   = 60*60,
            minutes = 60;
            $.fn.countdown = function (prop) {
                var options = $.extend(
                    {
                    callback    : function () {
                        alert("");
                    },
                    timestamp   : 0
                    },
                    prop
                );
                var left, d, h, m, s, positions;
                positions = this.find('.position');
                var initialize =  setInterval(
                    function () {
                    left = Math.floor((options.timestamp - (new Date())) / 1000);
                    if (left < 0) {
                        left = 0;
                    }
                    d = Math.floor(left / days);
                    left -= d*days;
                    h = Math.floor(left / hours);
                    left -= h*hours;
                    m = Math.floor(left / minutes);
                    left -= m*minutes;
                    s = left;
                    options.callback(d, h, m, s);
                    if (d==0 && h==0 && m==0 && s==0) {
                        clearInterval(initialize);
                    }
                    },
                    1000
                );
                return this;
            };
            $('.auction, .buy-it-now').each(
                function () {
                if ($(this).hasClass('auction')) {
                    var colckElm = $(this).find('.wk_cat_count_clock'),
                        timeStamp = new Date(2012, 0, 1),
                        stopTime = colckElm.attr('data-stoptime'),
                        newYear = true,
                        heighestamount = colckElm.attr('data-highest-bid');
                    if ((new Date()) > timeStamp) {
                        timeStamp = colckElm.attr('data-diff_timestamp')*1000;
                        timeStamp = (new Date()).getTime() + timeStamp;
                        newYear = false;
                    }
                    if (colckElm.length) {
                        colckElm.countdown(
                            {
                            timestamp : timeStamp,
                            callback : function (days, hours, minutes, seconds) {
                                var message = "",
                                    timez = "",
                                    distr = stopTime.split(' '),
                                    tzones =  distr[0].split('-'),
                                    months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                                if (hours < 10) {
                                    hours = "0"+hours;
                                }
                                if (minutes < 10) {
                                    minutes = "0"+minutes;
                                }
                                if (seconds < 10) {
                                    seconds = "0"+seconds;
                                }
                                message += "<span class='heighest_bid_amount' style='color:#1979c3;'>"+$t("Highest Bid :")+"<strong>"+heighestamount+"</strong></span><br>";
                                message += "<span class='wk_set_time' title='Days'>"+$t("Bid Ends In: ")+days +"</span>D, ";
                                message += "<span class='wk_set_time' title='Hours'>"+hours+"</span>H : ";
                                message += "<span class='wk_set_time' title='Minutes'>"+minutes+"</span>M : ";
                                message += "<span class='wk_set_time' title='Seconds'>"+seconds+"</span>S";
                                // message += "("+ tzones[2]+' '+months[tzones[1]-1]+', '+tzones[0]+' '+ timez +")";
                                colckElm.html(message);
                                if (hours == '00' && minutes == '00' && seconds == '00') {
                                    colckElm.remove();
                                }
                            }
                            }
                        );
                    }
                }
                var wish = $(".wish").length;
                if (wish) {
                    $('.field.qty').hide();
                }
                var thisParent = $(this).parent('td');
                if ($(this).hasClass('buy-it-now') && $(this).attr('data-winner') != 1) {
                    if (wish) {
                        $('.product-item-inner').show();
                        $(this).next().find('.tocart.primary span').text(viewCategoryOpt.buyItNow);
                    } else {
                        thisParent.find('.tocart.primary span').text(viewCategoryOpt.buyItNow);
                    }
                } else if ($(this).attr('data-winner') == 1) {
                    var cost = $(this).attr('data-winning-amt');
                    if (wish) {
                        $('.product-item-inner').show();
                        $(this).next().find('.tocart.primary span').text('Buy with '+cost);
                    } else {
                        thisParent.find('.tocart.primary span').text('Buy with '+cost);
                    }
                } else {
                    if (wish) {
                        var proLink = $(this).parent(".product-item-info").find('a').attr('href');
                    } else {
                        var proLink = thisParent.find('a').attr('href');
                    }
                    var viewDetail = $('<a />').attr('title', viewCategoryOpt.viewDetail)
                                                .attr('href', proLink)
                                                .addClass('action primary auto-bid-show')
                                               .append($('<span />').text(viewCategoryOpt.viewDetail));
                   if (wish) {
                         $(this).next().find('.actions-primary').html(viewDetail);
                    } else {
                        thisParent.find('.actions-primary').html(viewDetail);
                    }
                }
                }
            );
        }
        }
    );
    return $.auction.compare;
    }
);
