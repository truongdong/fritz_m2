define(
    [
        'Webkul_Auction/js/view/checkout/summary/discount',
        'Magento_Checkout/js/model/cart/totals-processor/default',
        'Magento_Checkout/js/model/totals'
    ],
    function (Component, defaultTotal, totals) {
        'use strict';

        return Component.extend({

            /**
             * Auction discount application procedure
             */
            initialize: function () {
                this._super();
                defaultTotal.estimateTotals();
            },

            /**
             * @override
             */
            isDisplayed: function () {
                var price = 0;
                if (this.totals()) {
                  if (totals.getSegment('auction_discount') !== null) {
                    price = totals.getSegment('auction_discount').value;
                  }
                }
                price = parseFloat(price);
                if (price) {
                    return true;
                } else {
                    return false;
                }
            }
        });
    }
);
