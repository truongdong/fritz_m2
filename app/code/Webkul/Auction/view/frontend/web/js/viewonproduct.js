/**
 * Webkul_Auction Product View Js
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

define(
    [
    "jquery",
    "mage/translate",
    "jquery/ui",
    'jquery/jquery-storageapi'
    ],
    function ($, $t) {
    "use strict";
    $.widget(
        'auction.productview',
        {
        _create: function () {
            $('.wk-auction-view-bid-link').click(
                function () {
                $('#tab-label-bid-details-title').trigger('click');
                }
            );
            var viewProductOption = this.options;

            $(document).ready(function(){

                let currentUrl =  window.location.href;
                $.ajax({
                    type: 'POST',
                    url: currentUrl,
                    dataType: "text",
                    showLoader: true, //use for display loader
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log( errorThrown );
                    },
                    success: function(data) {
                       let auctionBidTable  =  $(data).find('table.wk-auction-bid-detail-table').html();
                        let normalBidRecord  =  $(data).find('div#normal-bid-record').html();
                        let automaticBidRecord  =  $(data).find('div#automatic-bid-record').html();
                        let currrentBid  =  $(data).find('div.wk-auction-clock-span-current-amount').text();
                        let nxtminimumBidAmt  =  $(data).find('div.wk_row.wk_minimum_bid_amount').children('span').text();
                        let bidCount  =  $(data).find('a.wk-auction-view-bid-link.anchr').text();
                        let oldBid =  $('div.wk-auction-clock-span-current-amount').text();
                        //console.log(currrentBid);
                        //to remove message
                        if(currrentBid.replace('$', '') >= oldBid.replace('$', '')){
                             $.cookieStorage.set('mage-messages', '[]');
                        }
                        $('a.wk-auction-view-bid-link.anchr').text(bidCount);
                        $('div.wk_row.wk_minimum_bid_amount').children('span').text(nxtminimumBidAmt);
                        $('div.wk-auction-clock-span-current-amount').text(currrentBid);
                        $('table.wk-auction-bid-detail-table').html(auctionBidTable);
                        $('div#normal-bid-record').html(normalBidRecord);
                        $('div#automatic-bid-record').html(automaticBidRecord);
                    }
                });
                let id = $('.price-box.price-final_price').data('product-id');
                $.ajax({
                    url : viewProductOption.currentTimeUrl,
                    type : 'Post',
                    datatype : 'json',
                    data : {
                        id : id
                    },
                    success: function (response) {
                      $('#current_time').attr('data-diff-timestamp',response.currentDateTime);
                      $('#wk_front_dd_note').attr('data-diff-timestamp',response.auctiondiff.stopTime)
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                      console.log(errorMessage);
                    }
                });
            });
            // $('#bidding_amount').keypress(
            //     function (e) {
            //     var key, keychar;
            //     if (window.event) {
            //         key = window.event.keyCode;
            //     } else if (e) {
            //         key = e.which;
            //     } else {
            //         return true;
            //     }
            //     keychar = String.fromCharCode(key);
            //     if ((key==null) || (key==0) || (key==8) ||
            //         (key==9) || (key==13) || (key==27) || (key==46)) {
            //         return true;
            //     } else if ((("0123456789").indexOf(keychar) > -1)) {
            //         return true;
            //     } else {
            //         return false;
            //     }
            //     }
            // );

            var viewProductOpt = this.options,
            days    = 24*60*60,
            hours   = 60*60,
            minutes = 60;
            $.fn.countdown = function (prop) {
                var options = $.extend(
                    {
                    callback    : function () {
                    alert("");},
                    timestamp   : 0
                    },
                    prop
                );
                var left, d, h, m, s, positions;
                positions = this.find('.position');
                var initialize =  setInterval(
                    function () {
                    left = Math.floor((options.timestamp - (new Date())) / 1000);
                    if (left < 0) {
                        left = 0;
                    }
                    d = Math.floor(left / days);
                    left -= d*days;
                    h = Math.floor(left / hours);
                    left -= h*hours;
                    m = Math.floor(left / minutes);
                    left -= m*minutes;
                    s = left;
                    options.callback(d, h, m, s);
                    if (d==0 && h==0 && m==0 && s==0) {
                        clearInterval(initialize);
                    }
                    },
                    1000
                );
                return this;
            };


            var count = 0;
            var addToCartButtonLabel = $("#product-addtocart-button span").text();
            var clockOnPrice = viewProductOpt.auctionData.pro_auction,
                buyIdNow = viewProductOpt.auctionData.pro_buy_it_now,
                allowforbuy = $('#winner-data-container').hasClass('allow-for-buy');
            if (buyIdNow === 1 && !allowforbuy) {
                $('#product-addtocart-button span').text(viewProductOpt.buyItNow);
            } else if (allowforbuy) {
                $('.box-tocart').addClass('for-winner');
                var bidWinnerCart = $('#winner-data-container').attr('data-cart-label');
                $('#product-addtocart-button span').text(bidWinnerCart);
            } else if(!allowforbuy && viewProductOpt.auctionType=="buy-it-now"){
                $('#product-addtocart-button span').text(viewProductOpt.buyItNow);
            }else if ((buyIdNow == 1 || buyIdNow==0) || (allowforbuy && viewProductOpt.auctionType!="")){
                $('.product-add-form').remove();
            }
            $('.product-add-form').show();
            $('body').on('click', '#product-addtocart-button', function () {
                count = 0;
            });

            $('#product-addtocart-button span').bind("DOMSubtreeModified",function () {
              var title = $(this).text();
              if (title == addToCartButtonLabel) {
                count++;
                if (count == 1) {
                  if (allowforbuy) {
                      var bidWinnerCart = $('#winner-data-container').attr('data-cart-label');
                      $('#product-addtocart-button span').text(bidWinnerCart);
                  }
                }
              }
            });
            var checkCurrentTime = 0;
            checkCurrentTime = setInterval(function () {
                var currentTime =   $('.wk_front_dd_countdownnew_current_time').attr('data-diff-timestamp');
                if (!isNaN(currentTime)) {
                    clearInterval(checkCurrentTime);
                    updateTime();
                }
            }, 500);
            function updateTime()
            {
          var stopTime =   $('.wk_front_dd_countdownnew').attr('data-diff-timestamp');
          var currentTime =   $('.wk_front_dd_countdownnew_current_time').attr('data-diff-timestamp');
          var finalTime = stopTime - currentTime;
           $('.wk_front_dd_countdownnew').attr('data-diff-timestamp',finalTime);
            var note =  $('.wk_front_dd_countdownnew'),
                ts = new Date(2012, 0, 1),
                newYear = true;

                if (note.length) {
                    if ((new Date()) > ts) {
                        var t = note.attr('data-diff-timestamp')*1000;
                        ts = (new Date()).getTime() + t;
                        newYear = false;
                    }
                    note.countdown(
                        {
                        timestamp : ts,
                        callback : function (days, hours, minutes, seconds) {
                            var message = "",
                                stopt = viewProductOpt.auctionData.stop_auction_time,
                                timez = "",
                                distr = stopt.split(' '),
                                tzones =  distr[0].split('-'),
                                months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                            if (days < 10) {
                                days = "0"+days;
                            }
                            if (hours < 10) {
                                hours = "0"+hours;
                            }
                            if (minutes < 10) {
                                minutes = "0"+minutes;
                            }
                            if (seconds < 10) {
                                seconds = "0"+seconds;
                            }
                            message += '<span class="wk_front_dd_set_time wk-auction-clock-span" id="wk-auction-dd" title='+$t("Days ")+'>'+days+' <span class="label wk-auction-clock-label-dd" for="wk-auction-dd"><span>'+$t("Days ")+'</span></span></span>';
                            message += '<span class="wk_front_dd_set_time wk-auction-clock-span" id="wk-auction-hr" title='+$t("Hours")+'> '+hours+' :<span class="label wk-auction-clock-label-hr" for="wk-auction-hr"><span>'+$t("Hours")+'</span></span></span>';
                            message += '<span class="wk_front_dd_set_time wk-auction-clock-span" id="wk-auction-mi" title='+$t("Minutes")+'> '+minutes+' :<span class="label wk-auction-clock-label-mi" for="wk-auction-mi"><span>'+$t("Minutes")+'</span></span></span>';
                            message += '<span class="wk_front_dd_set_time wk-auction-clock-span" id="wk-auction-sec" title='+$t("Seconds")+'> '+seconds+' <span class="label wk-auction-clock-label-sec" for="wk-auction-sec"><span>'+$t("Seconds")+'</span></span></span>';
                            note.html(message);

                            if (hours == '00' && minutes == '00' && seconds == '00') {
                                $.ajax({
                                    type: 'POST',
                                    url: viewProductOpt.cacheFlush,
                                    data: {'product_id':$('input[name=product_id]').val()},
                                    async : false,
                                    success: function (resultData) {
                                       location.reload();
                                    }
                              });
                            }
                        }
                        }
                    );
                }
         }

            $('.reviews-actions .action').on('click', function () {
                $('.wk-auction-bids-record .product').removeClass('items');
                setTimeout(function () {
                    $('.wk-auction-bids-record .product').addClass('items');
                }, 1000);
            });

        }
        }
    );
    return $.auction.productview;

    }
);
