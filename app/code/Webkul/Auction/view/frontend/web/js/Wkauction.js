/**
 * Webkul Wkauction.js
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint jquery:true*/
define(
    [
    "jquery",
    'mage/translate',
    'Magento_Ui/js/modal/alert',
    'Magento_Ui/js/modal/confirm',
    'Magento_Customer/js/customer-data',
    'mage/url',
    "jquery/ui"
    ],
    function ($, $t, alert, confirm, customerData, url) {
    'use strict';
    $.widget(
        'mage.Wkauction',
        {
        _create: function () {
            var self = this;
            var dataForm = $(self.options.auctionformdata);
            dataForm.mage('validation', {});
            customerData.reload([], true);
        }
        }
    );
    return $.mage.Wkauction;
    }
);
