/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'Magento_Checkout/js/model/totals'
    ],
    function (Component, quote, priceUtils, totals) {
        "use strict";
        return Component.extend({
            defaults: {
                isFullTaxSummaryDisplayed: window.checkoutConfig.isFullTaxSummaryDisplayed || false,
                template: 'Webkul_Auction/checkout/cart/totals/discount'
            },
            totals: quote.getTotals(),
            isTaxDisplayedInGrandTotal: window.checkoutConfig.includeTaxInGrandTotal || false,
            isDisplayed: function () {
                var price = 0;
                if (this.totals()) {
                      price = totals.getSegment('auction_discount').value;
                }
                if (price) {
                    return true;
                } else {
                    return false;
                }
            },
            getValue: function () {
                var price = 0;
                if (this.totals()) {
                    price = totals.getSegment('auction_discount').value;
                }
                return this.getFormattedPrice(this.getBaseValue());
            },
            getBaseValue: function () {
                var price = 0;
                if (this.totals() && totals.getSegment('auction_discount').value) {
                    price = parseFloat(totals.getSegment('auction_discount').value);
                }
                return price;
            }
        });
    }
);
