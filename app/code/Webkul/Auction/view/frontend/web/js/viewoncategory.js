/**
 * Webkul_Auction Category View Js
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
define(["jquery", "mage/translate", "jquery/ui"], function ($, $t) {
    "use strict";
    $.widget("auction.categoryview", {
        _create: function () {
            var viewCategoryOpt = this.options;

            var days = 24 * 60 * 60,
                hours = 60 * 60,
                minutes = 60;

            $.fn.countdown = function (prop) {
                var options = $.extend(
                    {
                        callback: function () {
                            alert("");
                        },
                        timestamp: 0,
                    },
                    prop
                );
                var left, d, h, m, s, positions;
                positions = this.find(".position");
                var initialize = setInterval(function () {
                    left = Math.floor((options.timestamp - new Date()) / 1000);
                    if (left < 0) {
                        left = 0;
                    }
                    d = Math.floor(left / days);
                    left -= d * days;
                    h = Math.floor(left / hours);
                    left -= h * hours;
                    m = Math.floor(left / minutes);
                    left -= m * minutes;
                    s = left;
                    options.callback(d, h, m, s);
                    if (d == 0 && h == 0 && m == 0 && s == 0) {
                        clearInterval(initialize);
                    }
                }, 1000);
                return this;
            };
            var promises = [];
            $(".auction, .buy-it-now").each(function () {
                var self = $(this);
                if ($(this).hasClass("auction")) {
                    var wk_cat_count_clock = $(this);
                    let product_id = $(this)
                        .find(".wk_cat_count_clock")
                        .data("product_id");

                    if (product_id != undefined) {
                        var ajaxrequestdone = $.ajax({
                            url: viewCategoryOpt.AuctionDetailsUrl,
                            type: "Post",
                            datatype: "json",
                            data: {
                                id: product_id,
                            },
                            success: function (response) {
                                if (response.auctiondiff != undefined) {
                                    $("#wk_cat_count_clock_" + product_id).attr(
                                        "data-diff_timestamp",
                                        response.auctiondiff.diff
                                    );
                                }
                            },
                            error: function (jqXhr, textStatus, errorMessage) {
                                console.log(errorMessage);
                            },
                        });
                    }
                }
                promises.push(ajaxrequestdone);
            });
            $(".product-item-inner").hide();
            $.when.apply(null, promises).done(function () {
                setTimeout(function () {
                    $(".auction").each(function () {
                        console.log("here");
                        var self = $(this);
                        if ($(this).hasClass("auction")) {
                            var colckElm = $(this).find(".wk_cat_count_clock"),
                                timeStamp = new Date(2012, 0, 1),
                                stopTime = colckElm.attr("data-stoptime"),
                                startTlag = colckElm.attr('data-startflag'),
                                newYear = true,
                                dataTitle = colckElm.attr(
                                    "data-title"
                                ),
                                dataPrice = colckElm.attr(
                                    "data-price"
                                );

                            // Check Upcoming label
                            if(startTlag == 1){
                                var labelParent = $(this).closest(".product-item-info").find(".product-images-cart");
                                labelParent.append("<div class='out-of-stock-label'><img src='"+ window.location.origin +"/media/wysiwyg/labels/upcoming_auctions.png' alt=''></div>");
                            }

                            if (new Date() > timeStamp) {
                                timeStamp =
                                    colckElm.attr("data-diff_timestamp") * 1000;
                                timeStamp = new Date().getTime() + timeStamp;
                                newYear = false;
                            }
                            if (colckElm.length) {
                                colckElm.countdown({
                                    timestamp: timeStamp,
                                    callback: function (
                                        days,
                                        hours,
                                        minutes,
                                        seconds
                                    ) {
                                        var message = "",
                                            timez = "",
                                            distr = stopTime.split(" "),
                                            tzones = distr[0].split("-"),
                                            months = [
                                                "January",
                                                "February",
                                                "March",
                                                "April",
                                                "May",
                                                "June",
                                                "July",
                                                "August",
                                                "September",
                                                "October",
                                                "November",
                                                "December",
                                            ];
                                        if (hours < 10) {
                                            hours = "0" + hours;
                                        }
                                        if (minutes < 10) {
                                            minutes = "0" + minutes;
                                        }
                                        if (seconds < 10) {
                                            seconds = "0" + seconds;
                                        }
                                        message +=
                                            "<span class='data-price'>"
                                            +dataTitle+
                                            "<strong>" +
                                            dataPrice +
                                            "</strong></span>";
                                            if (startTlag == 1) {
                                                message += "<span class='wk_set_time' title='Days'>"+$t("Bid Start In: ")+days +"</span>D" + $t(' d, ');
                                            } else {
                                                message += "<span class='wk_set_time' title='Days'>"+$t("Bid Ends In: ")+days +"</span>" + $t(' d, ');
                                            }
                                        message +=
                                            "<span class='wk_set_time' title='Hours'>" +
                                            hours +
                                            "</span>" + $t(' h, ');
                                        message +=
                                            "<span class='wk_set_time' title='Minutes'>" +
                                            minutes +
                                            "</span> m, ";
                                        message +=
                                            "<span class='wk_set_time' title='Seconds'>" +
                                            seconds +
                                            "</span> s";
                                        // message += "("+ tzones[2]+' '+months[tzones[1]-1]+', '+tzones[0]+' '+ timez +")";
                                        colckElm.html(message);
                                        if (
                                            hours == "00" &&
                                            minutes == "00" &&
                                            seconds == "00"
                                        ) {
                                            $('.out-of-stock-label').remove();
                                            colckElm.remove();
                                        }
                                    },
                                });
                            }


                        }
                        var thisParent = $(this).parents(
                            ".product-item-details"
                        );
                        $(this).parent().addClass('auction');
                        console.log($(this).attr("data-winner"));
                        if (
                            $(this).hasClass("buy-it-now") &&
                            $(this).attr("data-winner") != 1
                        ) {
                            thisParent
                                .find(".tocart.primary span")
                                .text(viewCategoryOpt.buyItNow);
                        } else if ($(this).attr("data-winner") == 1) {
                            var cost = $(this).attr("data-winning-amt");
                            $(this).append("<span>"+$t('This product has already been sold for ')+"<span class='winner-price'>"+ cost +"</span></span>");
                        } else {
                            var proLink = thisParent
                                .find(".product-item-link")
                                .attr("href");
                            var viewDetail = $("<a />")
                                .attr("title", viewCategoryOpt.viewDetail)
                                .attr("href", proLink)
                                .addClass("action primary auto-bid-show")
                                .append(
                                    $("<span />").text(
                                        viewCategoryOpt.viewDetail
                                    )
                                );

                            thisParent
                                .find(".actions-primary")
                                .html(viewDetail);
                        }
                    });
                    $(".product-item-inner").show();
                }, 1000);
            });
        },
    });
    return $.auction.categoryview;
});
