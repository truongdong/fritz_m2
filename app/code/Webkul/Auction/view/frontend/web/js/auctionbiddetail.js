/**
 * Webkul_Auction Product View Js
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

define(
    [
    "jquery",
    "mage/translate",
    "jquery/ui"
    ],
    function ($, $t) {
    "use strict";
    $.widget(
        'auction.auctiondetail',
        {
        _create: function () {
            $(document).ready(
                function () {
                var token = true;
                if ((window.location.href).indexOf("#bid-details") != 0) {
                    setTimeout(
                        function () {
                        $('#tab-label-normal-bid-record').addClass('active');
                        $('#normal-bid-record').css('display','block');
                        },
                        1
                    );
                }
                $('#tab-label-bid-details-title').click(
                    function () {
                    
                    if (token) {
                        setTimeout(
                            function () {
                            $('#tab-label-normal-bid-record').addClass('active');
                            $('#normal-bid-record').css('display','block');
                            },
                            1
                        );
                    }
                    }
                );
                $('#tab-label-normal-bid-record').click(function(){
                    setTimeout(
                        function () {
                            $('#tab-label-automatic-bid-record').removeClass('active');
                            $('#tab-label-normal-bid-record').addClass('active');
                            $('#normal-bid-record').css('display','block');
                            $('#automatic-bid-record').css('display','none');
                        },
                        1
                    );

                });
                $('#tab-label-automatic-bid-record').click(function(){
                    setTimeout(
                        function () {
                            $('#tab-label-normal-bid-record').removeClass('active');
                            $('#tab-label-automatic-bid-record').addClass('active');
                            $('#automatic-bid-record').css('display','block');
                            $('#normal-bid-record').css('display','none');
                         },
                        1
                    );

                })
                $('#tab-label-normal-bid-record-title').click(
                    function () {
                    token = false;
                    $('#tab-label-bid-details-title').trigger('click');
                    setTimeout(
                        function () {
                        $('#tab-label-bid-details').addClass('active');
                        $('#bid-details').css('display','block');
                        token = true;
                        },
                        1
                    );
                    }
                );
                $('#tab-label-automatic-bid-record-title').click(
                    function () {
                    token = false;
                    $('#tab-label-bid-details-title').trigger('click');
                    setTimeout(
                        function () {
                        $('#tab-label-bid-details').addClass('active');
                        $('#bid-details').css('display','block');
                        token = true;
                        },
                        1
                    );
                    }
                );
                }
            )
        }
        }
    );
    return $.auction.auctiondetail;
    }
);
