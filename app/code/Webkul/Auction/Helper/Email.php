<?php
/**
 * Webkul_Auction email helper
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Helper;

use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Catalog\Model\Product;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * Webkul Auction Email helper
 */
class Email extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param \Magento\Framework\App\Helper\Context  $context,
     * @param tateInterface                          $inlineTranslation,
     * @param TransportBuilder                       $transportBuilder,
     * @param StoreManagerInterface                  $storeManager,
     * @param CustomerRepositoryInterface            $customer,
     * @param Product                                $product,
     * @param \Magento\Framework\Pricing\Helper\Data $priceHelper,
     * @param \Webkul\Auction\Helper\Data            $helperData
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        TimezoneInterface $localeDate,
        StateInterface $inlineTranslation,
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,
        CustomerRepositoryInterface $customer,
        Product $product,
        ManagerInterface $messageManager,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Webkul\Auction\Helper\Data $helperData
    ) {
        parent::__construct($context);
        $this->localeDate = $localeDate;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->_customer = $customer;
        $this->_product = $product;
        $this->messageManager = $messageManager;
        $this->_priceHelper = $priceHelper;
        $this->_helperData = $helperData;
    }

    /**
     * [generateTemplate description]
     * @param  Mixed $emailTemplateVariables
     * @param  Mixed $senderInfo
     * @param  Mixed $receiverInfo
     * @return void
     */
    public function generateTemplate(
        $emailTemplateVariables,
        $senderInfo,
        $receiverInfo,
        $emailTempId
    ) {
        $template =  $this->_transportBuilder->setTemplateIdentifier($emailTempId)
                            ->setTemplateOptions(
                                [
                                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                                    'store' => $this->_storeManager->getStore()->getId(),
                                ]
                            )->setTemplateVars($emailTemplateVariables)
                            ->setFrom($senderInfo)
                            ->addTo($receiverInfo['email'], $receiverInfo['name']);
        return $this;
    }

    /**
     * send mail to Winner
     * @param int $winnerId winner customer id
     * @param int $productId product id which bid win
     * @param float $winnerPrice bid amount on which user win auction
     * @return void
     */
    public function sendWinnerMail(
        $winnerId,
        $productId,
        $winnerPrice
    ) {
        
        $customer = $this->_customer->getById($winnerId);
        $product = $this->_product->load($productId);
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        if (!$auctionConfig['admin_email_address']) {
            $this->messageManager->addError(__("Unable to send mail."));
            return;
        }
        $senderInfo = [
                        'name' => 'Admin',
                        'email' => $auctionConfig['admin_email_address']
                    ];
        $receiverInfo = [
            'name' => $customer->getFirstName(),
            'email' => $customer->getEmail()
        ];
        $auctionDetail = $this->_helperData->getAuctionDetail($productId);
        $availdate = strtotime("+".$auctionDetail['days']." days", $auctionDetail['stop_auction_time_stamp']);
        $emailTempVariables = [
            'name' => $receiverInfo['name'],
            'productName' => $product->getName(),
            'ProductUrl' => $product->getProductUrl(),
            'message' => __('You won the bid.'),
            'comment'=> __('Please go and buy this product.'),
            'date' => $this->formatDateTime(date('d. M Y, H:i ')).__('Uhr'),
            'availdate' => $this->formatDateTime(date('d. M Y, H:i ', $availdate)).__('Uhr'),
            'winneramount' => $winnerPrice
        ];
        $this->generateTemplate(
            $emailTempVariables,
            $senderInfo,
            $receiverInfo,
            $auctionConfig['winner_notify_email_template']
        );

        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    /**
     * send mail to Bidder on submit bid
     * @param int $bidderId winner customer id
     * @param int $productId product id which bid win
     * @param float $bidamount bid amount on which user win auction
     * @return void
     */
    public function sendSubmitMailToBidder(
        $bidderId,
        $productId,
        $bidAmount
    ) {
        
        $customer = $this->_customer->getById($bidderId);
        $product = $this->_product->load($productId);
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        if (!$auctionConfig['admin_email_address']) {
            $this->messageManager->addError(__("Unable to send mail."));
            return;
        }
        $senderInfo = [
                        'name' => 'Admin',
                        'email' => $auctionConfig['admin_email_address']
                    ];
        $receiverInfo = [
            'name' => $customer->getFirstName(),
            'email' => $customer->getEmail()
        ];
        $auctionDetail = $this->_helperData->getAuctionDetail($productId);
        $emailTempVariables = [
            'name' => $receiverInfo['name'],
            'productName' => $product->getName(),
            'ProductUrl' => $product->getProductUrl(),
            'message' => __('Congratulation! Your bid has been submitted successfully'),
            'bidamount' => $this->formatPrice($bidAmount),
            'date' => $this->formatDateTime(date('d. M Y, H:i ')).__('Uhr'),
            'enddate' => $this->formatDateTime(date('d. M Y, H:i ', $auctionDetail['stop_auction_time_stamp'])).__('Uhr')
        ];
        $this->generateTemplate(
            $emailTempVariables,
            $senderInfo,
            $receiverInfo,
            $auctionConfig['bidder_notify_email_template']
        );

        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    /**
     * send mail to Bidder on submit auto bid
     * @param int $bidderId winner customer id
     * @param int $productId product id which bid win
     * @param float $bidamount bid amount on which user win auction
     * @return void
     */
    public function sendAutoSubmitMailToBidder(
        $bidderId,
        $productId,
        $bidAmount
    ) {
        
        $customer = $this->_customer->getById($bidderId);
        $product = $this->_product->load($productId);
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        if (!$auctionConfig['admin_email_address']) {
            $this->messageManager->addError(__("Unable to send mail."));
            return;
        }
        $senderInfo = [
                        'name' => 'Admin',
                        'email' => $auctionConfig['admin_email_address']
                    ];
        $receiverInfo = [
            'name' => $customer->getFirstName(),
            'email' => $customer->getEmail()
        ];
        $auctionDetail = $this->_helperData->getAuctionDetail($productId);
        $emailTempVariables = [
            'name' => $receiverInfo['name'],
            'productName' => $product->getName(),
            'ProductUrl' => $product->getProductUrl(),
            'message' => __('Congratulation! Your auto bid has been submitted successfully'),
            'bidamount' => $this->formatPrice($bidAmount),
            'date' => $this->formatDateTime(date('d. M Y, H:i ')).__('Uhr'),
            'enddate' => $this->formatDateTime(date('d. M Y, H:i ', $auctionDetail['stop_auction_time_stamp'])).__('Uhr')
        ];
        $this->generateTemplate(
            $emailTempVariables,
            $senderInfo,
            $receiverInfo,
            $auctionConfig['bidder_notify_email_template']
        );

        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    /**
     * send mail to admin
     * @param int $customerId customer id of bidder
     * @param int $productId product id on which bid apply
     * @param float $bidAmount bid amount which user bid
     * @return void
     */

    public function sendMailToAdmin($customerId, $productId, $bidAmount)
    {
        
        $customer = $this->_customer->getById($customerId);
        $product = $this->_product->load($productId);
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        if (!$auctionConfig['admin_email_address']) {
            $this->messageManager->addError(__("Unable to send mail."));
            return;
        }
        $senderInfo = [
            'name' => $this->scopeConfig->getValue('trans_email/ident_general/name'),
            'email' => $this->scopeConfig->getValue('trans_email/ident_general/email')
        ];
        $receiverInfo = [
            'name' => 'Admin',
            'email' => $auctionConfig['admin_email_address']
        ];
        $auctionDetail = $this->_helperData->getAuctionDetail($productId);
        $emailTempVariables = [
            'name' => $receiverInfo['name'],
            'productName' => $product->getName(),
            'ProductUrl' => $product->getProductUrl(),
            'message' => $customer->getFirstName()." ".$customer->getLastName()
                            .__(' has bidded ').$this->formatPrice($bidAmount)
                            .__(' on this product'),
            'amountlabel' => __('Bid Amount'),
            'comment' => __('Please go and see more bidders.'),
            'amount' => $this->formatPrice($bidAmount),
            'date' => $this->formatDateTime(date('d. M Y, H:i ')).__('Uhr'),
            'bidendtime' => $this->formatDateTime(date('d. M Y, H:i ', $auctionDetail['stop_auction_time_stamp'])).__('Uhr')
        ];

        $this->generateTemplate(
            $emailTempVariables,
            $senderInfo,
            $receiverInfo,
            $auctionConfig['admin_notify_email_template']
        );
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    /**
     * sendAutoMailToAdmin sends mail to admin of auto bid
     * @param int $customerId customer id of bidder
     * @param int $productId product id on which bid apply
     * @param float $bidAmount bid amount which user bid
     */
    public function sendAutoMailToAdmin($customerId, $productId, $bidAmount)
    {
        
        $customer = $this->_customer->getById($customerId);
        $product = $this->_product->load($productId);
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        if (!$auctionConfig['admin_email_address']) {
            $this->messageManager->addError(__("Unable to send mail."));
            return;
        }
        $senderInfo = [
            'name' => $this->scopeConfig->getValue('trans_email/ident_general/name'),
            'email' => $this->scopeConfig->getValue('trans_email/ident_general/email')
        ];

        $receiverInfo = [
            'name' => 'Admin',
            'email' => $auctionConfig['admin_email_address']
        ];
        $auctionDetail = $this->_helperData->getAuctionDetail($productId);
        $emailTempVariables = [
            'name' => $receiverInfo['name'],
            'productName' => $product->getName(),
            'productUrl' => $product->getProductUrl(),
            'message' => $customer->getFirstName()." ".$customer->getLastName()
                            .__(' has bidded auto bid ').$this->formatPrice($bidAmount).__(' on this product'),
            'comment' => __('Please go and see more bidders.'),
            'amountlabel' => __('Auto Bid Amount'),
            'amount' => $this->formatPrice($bidAmount),
            'date' => $this->formatDateTime(date('d. M Y, H:i ')).__('Uhr'),
            'bidendtime' => $this->formatDateTime(date('d. M Y, H:i ', $auctionDetail['stop_auction_time_stamp'])).__('Uhr')
        ];

        $this->generateTemplate(
            $emailTempVariables,
            $senderInfo,
            $receiverInfo,
            $auctionConfig['admin_notify_email_template']
        );
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    /**
     * sendOutBidAutoBidder send mail to auto bidder whose auto bid is out
     * @param int $bidUserId of whom mail has been send
     * @param int $userId in of customer who places higher bid
     * @param string $productId stores product id
     */
    public function sendOutBidAutoBidder($bidUserId, $userId, $productId)
    {
        
        $bidUser = $this->_customer->getById($bidUserId);
        $customer = $this->_customer->getById($userId);
        $customerName = $customer->getFirstName();
        $product = $this->_product->load($productId);
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        if (!$auctionConfig['admin_email_address']) {
            $this->messageManager->addError(__("Unable to send mail."));
            return;
        }
        $senderInfo = [
            'name' => 'Admin',
            'email' => $auctionConfig['admin_email_address']
        ];

        $receiverInfo = [
            'name' => $bidUser->getFirstName(),
            'email' => $bidUser->getEmail()
        ];
        $hike = 0;
        $higheramount = 0;
        $yourbid = 0;
        $auctionDetail = $this->_helperData->getAuctionDetail($productId);
        $bidUserPrice = $this->_helperData
            ->getAutomaticBidAmountDataByCustomerId($bidUserId, $productId, $auctionDetail['entity_id']);
        $userPrice = $this->_helperData
            ->getNormalBidAmountDataByCustomerId($userId, $productId, $auctionDetail['entity_id']);
        if ($bidUserPrice->getEntityId() && $userPrice->getEntityId()) {
            $hike = $userPrice->getAuctionAmount() - $bidUserPrice->getAmount();
            $yourbid = $bidUserPrice->getAmount();
            $higheramount = $userPrice->getAuctionAmount();
        }
        $emailTempVariables = [
            'name' => $receiverInfo['name'],
            'productName' => $product->getName(),
            'ProductUrl' => $product->getProductUrl(),
            'message' => __("New higher bid than your's bid on the product"),
            'comment' => __('Please bid again and get chance to win this product.'),
            'hike' => $this->formatPrice($hike),
            'higheramount' => $this->formatPrice($higheramount),
            'date' => $this->formatDateTime(date('d. M Y, H:i ')).__('Uhr'),
            'yourbid' => $this->formatPrice($yourbid),
            'bidendtime' => $this->formatDateTime(date('d. M Y, H:i ', $auctionDetail['stop_auction_time_stamp'])).__('Uhr')
        ];

        $this->generateTemplate(
            $emailTempVariables,
            $senderInfo,
            $receiverInfo,
            $auctionConfig['outbid_notify_email_template']
        );
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    /**
     * sendMailToMembers use to send mails to all the members of normal bid]
     * @param [int] $bidUserId  [holds customer id which place last bid]
     * @param [int] $bidUserId   [to whom mail has been send]
     * @param [string] $productId [product id on which customer places bid]
     */
    public function sendMailToMembers($bidUserId, $userId, $productId)
    {
        
        $bidUser = $this->_customer->getById($bidUserId);
        $customer = $this->_customer->getById($userId);
        $customerName = $customer->getFirstName()." ".$customer->getLastName();
        $product = $this->_product->load($productId);
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        if (!$auctionConfig['admin_email_address']) {
            $this->messageManager->addError(__("Unable to send mail."));
            return;
        }
        $senderInfo = [
            'name' => 'Admin',
            'email' => $auctionConfig['admin_email_address']
        ];

        $receiverInfo = [
            'name' => $bidUser->getFirstName(),
            'email' => $bidUser->getEmail()
        ];

        $hike = 0;
        $higheramount = 0;
        $yourbid = 0;
        $auctionDetail = $this->_helperData->getAuctionDetail($productId);
        $bidUserPrice = $this->_helperData
            ->getNormalBidAmountDataByCustomerId($bidUserId, $productId, $auctionDetail['entity_id']);
        $userPrice = $this->_helperData
            ->getNormalBidAmountDataByCustomerId($userId, $productId, $auctionDetail['entity_id']);
        if ($bidUserPrice->getEntityId() && $userPrice->getEntityId()) {
            $hike = $userPrice->getAuctionAmount() - $bidUserPrice->getAuctionAmount();
            $yourbid = $bidUserPrice->getAuctionAmount();
            $higheramount = $userPrice->getAuctionAmount();
        }
        $emailTempVariables = [
            'name' => $receiverInfo['name'],
            'productName' => $product->getName(),
            'ProductUrl' => $product->getProductUrl(),
            'message' => __("New higher bid than your's bid on the product"),
            'comment' => __('Please bid again and get chance to win this product.'),
            'hike' => $this->formatPrice($hike),
            'higheramount' => $this->formatPrice($higheramount),
            'date' => $this->formatDateTime(date('d. M Y, H:i ')).__('Uhr'),
            'yourbid' => $this->formatPrice($yourbid),
            'bidendtime' => $this->formatDateTime(date('d. M Y, H:i ', $auctionDetail['stop_auction_time_stamp'])).__('Uhr')
        ];

        $this->generateTemplate(
            $emailTempVariables,
            $senderInfo,
            $receiverInfo,
            $auctionConfig['outbid_notify_email_template']
        );
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    /**
     * sendAutoMailUsers sends mail to users of auto bid
     * @param int $bidUserId holds customer id which place last bid
     * @param int $userId customer id
     * @param int $productId holds product id on which bid has been placed
     */
    public function sendAutoMailUsers($bidUserId, $userId, $productId)
    {
        
        $bidUser = $this->_customer->getById($bidUserId);
        $customer = $this->_customer->getById($userId);
        $customerName = $customer->getFirstName()." ".$customer->getLastName();
        $product = $this->_product->load($productId);
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        if (!$auctionConfig['admin_email_address']) {
            $this->messageManager->addError(__("Unable to send mail."));
            return;
        }
        $senderInfo = [
            'name' => 'Admin',
            'email' => $auctionConfig['admin_email_address']
        ];

        $receiverInfo = [
            'name' => $bidUser->getFirstName(),
            'email' => $bidUser->getEmail()
        ];

        $hike = 0;
        $higheramount = 0;
        $yourbid = 0;
        $auctionDetail = $this->_helperData->getAuctionDetail($productId);
        $bidUserPrice = $this->_helperData
            ->getAutomaticBidAmountDataByCustomerId($bidUserId, $productId, $auctionDetail['entity_id']);
        $userPrice = $this->_helperData
            ->getAutomaticBidAmountDataByCustomerId($userId, $productId, $auctionDetail['entity_id']);
        if ($bidUserPrice->getEntityId() && $userPrice->getEntityId()) {
            $hike = $userPrice->getAmount() - $bidUserPrice->getAmount();
            $yourbid = $bidUserPrice->getAmount();
            $higheramount = $userPrice->getAmount();
        }
        $emailTempVariables = [
            'name' => $receiverInfo['name'],
            'productName' => $product->getName(),
            'ProductUrl' => $product->getProductUrl(),
            'message' => __("New higher bid than your's bid on the product"),
            'comment' => __('Please bid again and get chance to win this product.'),
            'hike' => $this->formatPrice($hike),
            'higheramount' => $this->formatPrice($higheramount),
            'date' => $this->formatDateTime(date('d. M Y, H:i ')).__('Uhr'),
            'yourbid' => $this->formatPrice($yourbid),
            'bidendtime' => $this->formatDateTime(date('d. M Y, H:i ', $auctionDetail['stop_auction_time_stamp'])).__('Uhr')
            
        ];

        $this->generateTemplate(
            $emailTempVariables,
            $senderInfo,
            $receiverInfo,
            $auctionConfig['outbid_notify_email_template']
        );
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    public function sendMailForTransaction($customerId, $params, $store)
    {
        $action = $params['action'];
        $customer = $this->_customer->getById($customerId);
        $this->sendEmailToCustomer($customer, $params, $store, $action);
        $this->sendEmailToAdmin($customer, $params, $store, $action);
    }

    public function notifyWalletExpiry($customerId, $expiredDate)
    {
        $customer = $this->_customer->getById($customerId);
        $customerName = $customer->getFirstName();
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        if (!$auctionConfig['admin_email_address']) {
            $this->messageManager->addError(__("Unable to send mail."));
            return;
        }
        $templateId = $this->_helperData->getWalletExpiryTemplateId();
        $senderInfo = [
          'name' => 'Admin',
          'email' => $auctionConfig['admin_email_address']
        ];

        $receiverInfo = [
          'name' => $customerName,
          'email' => $customer->getEmail()
        ];
        $emailTempVariables = [
          'customer_name' => $customerName,
          'date' => $expiredDate
        ];

        $this->generateTemplate(
            $emailTempVariables,
            $senderInfo,
            $receiverInfo,
            $templateId
        );
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }
    public function sendTransferRequestEmailToAdmin($wholedata)
    {
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        if (!$auctionConfig['admin_email_address']) {
            $this->messageManager->addError(__("Unable to send mail."));
            return;
        }
        $emailTemplateId = $this->_helperData->getMailTemplateForTransferRequestEmailToAdmin();
        try {
            $emailTempVariables = [];
            $customer = $this->_customer->getById($wholedata['customer_id']);
            $customerName = $customer->getFirstName();
            $emailTempVariables['customername'] = $customerName;
            $emailTempVariables['note'] = $wholedata['note'];
            $emailTempVariables['bankdetails'] = $wholedata['bank_details'];
            $senderInfo = [
              'name' => 'Admin',
              'email' => $auctionConfig['admin_email_address']
            ];
            $receiverInfo = [
              'name' => 'Admin',
              'email' => $auctionConfig['admin_email_address']
            ];
            $this->_inlineTranslation->suspend();
            $this->generateTemplate(
                $emailTempVariables,
                $senderInfo,
                $receiverInfo,
                $emailTemplateId
            );
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    public function sendEmailToCustomer($customer, $params, $store, $action)
    {
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        if (!$auctionConfig['admin_email_address']) {
            $this->messageManager->addError(__("Unable to send mail."));
            return;
        }
        $emailTemplateId = $this->getMailTemplateForTransactionForCustomer($action);
        try {
            $emailTempVariables = $params;
            $customerName = $customer->getFirstName();
            $emailTempVariables['customername'] = $customerName;
            $emailTempVariables['store'] = $store;
            $receiverInfo = [];

            $receiverInfo = [
                'name' => $customerName,
                'email' => $customer->getEmail(),
            ];
            $senderInfo = [
                'name' => 'Admin',
                'email' => $auctionConfig['admin_email_address']
            ];
            $this->_inlineTranslation->suspend();
            $this->generateTemplate(
                $emailTempVariables,
                $senderInfo,
                $receiverInfo,
                $emailTemplateId
            );
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    public function sendEmailToAdmin($customer, $params, $store, $action)
    {
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        if (!$auctionConfig['admin_email_address']) {
            $this->messageManager->addError(__("Unable to send mail."));
            return;
        }
        $emailTemplateId = $this->getMailTemplateForTransactionForAdmin($action);
        try {
            $emailTempVariables = $params;
            $customerName = $customer->getFirstName();
            $emailTempVariables['customername'] = $customerName;
            $emailTempVariables['store'] = $store;
            $senderInfo = [
                'name' => 'Admin',
                'email' => $auctionConfig['admin_email_address']
            ];
            $receiverInfo = [
                'name' => 'Admin',
                'email' => $auctionConfig['admin_email_address']
            ];
            $this->_inlineTranslation->suspend();
            $this->generateTemplate(
                $emailTempVariables,
                $senderInfo,
                $receiverInfo,
                $emailTemplateId
            );
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    public function getMailTemplateForTransactionForCustomer($action)
    {
        if ($action == 'credit') {
            return $this->_helperData->getWalletRechargeTemplateIdForCustomer();
        } else {
            return $this->_helperData->getWalletUsedTemplateIdForCustomer();
        }
    }

    public function getMailTemplateForTransactionForAdmin($action)
    {
        if ($action == 'credit') {
            return $this->_helperData->getWalletRechargeTemplateIdForAdmin();
        } else {
            return $this->_helperData->getWalletUsedTemplateIdForAdmin();
        }
    }

    /**
     * get currency in format
     * @param $amount float
     * @return string
     *
     */
    public function formatPrice($amount)
    {
        return $this->_priceHelper->currency($amount, true, false);
    }
    public function sendMailForNormalBiddingBehalf($customerId, $productId, $bidAmount)
    {
        
        $customer = $this->_customer->getById($customerId);
        $customerName = $customer->getFirstName();
        $product = $this->_product->load($productId);
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        if (!$auctionConfig['admin_email_address']) {
            $this->messageManager->addError(__("Unable to send mail."));
            return;
        }
        $senderInfo = [
            'name' => 'Admin',
            'email' => $auctionConfig['admin_email_address']
        ];

        $receiverInfo = [
            'name' => $customerName,
            'email' => $customer->getEmail(),
        ];
        $auctionDetail = $this->_helperData->getAuctionDetail($productId);
        $emailTempVariables = [
            'name' => $receiverInfo['name'],
            'productName' => $product->getName(),
            'productUrl' => $product->getProductUrl(),
            'message' => __(' We have bidded a bid of ').
            $this->formatPrice($bidAmount).__(' on Your behalf on this product'),
            'comment' => __('Please go and see more bidders.'),
            'amountlabel' => __('Bid Amount'),
            'amount' => $bidAmount,
            'date' => $this->localeDate->date()->format('j. F Y, G:i ').__('Uhr'),
            'bidendtime' => date('j. F Y, G:i ', $auctionDetail['stop_auction_time_stamp']).__('Uhr')
        ];

        $this->generateTemplate(
            $emailTempVariables,
            $senderInfo,
            $receiverInfo,
            $auctionConfig['notify_email_template_onbehalf']
        );
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    private function formatDateTime($date, $timezone = "Europe/Berlin"){
        $date = new \DateTime($date);

        // the international date formater object
        $formatter = new \IntlDateFormatter(
            "de-DE",
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::NONE,
            $timezone,
            \IntlDateFormatter::GREGORIAN,
            "dd. MMMM YYYY, H:mm "
        );
        return $formatter->format($date);
    }
}
