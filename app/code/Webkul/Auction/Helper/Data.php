<?php
/**
 * Webkul_Auction data helper
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Customer\Model\SessionFactory as CustomerSession;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Magento\Catalog\Model\Product;
use Magento\Sales\Model\OrderFactory;
use Webkul\Auction\Model\WallettransactionFactory;
use Webkul\Auction\Model\FeerecordFactory;
use Webkul\Auction\Model\ProductFactory as AuctionProductFactory;
use Webkul\Auction\Model\FeetransactionFactory;
use Webkul\Auction\Model\ResourceModel\Product\CollectionFactory as AuctCollFactory;
use Webkul\Auction\Model\AmountFactory;
use Webkul\Auction\Model\AutoAuctionFactory;
use Webkul\Auction\Model\IncrementalPriceFactory;
use Webkul\Auction\Model\ProductRepository;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const AUCTION_PRODUCT_SKU = 'wk_auction_amount';
    /**
     * @var Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dateTime;

    /**
     * @var TimezoneInterface
     */
    protected $_timezoneInterface;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Magento\Directory\Helper\Data
     */
    protected $_directoryHelper;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var Magento\Sales\Model\OrderFactory;
     */
    protected $_orderModel;

    /**
     * @var Webkul\Auction\Model\WallettransactionFactory
     */
    protected $_walletTransaction;

    /**
     * @var Webkul\Auction\Model\FeerecordFactory
     */
    protected $_feerecord;

    /**
     * @var Webkul\Auction\Model\FeetransactionFactory
     */
    protected $_feetransaction;

    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    protected $_localeCurrency;

    /**
     * @var AutoAuctionFactory
     */
    protected $_autoAuctionFactory;

    /**
     * @var AuctCollFactory
     */
    protected $_auctionProFactory;

    /**
     * @var IncrementalPriceFactory
     */
    protected $_incrementalPriceFactory;

    /**
     * @var AmountFactorye
     */
    protected $_aucAmountFactory;

    /**
     * @var \Webkul\Auction\Model\WinnerDataFactory
     */
    protected $_winnerData;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $_priceCurrency;

     /**
      * @var AuctionProductFactory
      */
    protected $auctionProductFactory;

    /**
     * @param \Magento\Framework\App\Helper\Context       $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param TimezoneInterface                           $timezoneInterface
     * @param MagentoStoreModelStoreManagerInterface      $storeManager
     * @param AuctCollFactory                             $auctionProFactory
     * @param AmountFactory                               $aucAmountFactory
     * @param OrderFactory                                $orderModel
     * @param WallettransactionFactory                    $walletTransaction
     * @param AutoAuctionFactory                          $autoAuctionFactory
     * @param IncrementalPriceFactory                     $incPriceFactory
     * @param AuctionProductFactory                       $auctionProductFactory
     */
    public function __construct(
        AuctionProductFactory $auctionProductFactory,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Directory\Model\Currency $currency,
        TimezoneInterface $timezoneInterface,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Helper\Data $directoryHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\Locale\CurrencyInterface $localeCurrency,
        PriceHelper $priceHelper,
        Product $product,
        OrderFactory $orderModel,
        WallettransactionFactory $walletTransaction,
        FeetransactionFactory $feetransaction,
        FeerecordFactory $feerecord,
        \Webkul\Auction\Model\WalletrecordFactory $walletRecord,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        AuctCollFactory $auctionProFactory,
        AmountFactory $aucAmountFactory,
        AutoAuctionFactory $autoAuctionFactory,
        IncrementalPriceFactory $incPriceFactory,
        \Webkul\Auction\Model\WinnerDataFactory $winnerData,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        CustomerSession $customerSession,
        \Webkul\Auction\Model\ResourceModel\Feerecord\CollectionFactory $feeRecord,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Webkul\Auction\Model\WalletrecordRepository $walletRecordRepositry,
        ProductRepository $productRepository,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->auctionProductFactory = $auctionProductFactory;
        $this->_dateTime = $dateTime;
        $this->_currency = $currency;
        $this->_auctionProFactory = $auctionProFactory;
        $this->_timezoneInterface = $timezoneInterface;
        $this->_storeManager = $storeManager;
        $this->_directoryHelper = $directoryHelper;
        $this->_productFactory = $productFactory;
        $this->_localeCurrency = $localeCurrency;
        $this->priceHelper = $priceHelper;
        $this->product = $product;
        $this->_orderModel = $orderModel;
        $this->_walletTransaction = $walletTransaction;
        $this->_feetransaction = $feetransaction;
        $this->_feerecord = $feerecord;
        $this->_walletRecordFactory = $walletRecord;
        $this->_localeDate = $localeDate;
        $this->feeRecord = $feeRecord;
        $this->_aucAmountFactory = $aucAmountFactory;
        $this->_autoAuctionFactory = $autoAuctionFactory;
        $this->_incrementalPriceFactory = $incPriceFactory;
        $this->_customerSession = $customerSession->create();
        $this->_priceCurrency = $priceCurrency;
        $this->_winnerData = $winnerData;
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->walletRecordRepositry = $walletRecordRepositry;
        $this->productRepository = $productRepository;
        $this->jsonHelper = $jsonHelper;
    }

    public function walletAmtReturn($auctionProductId, $auctionId = 0)
    {
        $transactionTable = $this->_feetransaction->create()->getResource()->getTable('wk_auction_fee_transaction');
        $auctionProductFactory = $this->auctionProductFactory->create()->getResource()->getTable('wk_auction_product');
        $customerArgs = [];
        $joinCollection = $this->feeRecord->create();
        $joinCollection->join(
            ['tr'=>$transactionTable],
            "main_table.entity_id = tr.bidfee_id"
        )->addFieldToFilter('main_table.product_id', $auctionProductId)
        ->addFieldToFilter('tr.status', 0)
        ->addFieldToFilter('main_table.stop_auction_time', ['gt'=>$this->_dateTime->gmtDate()]);
        $customId = [];
        foreach ($joinCollection->getData() as $key => $fee) {
            $customId[$key] = $fee['entity_id'];
            if (isset($customerArgs[$fee['customer_id']])) {
                $customerArgs[$fee['customer_id']] = $customerArgs[$fee['customer_id']] + $fee['deducted_amount'];
            } else {
                $customerArgs[$fee['customer_id']] =  $fee['deducted_amount'];
            }

        }

            $currentCurrencyCode = $this->getCurrentCurrencyCode();
            $productName = $this->product->load($auctionProductId)->getName();
        foreach ($customerArgs as $customerId => $amount) {
            $transactionNote = __('Auction cancel on %1: credited amount', $productName);
            $walletTransactionModel = $this->_walletTransaction->create();
            $walletTransactionModel->setCustomerId($customerId)
            ->setAmount($amount)
            ->setCurrAmount($amount)
            ->setStatus(1)
            ->setCurrencyCode($currentCurrencyCode)
            ->setAction('credit')
            ->setTransactionNote($transactionNote)
            ->setOrderId(0)
            ->setExpiredStatus(0)
            ->setTransactionAt($this->_dateTime->gmtDate());
            $walletTransactionModel->save();
            //Add wallet amount

            $totalWalletAmt = $this->walletRecordRepositry->getTotalWalletAmount($customerId);

            $totalRemainingAmount = floatval($totalWalletAmt->getRemainingAmount()) + floatval($amount);
            $totalUsedAmount = floatval($totalWalletAmt->getUsedAmount()) - floatval($amount);
            $totalWalletAmt->setRemainingAmount($totalRemainingAmount);
            $totalWalletAmt->setUsedAmount($totalUsedAmount);
            $totalWalletAmt->save();
        }
        foreach ($customId as $v) {
            $transactionTable = $this->_feetransaction->create()->load($v, 'entity_id')->setStatus(1)->save();
        }
    }

    /**
     * $dateTime
     *
     * @return timezone datetime
     */
    public function getLocaleDateTime($dateTime)
    {
        return $this->_timezoneInterface->date(new \DateTime($dateTime))->format('Y/m/d H:i:s a');
    }
     /**
      * Get Configuration Detail of Auction
      * @return array of Auction Configuration Detail
      */
    public function getAuctionConfiguration()
    {
        $auctionConfig=[
            'enable' => $this->scopeConfig->getValue('wk_auction/general_settings/enable'),
            'auction_rule' => $this->scopeConfig->getValue('wk_auction/general_settings/auction_rule'),
            'show_bidder' => $this->scopeConfig->getValue('wk_auction/general_settings/show_bidder'),
            'show_price' => $this->scopeConfig->getValue('wk_auction/general_settings/show_price'),
            'reserve_enable' => $this->scopeConfig->getValue('wk_auction/reserve_option/enable'),
            'reserve_price' => $this->scopeConfig->getValue('wk_auction/reserve_option/price'),
            'show_curt_auc_price' => $this->scopeConfig->getValue('wk_auction/general_settings/show_curt_auc_price'),
            'show_auc_detail' => $this->scopeConfig->getValue('wk_auction/general_settings/show_auc_detail'),
            'auto_enable' => $this->scopeConfig->getValue('wk_auction/auto/enable'),
            'auto_auc_limit' => $this->scopeConfig->getValue('wk_auction/auto/limit'),
            'show_auto_details' => $this->scopeConfig->getValue('wk_auction/auto/show_auto_details'),
            'auto_use_increment' => $this->scopeConfig->getValue('wk_auction/auto/use_increment'),
            'show_autobidder_name' => $this->scopeConfig->getValue('wk_auction/auto/show_autobidder_name'),
            'show_auto_bid_amount' => $this->scopeConfig->getValue('wk_auction/auto/show_bid_amount'),
            'show_auto_outbid_msg' => $this->scopeConfig->getValue('wk_auction/auto/show_auto_outbid_msg'),
            'enable_auto_outbid_msg' => $this->scopeConfig->getValue('wk_auction/auto/enable_auto_outbid_msg'),
            'show_winner_msg' => $this->scopeConfig->getValue('wk_auction/general_settings/show_winner_msg'),
            'increment_auc_enable' => $this->scopeConfig->getValue('wk_auction/increment_option/enable'),
            'enable_admin_email' => $this->scopeConfig->getValue('wk_auction/emails/enable_admin_email'),
            'admin_notify_email_template' => $this->scopeConfig
                                                    ->getValue('wk_auction/emails/notify_email_template_onbehalf'),
            'notify_email_template_onbehalf' => $this->scopeConfig
                                                    ->getValue('wk_auction/emails/admin_notify_email_template'),
            'enable_outbid_email' => $this->scopeConfig->getValue('wk_auction/emails/enable_outbid_email'),
            'outbid_notify_email_template' => $this->scopeConfig
                                                    ->getValue('wk_auction/emails/outbid_notify_email_template'),
            'enable_winner_notify_email'  =>  $this->scopeConfig
                                                    ->getValue('wk_auction/emails/enable_winner_notify_email'),
            'winner_notify_email_template' => $this->scopeConfig
                                                    ->getValue('wk_auction/emails/winner_notify_email_template'),
            'admin_email_address' => $this->scopeConfig->getValue('wk_auction/emails/admin_email_address'),
            'enable_submit_bid_email' => $this->scopeConfig->getValue('wk_auction/emails/enable_submit_bid_email'),
            'bidder_notify_email_template' => $this->scopeConfig
            ->getValue('wk_auction/emails/bidder_notify_email_template'),
            'fee_deduction_status' => $this->scopeConfig->getValue('wk_auction/auction_wallet/fee_deduction_status'),
            'enable_wallet' => $this->scopeConfig->getValue('wk_auction/auction_wallet/enable_wallet'),
            'fee_deduction' => $this->getFeePercent()
        ];
        return $auctionConfig;
    }

     /**
      * @param object $product
      * @return html string
      */
    public function getWishListPrice($product, $price)
    {
        $modEnable = $this->scopeConfig->getValue('wk_auction/general_settings/enable');
        $auctionConfig = $this->getAuctionConfiguration();
        $content = "";
        if ($modEnable) {
            $product = $this->_productFactory->create()
                                ->load($product->getId());
            $auctionOpt = $product->getAuctionType();
            $auctionOpt = explode(',', $auctionOpt);
            $content = $this->wishListProduct($product, $auctionOpt, $price, $auctionConfig);
            return  $content;
        }
    }
    public function wishListProduct($product, $auctionOpt, $price, $auctionConfig)
    {
        $status = true;
        $auctionData = $this->_auctionProFactory->create()
                                    ->addFieldToFilter('product_id', $product->getEntityId())
                                    ->addFieldToFilter('status', 0)
                                    ->addFieldToFilter('auction_status', 1)
                                    ->getFirstItem()->getData();
            $clock = "";
            $htmlDataAttr = "";
        if ($auctionData) {
            $currentAuctionPriceData = $this->_aucAmountFactory->create()->getCollection()
                                        ->addFieldToFilter('product_id', ['eq' => $product->getEntityId()])
                                        ->addFieldToFilter('auction_id', ['eq'=> $auctionData['entity_id']])
                                        ->setOrder('auction_amount', 'DESC')
                                        ->getFirstItem();
            if ($currentAuctionPriceData->getAuctionAmount()) {
                $highestamount = $this->priceHelper->currency(
                    $currentAuctionPriceData->getAuctionAmount(),
                    true,
                    false
                );
            } else {
                $highestamount = $this->priceHelper->currency(0.00, true, false);
            }
            $today = $this->_timezoneInterface->date()->format('m/d/y H:i:s');
            $startAuctionTime = $this->_timezoneInterface->date(new \DateTime($auctionData['start_auction_time']))
                                        ->format('m/d/y H:i:s');
            $stopAuctionTime = $this->_timezoneInterface->date(new \DateTime($auctionData['stop_auction_time']))
                                        ->format('m/d/y H:i:s');
            $difference = strtotime($stopAuctionTime) - strtotime($today);
            if ($difference > 0 && $startAuctionTime < $today) {
                $clock = '<p class="wk_cat_count_clock wk_auction_count_clock_'.$product->getEntityId().'"
                        id="wk_cat_count_clock_'.$product->getEntityId().'"
                        data-stoptime="'.$auctionData['stop_auction_time']
                        .'" data-diff_timestamp ="'.$difference.'"
                        data-product_id ="'.$product->getEntityId().'" data-highest-bid="'.$highestamount.'"></p>';
            } elseif (strtotime($startAuctionTime) > strtotime($today)) {
                $difference = strtotime($startAuctionTime) - strtotime($today);
                $clock = '<p class="wk_cat_count_clock" data-stoptime="'.$auctionData['stop_auction_time']
                .'" data-diff_timestamp ="'.$difference.'" data-highest-bid="'.
                $highestamount.'" data-highest-bid-amount="'.
                $highestamount.'"
                data-product_id ="'.$product->getEntityId().'"
                id="wk_cat_count_clock_'.$product->getEntityId().'"
                data-startflag="1" data-open-bid-amount="'.
                $this->priceHelper->currency($auctionData['starting_price'], true, false).'"></p>';
            }
            if ((!$auctionConfig['reserve_enable'] && $currentAuctionPriceData->getEntityId()) ||
            ($auctionConfig['reserve_enable'] && $currentAuctionPriceData->getEntityId()
            && $auctionData['reserve_price'] <= $currentAuctionPriceData->getAuctionAmount())) {
                $status = false;
            }
        }

            $auctionData = $this->_auctionProFactory->create()
                                    ->addFieldToFilter('product_id', ['eq'=>$product->getEntityId()])
                                    ->addFieldToFilter('auction_status', ['eq'=>0])
                                    ->addFieldToFilter('status', ['eq'=>0])->getFirstItem()->getData();

        if (isset($auctionData['entity_id'])) {
            $winnerBidDetail = $this->getWinnerBidDetail($auctionData['entity_id']);
            if ($winnerBidDetail && $this->_customerSession->isLoggedIn()) {
                $winnerCustomerId = $winnerBidDetail->getCustomerId();
                $currentCustomerId = $this->_customerSession->getCustomerId();
                if ($currentCustomerId == $winnerCustomerId) {
                    $price = $winnerBidDetail->getBidType() == 'normal' ? $winnerBidDetail->getAuctionAmount():
                                                            $winnerBidDetail->getWinningPrice();
                    $formatedPrice = $this->priceHelper->currency($price, true, false);
                    $htmlDataAttr = 'data-winner="1" data-winning-amt="'.$formatedPrice.'"';
                }
            }
        }
        $content = '';
         /**
             * 2 : use for auction
             * 1 : use for Buy it now
             */

            $auctionConfig = $this->getAuctionConfiguration();
        if (in_array(2, $auctionOpt) && in_array(1, $auctionOpt) && $status) {
            $content = '<p>'.$price.'</p><div class="wish"></div>
                <div class="auction buy-it-now " '.$htmlDataAttr.'>'.$clock.'</div>';
        } elseif (in_array(2, $auctionOpt)) {
            $content = '<p>'.$price.'</p><div class="wish"></div>
                <div class="auction" '.$htmlDataAttr.'>'.$clock.'</div>';
        } elseif (in_array(1, $auctionOpt)) {
            $content = '<p>'.$price.'</p><div class="wish"></div><div class="buy-it-now"></div>';
        }
        return  $content;
    }

    /**
     * @param object $product
     * @return html string
     */
    public function getProductAuctionDetail($product)
    {
        $clock = '';
        $modEnable = $this->scopeConfig->getValue('wk_auction/general_settings/enable');
        $auctionConfig = $this->getAuctionConfiguration();
        $content = "";
        $product = $this->getProductData($product->getId());
        $auctionOpt = $product->getAuctionType();
        $auctionOpt = explode(',', $auctionOpt);

        $auctionData = $this->getAuctionData($product->getEntityId());
        if ($auctionData) {
            $currentAuctionPriceData = $this->_aucAmountFactory->create()->getCollection()
                                        ->addFieldToFilter('product_id', ['eq' => $product->getEntityId()])
                                        ->addFieldToFilter('auction_id', ['eq'=> $auctionData['entity_id']])
                                        ->setOrder('auction_amount', 'DESC')
                                        ->getFirstItem();
            if ($currentAuctionPriceData->getAuctionAmount()) {
                $highestamount = $this->priceHelper->currency(
                    $currentAuctionPriceData->getAuctionAmount(),
                    true,
                    false
                );
            } else {
                $highestamount = $this->priceHelper->currency(0.00, true, false);
            }

            $today = $this->getCurrentDateTime();
            $startAuctionTime = $this->converToTz($auctionData['start_auction_time']);
            $stopAuctionTime = $this->converToTz($auctionData['stop_auction_time']);

            $difference = strtotime($stopAuctionTime) - strtotime($today);

            $highestPrice = floatval($currentAuctionPriceData->getAuctionAmount());
            $startPrice = floatval($auctionData['starting_price']);

            if ($difference > 0 && $startAuctionTime < $today) {
                if($highestPrice > 0){
                    $clock = '<p class="wk_cat_count_clock wk_auction_count_clock_'.$product->getEntityId().'"
                    id="wk_cat_count_clock_'.$product->getEntityId().'"
                     data-stoptime="'.$auctionData['stop_auction_time']
                    .'" data-diff_timestamp ="'.$difference.'"
                    data-product_id ="'.$product->getEntityId().'" data-title="'.__('highest bid ').'" data-price="'.$highestamount.'"></p>';
                }
                elseif ($highestPrice == 0 && $startPrice == 0 ){
                    $clock = '<p class="wk_cat_count_clock wk_auction_count_clock_'.$product->getEntityId().'"
                    id="wk_cat_count_clock_'.$product->getEntityId().'"
                     data-stoptime="'.$auctionData['stop_auction_time']
                    .'" data-diff_timestamp ="'.$difference.'"
                    data-product_id ="'.$product->getEntityId().'" data-title="'.__('current bid price ').'" data-price="'.__('no bit yet ').'"></p>';
                }
                else{
                    $clock = '<p class="wk_cat_count_clock wk_auction_count_clock_'.$product->getEntityId().'"
                    id="wk_cat_count_clock_'.$product->getEntityId().'"
                     data-stoptime="'.$auctionData['stop_auction_time']
                    .'" data-diff_timestamp ="'.$difference.'"
                    data-product_id ="'.$product->getEntityId().'" data-title="'.__('starting price ').'" data-price="'.$this->priceHelper->currency($auctionData['starting_price'], true, false).'"></p>';
                }
            } elseif (strtotime($startAuctionTime) > strtotime($today)) {
                $difference = strtotime($startAuctionTime) - strtotime($today);
                $clock = '<p class="wk_cat_count_clock" data-stoptime="'.$auctionData['stop_auction_time']
                .'" data-diff_timestamp ="'.$difference.'" data-highest-bid="'.
                $highestamount.'" data-highest-bid-amount="'.
                $highestamount.'"
                data-product_id ="'.$product->getEntityId().'"
                id="wk_cat_count_clock_'.$product->getEntityId().'"
                data-startflag="1" data-title="'.__('starting price ').'" data-price="'.
                $this->priceHelper->currency($auctionData['starting_price'], true, false).'"></p>';
            }
            if ((!$auctionConfig['reserve_enable'] && $currentAuctionPriceData->getEntityId()) ||
            ($auctionConfig['reserve_enable'] && $currentAuctionPriceData->getEntityId() &&
            $auctionData['reserve_price'] <= $currentAuctionPriceData->getAuctionAmount())) {
                $status = false;
            }
        }
        $content = $this->productAuctionData($product, $auctionOpt, $clock);

        return $content;
    }

    public function productAuctionData($product, $auctionOpt, $clock)
    {
        $status = true;

        $htmlDataAttr = "";
        $auctionData = $this->_auctionProFactory->create()
        ->addFieldToFilter('product_id', ['eq'=>$product->getEntityId()])
        ->addFieldToFilter('auction_status', ['eq'=>0])
        ->addFieldToFilter('status', ['eq'=>0])->getFirstItem()->getData();
        if (isset($auctionData['entity_id'])) {
            $winnerBidDetail = $this->getWinnerBidDetail($auctionData['entity_id']);
            if ($winnerBidDetail && $this->_customerSession->isLoggedIn()) {
                $winnerCustomerId = $winnerBidDetail->getCustomerId();
                $currentCustomerId = $this->_customerSession->getCustomerId();
                if ($currentCustomerId == $winnerCustomerId) {
                    $price = $winnerBidDetail->getBidType() == 'normal' ? $winnerBidDetail->getAuctionAmount():
                                                        $winnerBidDetail->getWinningPrice();
                    $formatedPrice = $this->priceHelper->currency($price, true, false);
                    $htmlDataAttr = 'data-winner="1" data-winning-amt="'.$formatedPrice.'"';
                }
            }
        }
        $content = '';
        /**
        * 2 : use for auction
        * 1 : use for Buy it now
        */

        $auctionConfig = $this->getAuctionConfiguration();
        if (in_array(2, $auctionOpt) && in_array(1, $auctionOpt) && $status) {
            $content = '<div class="auction buy-it-now " '.$htmlDataAttr.'>'.$clock.'</div>';
        } elseif (in_array(2, $auctionOpt)) {
            $content = '<div class="auction" '.$htmlDataAttr.'>'.$clock.'</div>';
        } elseif (in_array(1, $auctionOpt)) {
            $content = '<div class="buy-it-now"></div>';
        }
        return $content;
    }
    /**
     * getToday date time
     *
     * @return timezone
     */
    public function getCurrentDateTime()
    {
        return  $this->_timezoneInterface->date()->format('m/d/y H:i:s');
    }
     /**
      * convert to current timezone
      *
      * @param string $dateTime
      * @return void
      */
    public function converToTz($dateTime = " ")
    {
        return  $this->_timezoneInterface->date(new \DateTime($dateTime))
                                            ->format('m/d/y H:i:s');
    }

    /**
     * $auctionId
     * @param int $auctionId auction product id
     * @return AmountFactory || AutoAuctionFactory
     */
    public function getWinnerBidDetail($auctionId)
    {
        $aucAmtData = $this->_aucAmountFactory->create()->getCollection()
                                        ->addFieldToFilter('auction_id', ['eq'=>$auctionId])
                                        ->addFieldToFilter('winning_status', ['eq'=>1])
                                        ->addFieldToFilter('status', ['eq'=>0])->getFirstItem();
        if ($aucAmtData->getEntityId()) {
            $aucAmtData->setBidType('normal');
            return $aucAmtData;
        } else {
            $aucAmtData = $this->_autoAuctionFactory->create()->getCollection()
                                        ->addFieldToFilter('auction_id', ['eq'=> $auctionId])
                                        ->addFieldToFilter('flag', ['eq'=>1])
                                        ->addFieldToFilter('status', ['eq'=>0])->getFirstItem();
            if ($aucAmtData->getEntityId()) {
                $aucAmtData->setBidType('auto');
                return $aucAmtData;
            }
        }
        return false;
    }

    /**
     * get incremental price value
     * @var float $minAmount
     * @var float
     */
    public function getIncrementPriceAsRange($minAmount)
    {
        $incPriceRang = $this->_incrementalPriceFactory->create()->getCollection()->getFirstItem();
        if ($incPriceRang->getEntityId()) {
            $incPriceRang = json_decode($incPriceRang->getIncval(), true);
            foreach ($incPriceRang as $range => $value) {
                $range = explode('-', $range);
                if ($minAmount >= $range[0] && $minAmount <= $range[1]) {
                    return floatval($value);
                }
            }
        }
        return false;
    }

    /**
     * get Active Auction Id
     * @param $productId int
     * @return int|false
     */
    public function getActiveAuctionId($productId)
    {
        $auctionData = $this->_auctionProFactory->create()
                                        ->addFieldToFilter('product_id', ['eq'=>$productId])
                                        ->addFieldToFilter('status', ['eq'=>0])
                                        ->setOrder('entity_id')->getFirstItem();

        return $auctionData->getEntityId() ? $auctionData->getEntityId() : false;
    }

    /**
     * @var $productList Product list of current page
     * @return array of current category product in auction and buy it now
     */
    public function getAuctionDetail($currentProId = false)
    {
        $auctionConfig = $this->getAuctionConfiguration();
        $auctionData = false;
        if ($auctionConfig['enable']) {
            if ($currentProId) {
                $curPro = $this->product->load($currentProId);
            } else {
                $auctionId = $this->getRequest()->getParam('id');
                $currentProId = $this->getAuctionProId($auctionId);
                $curPro = $this->product->load($currentProId);
            }

            $auctionOpt = $curPro->getAuctionType();
            $auctionOpt = explode(',', $auctionOpt);
            /**
             * 2 : use for auction
             * 1 : use for Buy it now
             */
            if (in_array(2, $auctionOpt)) {
                $auctionData = $this->_auctionProFactory->create()
                                        ->addFieldToFilter('product_id', ['eq'=>$currentProId])
                                        ->addFieldToFilter('auction_status', ['in' => [0,1]])
                                        ->addFieldToFilter('status', ['eq'=>0])->getFirstItem()->getData();
                if (isset($auctionData['entity_id'])) {
                    if ($auctionData['increment_opt'] && $auctionConfig['increment_auc_enable']) {
                        $incVal = $this->getIncrementPriceAsRange($auctionData['min_amount']);
                        $auctionData['min_amount'] = $incVal ? $auctionData['min_amount'] + $incVal
                                                                            : $auctionData['min_amount'];
                    }
                    $aucAmtData = $this->_aucAmountFactory->create()->getCollection()
                                            ->addFieldToFilter('product_id', ['eq' => $currentProId])
                                            ->addFieldToFilter('auction_id', ['eq'=> $auctionData['entity_id']])
                                            ->addFieldToFilter('winning_status', ['eq'=>1])
                                            ->addFieldToFilter('shop', ['neq'=>0])->getFirstItem();

                    if ($aucAmtData->getEntityId()) {
                        $aucAmtData = $this->_autoAuctionFactory->create()->getCollection()
                                                ->addFieldToFilter('auction_id', ['eq'=> $auctionData['entity_id']])
                                                ->addFieldToFilter('flag', ['eq'=>1])
                                                ->addFieldToFilter('shop', ['neq'=>0])->getFirstItem();
                    }
                    $today = $this->_timezoneInterface->date()->format('m/d/y H:i:s');
                    $auctionData['stop_auction_time'] = $this->_timezoneInterface
                                                                ->date(
                                                                    new \DateTime($auctionData['stop_auction_time'])
                                                                )->format('Y-m-d H:i:s');
                    $auctionData['start_auction_time'] = $this->_timezoneInterface
                                                                ->date(
                                                                    new \DateTime($auctionData['start_auction_time'])
                                                                )->format('Y-m-d H:i:s');
                    $auctionData['current_time_stamp'] = strtotime($today);
                    $auctionData['start_auction_time_stamp'] = strtotime($auctionData['start_auction_time']);
                    $auctionData['stop_auction_time_stamp'] = strtotime($auctionData['stop_auction_time']);
                    $auctionData['new_auction_start'] = $aucAmtData->getEntityId() ? true : false;
                    $auctionData['auction_title'] = __('Bid on ').$curPro->getName();
                    $auctionData['pro_url'] = $this->_urlBuilder->getUrl().$curPro->getUrlKey().'.html';

                    $auctionData['pro_name'] = $curPro->getName();
                    $auctionData['pro_buy_it_now'] = in_array(1, $auctionOpt) !== false ? 1:0;
                    $auctionData['pro_auction'] = in_array(2, $auctionOpt) !== false ? 1:0;
                    if ($auctionData['min_amount'] < $auctionData['starting_price']) {
                        $auctionData['min_amount'] = $auctionData['starting_price'];
                    }
                } else {
                    $auctionData = false;
                }
            }
        }
        return $auctionData;
    }
    public function getNormalBidAmountDataByCustomerId($customerId, $productId, $auctionId)
    {
        return $this->_aucAmountFactory->create()->getCollection()
                    ->addFieldToFilter('product_id', ['eq' => $productId])
                    ->addFieldToFilter('auction_id', ['eq'=> $auctionId])
                    ->addFieldToFilter('customer_id', ['eq'=> $customerId])
                    ->setOrder('auction_amount', 'DESC')
                    ->getFirstItem();
    }
    public function getAutomaticBidAmountDataByCustomerId($customerId, $productId, $auctionId)
    {
        return $this->_autoAuctionFactory->create()->getCollection()
                    ->addFieldToFilter('product_id', ['eq' => $productId])
                    ->addFieldToFilter('auction_id', ['eq'=> $auctionId])
                    ->addFieldToFilter('customer_id', ['eq'=> $customerId])
                    ->setOrder('amount', 'DESC')
                    ->getFirstItem();
    }
    public function checkByItOptionStatus($proId, $auctionId, $reserveAmount)
    {
        $auctionType = $this->product->load($proId)->getAuctionType();
        $types = explode(',', $auctionType);
        if (!in_array(1, $types)) {
            return false;
        }
        $reserveAuctionStatus = $this->getAuctionConfiguration()['reserve_enable'];
        if ($reserveAuctionStatus) {
            $auctionData = $this->_aucAmountFactory->create()->getCollection()
                    ->addFieldToFilter('auction_id', ['eq'=> $auctionId])
                    ->setOrder('auction_amount', 'DESC');
            foreach ($auctionData as $amount) {
                if ($amount->getAuctionAmount() >= $reserveAmount) {
                    return false;
                }
            }
        } else {
            $amountData = $this->_aucAmountFactory->create()->getCollection()
                    ->addFieldToFilter('auction_id', ['eq'=> $auctionId]);
            if (!empty($amountData)) {
                return false;
            }
        }
        return true;
    }

    // return wallet amount is enabled or not
    public function getWalletenabled()
    {
        return  $this->scopeConfig->getValue(
            'wk_auction/auction_wallet/enable_wallet'
        );
    }

    // return customer id from customer session
    public function getCustomerId()
    {
        return $this->_customerSession->getCustomerId();
    }

    // return currency currency code
    public function getCurrentCurrencyCode()
    {
        return $this->_storeManager->getStore()->getCurrentCurrencyCode();
    }

    // get base currency code
    public function getBaseCurrencyCode()
    {
        return $this->_storeManager->getStore()->getBaseCurrencyCode();
    }

    // return auction amount product id
    public function getWalletProductId()
    {
        $auctionProductId = $this->_productFactory->create()
            ->getIdBySku(self::AUCTION_PRODUCT_SKU);

        return $auctionProductId;
    }

    // return maximum amount set in system config
    public function getMaximumAmount()
    {
        return  $this->scopeConfig->getValue(
            'wk_auction/auction_wallet/maximumamounttoadd',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    // return minimum amount set in system config
    public function getMinimumAmount()
    {
        return  $this->scopeConfig->getValue(
            'wk_auction/auction_wallet/minimumamounttoadd',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getWalletUsedTemplateIdForCustomer()
    {
        return  $this->scopeConfig->getValue(
            'wk_auction/emails/wallet_amount_debit_customer_notify',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getWalletUsedTemplateIdForAdmin()
    {
        return  $this->scopeConfig->getValue(
            'wk_auction/emails/wallet_amount_debit_admin_notify',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getWalletRechargeTemplateIdForCustomer()
    {
        return  $this->scopeConfig->getValue(
            'wk_auction/emails/wallet_amount_credit_customer_notify',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getWalletRechargeTemplateIdForAdmin()
    {
        return  $this->scopeConfig->getValue(
            'wk_auction/emails/wallet_amount_credit_admin_notify',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getMailTemplateForTransferRequestEmailToAdmin()
    {
        return  $this->scopeConfig->getValue(
            'wk_auction/emails/admin_transfer_notify',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getWalletExpiryTemplateId()
    {
        return  $this->scopeConfig->getValue(
            'wk_auction/emails/wallet_amount_customer_expiry_notify',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    // get currency symbol of an currency code
    public function getCurrencySymbol($currencycode)
    {
        $currency = $this->_localeCurrency->getCurrency($currencycode);

        return $currency->getSymbol() ? $currency->getSymbol() : $currency->getShortName();
    }

    // convert currency amount
    public function getwkconvertCurrency($fromCurrency, $toCurrency, $amount)
    {
        $baseCurrencyCode = $this->getBaseCurrencyCode();
        $allowedCurrencies = $this->getConfigAllowCurrencies();
        $rates = $this->getCurrencyRates(
            $baseCurrencyCode,
            array_values($allowedCurrencies)
        );
        if (empty($rates[$fromCurrency])) {
            $rates[$fromCurrency] = 1;
        }

        if ($baseCurrencyCode==$toCurrency) {
            $currencyAmount = $amount/$rates[$fromCurrency];
        } else {
            $amount = $amount/$rates[$fromCurrency];
            $currencyAmount = $this->convertCurrency($amount, $baseCurrencyCode, $toCurrency);
        }
        return $currencyAmount;
    }

    // get all allowed currency in system config
    public function getConfigAllowCurrencies()
    {
        return $this->_currency->getConfigAllowCurrencies();
    }

    // get currency rates
    public function getCurrencyRates($currency, $toCurrencies = null)
    {
        return $this->_currency->getCurrencyRates($currency, $toCurrencies); // give the currency rate
    }

    //get amount in base currency amount from current currency
    public function baseCurrencyAmount($amount, $store = null)
    {
        if ($store == null) {
            $store = $this->_storeManager->getStore()->getStoreId();
        }
        if ($amount == 0) {
            return $amount;
        }
        $rate = $this->_priceCurrency->convert($amount, $store) / $amount;
        $amount = $amount / $rate;

        return round($amount, 4);
    }

    // convert amount according to currenct currency
    public function convertCurrency($amount, $from, $to)
    {
        $finalAmount = $this->_directoryHelper
            ->currencyConvert($amount, $from, $to);

        return $finalAmount;
    }

    public function checkAndUpdateWalletAmount($order)
    {
        if (!empty($order->getInvoiceCollection())) {
            $orderId = $order->getId();
            if ($orderId) {
                $totalAmount = 0;
                $remainingAmount = 0;
                $orderModel = $this->_orderModel
                    ->create()
                    ->load($orderId);
                $orderItem = $orderModel->getAllItems();
                $productIdArray = [];
                foreach ($orderItem as $value) {
                    $productIdArray[] = $value->getProductId();
                }
                $auctionProductId = $this->getWalletProductId();
                if (in_array($auctionProductId, $productIdArray)) {
                    $walletCollection = $this->_walletTransaction
                        ->create()
                        ->getCollection()
                        ->addFieldToFilter('order_id', ['eq' => $orderId])
                        ->addFieldToFilter('status', 0);
                    if ($walletCollection->getSize()) {
                        foreach ($walletCollection as $record) {
                            $rowId = $record->getId();
                            $customerId = $record->getCustomerId();
                            $amount = $record->getAmount();
                            $action = $record->getAction();
                        }
                        $data = ['status' => 1];
                        $walletTansactionModel = $this->_walletTransaction
                            ->create()
                            ->load($rowId)
                            ->addData($data);
                        $walletTansactionModel->setId($rowId)->save();
                        $transactionNote = $walletTansactionModel->getTransactionNote();
                        $walletRecordCollection = $this->_walletRecordFactory
                            ->create()
                            ->getCollection()
                            ->addFieldToFilter(
                                'customer_id',
                                ['eq' => $customerId]
                            );
                        if ($action == 'credit') {
                            $emailParams = $this->updateWalletDataAmount(
                                $walletRecordCollection,
                                $amount,
                                $customerId,
                                $transactionNote
                            );
                            return $emailParams;
                        }
                    }
                }
            }
        }
    }

    // get customer remaining wallet amount
    public function customerWalletAmount($customerId = 0)
    {
        $remainingAmount = 0;
        $walletRecordCollection = $this->_walletRecordFactory
          ->create()
          ->getCollection()
        ->addFieldToFilter(
            'customer_id',
            ['eq' => $customerId]
        );
        if ($walletRecordCollection->getSize()) {
            foreach ($walletRecordCollection as $customerWallet) {
                $remainingAmount = $customerWallet->getRemainingAmount();
            }
        }
        return $remainingAmount;
    }

    // check customer bidding status
    public function checkBiddingStatus($productId = 0, $customerId = 0)
    {
        $feeId = 0;
        if (!$customerId) {
            $customerId = $this->getCustomerId();
        }
        $auctionData = $this->_auctionProFactory->create()
                              ->addFieldToFilter('product_id', $productId)
                              ->addFieldToFilter('status', 0)
                              ->addFieldToFilter('auction_status', ['in'=>[0,1]])
                              ->getFirstItem()->getData();
        $feeRecordCollection = $this->_feerecord->create()->getCollection()
                             ->addFieldToFilter('customer_id', ['eq'=>$customerId])
                             ->addFieldToFilter('product_id', ['eq'=>$productId])
                             ->addFieldToFilter('start_auction_time', ['eq'=>$auctionData['start_auction_time']])
                             ->addFieldToFilter('stop_auction_time', ['eq'=>$auctionData['stop_auction_time']]);
        if ($feeRecordCollection->getSize()) {
            return true;
        }
        return false;
    }

    // get formatted price according to currenct currency
    public function getformattedPrice($price)
    {
        return $this->priceHelper
            ->currency($price, true, false);
    }

    // get currenct store
    public function getStore()
    {
        return $this->_storeManager->getStore();
    }

    //update fee amountData
    public function updateFeeAmount($wholeData)
    {
        $curramount=0;
        $customerId = $this->getCustomerId();
        $productName = $this->product->load($wholeData['product_id'])->getName();
        $auctionData = $this->_auctionProFactory->create()
                               ->addFieldToFilter('product_id', $wholeData['product_id'])
                                ->addFieldToFilter('status', 0)
                                ->addFieldToFilter('auction_status', ['in'=>[0,1]])
                                ->getFirstItem()->getData();
        $auctionId=$wholeData['auction_id'];
        $feeReordData['customer_id'] = $customerId;
        $feeReordData['product_id'] = $wholeData['product_id'];
        $feeReordData['start_auction_time'] = $auctionData['start_auction_time'];
        $feeReordData['stop_auction_time'] = $auctionData['stop_auction_time'];
        $feeReordData['created_at'] = $this->_dateTime->gmtDate();
        $feeReordData['updated_at'] = $this->_dateTime->gmtDate();
        $feeRecordCollection = $this->_feerecord->create()->getCollection()
           ->addFieldToFilter('customer_id', ['eq'=>$customerId])
           ->addFieldToFilter('product_id', ['eq'=>$feeReordData['product_id']])
           ->addFieldToFilter('start_auction_time', ['eq'=>$auctionData['start_auction_time']])
           ->addFieldToFilter('stop_auction_time', ['eq'=>$auctionData['stop_auction_time']]);
        $walletCollection = $this->_walletTransaction
           ->create()
           ->getCollection()
           ->addFieldToFilter('customer_id', ['eq' => $customerId])
           ->addFieldToFilter('auction_id', $auctionId)
           ->addFieldToFilter('action', "debit");

        if ($walletCollection->getSize()>0) {
            foreach ($walletCollection as $walletCollection) {
                $walletcurrentamount=$walletCollection->getCurrAmount();
                $curramount=$curramount+$walletcurrentamount;
            }
        }
        if ($feeRecordCollection->getSize()) {
            foreach ($feeRecordCollection as $record) {
                $bidFeeId = $record->getEntityId();
                $record->setEntityId($bidFeeId);
                $record->setUpdatedAt($feeReordData['updated_at'])->save();
            }
        } else {
            $feeRecordModel = $this->_feerecord->create();
            $feeRecordModel->setData($feeReordData)->save();
            $bidFeeId = $feeRecordModel->getEntityId();
        }
        $feePercent = $this->getFeePercent();
        $deductedAmount = ($wholeData['bidding_amount']*$feePercent)/100;
        if ($curramount>0) {
            $deductedAmount = $deductedAmount-$curramount;
        }
        $feeReordData['bidfee_id'] = $bidFeeId;
        $feeReordData['bid_amount'] = $wholeData['bidding_amount'];
        $feeReordData['deducted_amount'] = $deductedAmount;
        $feeReordData['percent'] = $feePercent;
        $feeReordData['refund_status'] = $this->getRefundStatus();
        $feeReordData['status'] = 0;
        $feetransactionModel = $this->_feetransaction->create();
        $feetransactionModel->setData($feeReordData)->save();
        $currentCurrencyCode = $this->getCurrentCurrencyCode();
        $transferAmountData = [
             'customerid' => $customerId,
             'walletamount' => $deductedAmount,
             'walletactiontype' => 'debit',
             'curr_code' => $currentCurrencyCode,
             'curr_amount' => $deductedAmount,
             'walletnote' => __('Bidding on %1 : debited amount', $productName),
             'order_id' => 0,
             'status' => 1,
             'auction_id'=>$wholeData['auction_id']
        ];
        return $transferAmountData;
    }

    // get minimum amount to bid in wallet
    public function getMinimumAmountToBid()
    {
        return $this->scopeConfig->getValue('wk_auction/auction_wallet/min_amount_to_bid');
    }

    // get fee percent value
    public function getFeePercent()
    {
        $percent = $this->scopeConfig->getValue('wk_auction/auction_wallet/fee_deduction');
        if ($percent > 100) {
            return 100;
        }
        return $percent;
    }

    public function cancelWalletAmtRefund()
    {
        return $this->scopeConfig->getValue('wk_auction/auction_wallet/cancel_refund');
    }

    //get expire amount in Days
    public function getExpireInDays()
    {
        return $this->scopeConfig->getValue('wk_auction/auction_wallet/expire_days');
    }

    //get notification start Days
    public function getNotificationDays()
    {
        return $this->scopeConfig->getValue('wk_auction/auction_wallet/expire_notification');
    }

    //get fee deduction Status
    public function getFeeDeductionStatus()
    {
        return $this->scopeConfig->getValue('wk_auction/auction_wallet/fee_deduction_status');
    }
    //get Wallet Status
    public function getWalletStatus()
    {
        return $this->scopeConfig->getValue('wk_auction/auction_wallet/enable_wallet');
    }
    //get Refund Status
    public function getRefundStatus()
    {
        return $this->scopeConfig->getValue('wk_auction/auction_wallet/fee_deduction_refund_status');
    }

    //check seller amount expiry Status
    public function checkSellerAmountExpiryStatus($customerId = 0, $notificationDays = 0)
    {
        $walletRecordModel = $this->_walletRecordFactory->create()->getCollection()
                           ->addFieldToFilter('customer_id', $customerId);
        if ($walletRecordModel->getSize()) {
            foreach ($walletRecordModel as $record) {
                $expiredDate = $walletRecordModel->getExpiredDate();
                $todayDate = $this->_dateTime->gmtDate('Y-m-d');
                $expiredStatus = ($todayDate > $expiredDate ? true : false);
                if (!$expiredStatus) {
                    $expiredTime = strtotime($expiredDate);
                    $todayTime = strtotime($todayDate);
                    $datediff = $expiredTime - $todayTime;
                    $days = ($datediff / (60 * 60 * 24));
                    $notifyStatus = ($notificationDays >= $days ? true : false);
                } else {
                    $notifyStatus = false;
                }
            }
            return [$expiredStatus,$notifyStatus,$expiredDate];
        }
        return [false,false,false];
    }

    public function updateWalletDataAmount($walletRecordCollection, $amount, $customerId, $transactionNote)
    {
        $remainingAmount = 0;
        $totalAmount = 0;
        $expiredDate = $this->_dateTime->gmtDate('Y-m-d', strtotime('+'.$this->getExpireInDays().' days'));
        if ($walletRecordCollection->getSize()) {
            foreach ($walletRecordCollection as $record) {
                $totalAmount = $record->getTotalAmount();
                $remainingAmount = $record->getRemainingAmount();
                $recordId = $record->getId();
            }
            $finalAmount = $amount + $remainingAmount;
            $data = [
                'total_amount' => $amount + $totalAmount,
                'remaining_amount' => $amount + $remainingAmount,
                'expired_date' => $expiredDate,
                'transaction_at' =>$this->_dateTime->gmtDate(),
                'updated_at' => $this->_dateTime->gmtDate(),
            ];
            $walletRecordModel = $this->_walletRecordFactory
                ->create()
                ->load($recordId)
                ->addData($data);
            $saved = $walletRecordModel->setId($recordId)->save();
        } else {
            $walletRecordModel = $this->_walletRecordFactory
                ->create();
            $finalAmount = $amount + $remainingAmount;
            $walletRecordModel->setTotalAmount($amount + $totalAmount)
                ->setCustomerId($customerId)
                ->setRemainingAmount($amount + $remainingAmount)
                ->setExpiredDate($expiredDate)
                ->setUpdatedAt($this->_dateTime->gmtDate())
                ->setTransactionAt($this->_dateTime->gmtDate());
            $saved = $walletRecordModel->save();
        }
        if ($saved->getId() != 0) {
            $date = $this->_localeDate->date(new \DateTime($this->_dateTime->gmtDate()));
            $formattedDate = $date->format('g:ia \o\n l jS F Y');
            $store = $this->getStore();
            $emailParams = [
              'walletamount' => $this->getformattedPrice($amount),
              'remainingamount' => $this->getformattedPrice($finalAmount),
              'action' => 'credit',
              'transaction_at' => $formattedDate,
              'walletnote' => $transactionNote,
              'store' => $store,
              'customer_id' => $customerId
            ];
            return $emailParams;
        } else {
            return [];
        }
    }

    public function deductWalletDataAmount($walletRecordCollection, $amount, $customerId, $transactionNote)
    {
        $remainingAmount = 0;
        $totalAmount = 0;
        if ($walletRecordCollection->getSize()) {
            foreach ($walletRecordCollection as $record) {
                $usedAmount = $record->getUsedAmount();
                $remainingAmount = $record->getRemainingAmount();
                $recordId = $record->getId();
            }
            $finalAmount = $remainingAmount - $amount;
            $usedAmount = $usedAmount + $amount;
            $data = [
                'remaining_amount' => $finalAmount,
                'used_amount' => $usedAmount,
                'transaction_at' =>$this->_dateTime->gmtDate(),
                'updated_at' => $this->_dateTime->gmtDate(),
            ];
            $walletRecordModel = $this->_walletRecordFactory
                ->create()
                ->load($recordId)
                ->addData($data);
            $saved = $walletRecordModel->setId($recordId)->save();
        }
        if ($saved->getId() != 0) {
            $date = $this->_localeDate->date(new \DateTime($this->_dateTime->gmtDate()));
            $formattedDate = $date->format('g:ia \o\n l jS F Y');
            $store = $this->getStore();
            $emailParams = [
              'walletamount' => $this->getformattedPrice($amount),
              'remainingamount' => $this->getformattedPrice($finalAmount),
              'action' => 'debit',
              'transaction_at' => $formattedDate,
              'walletnote' => $transactionNote,
              'store' => $store,
              'customer_id' => $customerId
            ];
            return $emailParams;
        } else {
            return [];
        }
    }

    public function getAuctionDiscount($items)
    {
        $discountAmount = 0;
        foreach ($items as $quoteItem) {
            $auctionId = $this->getActiveAuctionId($quoteItem->getProductId());
            if ($auctionId) {
                $discountAmount += $this->getPerProductDiscount($quoteItem->getProductId(), $auctionId);
            }
        }
        $status = $discountAmount ? true : false;
        return [$status, $discountAmount];
    }

    public function getPerProductDiscount($productId = 0, $auctionId = 0)
    {
        $amount = 0;
        $bidProCol = $this->_winnerData->create()->getCollection()
                              ->addFieldToFilter('product_id', $productId)
                              ->addFieldToFilter('complete', 0)->addFieldToFilter('status', 1)
                              ->addFieldToFilter('auction_id', $auctionId);
        $biddingProductCollection = $bidProCol->setOrder('auction_id')->getFirstItem();
        if ($biddingProductCollection->getEntityId()) {
            $amount = ($biddingProductCollection->getWinAmount() - $biddingProductCollection->getShowAmount());
        }
        return $amount;
    }

    public function cacheFlush()
    {
            $types = ['block_html','full_page'];
        foreach ($types as $type) {
            $this->_cacheTypeList->cleanType($type);
        }
        foreach ($this->_cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }
    public function getUrl($url)
    {
        $url = $this->_urlBuilder->getUrl('admin/dashboard/index', $paramsHere = []);
        return $url;
    }
    /**
     * Is AuctionAvail
     *
     * @param  $id
     * @return boolean
     */
    public function isAuctionAvail($id)
    {

        $product = $this->getProductData($id);
        $auctionData = $this->getAuctionData($product->getEntityId());
        $isAuctionAvail = false;
        if ($auctionData) {
            $isAuctionAvail = true;
        }
        return $isAuctionAvail;
    }

    public function isUserWinner($id)
    {
        $product = $this->getProductData($id);
        $auctionData = $this->getAuctionEndData($product->getEntityId());

        $isUserWinner = false;
        if (!empty($auctionData)) {
            $winnerBidDetail =   $this->getWinnerBidDetail($auctionData['entity_id']);
            if ($winnerBidDetail && $this->_customerSession->isLoggedIn()) {
                $winnerCustomerId = $winnerBidDetail->getCustomerId();
                $currentCustomerId = $this->_customerSession->getCustomerId();
                if ($currentCustomerId == $winnerCustomerId) {
                    $isUserWinner = true;
                }
            }

        }
        return $isUserWinner;
    }

    public function getAuctionEndData($id)
    {
        return  $this->productRepository->getAuctionEndData($id);
    }

    /**
     * get Product data
     *
     * @param int $id
     * @return collection
     */
    public function getProductData($id)
    {
        return $this->_productFactory->create()->load($id);
    }
    /**
     * getAuction Data
     *
     * @param int $id
     * @return void
     */
    public function getAuctionData($id)
    {
        return $this->productRepository->getAuctionData($id);
    }
    /**
     * getAuction difference
     *
     * @param int $productId
     * @return array
     */
    public function getAuctionDiff($productId)
    {
        $auctionData = $this->getAuctionData($productId);
        $data = [];
        if ($auctionData) {
            $currentDateTime = $this->getCurrentDateTime();
            $startAuctionTime = $this->converToTz($auctionData['start_auction_time']);
            $stopAuctionTime = $this->converToTz($auctionData['stop_auction_time']);
            $difference = strtotime($stopAuctionTime) - strtotime($currentDateTime);
            $stopTime = strtotime($stopAuctionTime);
            if (strtotime($currentDateTime) < strtotime($startAuctionTime)) {
                $difference = strtotime($startAuctionTime) - strtotime($currentDateTime);
                $stopTime = strtotime($startAuctionTime);
            }

            $data['diff'] = $difference;
            $data['stopTime'] = $stopTime;
        }
        return $data;
    }
    /**
     * encode data into json
     *
     * @param array $data
     * @return void
     */
    public function jsonEncode($data)
    {
        return $this->jsonHelper->jsonEncode($data);
    }
}
