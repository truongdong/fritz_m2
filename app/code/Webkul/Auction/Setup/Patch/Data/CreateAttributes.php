<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Setup\Patch\Data;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Setup\Patch\SchemaPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;

class CreateAttributes implements DataPatchInterface, PatchRevertableInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_productModel;
    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $_eavConfig;
    /**
     * @var Installer
     */
    protected $_productType = \Magento\Catalog\Model\Product\Type::TYPE_VIRTUAL;
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $_eavSetupFactory;
    /**
     * @var \Magento\Framework\App\State
     */
    protected $_appState;
    /**
     * @var \Magento\Framework\Module\Dir\Reader
     */
    protected $moduleReader;
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;
    /**
     * @var \Magento\Catalog\Model\Product\TypeTransitionManager
     */
    protected $_catalogProductTypeManager;
    /**
     * @param \Magento\Catalog\Model\Product                $productModel
     * @param \Magento\Store\Model\StoreManagerInterface    $storeManager
     * @param \Magento\Catalog\Model\ProductFactory         $productFactory
     * @param \Magento\Eav\Model\Config                     $eavConfig
     * @param EavSetupFactory                               $eavSetupFactory
     * @param \Magento\Framework\App\State                  $appstate
     * @param \Magento\Framework\Module\Dir\Reader          $moduleReader
     * @param \Magento\Framework\Filesystem                 $filesystem
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\App\State $appstate,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Catalog\Model\Product\TypeTransitionManager $catalogProductTypeManager,
        Reader $reader,
        Filesystem $filesSystem,
        File $fileDriver
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->_productModel = $productModel;
        $this->_storeManager = $storeManager;
        $this->_eavConfig = $eavConfig;
        $this->_productFactory = $productFactory;
        $this->_eavSetupFactory = $eavSetupFactory;
        $this->_appState = $appstate;
        $this->moduleReader = $moduleReader;
        $this->_filesystem = $filesystem;
        $this->_catalogProductTypeManager = $catalogProductTypeManager;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->reader = $reader;
        $this->filesystem = $filesSystem;
        $this->fileDriver = $fileDriver;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        /**
         * Move Dir To media Dir
         */
        $this->moveDirToMediaDir();
        $this->moduleDataSetup->getConnection()->startSetup();
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'auction_type',
            [
                'group' => 'Product Details',
                'type' => 'text',
                'backend' => \Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend::class,
                'frontend' => '',
                'label' => 'Auction Options',
                'input' => 'multiselect',
                'class' => '',
                'source' => \Webkul\Auction\Model\Config\Source\AuctionType::class,
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to' => 'simple,downloadable,virtual',
            ]
        );

        $appState = $this->_appState;
        $appState->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);

        $this->_eavConfig->clear();
            $product = $this->_productFactory->create();
            $store = \Magento\Store\Model\Store::DEFAULT_STORE_ID;
            $attributeSetId = $this->_productModel->getDefaultAttributeSetId();
            $productId = $this->_productFactory->create()->getIdBySku('wk_auction_amount');
        if (!$productId) {
            $mageProduct = $this->_productFactory->create();
            $mageProduct->setAttributeSetId($attributeSetId);
            $mageProduct->setTypeId($this->_productType);
            $mageProduct->setStoreId($store);

            $requestData = [
                'product' => [
                    'name' => 'Auction Amount',
                    'attribute_set_id' => $attributeSetId,
                    'status' => \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED,
                    'visibility' => \Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE,
                    'sku' => 'wk_auction_amount',
                    'tax_class_id' => 0,
                    'description' => 'auction amount',
                    'short_description' => 'auction amount',
                    'stock_data' => [
                        'use_config_manage_stock' => 0,
                        'manage_stock' => 0,
                        'is_decimal_divided' => 0
                    ],
                    'quantity_and_stock_status' => [
                        'qty' => 1,
                        'is_in_stock' => 1
                    ]
                ]
            ];
            $catalogProduct = $this->productInitialize($mageProduct, $requestData);
            $this->_catalogProductTypeManager->processProduct($catalogProduct);
            if ($catalogProduct->getSpecialPrice() == '') {
                $catalogProduct->setSpecialPrice(null);
                $catalogProduct->getResource()->saveAttribute($catalogProduct, 'special_price');
            }
            $catalogProduct->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
            ->save();
            $imagePath = $this->getViewFileUrl().'auction/auction.png';
            $catalogProduct->addImageToMediaGallery(
                $imagePath,
                ['image', 'small_image', 'thumbnail'],
                false,
                false
            );
            $catalogProduct->save();
        }
            $product = $this->_productFactory->create();
            $productId = $this->_productFactory->create()->getIdBySku('wk_auction_amount');
            $products=$this->_productModel->load($productId);
            $products->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_IN_CATALOG);
            $products->save();
            $this->moduleDataSetup->getConnection()->endSetup();
    }
    
     /**
      * Copy Sample Image file to Media
      */
    private function moveDirToMediaDir()
    {
        try {
            $type = \Magento\Framework\App\Filesystem\DirectoryList::MEDIA;
            $smpleFilePath = $this->filesystem->getDirectoryRead($type)
                                        ->getAbsolutePath().'auction/';
            $files = [
                'auction.png'
            ];
            if ($this->fileDriver->isExists($smpleFilePath)) {
                $this->fileDriver->deleteDirectory($smpleFilePath);
            }
            if (!$this->fileDriver->isExists($smpleFilePath)) {
                $this->fileDriver->createDirectory($smpleFilePath, 0777);
            }
            foreach ($files as $file) {
                $filePath = $smpleFilePath.$file;
                if (!$this->fileDriver->isExists($filePath)) {
                    $path = '/pub/media/auction/'.$file;
                    $mediaFile = $this->reader->getModuleDir('', 'Webkul_Auction').$path;
                    if ($this->fileDriver->isExists($mediaFile)) {
                        $this->fileDriver->copy($mediaFile, $filePath);
                    }
                }
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getViewFileUrl()
    {
        return $this->_filesystem
                    ->getDirectoryRead(DirectoryList::MEDIA)
                    ->getAbsolutePath();
    }
    
    private function productInitialize(\Magento\Catalog\Model\Product $catalogProduct, $requestData)
    {
        $requestProductData = $requestData['product'];
        $requestProductData['product_has_weight'] = 0;
        $catalogProduct->addData($requestProductData);
        $websiteIds = [];
        $allWebsites = $this->_storeManager->getWebsites();
        foreach ($allWebsites as $website) {
            $websiteIds[] = $website->getId();
        }
        $catalogProduct->setWebsiteIds($websiteIds);
        return $catalogProduct;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        /** @var CustomerSetup $customerSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'auction_type'
        );
        $this->moduleDataSetup->getConnection()->startSetup();
        //Here should go code that will revert all operations from `apply` method
        //Please note, that some operations, like removing data from column, that is in role of foreign key reference
        //is dangerous, because it can trigger ON DELETE statement
        $this->moduleDataSetup->getConnection()->endSetup();
        $this->moduleDataSetup->getConnection()->endSetup();
    }
}
