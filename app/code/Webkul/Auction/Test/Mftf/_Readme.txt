Magento version. 2.2.6-dev
MFTF version 1.0.0

Admin Configuration 
=> WYSIWYG Options section set "Disabled Completely" WYSIWYG Editor option .
=> Set Add Secret Key to URLs to No.

Install the MFTF.
=>composer install on MagentoRoot/dev/tests/acceptance/ location.

=> Download Selenium Standalone Server jar file and Chrome Driver from "https://www.seleniumhq.org/download/" link and place on dev/tests/acceptance/vendor/bin directory.

=>Edit .env file on MagentoRoot/dev/tests/acceptance directory.
Example :-
        MAGENTO_BASE_URL=http://magento.test
        MAGENTO_BACKEND_NAME=admin
        MAGENTO_ADMIN_USERNAME=admin
        MAGENTO_ADMIN_PASSWORD=123123q

=> Run command  "vendor/bin/robo build:project" on  MagentoRoot/dev/tests/acceptance location

=>Run Selenium server command "java -jar <selenium server name>" on  MagentoRoot/dev/tests/acceptance location

=>Open http://127.0.0.1:4444/wd/hub/static/resource/hub.html link on chrome browser and set session of chrome browser

=> Generate Tests command run "vendor/bin/robo generate:tests" on  MagentoRoot/dev/tests/acceptance location

=>Run test command run on  MagentoRoot/dev/tests/acceptance location
1- "vendor/bin/codecept run functional --group automaticBid" 
2- "vendor/bin/codecept run functional --group walletCheck" 
3- "vendor/bin/codecept run functional --group walletEnable" 


