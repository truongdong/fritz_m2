<?php
/**
 * Webkul Auction detail controller.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Controller\Account;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Auctiondetails extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;
    /**
     *
     * @var \Webkul\Auction\Helper\Data $helperData
     */
    public $helperData;
    
    /**
     * construce function
     *
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Webkul\Auction\Helper\Data $helperData
     * @param PageFactory $_resultPageFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Webkul\Auction\Helper\Data $helperData,
        PageFactory $_resultPageFactory
    ) {
        $this->_resultPageFactory = $_resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->helperData = $helperData;
        parent::__construct($context);
    }

    /**
     * Auction Detail page
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $resultJson = $this->_resultJsonFactory->create();

        $id = $this->getRequest()->getParam('id');
      
        $auctionDiff = $this->helperData->getAuctionDiff($id);

        $response = ['auctiondiff' => $auctionDiff];
        $resultJson->setData($response);
        return $resultJson;
    }
}
