<?php
/**
 * Webkul Auction bid delete controller.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Controller\Account;

use Magento\Framework\App\Action\Context;
use Webkul\Auction\Model\Amount;

class DeleteBid extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var PageFactory
     */
    protected $_customerSession;

    /**
     * @param Context $context
     * @param \Magento\Customer\Model\Session $_customerSession
     * @param Amount $auctionAmt
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $customerSession,
        Amount $auctionAmt
    ) {
        $this->_customerSession = $customerSession;
        $this->_auctionAmt = $auctionAmt;
        parent::__construct($context);
    }

    /**
     * Auction bid delete controller
     *
     * @return Magento\Backend\Model\View\Result\Redirect $resultRedirect
     */
    public function execute()
    {
        /** @var int $curntCustomerId */
        $curntCustomerId = $this->_customerSession->getCustomerId();

        /** @var int $bidId */
        $bidId=$this->_request->getParam('id');

        /** @var Webkul\Auction\Model\Amount $bidRecord */
        $bidRecord = $this->_auctionAmt->load($bidId);
        if ($bidRecord->getEntityId()
            && $curntCustomerId == $bidRecord->getCustomerId()) {
            $bidRecord->delete();
            $this->messageManager
                    ->addSuccess(__('Auction bid removed successfuly.'));
        } else {
            $this->messageManager->addError(__('Not permitted.'));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setUrl(
            $this->_url->getUrl('auction/account/')
        );
    }
}
