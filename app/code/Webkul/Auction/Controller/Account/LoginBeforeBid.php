<?php
/**
 * Webkul_Auction bid save controller for login user
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Controller\Account;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use Webkul\Auction\Model\WalletUpdateData;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Customer\Model\Url as CustomerUrl;

class LoginBeforeBid extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Directory\Model\CurrencyFactory
     */
    protected $_dirCurrencyFactory;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dateTime;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_timeZone;

    /**
     * @var Webkul\Auction\Model\WalletUpdateData
     */
    protected $walletUpdateData;

    /**
     * @var \Webkul\Auction\Model\AmountFactory
     */
    protected $_auctionAmount;

    /**
     * @var \Webkul\Auction\Model\ProductFactory
     */
    protected $_auctionProductFactory;

    /**
     * @var \Webkul\Auction\Helper\Data
     */
    protected $_helperData;

    /**
     * @var \Webkul\Auction\Model\AutoAuctionFactory
     */
    protected $_autoAuction;

    /**
     * @var CustomerUrl
     */
    protected $_customerUrl;
    protected $cacheTypeList;
    protected $cacheFrontendPool;
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Directory\Model\CurrencyFactory $dirCurrencyFactory
     * @param RequestInterface $request
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Webkul\Auction\Model\AmountFactory $auctionAmount
     * @param \Webkul\Auction\Model\ProductFactory $auctionProductFactory
     * @param \Webkul\Auction\Helper\Data $helperData
     * @param \Webkul\Auction\Model\AutoAuctionFactory $autoAuction
     * @param CustomerUrl $customerUrl
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\CurrencyFactory $dirCurrencyFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        TimezoneInterface $timeZone,
        WalletUpdateData $walletUpdateData,
        \Webkul\Auction\Model\AmountFactory $auctionAmount,
        \Webkul\Auction\Model\ProductFactory $auctionProductFactory,
        \Webkul\Auction\Model\ProductFactory $auctionProFactory,
        \Webkul\Auction\Helper\Data $helperData,
        \Webkul\Auction\Helper\Email $helperEmail,
        CustomerUrl $customerUrl,
        \Webkul\Auction\Model\AutoAuctionFactory $autoAuction,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->cacheTypeList = $cacheTypeList;
        $this->cacheFrontendPool = $cacheFrontendPool;
        $this->_customerSession = $customerSession;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_storeManager = $storeManager;
        $this->_dirCurrencyFactory = $dirCurrencyFactory;
        $this->_dateTime = $dateTime;
        $this->_timeZone = $timeZone;
        $this->walletUpdateData = $walletUpdateData;
        $this->_auctionAmount = $auctionAmount;
        $this->_auctionProductFactory = $auctionProductFactory;
        $this->_auctionProFactory=$auctionProFactory;
        $this->_helperData = $helperData;
        $this->_helperEmail = $helperEmail;
        $this->_customerUrl = $customerUrl;
        $this->_autoAuction = $autoAuction;
        $this->request = $request;
        $this->productFactory = $productFactory;
        parent::__construct($context);
    }

    /**
     * Check customer authentication
     *
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     */
//    public function dispatch(RequestInterface $request)
//    {
//        $loginUrl = $this->_customerUrl->getLoginUrl();
//
//        if (!$this->_customerSession->authenticate($loginUrl)) {
//            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
//            //set Data in session if bidder not login
//            $this->_customerSession->setAuctionBidData($request->getParams());
//        }
//        return parent::dispatch($request);
//    }

    /**
     * Default customer account page
     * @var $cuntCunyCode current Currency Code
     * @var $baseCunyCode base Currency Code
     * @return \Magento\Backend\Model\View\Result\Redirect $resultRedirect
     */
    public function execute()
    {
        $auctionData = [];
        $data = $this->_customerSession->getAuctionBidData();

        $customerId = $this->_customerSession->getCustomerId();
        $customerWalletAmount = $this->_helperData->customerWalletAmount($customerId);
        $getMinimumAmountToBid = $this->_helperData->getMinimumAmountToBid();
        $auctionData = $data = $data ? $data: $this->getRequest()->getParams();
        $product = $this->productFactory->create()->load($data['product_id']);

        if(!$this->_customerSession->isLoggedIn()) {
            $resultRedirect = $this->resultRedirectFactory->create();
            $this->messageManager->addNotice(__(
                'You can bid if you log in beforehand. <a href="%1">Log in now.</a>',
                $this->_customerUrl->getLoginUrl()
            ));
            return $resultRedirect->setUrl($auctionData['pro_url']);
        } else {
            if($product->getTypeId() != 'virtual') {
                $defaultShippingAddress = $this->_customerSession->getCustomer()->getDefaultShippingAddress();
                if(!$defaultShippingAddress || $defaultShippingAddress->getCountryId() != 'DE') {
                    $resultRedirect = $this->resultRedirectFactory->create();
                    $this->messageManager->addNotice(__(
                        'Unfortunately, we only ship auction items within germany. therefore, please enter a shipping address within germany in your <a href="%1">customer account.</a>',
                        $this->_url->getUrl('customer/account')
                    ));
                    return $resultRedirect->setUrl($auctionData['pro_url']);
                }
            }
        }

        $setBidStatus = $this->_helperData->checkBiddingStatus($auctionData['id'], $customerId);
        $feeDeductionStatus = $this->_helperData->getFeeDeductionStatus();
        $walletStatus = $this->_helperData->getWalletStatus();
        if ($customerWalletAmount >= $getMinimumAmountToBid || $setBidStatus ||
         !$feeDeductionStatus || !$walletStatus) {
            $this->_customerSession->unsAuctionBidData();
            $today = $this->_timeZone->date()->format('m/d/y H:i:s');
            try {
                $difference = $auctionData['stop_auction_time_stamp'] - strtotime($today);
                if ($data && $difference > 0) {
                    //auction configuration detail
                    $auctionConfig = $this->_helperData->getAuctionConfiguration();
                    //get currency according to store
                    $store = $this->_storeManager->getStore();
                    $currencyModel = $this->_dirCurrencyFactory->create();
                    $baseCunyCode = $store->getBaseCurrencyCode();
                    $cuntCunyCode = $store->getCurrentCurrencyCode();

                    $allowedCurrencies = $currencyModel->getConfigAllowCurrencies();
                    $rates = $currencyModel->getCurrencyRates(
                        $baseCunyCode,
                        array_values($allowedCurrencies)
                    );

                    $rates[$cuntCunyCode] = isset($rates[$cuntCunyCode]) ? $rates[$cuntCunyCode] : 1;
                    $data['auction_id'] = $auctionData['entity_id'];
                    $data['product_id'] = $auctionData['product_id'];
                    $data['pro_name'] = $auctionData['pro_name'];
                    $auctionData['bidding_amount'] = floatval(str_replace(',','.',$auctionData['bidding_amount']));
                    $data['bidding_amount']=$auctionData['bidding_amount'];
                    $data['bidding_amount'] = $data['bidding_amount']/$rates[$cuntCunyCode];
                    $biddingModel = $this->_auctionProductFactory->create()->load($data['auction_id']);
                    $feePercent = $this->_helperData->getFeePercent();
                    $deductedAmount = ($data['bidding_amount']*$feePercent)/100;
                    if ($customerWalletAmount >= $deductedAmount || !$walletStatus || !$feeDeductionStatus) {
                        if ($auctionConfig['auto_enable'] && $auctionData['auto_auction_opt']
                          && array_key_exists("auto_bid_allowed", $data)) {
                            $status = $this->_saveAutobiddingAmount($data);
                        } else {
                            $status = $this->_saveBiddingAmount($data);
                            $this->bidSave($data);
                        }
                        if ($feeDeductionStatus && $walletStatus && $status) {
                            $transferAmountData = $this->_helperData->updateFeeAmount($data);
                            $result = $this->walletUpdateData->debitAmount($customerId, $transferAmountData);
                        }

                        if ($biddingModel->getMinAmount()==$biddingModel->getStartingPrice()
                            && $product->getAuctionType() != 2
                        ) {
                            $product->setAuctionType('2')->save();
                            ;
                        }
                    } else {
                        $this->messageManager->addError(
                            __('You do not have enough amount in your wallet to place this bid.')
                        );
                    }
                } else {
                    $this->messageManager->addError(__('Auction time expired.'));
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError(__('Something went wrong.'));
            }
        } else {
            $baseCurrenySymbol = $this->_helperData->getCurrencySymbol($this->_helperData->getBaseCurrencyCode());
            $this->messageManager->addNotice(__(
                'You should have minimum %1 in your auction wallet to place a bid',
                $baseCurrenySymbol.$getMinimumAmountToBid
            ));
        }
        if (!isset($auctionData['pro_url']) || $auctionData['pro_url']=='') {
            $auctionData['pro_url'] = $this->_url->getBaseUrl();
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setUrl($auctionData['pro_url']);
    }

    /**
     * _saveBiddingAmount saves amount of normal bid placed by customer
     * @param array $data stores bid data
     * @var int $val bid amount
     * @var int $userId current logged in customer's id
     * @var object $biddingModel holds data for particular bid
     * @var $minPrice int stores minimum price of bidding
     * @var $incopt stores increament option is allowed on bidding or not
     * @var $incPrice holds increment price for product
     * @var $bidCusrRecord object id customer already placed a bid
     * @var $bidModel use to strore new bidding
     */

    protected function _saveBiddingAmount($data)
    {
        $sendUserId = "";
        $customerArray = [];
        $val = $data['bidding_amount'];
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        $userId = $this->_customerSession->getCustomerId();
        $biddingModel = $this->_auctionProductFactory->create()->load($data['auction_id']);
        $minPrice = $biddingModel->getMinAmount();

        if ($biddingModel->getIncrementOpt() && $auctionConfig['increment_auc_enable']) {
            $incVal = $this->_helperData->getIncrementPriceAsRange($minPrice);
            $minPrice = $incVal ? $minPrice + $incVal : $minPrice;
        }

        if ($minPrice + 0.009999999999 >= $val) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('You can not bid less than or equal to current bid price.')
            );
        } else {
            $bidCusrRecord = $this->_auctionAmount->create()->getCollection()
                                            ->addFieldToFilter('product_id', ['eq' => $data['product_id']])
                                            ->addFieldToFilter('customer_id', ['eq' => $userId])
                                            ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                            ->addFieldToFilter('status', ['eq' => '1'])->getFirstItem();
            if ($bidCusrRecord->getEntityId()) {
                $bidCusrRecord->setId($bidCusrRecord->getEntityId());
                $bidCusrRecord->setAuctionAmount($data['bidding_amount']);
                $bidCusrRecord->setCreatedAt($this->_dateTime->date('Y-m-d H:i:s'));
                $bidCusrRecord->save();
            } else {
                $bidModel = $this->_auctionAmount->create();
                $bidModel->setAuctionAmount($data['bidding_amount']);
                $bidModel->setCustomerId($userId);
                $bidModel->setProductId($data['product_id']);
                $bidModel->setAuctionId($data['auction_id']);
                $bidModel->setStatus(1);
                $bidModel->save();
            }
            if ($auctionConfig['enable_submit_bid_email']) {
                $this->_helperEmail->sendSubmitMailToBidder($userId, $data['product_id'], $data['bidding_amount']);
            }
            if ($auctionConfig['enable_admin_email']) {
                $this->_helperEmail->sendMailToAdmin($userId, $data['product_id'], $data['bidding_amount']);
            }
            $autoBidList = $this->_autoAuction->create()->getCollection()
                                        ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                        ->addFieldToFilter('status', ['eq' => '1']);

            $this->bidAutoBidder($autoBidList, $auctionConfig, $userId, $data, $val);
            if ($biddingModel->getMinAutoAmount() < $val) {
                $biddingModel->setMinAutoAmount($val);
            }
            $biddingModel->setMinAmount($val);
            $biddingModel->save();
            $this->messageManager->addSuccess(__('Your bid amount successfully saved.'));
            return true;
        }
    }

    public function bidAutoBidder($autoBidList, $auctionConfig, $userId, $data, $val)
    {
        $sendUserId = "";
        if (!empty($autoBidList) && $auctionConfig['enable_outbid_email']) {
            $autoBidArray = [];
            foreach ($autoBidList as $autoBidData) {
                $autoBidArray[$autoBidData['customer_id']] = $autoBidData['amount'];
            }

            if (!empty($autoBidArray)) {
                if ($val > max($autoBidArray)) {
                    $sendUserId = 0;
                    foreach ($autoBidArray as $buyr => $amnt) {
                        $sendUserId = $buyr;
                        break;
                    }
                    if ($sendUserId != 0 && $sendUserId != $userId) {
                        $this->_helperEmail->sendOutBidAutoBidder($sendUserId, $userId, $data['product_id']);
                    }
                }
            }
        }
        if ($auctionConfig['enable_outbid_email']) {
            $listForSendMail = $this->_auctionAmount->create()->getCollection()
                                        ->addFieldToFilter('product_id', ['eq' => $data['product_id']])
                                        ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']]);
            /**
             * @var int $customerId previous bidder customer
             * @var int $userId current bidder user
             */
            foreach ($listForSendMail as $key) {
                $custmerId = $key->getCustomerId();
                if ($custmerId != $userId && $custmerId != $sendUserId) {
                    $this->_helperEmail->sendMailToMembers($custmerId, $userId, $data['product_id']);
                }
            }
        }
    }

    /**
     * _saveAutobiddingAmount calls to store auto bid placed by customer
     * @param array $data holda data related to bidding
     * @var $userId int holds current customer id
     * @var $biddingModel object stores bidding details
     * @var $auctionConfig['auto_use_increment'] int stores whether increment option is enable in admin panel or not
     * @var $auctionConfig['auto_auc_limit'] stores whether customer can place auto bid multiple times or not
     * @var $minPrice int stores minimum price of bidding
     * @var $incopt stores increament option is allowed on bidding or not
     * @var $incprice holds increment price for product
     * @var $pid int product id on which bid is placed
     * @var $val int bidding amount placed by customer
     * @var $autoBidRecord object checks whether there is already bid already exist for current customer or not
     * @var $autoBidModel autobid model to store auto bid
     * @var $listToSendMail use to get maximum auto bid amount
     */

    protected function _saveAutobiddingAmount($data)
    {
        $val = $data['bidding_amount'];
        $userId = $this->_customerSession->getCustomerId();
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        $biddingModel = $this->_auctionProductFactory->create()->load($data['auction_id']);
        $changeprice = $minPrice = $biddingModel->getMinAutoAmount();
        $autoBidRecordList = $this->_autoAuction->create()->getCollection()
                                    ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                    ->addFieldToFilter('status', ['eq' => '1'])
                                    ->addFieldToFilter('amount', ['gteq' => $val])->getFirstItem();

        if ($biddingModel->getIncrementOpt() && $auctionConfig['auto_use_increment']) {
            $incVal = $this->_helperData->getIncrementPriceAsRange($minPrice);
            $changeprice = $minPrice = $incVal ? $minPrice + $incVal : $minPrice;
        }
        if ($minPrice + 0.009999999999 >= $val) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('You can not auto bid less than or equal to current bid price.')
            );
        }
        if ($autoBidRecordList->getEntityId()) {
            $this->minAutoBidSave($data);
            return true;
        } else {
            $autoBidRecord = $this->_autoAuction->create()->getCollection()
                                    ->addFieldToFilter('customer_id', ['eq' => $userId])
                                    ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                    ->addFieldToFilter('status', ['eq' => '1'])->getFirstItem();
            if ($autoBidRecord->getEntityId()) {
                if (!$auctionConfig['auto_auc_limit']) {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        __('You are not allowed to auto bid again.')
                    );
                } else {
                    $this->autoBidSave($data);
                    $autoBidRecord->setId($autoBidRecord->getEntityId());
                    $autoBidRecord->setAmount($data['bidding_amount']);
                    $autoBidRecord->setCreatedAt($this->_dateTime->date('Y-m-d H:i:s'));
                    $autoBidRecord->save();
                }
            } else {
                $this->autoBidSave($data);
                $autoBidModel = $this->_autoAuction->create();
                $autoBidModel->setAmount($data['bidding_amount']);
                $autoBidModel->setCustomerId($userId);
                $autoBidModel->setProductId($data['product_id']);
                $autoBidModel->setAuctionId($data['auction_id']);
                $autoBidModel->setStatus(1);
                $autoBidModel->save();
            }
            if ($auctionConfig['enable_submit_bid_email']) {
                $this->_helperEmail->sendAutoSubmitMailToBidder($userId, $data['product_id'], $data['bidding_amount']);
            }
            $maxar=[0];
            $listToSendMail = $this->_autoAuction->create()->getCollection()
                                            ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                            ->addFieldToFilter('status', ['eq' => '1']);
            foreach ($listToSendMail as $ky) {
                array_push($maxar, $ky->getAmount());
            }
            $max = max($maxar);
            return $this->autobidAmountSave($auctionConfig, $max, $listToSendMail, $userId, $data, $val, $biddingModel);
        }
    }

    public function autobidAmountSave($auctionConfig, $max, $listToSendMail, $userId, $data, $val, $biddingModel)
    {
        if ($auctionConfig['enable_outbid_email'] && $max<=$val) {
            foreach ($listToSendMail as $ky) {
                $custmerId ='';
                if ($ky->getAmount()<=$val) {
                    $custmerId = $ky->getCustomerId();
                    if ($userId != $custmerId) {
                        $this->_helperEmail->sendAutoMailUsers($custmerId, $userId, $data['product_id']);
                    }
                }
            }
        }

        if ($auctionConfig['enable_admin_email']) {
            $this->_helperEmail->sendAutoMailToAdmin($userId, $data['product_id'], $data['bidding_amount']);
        }

        if ($auctionConfig['enable_auto_outbid_msg'] && $max > $val) {
            $this->messageManager->addError(__($auctionConfig['show_auto_outbid_msg']));
            return false;
        } else {
            $biddingModel->setMinAutoAmount($val);
            $biddingModel->save();
            $this->messageManager->addSuccess(__('Your auto bid amount successfully saved.'));
            return true;
        }
        return false;
    }
    public function bidSave($data)
    {
        $val = $data['bidding_amount'];
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        $biddingModel = $this->_auctionProductFactory->create()->load($data['auction_id']);
        //$data = $this->_customerSession->getAuctionBidData();
        $minPrice = $biddingModel->getMinAmount();
        //$auctionData = $data = $data ? $data: $this->getRequest()->getParams();
        //$data['product_id'] = $auctionData['product_id'];
        //$data['auction_id'] = $auctionData['entity_id'];
        if ($biddingModel->getIncrementOpt() && $auctionConfig['increment_auc_enable']) {
            $incVal = $this->_helperData->getIncrementPriceAsRange($minPrice);
            $data['bidding_amount']= $incVal ? $minPrice + $incVal : $minPrice;
        }
        $autoAuction =$this->_autoAuction->create()->getCollection()
                                            ->addFieldToFilter('product_id', ['eq' => $data['product_id']])
                                            ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                            ->setOrder('amount', 'DESC')
                                            ->getFirstItem();
        if ($data['bidding_amount']<=$autoAuction->getAmount()) {
            $userId = $autoAuction->getCustomerId();
            $bidCusrRecord = $this->_auctionAmount->create()->getCollection()
                                    ->addFieldToFilter('product_id', ['eq' => $data['product_id']])
                                    ->addFieldToFilter('customer_id', ['eq' => $userId])
                                    ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                    ->addFieldToFilter('status', ['eq' => '1'])->getFirstItem();
            if ($bidCusrRecord->getEntityId()) {
                $bidCusrRecord->setId($bidCusrRecord->getEntityId());
                $bidCusrRecord->setAuctionAmount($data['bidding_amount']);
                $bidCusrRecord->setCreatedAt($this->_dateTime->date('Y-m-d H:i:s'));
                $bidCusrRecord->save();
                $user = $autoAuction->getCustomerId();
                if ($auctionConfig['notify_email_template_onbehalf']) {
                    $this->_helperEmail
                    ->sendMailForNormalBiddingBehalf($user, $data['product_id'], $data['bidding_amount']);
                }
            } else {
                $bidModel = $this->_auctionAmount->create();
                $bidModel->setAuctionAmount($data['bidding_amount']);
                $bidModel->setCustomerId($userId);
                $bidModel->setProductId($data['product_id']);
                $bidModel->setAuctionId($data['auction_id']);
                $bidModel->setStatus(1);
                $bidModel->save();
                $user=$autoAuction->getCustomerId();
                if ($auctionConfig['notify_email_template_onbehalf']) {
                    $this->_helperEmail
                    ->sendMailForNormalBiddingBehalf($user, $data['product_id'], $data['bidding_amount']);
                }
            }
            if ($auctionConfig['enable_submit_bid_email']) {
                $this->_helperEmail->sendSubmitMailToBidder($userId, $data['product_id'], $data['bidding_amount']);
            }
            if ($auctionConfig['enable_admin_email']) {
                $this->_helperEmail->sendMailToAdmin($userId, $data['product_id'], $data['bidding_amount']);
            }
            $autoBidList = $this->_autoAuction->create()->getCollection()
                                ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                ->addFieldToFilter('status', ['eq' => '1']);

            $this->bidAutoBidder($autoBidList, $auctionConfig, $userId, $data, $val);
            if ($biddingModel->getMinAutoAmount() < $val) {
                $biddingModel->setMinAutoAmount($val);
            }
            $biddingModel->setMinAmount($data['bidding_amount']);
            $biddingModel->save();
        }
    }
    public function autoBidSave($data)
    {
           $val = $data['bidding_amount'];
           $auctionConfig = $this->_helperData->getAuctionConfiguration();
           $biddingModel = $this->_auctionProductFactory->create()->load($data['auction_id']);
           $minPrice = $biddingModel->getMinAmount();
           //$data = $this->_customerSession->getAuctionBidData();
           $userId = $this->_customerSession->getCustomerId();
           //$auction = $data = $data ? $data: $this->getRequest()->getParams();
           //$data['product_id'] = $auction['product_id'];
           //$data['auction_id'] = $auction['entity_id'];
           $auctionData = $this->_auctionProFactory->create()->getCollection()
                                    ->addFieldToFilter('product_id', ['eq'=>$data['product_id']])
                                    ->addFieldToFilter('auction_status', ['neq'=>3])
                                    ->addFieldToFilter('status', ['eq'=>0])->getFirstItem()->getData();

           $autoAmountCollection=$this->_autoAuction->create()->getCollection()
                                           ->addFieldToFilter('product_id', ['eq' => $data['product_id']])
                                           ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                           ->setOrder('amount', 'DESC');
        if ($biddingModel->getIncrementOpt() && $auctionConfig['auto_use_increment']) {
            $incVal = $this->_helperData->getIncrementPriceAsRange($minPrice);
            $data['bidding_amount']= $incVal ? $minPrice + $incVal : $minPrice;

        } else {
            $data['bidding_amount'] = $minPrice;
        }
        if ($autoAmountCollection->getSize()) {
            $autoAmount=$autoAmountCollection->getFirstItem();
            if ($data['bidding_amount']<=$autoAmount->getAmount()) {
                $data['bidding_amount']=$autoAmount->getAmount();
                if ($biddingModel->getIncrementOpt() && $auctionConfig['auto_use_increment']) {
                    $incVal = $this->_helperData->getIncrementPriceAsRange($minPrice);
                    $data['bidding_amount']= $incVal?$data['bidding_amount'] + $incVal
                                                                 : $data['bidding_amount'];
                }
            }
        }

        if ($biddingModel->getReservePrice() && $data['bidding_amount'] < $biddingModel->getReservePrice()) {
            $data['bidding_amount'] = $val;
            if ($biddingModel->getReservePrice() <= $val) {
                $data['bidding_amount'] = $biddingModel->getReservePrice();
            }
        }
        $bidCusrRecord = $this->_auctionAmount->create()->getCollection()
                            ->addFieldToFilter('product_id', ['eq' => $data['product_id']])
                            ->addFieldToFilter('customer_id', ['eq' => $userId])
                            ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                            ->addFieldToFilter('status', ['eq' => '1'])->getFirstItem();

        if ($bidCusrRecord->getEntityId()) {
            $bidCusrRecord->setId($bidCusrRecord->getEntityId());
            $bidCusrRecord->setAuctionAmount($data['bidding_amount']);
                $bidCusrRecord->setCreatedAt($this->_dateTime->date('Y-m-d H:i:s'));
                $bidCusrRecord->save();
                $biddingModel->setMinAmount($data['bidding_amount']);
                $biddingModel->save();
                $user=$this->_customerSession->getCustomerId();
            if ($auctionConfig['notify_email_template_onbehalf']) {
                $this->_helperEmail->sendMailForNormalBiddingBehalf(
                    $user,
                    $data['product_id'],
                    $data['bidding_amount']
                );
            }
        } else {
            $bidModel = $this->_auctionAmount->create();
            $bidModel->setAuctionAmount($data['bidding_amount']);
            $bidModel->setCustomerId($userId);
            $bidModel->setProductId($data['product_id']);
            $bidModel->setAuctionId($data['auction_id']);
            $bidModel->setStatus(1);
            $bidModel->save();
            $biddingModel->setMinAmount($data['bidding_amount']);
            $biddingModel->save();
            $user=$this->_customerSession->getCustomerId();
            if ($auctionConfig['notify_email_template_onbehalf']) {
                $this->_helperEmail->sendMailForNormalBiddingBehalf(
                    $user,
                    $data['product_id'],
                    $data['bidding_amount']
                );
            }
        }
        if ($auctionConfig['enable_submit_bid_email']) {
            $this->_helperEmail->sendSubmitMailToBidder($userId, $data['product_id'], $data['bidding_amount']);
        }
        if ($auctionConfig['enable_admin_email']) {
            $this->_helperEmail->sendMailToAdmin($userId, $data['product_id'], $data['bidding_amount']);
        }
    }
    public function minAutoBidSave($data)
    {
           $val = $data['bidding_amount'];
           $userId = $this->_customerSession->getCustomerId();
           $auctionConfig = $this->_helperData->getAuctionConfiguration();
           $auctionData = $data = $data ? $data: $this->getRequest()->getParams();
           $data['product_id'] = $auctionData['product_id'];
           $data['auction_id'] = $auctionData['entity_id'];
           $biddingModel = $this->_auctionProductFactory->create()->load($data['auction_id']);
           $changeprice = $minPrice = $biddingModel->getMinAutoAmount();
           $autoBidRecordList = $this->_autoAuction->create()->getCollection()
                                    ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                    ->addFieldToFilter('status', ['eq' => '1'])
                                    ->addFieldToFilter('amount', ['gteq' => $val])->getFirstItem();

        if ($biddingModel->getIncrementOpt() && $auctionConfig['auto_use_increment']) {
            $incVal = $this->_helperData->getIncrementPriceAsRange($minPrice);
        }
        $autoBidRecord = $this->_autoAuction->create()->getCollection()
                                    ->addFieldToFilter('customer_id', ['eq' => $userId])
                                    ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                    ->addFieldToFilter('status', ['eq' => '1'])->getFirstItem();
        if ($autoBidRecord->getEntityId()) {
            if (!$auctionConfig['auto_auc_limit']) {
                return false;
            } else {
                $autoBidRecord->setId($autoBidRecord->getEntityId());
                $autoBidRecord->setAmount($data['bidding_amount']);
                $autoBidRecord->setCreatedAt($this->_dateTime->date('Y-m-d H:i:s'));
                $autoBidRecord->save();
                $biddingModel->setMinAmount($data['bidding_amount']);
                $biddingModel->save();
            }
        } else {
            $autoBidModel = $this->_autoAuction->create();
            $autoBidModel->setAmount($data['bidding_amount']);
            $autoBidModel->setCustomerId($userId);
            $autoBidModel->setProductId($data['product_id']);
            $autoBidModel->setAuctionId($data['auction_id']);
            $autoBidModel->setStatus(1);
            $autoBidModel->save();
            $biddingModel->setMinAmount($data['bidding_amount']);
            $biddingModel->save();
        }
        if ($auctionConfig['enable_submit_bid_email']) {
            $this->_helperEmail->sendAutoSubmitMailToBidder($userId, $data['product_id'], $data['bidding_amount']);
        }
            $maxar=[0];
            $listToSendMail = $this->_autoAuction->create()->getCollection()
                                        ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                        ->addFieldToFilter('status', ['eq' => '1']);
        foreach ($listToSendMail as $ky) {
            array_push($maxar, $ky->getAmount());
        }
            $max = max($maxar);
        if ($biddingModel->getIncrementOpt() && $auctionConfig['increment_auc_enable']) {
             $incVal = $this->_helperData->getIncrementPriceAsRange($minPrice);
             $data['bidding_amount']= $incVal ? $data['bidding_amount'] + $incVal : $data['bidding_amount'];
        }
            $autoAmount=$this->_autoAuction->create()->getCollection()
                                           ->addFieldToFilter('product_id', ['eq' => $data['product_id']])
                                           ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                           ->setOrder('amount', 'DESC')
                                           ->getFirstItem();
        if ($data['bidding_amount']<=$autoAmount->getAmount()) {
            $userId = $autoAmount->getCustomerId();
              $bidCusrRecord = $this->_auctionAmount->create()->getCollection()
                                      ->addFieldToFilter('product_id', ['eq' => $data['product_id']])
                                      ->addFieldToFilter('customer_id', ['eq' => $userId])
                                      ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                      ->addFieldToFilter('status', ['eq' => '1'])->getFirstItem();
            if ($bidCusrRecord->getEntityId()) {
                $bidCusrRecord->setId($bidCusrRecord->getEntityId());
                $bidCusrRecord->setAuctionAmount($data['bidding_amount']);
                $bidCusrRecord->setCreatedAt($this->_dateTime->date('Y-m-d H:i:s'));
                $bidCusrRecord->save();
                $biddingModel->setMinAmount($data['bidding_amount']);
                $biddingModel->save();
                $this->messageManager->addSuccess(__('Your auto bid amount successfully saved.'));
                $user=$autoAmount->getCustomerId();
                if ($auctionConfig['notify_email_template_onbehalf']) {
                    $this->_helperEmail->
                    sendMailForNormalBiddingBehalf($user, $data['product_id'], $data['bidding_amount']);
                }
            } else {
                $bidModel = $this->_auctionAmount->create();
                $bidModel->setAuctionAmount($data['bidding_amount']);
                $bidModel->setCustomerId($userId);
                $bidModel->setProductId($data['product_id']);
                $bidModel->setAuctionId($data['auction_id']);
                $bidModel->setStatus(1);
                $bidModel->save();
                $biddingModel->setMinAmount($data['bidding_amount']);
                $biddingModel->save();
                $this->messageManager->addSuccess(__('Your auto bid amount successfully saved.'));
                $user=$autoAmount->getCustomerId();
                if ($auctionConfig['notify_email_template_onbehalf']) {
                    $this->_helperEmail->
                    sendMailForNormalBiddingBehalf($user, $data['product_id'], $data['bidding_amount']);
                }
            }
            if ($auctionConfig['enable_submit_bid_email']) {
                $this->_helperEmail->sendSubmitMailToBidder($userId, $data['product_id'], $data['bidding_amount']);
            }
            if ($auctionConfig['enable_admin_email']) {
                $this->_helperEmail->sendMailToAdmin($userId, $data['product_id'], $data['bidding_amount']);
            }
        }
    }
}
