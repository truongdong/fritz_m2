<?php
/**
 * Webkul Auction auto bid delete controller.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Controller\Account;

use Magento\Framework\App\Action\Context;
use Webkul\Auction\Model\AutoAuction;

class DeleteAutoBid extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var PageFactory
     */
    protected $_customerSession;

    /**
     * @param Context $context
     * @param \Magento\Customer\Model\Session $_customerSession
     * @param Amount $auctionAmt
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $customerSession,
        AutoAuction $autoAuction
    ) {
        $this->_customerSession = $customerSession;
        $this->_autoAuction = $autoAuction;
        parent::__construct($context);
    }

    /**
     * Auction bid delete controller
     *
     * @return Magento\Backend\Model\View\Result\Redirect $resultRedirect
     */
    public function execute()
    {
        /** @var int $curntCustomerId */
        $curntCustomerId = $this->_customerSession->getCustomerId();

        /** @var int $bidId */
        $bidId=$this->_request->getParam('id');

        /** @var Webkul\Auction\Model\AutoAuction $autoBidRecord */
        $autoBidRecord = $this->_autoAuction->load($bidId);
        if ($autoBidRecord->getEntityId()
            && $curntCustomerId == $autoBidRecord->getCustomerId()) {
            $autoBidRecord->delete();
            $this->messageManager
                    ->addSuccess(__('Auction auto bid removed successfuly.'));
        } else {
            $this->messageManager->addError(__('Not permitted.'));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setUrl(
            $this->_url->getUrl('auction/account/autobidding')
        );
    }
}
