<?php
/**
 * Webkul Auction detail controller.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Controller\Account;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Webkul\Auction\Helper\Data;

class Product extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
    /**
     *
     * @var fullPageCache
     */
    private $fullPageCache;
    /**
     * @var \Webkul\Auction\Helper\Data
     */
    public $helperData;

   /**
    * Undocumented function
    *
    * @param Context $context
    * @param PageFactory $_resultPageFactory
    * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
    * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
    * @param Data $helperData
    */
    public function __construct(
        Context $context,
        PageFactory $_resultPageFactory,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        Data $helperData
    ) {
        $this->_resultPageFactory = $_resultPageFactory;
        $this->helperData = $helperData;
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
    }

    /**
     * Auction Detail page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $this->helperData->cacheFlush();
        return $resultPage;
    }
    private function getCache()
    {
        if (!$this->fullPageCache) {
            $this->fullPageCache = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\PageCache\Model\Cache\Type::class
            );
        }
        return $this->fullPageCache;
    }

    public function cleanByTags($productId)
    {
        
        $tags = ['CAT_P_'.$productId];
        $this->getCache()->clean(\Zend_Cache::CLEANING_MODE_MATCHING_TAG, $tags);
    }
}
