<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Checkout\Model\Cart;

class Addwallettocart extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $_formKey;
    /**
     * @var ProductFactory
     */
    protected $_productFactory;
    /**
     * @var Magento\Checkout\Model\Cart
     */
    protected $_cartModel;

    protected $helper;
    /**
     * @param Context                              $context
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param PageFactory                          $resultPageFactory
     * @param ProductFactory                       $productFactory
     * @param cart                                 $cartModel
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Data\Form\FormKey $formKey,
        PageFactory $resultPageFactory,
        ProductFactory $productFactory,
        cart $cartModel,
        \Webkul\Auction\Helper\Data $helper,
        \Magento\Framework\Pricing\Helper\Data $priceHelper
    ) {
        parent::__construct($context);
        $this->_formKey = $formKey;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_productFactory = $productFactory;
        $this->_cartModel = $cartModel;
        $this->helper = $helper;
        $this->priceHelper=$priceHelper;
    }

    /**
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
            $wholedata = $this->getRequest()->getParams();
            $price=$wholedata['price'];
            $formattedPrice = $this->priceHelper->currency($price, true, false);
            $product = $this->_productFactory->create()->load($wholedata['product']);
            $allItems = $this->_cartModel->getQuote()->getAllItems();
            $alreadyAdded = false;
        foreach ($allItems as $item) {
            if ($product->getId() == $item->getProductId()) {
                $alreadyAdded = true;
            }
        }
            $resultRedirect = $this->resultRedirectFactory->create();
        if ($alreadyAdded) {
            $this->messageManager->addError(__('You have already added an auction wallet to cart.'));
        } elseif (array_key_exists('product', $wholedata)) {
            $params = [
            'form_key' => $this->_formKey->getFormKey(),
            'product' =>$wholedata['product'],
            'qty'   =>1,
            'price' =>$wholedata['price']
            ];
            $maximumAmount = $this->helper->getMaximumAmount();
            $this->_cartModel->addProduct($product, $params);
            $this->_cartModel->save();
            if ($maximumAmount < $price) {
                $formattedPrice = $this->priceHelper->currency($maximumAmount, true, false);
            }
            $this->messageManager
            ->addSuccess(__('Auction Wallet amount of %1 added to cart.', $formattedPrice));
        } else {
            $this->messageManager->addError(__('Something went wrong'));
        }
        $resultRedirect->setPath('auction/index/index');
        return $resultRedirect;
    }
}
