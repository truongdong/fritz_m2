<?php
/**
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Controller\Index;

use Magento\Catalog\Model\Product\Exception as ProductException;
use Magento\Framework\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Model\SessionFactory as CustomerSession;
use Magento\Catalog\Model\ProductFactory;

/**
 * Add wishlist item to shopping cart and remove from wishlist controller.
 *
 */
class Cart extends \Magento\Wishlist\Controller\Index\Cart
{
    /**
     * @var \Magento\Wishlist\Controller\WishlistProviderInterface
     */
    protected $wishlistProvider;

    /**
     * @var \Magento\Wishlist\Model\LocaleQuantityProcessor
     */
    protected $quantityProcessor;

    /**
     * @var \Magento\Wishlist\Model\ItemFactory
     */
    protected $itemFactory;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    /**
     * @var \Magento\Checkout\Helper\Cart
     */
    protected $cartHelper;

    /**
     * @var \Magento\Wishlist\Model\Item\OptionFactory
     */
    private $optionFactory;

    /**
     * @var \Magento\Catalog\Helper\Product
     */
    protected $productHelper;

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $escaper;

    /**
     * @var \Magento\Wishlist\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $formKeyValidator;

    /**
     * @param Action\Context $context
     * @param \Magento\Wishlist\Controller\WishlistProviderInterface $wishlistProvider
     * @param \Magento\Wishlist\Model\LocaleQuantityProcessor $quantityProcessor
     * @param \Magento\Wishlist\Model\ItemFactory $itemFactory
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\Wishlist\Model\Item\OptionFactory $optionFactory
     * @param \Magento\Catalog\Helper\Product $productHelper
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Wishlist\Helper\Data $helper
     * @param \Magento\Checkout\Helper\Cart $cartHelper
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     */
    public function __construct(
        Action\Context $context,
        \Magento\Wishlist\Controller\WishlistProviderInterface $wishlistProvider,
        \Magento\Wishlist\Model\LocaleQuantityProcessor $quantityProcessor,
        \Magento\Wishlist\Model\ItemFactory $itemFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Webkul\Auction\Model\ProductFactory $auctionProductFactory,
        \Magento\Wishlist\Model\Item\OptionFactory $optionFactory,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Framework\Escaper $escaper,
        \Webkul\Auction\Helper\Data $helperData,
        \Magento\Wishlist\Helper\Data $helper,
        \Magento\Checkout\Helper\Cart $cartHelper,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        CustomerSession $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        ProductFactory $product
    ) {
        $this->wishlistProvider = $wishlistProvider;
        $this->quantityProcessor = $quantityProcessor;
        $this->itemFactory = $itemFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->helperData = $helperData;
        $this->cart = $cart;
        $this->optionFactory = $optionFactory;
        $this->productHelper = $productHelper;
        $this->_auctionProductFactory = $auctionProductFactory;
        $this->escaper = $escaper;
        $this->product = $product;
        $this->helper = $helper;
        $this->cartHelper = $cartHelper;
        $this->formKeyValidator = $formKeyValidator;
        $this->customerSession = $customerSession->create();
        parent::__construct(
            $context,
            $wishlistProvider,
            $quantityProcessor,
            $itemFactory,
            $cart,
            $optionFactory,
            $productHelper,
            $escaper,
            $helper,
            $cartHelper,
            $formKeyValidator
        );
    }

    /**
     * Add wishlist item to shopping cart and remove from wishlist
     *
     * If Product has required options - item removed from wishlist and redirect
     * to product view page with message about needed defined required options
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $cartItems = $this->_checkoutSession->getQuote()->getAllItems();
      
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirectFactory */
        $resultRedirectFactory = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            return $resultRedirectFactory->setPath('*/*/');
        }

        $itemId = (int)$this->getRequest()->getParam('item');
        /* @var $item \Magento\Wishlist\Model\Item */
        $item = $this->itemFactory->create()->load($itemId);
        $proId = $item->getProductId();
        $auctionOpt = $this->product->create()->load($proId)->getAuctionType();
        $auctionOpt = explode(',', $auctionOpt);
        $auctionProductData = $this->_auctionProductFactory->create()->getCollection()
            ->addFieldToFilter('product_id', ['eq' => $proId])
            ->addFieldToFilter('auction_status', ['in' => [1,0]])->setPageSize(1)->getLastItem();
        
        if (!in_array(1, $auctionOpt) && $auctionProductData->getEntityId()) {
                $auctionProductDataTemp = $this->_auctionProductFactory->create()
                                                        ->getCollection()
                                                        ->addFieldToFilter('product_id', ['eq' => $proId])
                                                        ->addFieldToFilter('auction_status', ['in' => [0]])
                                                        ->addFieldToFilter('status', ['in' => [0]])
                                                        ->setPageSize(1)->getLastItem();
                $tempId = $auctionProductDataTemp ? $auctionProductDataTemp->getId() : 0;
                $winnerBidDetail = $this->helperData->getWinnerBidDetail($tempId);
            if (!($winnerBidDetail && $this->customerSession->getCustomerId() == $winnerBidDetail->getCustomerId())) {
                $msg = 'You can not add auction product to cart.';
                $this->messageManager->addErrorMessage(__($msg));
                $resultRedirectFactory->setPath('*/*');
                 return $resultRedirectFactory;
            }
        }

        $qty = 0;
        $maxQty = $auctionProductData->getMaxQty();
        $nonAuctionProduct = false;
        $auctionProductStatus = false;
        foreach ($cartItems as $item) {
            $auctionProduct = $this->_auctionProductFactory->create()->getCollection()
                    ->addFieldToFilter('product_id', ['eq' => $item->getProductId()])
                    ->addFieldToFilter('status', ['eq' => 0])->getLastItem();
            if ($auctionProduct->getEntityId()) {
                $auctionProductStatus = true;
                $auctionProductId = $auctionProduct->getProductId();
            }
            if ($item->getProductId() == $proId) {
                $qty = $qty + $item->getQty();
            } else {
                $nonAuctionProduct = true;
            }
        }

        if ($auctionProductStatus && ($auctionProductId != $proId)) {
            $msg = 'You can not add another product with auction Product.';
            $this->messageManager->addErrorMessage(__($msg));
            $resultRedirectFactory->setPath('*/*');
            return $resultRedirectFactory;
        }
        if ($auctionProductData->getEntityId()) {
            $resultdata = false;
            if ($nonAuctionProduct) {
                $resultdata = true;
                $msg = 'You can not add auction Product with another product.';
                $this->messageManager->addError(__($msg));
            }
            if ($qty == $maxQty) {
                $resultdata =true;
                $msg = 'Maximum '. $maxQty.' quantities are allowed to purchase this item.';
                $this->messageManager->addNotice(__($msg));
            } elseif ($qty < $maxQty && $this->getRequest()->getParam('qty') > $maxQty-$qty) {
                $resultdata = true;
                $msg = 'Maximum '. $maxQty.' quantities are allowed to purchase this item.';
                $this->messageManager->addNotice(__($msg));
            }
            if ($resultdata) {
                $resultRedirectFactory->setPath('*/*');
                return $resultRedirectFactory;
            }
        }
        $getItemId = $item->getId();
        if (!$getItemId) {
            $resultRedirectFactory
            ->setPath('*/*');
            return $resultRedirectFactory;
        }
        $wishlist = $this->wishlistProvider->getWishlist($item->getWishlistId());
        if (!$wishlist) {
            $resultRedirectFactory->setPath('*/*');
            return $resultRedirectFactory;
        }

        // Set qty
        $getQty = $this->getQty();
        $getPostQty = $this->getPostQty();
        if ($getPostQty !== null
         && $getQty !== $getPostQty) {
            $getQty = $getPostQty;
        }
        if (is_array($getQty)) {
            if (isset($getQty[$itemId])) {
                $getQty = $getQty[$itemId];
            } else {
                $getQty = 1;
            }
        }
        $getQty = $this->quantityProcessor->process($getQty);
        if ($getQty) {
            $item->setQty($getQty);
        }

        $redirectUrl = $this->getRedirectedUrl();
        $configureUrl = $this->_url->getUrl(
            '*/*/configure/',
            [
                'id' => $item->getId(),
                'product_id' => $item->getProductId(),
            ]
        );
        try {
            /** @var \Magento\Wishlist\Model\ResourceModel\Item\Option\Collection $options */
            $options = $this->optionFactory->create()->getCollection()->addItemFilter([$itemId]);
            $item->setOptions($options->getOptionsByItem($itemId));

            $buyRequest = $this->productHelper->addParamsToBuyRequest(
                $this->getRequest()->getParams(),
                ['current_config' => $item->getBuyRequest()]
            );

            $item->mergeBuyRequest($buyRequest);
            $item->addToCart($this->cart, true);
            $this->cart->save()->getQuote()->collectTotals();
            $wishlist->save();

            if (!$this->cart->getQuote()->getHasError()) {
                $message = __(
                    'You added %1 to your shopping cart.',
                    $this->escaper->escapeHtml($item->getProduct()->getName())
                );
                $this->messageManager->addSuccessMessage($message);
            }

            if ($this->cartHelper->getShouldRedirectToCart()) {
                $redirectUrl = $this->cartHelper->getCartUrl();
            } else {
                $refererUrl = $this->_redirect->getRefererUrl();
                if ($refererUrl && $refererUrl != $configureUrl) {
                    $redirectUrl = $refererUrl;
                }
            }
        } catch (ProductException $e) {
            $this->messageManager->addErrorMessage(__('This product(s) is out of stock.'));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addNoticeMessage($e->getMessage());
            $redirectUrl = $configureUrl;
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('We can\'t add the item to the cart right now.'));
        }

        $this->helper->calculate();

        if ($this->getRequest()->isAjax()) {
            /** @var \Magento\Framework\Controller\Result\Json $resultJson */
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData(['backUrl' => $redirectUrl]);
            return $resultJson;
        }

        $resultRedirectFactory->setUrl($redirectUrl);
        return $resultRedirectFactory;
    }

    public function getRedirectedUrl()
    {
        return $this->_url->getUrl('*/*');
    }
   
   /**
    * get Qty
    *
    * @return number
    */
    public function getQty()
    {
        return $this->getRequest()->getParam('qty');
    }

    public function getPostQty()
    {
        return $this->getRequest()->getPostValue('qty');
    }
}
