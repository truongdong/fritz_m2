<?php
/**
 * Webkul_Auction bid save controller for login user
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use Webkul\Auction\Model\BankdetailsFactory;
use Webkul\Auction\Model\WallettransactionFactory;
use Magento\Customer\Model\Url as CustomerUrl;

class Save extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dateTime;

    /**
     * @var \Webkul\Auction\Helper\Data
     */
    protected $_helperData;

    /**
     * @var \Webkul\Auction\Model\BankdetailsFactory
     */
    protected $_bankdetails;

    /**
     * @var Webkul\Auction\Model\WallettransactionFactory
     */
    protected $_walletTransaction;

    /**
     * @var CustomerUrl
     */
    protected $_customerUrl;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Directory\Model\CurrencyFactory $dirCurrencyFactory
     * @param RequestInterface $request
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Webkul\Auction\Helper\Data $helperData
     * @param \Webkul\Auction\Model\BankdetailsFactory $bankdetailsFactory
     * @param WallettransactionFactory $walletTransaction
     * @param CustomerUrl $customerUrl
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        BankdetailsFactory $bankdetails,
        WallettransactionFactory $walletTransaction,
        \Webkul\Auction\Helper\Data $helperData,
        CustomerUrl $customerUrl,
        \Webkul\Auction\Helper\Email $helperEmail
    ) {
        $this->_customerSession = $customerSession;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_dateTime = $dateTime;
        $this->_bankdetails = $bankdetails;
        $this->_walletTransaction = $walletTransaction;
        $this->_helperData = $helperData;
        $this->_customerUrl = $customerUrl;
        $this->_helperEmail = $helperEmail;
        parent::__construct($context);
    }

    /**
     * Check customer authentication
     *
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->_customerUrl->getLoginUrl();

        if (!$this->_customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
            //set Data in session if bidder not login
            $this->_customerSession->setAuctionBidData($request->getParams());
        }
        return parent::dispatch($request);
    }

    /**
     * Default customer account page
     * @var $cuntCunyCode current Currency Code
     * @var $baseCunyCode base Currency Code
     * @return \Magento\Backend\Model\View\Result\Redirect $resultRedirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $customerId = $this->_customerSession->getCustomerId();
        $customerWalletAmount = $this->_helperData->customerWalletAmount($customerId);
        if ($customerWalletAmount >= $data['amount']) {
            $data['customer_id'] = $customerId;
            $data['status'] = 0;
            $data['created_at'] = $this->_dateTime->date('Y-m-d H:i:s');
            $transactionData = [
                              'customer_id' => $customerId,
                              'amount' => $data['amount'],
                              'status' => 0,
                              'action' => 'debit',
                              'order_id' => 0,
                              'transaction_at' => $data['created_at'],
                              'currency_code' => $this->_helperData->getCurrentCurrencyCode(),
                              'curr_amount' => $data['amount'],
                              'expired_status' => 1,
                              'transaction_note' => strip_tags($data['note'])
                            ];
            $walletTansactionModel = $this->_walletTransaction
              ->create();
            $walletTansactionModel->setData($transactionData)->save();
            $data['transaction_id'] = $walletTansactionModel->getId();
            $data['bank_details'] = strip_tags($data['bank_details']);
            $data['note'] = strip_tags($data['note']);
            $detailsModel = $this->_bankdetails->create();
            $detailsModel->setData($data);
            $detailsModel->save();
            if ($detailsModel->getId()) {
                $this->_helperEmail->sendTransferRequestEmailToAdmin($data);
            }
            $this->messageManager->addSuccess(__('Your transfer request has been submitted successfully'));
        } else {
            $this->messageManager->addError(__('You do not have requested amount in your auction wallet to transfer'));
        }
        return $this->resultRedirectFactory->create()->setPath('auction/account/transfer');
    }
}
