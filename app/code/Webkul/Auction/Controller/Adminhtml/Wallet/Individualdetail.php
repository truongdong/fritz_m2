<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Controller\Adminhtml\Wallet;

use Webkul\Auction\Controller\Adminhtml\Wallet as WalletController;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;

class Individualdetail extends WalletController
{
    /**
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    protected $_resultLayoutFactory;
    /**
     * @var Magento\Customer\Model\CustomerFactory
     */
    protected $_customerModel;

    /**
     * @param \Magento\Backend\App\Action\Context          $context
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Customer\Model\CustomerFactory $customerModel
    ) {
        parent::__construct($context);
        $this->_customerModel = $customerModel;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $customer = $this->_customerModel
            ->create()
            ->load($params['customer_id']);
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Webkul_Auction::manager');
        $resultPage->getConfig()->getTitle()
            ->prepend(
                ucwords(
                    __(
                        "%1's Transaction details",
                        $customer->getName()
                    )
                )
            );
        $resultPage->addBreadcrumb(
            __("%1's Transaction details", $customer->getName()),
            __("%1's Transaction details", $customer->getName())
        );
        return $resultPage;
    }
}
