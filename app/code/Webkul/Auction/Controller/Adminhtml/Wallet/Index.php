<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Controller\Adminhtml\Wallet;

use Webkul\Auction\Controller\Adminhtml\Wallet as WalletController;
use Magento\Framework\Controller\ResultFactory;

class Index extends WalletController
{
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Webkul_Auction::manager');
        $resultPage->getConfig()->getTitle()->prepend(__('Auction Wallet Details'));
        $resultPage->addBreadcrumb(__('Auction Wallet Details'), __('Auction Wallet Details'));
        return $resultPage;
    }
}
