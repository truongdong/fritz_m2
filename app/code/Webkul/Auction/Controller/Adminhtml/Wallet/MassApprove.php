<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Controller\Adminhtml\Wallet;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Webkul\Auction\Model\ResourceModel\Bankdetails\CollectionFactory;

/**
 * Class MassApprove
 * For Mass Approve
 */
class MassApprove extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var \Webkul\Auction\Model\BankdetailsFactory
     */
    protected $_bankDetailsModel;

    /**
     * @var \Webkul\Auction\Model\WallettransactionFactory
     */
    protected $_wallettransactionModel;

    /**
     * @var \Webkul\Auction\Model\WalletrecordFactory
     */
    protected $_walletrecordModel;

    /**
     * @var \Webkul\Auction\Helper\Emai
     */
    protected $_emailHelper;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

     /**
      * Store manager
      *
      * @var \Magento\Store\Model\StoreManagerInterface
      */
    protected $_storeManager;

    /**
     * @param Context                                     $context
     * @param Filter                                      $filter
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param CollectionFactory                           $collectionFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Webkul\Auction\Helper\Data $dataHelper,
        \Webkul\Auction\Model\BankdetailsFactory $bankDetailsModel,
        \Webkul\Auction\Model\WallettransactionFactory $wallettransactionModel,
        \Webkul\Auction\Model\WalletrecordFactory $walletrecordModel,
        \Webkul\Auction\Helper\Email $emailHelper,
        CollectionFactory $collectionFactory
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
        $this->_date = $date;
        $this->_storeManager = $storeManager;
        $this->_helper = $dataHelper;
        $this->_bankDetailsModel = $bankDetailsModel;
        $this->_wallettransactionModel = $wallettransactionModel;
        $this->_walletrecordModel = $walletrecordModel;
        $this->_emailHelper = $emailHelper;
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $error = 0;
        $success = 0;
        $approved = 0;
        $collection = $this->filter->getCollection(
            $this->collectionFactory->create()
        );
        $customerId = 0;
        foreach ($collection as $item) {
            $customerId = $item->getCustomerId();
            if (!$item->getStatus()) {
                $customerWalletAmount = $this->_helper->customerWalletAmount($item->getCustomerId());
                if ($customerWalletAmount >= $item->getAmount()) {
                    $emailParams = $this->approvedRequest($item);
                    $item->setStatus(1)->save();
                    $success = $success++;
                    if (!empty($emailParams)) {
                        $this->_emailHelper->sendMailForTransaction(
                            $emailParams['customer_id'],
                            $emailParams,
                            $emailParams['store']
                        );
                    }
                } else {
                    $error = $error++;
                }
            } else {
                $approved = $approved++;
            }
        }
        if ($success) {
            $this->messageManager->addSuccess(
                __(
                    'A total of %1 record(s) have been approved',
                    $success
                )
            );
        }
        if ($error) {
            $this->messageManager->addError(
                __(
                    'A total of %1 record(s) have not been approved due to insufficient amount in wallet',
                    $error
                )
            );
        }
        if ($approved) {
            $this->messageManager->addNotice(
                __(
                    'A total of %1 record(s) already approved',
                    $approved
                )
            );
        }
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(
            ResultFactory::TYPE_REDIRECT
        );
        return $resultRedirect->setPath('*/*/transferrequest', ['customer_id' => $customerId]);
    }

    public function approvedRequest($record)
    {
        $data = ['status' => 1];
        $rowId = $record->getTransactionId();
        $amount = $record->getAmount();
        $customerId = $record->getCustomerId();
        $walletTansactionModel = $this->_wallettransactionModel
          ->create()
          ->load($rowId)
          ->addData($data);
        $walletTansactionModel->setId($rowId)->save();
        $transactionNote = $walletTansactionModel->getTransactionNote();
        $action = $walletTansactionModel->getAction();
        $walletRecordCollection = $this->_walletrecordModel
          ->create()
          ->getCollection()
        ->addFieldToFilter(
            'customer_id',
            ['eq' => $customerId]
        );
        if ($action == 'debit') {
            $emailParams = $this->_helper->deductWalletDataAmount(
                $walletRecordCollection,
                $amount,
                $customerId,
                $transactionNote
            );
            return $emailParams;
        }
        return [];
    }
    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Auction::wallet');
    }
}
