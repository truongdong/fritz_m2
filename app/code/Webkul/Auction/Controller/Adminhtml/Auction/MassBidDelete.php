<?php
/**
 * Webkul Auction Bid MassDelete Controller
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Controller\Adminhtml\Auction;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Webkul\Auction\Model\ResourceModel\Amount\CollectionFactory;

class MassBidDelete extends \Magento\Backend\App\Action
{
    /**
     * Massactions for delete bid filter.
     *
     * @var Filter
     */
    protected $_filter;

    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var \Webkul\Auction\Model\ProductFactory
     */
    protected $_auction;

    /**
     * @param Context           $context
     * @param Filter            $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        \Webkul\Auction\Model\ProductFactory $auction,
        CollectionFactory $collectionFactory
    ) {
        $this->_filter = $filter;
        $this->_collectionFactory = $collectionFactory;
        $this->_auction = $auction;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        try {
            $recordDeleted = 0;
            $auctionId = 0;
            $data = $this->getRequest()->getParams();
            $auctionId = $this->_session->getAuctionId();
            if (isset($data['selected'])) {
                $bidIds = $data['selected'];
                $collection = $this->_collectionFactory->create()
                    ->addFieldToFilter('entity_id', ['in'=> $bidIds]);
                if ($collection->getSize()) {
                    foreach ($collection as $auctionProductBid) {
                        $auctionId = $auctionProductBid->getAuctionId();
                        $auctionProductBid->setId($auctionProductBid->getEntityId());
                        $this->deleteObj($auctionProductBid);
                        $recordDeleted++;
                    }
                }
                $this->updateAuctionMinAmount($auctionId);
            } elseif (isset($data['excluded'])) {
                $collection = $this->_collectionFactory->create()
                    ->addFieldToFilter('auction_id', ['eq'=> $auctionId]);
                foreach ($collection as $auctionProductBid) {
                    $auctionProductBid->setId($auctionProductBid->getEntityId());
                    $this->deleteObj($auctionProductBid);
                    $recordDeleted++;
                }
                $this->updateAuctionMinAmount($auctionId);
            }
            $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $recordDeleted));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)
                                        ->setPath('*/*/addauction/', ['id' => $auctionId, 'auction_id' => $auctionId]);
    }

    /**
     * Check Auction Product bid delete Permission.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Auction::add_auction');
    }

    /**
     * deleteObj
     * @param Object
     * @return void
     */
    private function deleteObj($object)
    {
        $object->delete();
    }

    public function updateAuctionMinAmount($id)
    {
        $auction = $this->_auction->create()->load($id);
        $minAmount = 0;
        $collection =  $this->_collectionFactory->create()->setOrder('auction_amount', 'DESC');
        if ($collection->getSize()) {
            foreach ($collection as $amount) {
                $minAmount = $amount->getAuctionAmount();
                break;
            }
        }
        if (!$minAmount) {
            $minAmount = $auction->getStartingPrice();
        }
        $auction->setMinAmount($minAmount)->save();
    }
}
