<?php
/**
 * Webkul Auction AddAuction Controller
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Controller\Adminhtml\Auction;

use Magento\Framework\Controller\ResultFactory;
use Webkul\Auction\Model\ProductFactory;

class AddAuction extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;

    /**
     * @var Webkul\Auction\Model\ProductFactory
     */
    protected $auctionProduct;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry  $coreRegistry
     * @param \Magento\Catalog\Model\Product $product
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Catalog\Model\Product $product,
        ProductFactory $productFactory
    ) {

        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->product = $product;
        $this->auctionProduct = $productFactory;
    }
    /**
     * Add New Auction Form page.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $auctionId = (int) $this->getRequest()->getParam('id');
        $auctionProduct = $this->auctionProduct->create();
        if ($auctionId) {
            $auctionProduct = $auctionProduct->load($auctionId);
            $productName = $this->product->load($auctionProduct->getProductId())->getName();
            if (!$auctionProduct->getEntityId()) {
                $this->messageManager->addError(__('Product no longer exist.'));
                $this->_redirect('auction/auction/addauction');
                return;
            }
        }
        $this->_session->setAuctionId('');
        $this->_session->setAuctionId($auctionProduct->getEntityId());
        $this->coreRegistry->register('auction_product', $auctionProduct);
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $title = $auctionId ? __('Edit Auction For ').$productName : __('Add New Auction');
        $resultPage->getConfig()->getTitle()->prepend($title);
        return $resultPage;
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Auction::add_auction');
    }
}
