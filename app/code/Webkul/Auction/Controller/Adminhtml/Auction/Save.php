<?php

/**
 * Webkul Auction Save Controller
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Controller\Adminhtml\Auction;

use Magento\Backend\App\Action;
use Webkul\Auction\Model\ProductFactory;

class Save extends Action
{
    /**
     * @var  \Magento\Backend\Model\UrlInterface
     */
    protected $backendUrl;
    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $coreSession;

    /**
     * @var ProductFactory
     */
    private $_productFactory;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $_timezone;
    private $_logger;
    /**
     * @param \Magento\Framework\Session\SessionManagerInterface $coreSession
     * @param Action\Context       $context
     * @param ProductFactory       $productFactory
     * @param $_productFactory     $_productFactory
     * @param $_timezone           $_timezone
     * @param $_logger             $_logger
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface  $timezone
     */
    public function __construct(
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        Action\Context $context,
        ProductFactory $productFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Webkul\Auction\Helper\Data $helper
    ) {
        $this->backendUrl = $backendUrl;
        $this->coreSession = $coreSession;
        $this->_logger = $logger;
        parent::__construct($context);
        $this->_productFactory = $productFactory;
        $this->_timezone = $timezone;
        $this->helper = $helper;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $this->coreSession->start();
        if (!$data) {
            $this->_redirect('auction/auction/addauction');
            return;
        }
        try {
            if (!isset($data['min_qty']) && !isset($data['max_qty'])) {
                $data['min_qty'] = 1;
                $data['max_qty'] = 1;
            }
            $auctionProduct = $this->_productFactory->create();
            $timezone = $this->_timezone;
            if (isset($data['start_auction_time'])) {
                $data['start_auction_time'] = $this->converToTz(
                    $data['start_auction_time'],
                    $timezone->getDefaultTimezone(),
                    $timezone->getConfigTimezone()
                );
            }
            if (isset($data['stop_auction_time'])) {
                $data['stop_auction_time'] = $this->converToTz(
                    $data['stop_auction_time'],
                    $timezone->getDefaultTimezone(),
                    $timezone->getConfigTimezone()
                );
            }
            if ((isset($data['stop_auction_time']) && isset($data['start_auction_time'])) &&
                $data['stop_auction_time'] < $data['start_auction_time']
            ) {
                $this->messageManager->addError(__('Start Auction Time is less than Stop Auction Time.'));
                if (isset($data['entity_id']) && !empty($data['entity_id'])) {
                    $this->_redirect(
                        $this->backendUrl->getUrl(
                            'auction/auction/addauction',
                            ['id'=>$data['entity_id'],'auction_id'=>$data['entity_id']]
                        )
                    );
                } else {
                    $this->_redirect(
                        $this->backendUrl->getUrl(
                            'auction/auction/addauction'
                        )
                    );
                }
                return;
            }
            if (isset($data['starting_price'])) {
                $data['min_amount'] = $data['starting_price'];
                $data['min_auto_amount'] = $data['starting_price'];
            }
            if (!isset($data['reserve_price']) || !is_numeric($data['reserve_price'])) {
                $data['reserve_price'] = null;
            }
            $auctionProduct->setData($data);
            if (isset($data['entity_id'])) {
                $auctionProduct->setEntityId($data['entity_id']);
            } else {
                $auctionProduct->setAuctionStatus(1);
                $auctionProduct->setStatus(0);
            }
            if ($data['min_qty'] > $data['max_qty']) {

                $this->messageManager->addError(__('Enter max quantity equal or greater than min quantity.'));
                if (isset($data['entity_id']) && !empty($data['entity_id'])) {
                    $this->_redirect(
                        $this->backendUrl->getUrl(
                            'auction/auction/addauction',
                            ['id'=>$data['entity_id'],'auction_id'=>$data['entity_id']]
                        )
                    );
                } else {
                    $this->_redirect(
                        $this->backendUrl->getUrl(
                            'auction/auction/addauction'
                        )
                    );
                }
                return;
            } else {
                $auctionProduct->save();
                $this->messageManager->addSuccess(__('Auction product has been successfully saved.'));
            }
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->helper->cacheFlush();
        $this->_redirect('auction/auction/index');
    }

    /**
     * convert Datetime from one zone to another
     * @param string $dateTime which we want to convert
     * @param string $toTz timezone in which we want to convert
     * @param string $fromTz timezone from which we want to convert
     */
    protected function converToTz($dateTime = "", $toTz = '', $fromTz = '')
    {
        // timezone by php friendly values
        $date = new \DateTime($dateTime, new \DateTimeZone($fromTz));
        $date->setTimezone(new \DateTimeZone('UTC'));
        $dateTime = $date->format('m/d/Y H:i:s');
        return $dateTime;
    }

    /**
     * Check Category Map permission.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Auction::add_auction');
    }
}
