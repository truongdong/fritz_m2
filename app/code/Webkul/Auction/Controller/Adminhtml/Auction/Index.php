<?php
/**
 * Webkul Auction Controller
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Controller\Adminhtml\Auction;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context        $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Webkul\Auction\Helper\Data $helper
    ) {

        parent::__construct($context);
        $this->_resultPageFactory = $resultPageFactory;
        $this->_helper= $helper;
    }

    /**
     * Auction List page.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Webkul_Auction::product_list');
        $resultPage->getConfig()
                    ->getTitle()
                    ->prepend(__('Auction Product List'));
        $this->_helper->cacheFlush();
        return $resultPage;
    }

    /**
     * Check Auction List Permission.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
                    ->isAllowed('Webkul_Auction::add_auction');
    }
}
