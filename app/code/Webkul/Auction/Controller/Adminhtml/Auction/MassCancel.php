<?php
/**
 * Webkul Auction MassCancel Controller
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Controller\Adminhtml\Auction;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Webkul\Auction\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\Product;
use Webkul\Auction\Model\WallettransactionFactory;

class MassCancel extends \Magento\Backend\App\Action
{
    /**
     * Massactions filter.
     *
     * @var Filter
     */
    private $filter;
    /**
     * @var Webkul\Auction\Model\FeerecordFactory
     */
    private $feeRecord;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $helperData;
    private $product;
    private $date;
    private $feetransaction;
    private $walletRecord;
    /**
     * @var Webkul\Auction\Model\WallettransactionFactory
     */
    private $walletTransaction;
    /**
     * @param Context           $context
     * @param Filter            $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        WallettransactionFactory $transactionFactory,
        CollectionFactory $collectionFactory,
        \Webkul\Auction\Model\ResourceModel\Feerecord\CollectionFactory $feeRecord,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\Auction\Helper\Data $helperData,
        Product $product,
        \Webkul\Auction\Model\ResourceModel\Feetransaction\Collection $feeTransaction,
        \Webkul\Auction\Model\Walletrecord $walletrecord
    ) {
        $this->feetransaction = $feeTransaction;
        $this->walletRecord = $walletrecord;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->feeRecord = $feeRecord;
        $this->date = $date;
        $this->helperData = $helperData;
        $this->product = $product;
        $this->walletTransaction = $transactionFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
     
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $recordCancel = 0;
            $cancelWalletAmt = $this->helperData->cancelWalletAmtRefund();
            
            foreach ($collection->getItems() as $auctionProduct) {
                if ($auctionProduct->getAuctionStatus() != 3
                 && $auctionProduct->getAuctionStatus() != 4
                 && $cancelWalletAmt) {
                    $this->helperData->walletAmtReturn($auctionProduct->getProductId(), $auctionProduct->getId());
                }
              
                if ($auctionProduct->getAuctionStatus() == 0 || $auctionProduct->getAuctionStatus() == 1) {
                    $auctionProduct->setId($auctionProduct->getEntityId());
                    $auctionProduct->setAuctionStatus(3);
                    $this->saveObj($auctionProduct);
                    $recordCancel++;
                }
            }
            $this->messageManager->addSuccess(__('A total of %1 record(s) have been cancel.', $recordCancel));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('*/*/index');
    }

    /**
     * Check Auction Product delete Permission.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Auction::add_auction');
    }

    /**
     * saveObj
     * @param Object
     * @return void
     */
    private function saveObj($object)
    {
        $object->save();
    }
}
