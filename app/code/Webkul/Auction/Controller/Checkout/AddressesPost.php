<?php
/**
 * Webkul Auction Amount Model.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Controller\Checkout;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Multishipping\Model\Checkout\Type\Multishipping\State;
use Webkul\Auction\Model\ProductFactory as AuctionProductFactory;
use Webkul\Auction\Model\WinnerDataFactory;

class AddressesPost extends \Magento\Multishipping\Controller\Checkout\AddressesPost
{
    /**
     * @var \Webkul\Auction\Model\WinnerDataFactory
     */

    private $winnerData;
    /**
     * @var \Webkul\Auction\Model\ProductFactory
     */

    private $auctionProductFactory;

    /**
     * @var \Webkul\Auction\Helper\Data
     */
    private $helperData;

    /**
     * @var TimezoneInterface
     */
    private $_timezoneInterface;

    /**
     * construct
     *
     * @param WinnerDataFactory $winnerData
     * @param AuctionProductFactory $auctionProductFactory
     * @param TimezoneInterface $timezoneInterface
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param AccountManagementInterface $accountManagement
     * @param \Webkul\Auction\Helper\Data $helperData
     */
    public function __construct(
        WinnerDataFactory $winnerData,
        AuctionProductFactory $auctionProductFactory,
        TimezoneInterface $timezoneInterface,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface $accountManagement,
        \Webkul\Auction\Helper\Data $helperData
    ) {
        $this->winnerData = $winnerData;
        $this->auctionProductFactory = $auctionProductFactory;
        $this->_timezoneInterface = $timezoneInterface;
        $this->helperData = $helperData;
        parent::__construct(
            $context,
            $customerSession,
            $customerRepository,
            $accountManagement
        );
    }

    /**
     * Multishipping checkout process posted addresses
     *
     * @return void
     */
    public function execute()
    {
      
        $auctionpurchaseAllowedQty = $this->auctionpurchaseAllowedQty();
       
        if (!$this->_getCheckout()->getCustomerDefaultShippingAddress()) {
            $this->_redirect('*/checkout_address/newShipping');
            return;
        }

        $requestShipData = $this->request()->getPost('ship');

        if (isset($requestShipData[0]) && !empty($auctionpurchaseAllowedQty)) {
            $reqeustQty = $this->getRequestQty();

            if ($reqeustQty > $auctionpurchaseAllowedQty['max_qty']) {
                $this->messageManager->
                addWarning(__(
                    'Maximum %1 quantities are allowed to purchase this item.',
                    $auctionpurchaseAllowedQty['max_qty']
                ));
                $this->_redirect('*/*/addresses');
                return $this;
            }

            if ($reqeustQty < $auctionpurchaseAllowedQty['min_qty']) {
                $this->messageManager
                ->addWarning(__(
                    'Minimum %1 quantities are allowed to purchase this item.',
                    $auctionpurchaseAllowedQty['min_qty']
                ));
                $this->_redirect('*/*/addresses');
                return $this;
            }
        }
        try {
            if ($this->request()->getParam('continue', false)) {
                $this->_getCheckout()->setCollectRatesFlag(true);
                $this->_getState()->setActiveStep(State::STEP_SHIPPING);
                $this->_getState()->setCompleteStep(State::STEP_SELECT_ADDRESSES);
                $this->_redirect('*/*/shipping');
            } elseif ($this->request()->getParam('new_address')) {
                $this->_redirect('*/checkout_address/newShipping');
            } else {
                $this->_redirect('*/*/addresses');
            }
            if ($shipToInfo = $this->request()->getPost('ship')) {
                $this->_getCheckout()->setShippingItemsInformation($shipToInfo);
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
            $this->_redirect('*/*/addresses');
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('Data saving problem'));
            $this->_redirect('*/*/addresses');
        }
    }
    /**
     * get Reqeust Qty
     *
     * @return requestQty
     */
    public function getRequestQty()
    {
        $requestShipData = $this->request()->getPost('ship');
        $reqeustQty = 0;
        foreach ($requestShipData as $val) {
            foreach ($val as $v) {
                $reqeustQty += $v['qty'];
            }
        }
        return $reqeustQty;
    }

    public function request()
    {
        return $this->getRequest();
    }
    public function auctionpurchaseAllowedQty()
    {
        $data = [];
        $cart = $this->_getCheckoutSession()->getQuote()->getAllItems();
        foreach ($cart as $item) {
            $product = $item->getProduct();
            $productId = $product->getId();

            $bidProCol = $this->winnerData->create()->getCollection()->addFieldToFilter('product_id', $productId)
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('complete', 0)
                ->setOrder('entity_id', 'desc')
                ->setPageSize(1);

            if (count($bidProCol)) {
                foreach ($bidProCol as $winner) {
                    $day = strtotime($winner->getStopAuctionTime() . ' + ' . $winner->getDays() . ' days');
                    $difference = $day - strtotime($this->_timezoneInterface->date()->format('m/d/y H:i:s'));
                    if ($difference > 0) {
                        $auctionPro = $this->auctionProductFactory->create()->getCollection()
                            ->addFieldToFilter('entity_id', $winner->getAuctionId())
                            ->addFieldToFilter('auction_status', 0)
                            ->getLastItem();
                        if ($auctionPro->getEntityId()) {
                            $customerId = $this->helperData->getCustomerId();
                            $auctionId = $this->helperData->getActiveAuctionId($productId);

                            $data['max_qty'] = $winner->getMaxQty();
                            $data['min_qty'] = $winner->getMinQty();

                        }
                    }
                }

            }
        }
        return $data;
    }
}
