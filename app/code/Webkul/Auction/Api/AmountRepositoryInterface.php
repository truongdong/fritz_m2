<?php
namespace Webkul\Auction\Api;
 
use Magento\Framework\Api\SearchCriteriaInterface;
use Webkul\Auction\Api\Data\AmountInterface;
 
interface AmountRepositoryInterface
{
 /**
  * @param int $id
  * @return \Webkul\Auction\Api\Data\AmountInterface
  * @throws \Magento\Framework\Exception\NoSuchEntityException
  */
    public function getById($id);
 
 /**
  * @param \Webkul\Auction\Api\Data\AmountInterface $amountInterface
  * @return \Webkul\Auction\Api\Data\AmountInterface
  */
    public function save(AmountInterface $amountInterface);
 
 /**
  * @param \Webkul\Auction\Api\Data\AmountInterface $amountInterface
  * @return void
  */
    public function delete(AmountInterface $amountInterface);

 /**
  * @param int $id
  * @return \Webkul\Auction\Api\Data\AmountInterface
  * @throws \Magento\Framework\Exception\NoSuchEntityException
  */
    public function deleteById($id);
}
