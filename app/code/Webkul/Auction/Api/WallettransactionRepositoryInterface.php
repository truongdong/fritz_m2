<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface WallettransactionRepositoryInterface
{
    public function save(\Webkul\Auction\Api\Data\WallettransactionInterface $items);

    public function getById($id);

    public function delete(\Webkul\Auction\Api\Data\WallettransactionInterface $item);

    public function deleteById($id);
}
