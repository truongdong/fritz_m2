<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface WalletrecordRepositoryInterface
{
    public function save(\Webkul\Auction\Api\Data\WalletrecordInterface $items);

    public function getById($id);

    public function delete(\Webkul\Auction\Api\Data\WalletrecordInterface $item);

    public function deleteById($id);

     /**
      * Interface to delete auction.
      *
      * @api
      *
      * @param int $customerId     this auction will be delete
      *
      * @return bool Will returned True if deleted
      */

    public function getTotalWalletAmount($customerid);
}
