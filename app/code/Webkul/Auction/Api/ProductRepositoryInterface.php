<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ProductRepositoryInterface
{
    public function save(\Webkul\Auction\Api\Data\ProductInterface $items);

    public function getById($id);

    public function delete(\Webkul\Auction\Api\Data\ProductInterface $item);

    public function deleteById($id);
      /**
       * getAuctionData by id
       *
       * @param string $id
       * @return array
       * @throws CouldNotDeleteException
       * @throws NoSuchEntityException
       */
    public function getAuctionData($id);
}
