<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Api;

interface AuctionAddRepositoryInterface
{
   
     /**
      * Save auction.
      *
      * @param \Webkul\Auction\Api\Data\AuctionInterface $auction
      * @throws TemporaryCouldNotSaveException
      * @throws InputException
      * @throws CouldNotSaveException
      * @throws LocalizedException
      * @return Webkul\Auction\Api\Data\ResponseInterface
      */
    public function save($auction);

    /**
     * Interface to delete auction.
     *
     * @api
     *
     * @param int $id     this auction will be delete
     *
     * @return bool Will returned True if deleted
     */
    public function delete($id);
}
