<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Api\Data;

interface WalletrecordInterface
{
    const ENTITY_ID                 = 'entity_id';
    const CUSTOMER_ID               = 'customer_id';
    const TOTAL_AMOUNT              = 'total_amount';
    const REMAINING_AMOUNT          = 'remaining_amount';
    const USED_AMOUNT               = 'used_amount';
    const EXPIRED_DATE              = 'expired_date';
    const TRANSACTION_AT            = 'transaction_at';
    const UPDATED_AT                = 'updated_at';
    /**
     * Get Entity ID
     *
     * @return int|null
     */
    public function getEntityId();
    /**
     * Get Customer ID
     *
     * @return int|null
     */
    public function getCustomerId();

    /**
     * Get Total Amount
     *
     * @return float|null
     */
    public function getTotalAmount();

    /**
     * Get Remaining Amount
     *
     * @return float|null
     */
    public function getRemainingAmount();

    /**
     * Get Used Amount
     *
     * @return float|null
     */
    public function getUsedAmount();

    /**
     * Get Expired Date
     *
     * @return string|null
     */
    public function getExpiredDate();

    /**
     * Get Transaction At
     *
     * @return string|null
     */
    public function getTransactionAt();

    /**
     * Get Updated At
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set Entity ID
     *
     * @param int $id
     * @return \Webkul\Auction\Api\Data\WalletrecordInterface
     */
    public function setEntityId($id);

    /**
     * Set Customer ID
     *
     * @param int $customerId
     * @return \Webkul\Auction\Api\Data\WalletrecordInterface
     */
    public function setCustomerId($customerId);

    /**
     * Set Total Amount
     *
     * @param float $totalAmount
     * @return \Webkul\Auction\Api\Data\WalletrecordInterface
     */
    public function setTotalAmount($totalAmount);

    /**
     * Set Remaining Amount
     *
     * @param float $remainingAmount
     * @return \Webkul\Auction\Api\Data\WalletrecordInterface
     */
    public function setRemainingAmount($remainingAmount);

    /**
     * Set Used Amount
     *
     * @param float $usedAmount
     * @return \Webkul\Auction\Api\Data\WalletrecordInterface
     */
    public function setUsedAmount($usedAmount);

    /**
     * Set Expired Date
     *
     * @param string $expiredDate
     * @return \Webkul\Auction\Api\Data\WalletrecordInterface
     */
    public function setExpiredDate($expiredDate);

    /**
     * Set Transaction At
     *
     * @param string $transactionAt
     * @return \Webkul\Auction\Api\Data\WalletrecordInterface
     */
    public function setTransactionAt($transactionAt);

    /**
     * Set Updated At
     *
     * @param string $updatedAt
     * @return \Webkul\Auction\Api\Data\WalletrecordInterface
     */
    public function setUpdatedAt($updatedAt);
}
