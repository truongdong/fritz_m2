<?php
/**
 * Webkul_Auction Product Product Attribute Adminhtml Block.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Api\Data;

interface WinnerDataInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const ENTITY_ID          = 'entity_id';
    const AUCTION_ID         = 'auction_id';
    const PRODUCT_ID         = 'product_id';
    const CUSTOMER_ID        = 'customer_id';
    const WIN_AMOUNT         = 'win_amount';
    const DAYS               = 'days';
    const MAX_QTY            = 'max_qty';
    const MIN_QTY            = 'min_qty';
    const START_AUCTION_TIME = 'start_auction_time';
    const STOP_AUCTION_TIME  = 'stop_auction_time';
    const COMPLETE           = 'complete';
    const STATUS             = 'status';
    const CREATED_AT         = 'created_at';

    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getEntityId();

    /**
     * Set EntityId.
     */
    public function setEntityId($entityId);

    /**
     * Get AuctionId.
     *
     * @return int
     */
    public function getAuctionId();

    /**
     * Set AuctionId.
     */
    public function setAuctionId($auctionId);

    /**
     * Get ProductId.
     *
     * @return varchar
     */
    public function getProductId();

    /**
     * Set ProductId.
     */
    public function setProductId($productId);

    /**
     * Get getCustomerId.
     *
     * @return varchar
     */
    public function getCustomerId();

    /**
     * Set CustomerId.
     */
    public function setCustomerId($name);

    /**
     * Get WinAmount.
     *
     * @return varchar
     */
    public function getWinAmount();

    /**
     * Set WinAmount.
     */
    public function setWinAmount($winAmount);

    /**
     * Get Days.
     *
     * @return varchar
     */
    public function getDays();

    /**
     * Set Days.
     */
    public function setDays($days);

    /**
     * Get MaxQty.
     *
     * @return varchar
     */
    public function getMaxQty();

    /**
     * Set MaxQty.
     */
    public function setMaxQty($maxQty);

    /**
     * Get MinQty.
     *
     * @return varchar
     */
    public function getMinQty();

    /**
     * Set MinQty.
     */
    public function setMinQty($minQty);

    /**
     * Get StartingPrice.
     *
     * @return varchar
     */
    public function getStartAuctionTime();

    /**
     * Set StartingPrice.
     */
    public function setStartAuctionTime($startAuctionTime);

    /**
     * Get StopAuctionTime.
     *
     * @return varchar
     */
    public function getStopAuctionTime();

    /**
     * Set StopAuctionTime.
     */
    public function setStopAuctionTime($stopAuctionTime);
    /**
     * Get Complete.
     *
     * @return varchar
     */
    public function getComplete();

    /**
     * Set Complete.
     */
    public function setComplete($complete);

    /**
     * Get Status.
     *
     * @return varchar
     */
    public function getStatus();

    /**
     * Set Status.
     */
    public function setStatus($status);

    /**
     * Get CreatedAt.
     *
     * @return varchar
     */
    public function getCreatedAt();

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt);
}
