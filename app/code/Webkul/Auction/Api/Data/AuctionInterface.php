<?php
/**
 * Webkul_Auction Product Product Attribute Adminhtml Block.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Api\Data;

interface AuctionInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
   // const ID = 'entity_id';
    const PRODUCT_ID = 'product_id';
    const STARTING_PRICE = 'starting_price';
    const RESERVE_PRICE = 'reserve_price';
    const START_AUCTION_TIME = 'start_auction_time';
    const STOP_AUCTION_TIME = 'stop_auction_time';
    const DAYS = 'days';
    const MIN_QTY = 'min_qty';
    const MAX_QTY = 'max_qty';
    const INCREMENT_OPT = 'increment_opt';
    const AUTO_AUCTION_OPT = 'auto_auction_opt';

    /**
     * Get Entity ID.
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set Entity ID
     *
     * @param int $entityId
     * @return \Webkul\Auction\Api\Data\AuctionInterface
     */
    public function setId($entityId);

   /**
    * Get ProductId.
    *
    * @return int|null
    */
    public function getProductId();

    /**
     * Set ProductId
     *
     * @param int $productId
     * @return \Webkul\Auction\Api\Data\AuctionInterface
     */
    public function setProductId($productId);

   /**
    * Get StartingPrice.
    *
    * @return float|null
    */
    public function getStartingPrice();

    /**
     * Set StartingPrice
     *
     * @param float $startingPrice
     * @return \Webkul\Auction\Api\Data\AuctionInterface
     */
    public function setStartingPrice($startingPrice);

   /**
    * Get ReservePrice.
    *
    * @return float|null
    */
    public function getReservePrice();

    /**
     * Set ReservePrice
     *
     * @param float $reservePrice
     * @return \Webkul\Auction\Api\Data\AuctionInterface
     */
    public function setReservePrice($reservePrice);
    /**
     * Get StartAuctionTime.
     *
     * @return string|null
     */
    public function getStartAuctionTime();

    /**
     * Set StartAuctionTime
     *
     * @param string $startAuctionTime
     * @return \Webkul\Auction\Api\Data\AuctionInterface
     */
    public function setStartAuctionTime($startAuctionTime);

    /**
     * Get StopAuctionTime.
     *
     * @return string|null
     */
    public function getStopAuctionTime();

    /**
     * Set StopAuctionTime
     *
     * @param string $stopAuctionTime
     * @return \Webkul\Auction\Api\Data\AuctionInterface
     */
    public function setStopAuctionTime($stopAuctionTime);
    /**
     * Get Days.
     *
     * @return int|null
     */
    public function getDays();

    /**
     * Set Days
     *
     * @param int $days
     * @return \Webkul\Auction\Api\Data\AuctionInterface
     */
    public function setDays($days);

    /**
     * Get MinQty.
     *
     * @return int|null
     */
    public function getMinQty();

    /**
     * Set MinQty
     *
     * @param int $minQty
     * @return \Webkul\Auction\Api\Data\AuctionInterface
     */
    public function setMinQty($minQty);
    /**
     * Get MaxQty.
     *
     * @return int|null
     */
    public function getMaxQty();

    /**
     * Set MaxQty
     *
     * @param int $maxQty
     * @return \Webkul\Auction\Api\Data\AuctionInterface
     */
    public function setMaxQty($maxQty);
    
    /**
     * Get IncrementOpt.
     *
     * @return int|null
     */
    public function getIncrementOpt();

    /**
     * Set IncrementOpt
     *
     * @param int $incrementOpt
     * @return \Webkul\Auction\Api\Data\AuctionInterface
     */
    public function setIncrementOpt($incrementOpt);
    /**
     * Get AutoAuctionOpt.
     *
     * @return int|null
     */
    public function getAutoAuctionOpt();

    /**
     * Set AutoAuctionOpt
     *
     * @param int $autoAuctionOpt
     * @return \Webkul\Auction\Api\Data\AuctionInterface
     */
    public function setAutoAuctionOpt($autoAuctionOpt);
}
