<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Api\Data;

interface AmountRequestTransferInterface
{
    const ENTITY_ID       = 'entity_id';
    const CUSTOMER_ID     = 'customer_id';
    const BANK_DETAILS    = 'bank_details';
    const NOTE            = 'note';
    const STATUS          = 'status';
    const AMOUNT          = 'amount';
    const TRANSACTION_ID  = 'transaction_id';
    const CREATED_AT      = 'created_at';

    /**
     * Get Entity ID
     *
     * @return int|null
     */
    public function getEntityId();
    
    /**
     * Set Entity ID
     *
     * @param int $id
     * @return \Webkul\Auction\Api\Data\AmountRequestTransferInterface
     */
    public function setEntityId($id);

    /**
     * Get Customer ID
     *
     * @return int|null
     */
    public function getCustomerId();

    /**
     * Set Customer ID
     *
     * @param int $customerId
     * @return \Webkul\Auction\Api\Data\AmountRequestTransferInterface
     */
    public function setCustomerId($customerId);

    /**
     * Get BankDetails
     *
     * @return string|null
     */
    public function getBankDetails();

    /**
     * Set BankDetails
     *
     * @param string $bankDetails
     * @return \Webkul\Auction\Api\Data\AmountRequestTransferInterface
     */
    public function setBankDetails($bankDetails);

    /**
     * Get Note
     *
     * @return string|null
     */
    public function getNote();

    /**
     * Set Note
     *
     * @param string $note
     * @return \Webkul\Auction\Api\Data\AmountRequestTransferInterface
     */
    public function setNote($note);

    /**
     * Get Status
     *
     * @return int|null
     */
    public function getStatus();

    /**
     * Set Status
     *
     * @param int $status
     * @return \Webkul\Auction\Api\Data\AmountRequestTransferInterface
     */
    public function setStatus($status);

    /**
     * Get Amount
     *
     * @return float|null
     */
    public function getAmount();

    /**
     * Set Amount
     *
     * @param float $amount
     * @return \Webkul\Auction\Api\Data\AmountRequestTransferInterface
     */
    public function setAmount($amount);

    /**
     * Get TransactionId
     *
     * @return int|null
     */
    public function getTransactionId();

    /**
     * Set TransactionId
     *
     * @param int $transactionId
     * @return \Webkul\Auction\Api\Data\AmountRequestTransferInterface
     */
    public function setTransactionId($transactionId);

    /**
     * Get CreatedAt
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set CreatedAt
     *
     * @param string $createdAt
     * @return \Webkul\Auction\Api\Data\AmountRequestTransferInterface
     */
    public function setCreatedAt($createdAt);
}
