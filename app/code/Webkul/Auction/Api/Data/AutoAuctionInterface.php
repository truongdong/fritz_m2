<?php
/**
 * Webkul_Auction Product Product Attribute Adminhtml Block.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Api\Data;

interface AutoAuctionInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const ENTITY_ID = 'entity_id';
    const AUCTION_ID = 'auction_id';
    const PRODUCT_ID = 'product_id';
    const CUSTOMER_ID = 'customer_id';
    const AMOUNT = 'amount';
    const WINNING_PRICE = 'winning_price';
    const STATUS = 'status';
    const SHOP = 'shop';
    const FLAG = 'flag';
    const CREATED_AT = 'created_at';

    /**
     * Get EntityId.
     *
     * @return int|null
     */
    public function getEntityId();

    /**
     * Set EntityId
     *
     * @param int $entityId
     * @return \Webkul\Auction\Api\Data\AutoAuctionInterface
     */
    public function setEntityId($entityId);

    /**
     * Get AuctionId.
     *
     * @return int|null
     */
    public function getAuctionId();

    /**
     * Set AuctionId
     *
     * @param int $auctionId
     * @return \Webkul\Auction\Api\Data\AutoAuctionInterface
     */
    public function setAuctionId($auctionId);

    /**
     * Get CustomerId.
     *
     * @return int|null
     */
    public function getCustomerId();

    /**
     * Set CustomerId
     *
     * @param int $customerId
     * @return \Webkul\Auction\Api\Data\AutoAuctionInterface
     */
    public function setCustomerId($customerId);

    /**
     * Get ProductId.
     *
     * @return int|null
     */
    public function getProductId();

    /**
     * Set ProductId
     *
     * @param int $productId
     * @return \Webkul\Auction\Api\Data\AutoAuctionInterface
     */
    public function setProductId($productId);

    /**
     * Get Amount.
     *
     * @return float|null
     */
    public function getAmount();

    /**
     * Set Amount
     *
     * @param float $amount
     * @return \Webkul\Auction\Api\Data\AutoAuctionInterface
     */
    public function setAmount($amount);

    /**
     * Get WinningPrice.
     *
     * @return float|null
     */
    public function getWinningPrice();

    /**
     * Set WinningPrice
     *
     * @param float $winningPrice
     * @return \Webkul\Auction\Api\Data\AutoAuctionInterface
     */
    public function setWinningPrice($winningPrice);

    /**
     * Get Status.
     *
     * @return string|null
     */
    public function getStatus();

    /**
     * Set Status
     *
     * @param string $status
     * @return \Webkul\Auction\Api\Data\AutoAuctionInterface
     */
    public function setStatus($status);

    /**
     * Get Shop.
     *
     * @return int|null
     */
    public function getShop();

    /**
     * Set Shop
     *
     * @param int $shop
     * @return \Webkul\Auction\Api\Data\AutoAuctionInterface
     */
    public function setShop($shop);

    /**
     * Get Flag.
     *
     * @return int|null
     */
    public function getFlag();

    /**
     * Set Flag
     *
     * @param int $flag
     * @return \Webkul\Auction\Api\Data\AutoAuctionInterface
     */
    public function setFlag($flag);

    /**
     * Get CreatedAt.
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set CreatedAt
     *
     * @param string $createdAt
     * @return \Webkul\Auction\Api\Data\AutoAuctionInterface
     */
    public function setCreatedAt($createdAt);
}
