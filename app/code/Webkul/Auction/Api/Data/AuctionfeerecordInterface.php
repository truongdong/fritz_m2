<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Api\Data;

interface AuctionfeerecordInterface
{
    const ENTITY_ID          = 'entity_id';
    const CUSTOMER_ID        = 'customer_id';
    const PRODUCT_ID         = 'product_id';
    const START_AUCTION_TIME = 'start_auction_time';
    const STOP_AUCTION_TIME  = 'stop_auction_time';
    const CREATED_AT         = 'created_at';
    const UPDATED_AT         = 'updated_at';

    /**
     * Get Entity ID
     *
     * @return int|null
     */
    public function getEntityId();

    /**
     * Set Entity ID
     *
     * @param int $id
     * @return \Webkul\Auction\Api\Data\AuctionfeerecordInterface
     */
    public function setEntityId($id);

    /**
     * Get Customer ID
     *
     * @return int|null
     */
    public function getCustomerId();

    /**
     * Set Customer ID
     *
     * @param int $customerId
     * @return \Webkul\Auction\Api\Data\AuctionfeerecordInterface
     */
    public function setCustomerId($customerId);

    /**
     * Get Product ID
     *
     * @return int|null
     */
    public function getProductId();

    /**
     * Set Product ID
     *
     * @param int $productId
     * @return \Webkul\Auction\Api\Data\AuctionfeerecordInterface
     */
    public function setProductId($productId);

    /**
     * Get Start Auction Time
     *
     * @return string|null
     */
    public function getStartAuctionTime();

    /**
     * Set Start Auction Time
     *
     * @param string $startAuctionTime
     * @return \Webkul\Auction\Api\Data\AuctionfeerecordInterface
     */
    public function setStartAuctionTime($startAuctionTime);

    /**
     * Get Stop Auction Time
     *
     * @return string|null
     */
    public function getStopAuctionTime();

    /**
     * Set Stop Auction Time
     *
     * @param string $stopAuctionTime
     * @return \Webkul\Auction\Api\Data\AuctionfeerecordInterface
     */
    public function setStopAuctionTime($stopAuctionTime);

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set Created At
     *
     * @param string $createdAt
     * @return \Webkul\Auction\Api\Data\AuctionfeerecordInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get Updated At
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set Updated At
     *
     * @param string $updatedAt
     * @return \Webkul\Auction\Api\Data\AuctionfeerecordInterface
     */
    public function setUpdatedAt($updatedAt);
}
