<?php
/**
 * Webkul_Auction Product Product Attribute Adminhtml Block.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Api\Data;

interface AmountInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const ID = 'entity_id';
    const AUCTION_ID = 'auction_id';
    const PRODUCT_ID = 'product_id';
    const CUSTOMER_ID = 'customer_id';
    const AUCTION_AMOUNT = 'auction_amount';
    const WINNING_STATUS = 'winning_status';
    const SHOP = 'shop';
    const STATUS = 'status';
    const CREATED_AT = 'created_at';

    /**
     * Get Entity ID.
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set Entity ID
     *
     * @param int $entityId
     * @return \Webkul\Auction\Api\Data\AmountInterface
     */
    public function setId($entityId);

   /**
    * Get AuctionId.
    *
    * @return int|null
    */
    public function getAuctionId();

    /**
     * Set AuctionId
     *
     * @param int $auctionId
     * @return \Webkul\Auction\Api\Data\AmountInterface
     */
    public function setAuctionId($auctionId);

   /**
    * Get ProductId.
    *
    * @return int|null
    */
    public function getProductId();

    /**
     * Set ProductId
     *
     * @param int $productId
     * @return \Webkul\Auction\Api\Data\AmountInterface
     */
    public function setProductId($productId);

   /**
    * Get CustomerId.
    *
    * @return int|null
    */
    public function getCustomerId();

    /**
     * Set CustomerId
     *
     * @param int $customerId
     * @return \Webkul\Auction\Api\Data\AmountInterface
     */
    public function setCustomerId($customerId);

    /**
     * Get AuctionAmount.
     *
     * @return float|null
     */
    public function getAuctionAmount();

    /**
     * Set AuctionAmount
     *
     * @param int $auctionAmount
     * @return \Webkul\Auction\Api\Data\AmountInterface
     */
    public function setAuctionAmount($auctionAmount);

    /**
     * Get WinningStatus.
     *
     * @return int|null
     */
    public function getWinningStatus();

    /**
     * Set WinningStatus
     *
     * @param int $winningStatus
     * @return \Webkul\Auction\Api\Data\AmountInterface
     */
    public function setWinningStatus($winningStatus);

    /**
     * Get Shop.
     *
     * @return int|null
     */
    public function getShop();

    /**
     * Set Shop
     *
     * @param int $shop
     * @return \Webkul\Auction\Api\Data\AmountInterface
     */
    public function setShop($shop);

    /**
     * Get Status.
     *
     * @return int|null
     */
    public function getStatus();

    /**
     * Set Status
     *
     * @param int $status
     * @return \Webkul\Auction\Api\Data\AmountInterface
     */
    public function setStatus($status);

   /**
    * Get CreatedAt.
    *
    * @return string|null
    */
    public function getCreatedAt();

    /**
     * Set CreatedAt
     *
     * @param string $createdAt
     * @return \Webkul\Auction\Api\Data\AmountInterface
     */
    public function setCreatedAt($createdAt);

     /**
      * get getAuctionAmountData
      *
      * @param string $currentProId
      * @param int $aucEntityId
      * @return \Webkul\Auction\Api\Data\AmountInterface
      */
    public function getAmountDataByAuctionId($currentProId, $aucEntityId);
}
