<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Api\Data;

interface WallettransactionInterface
{
    const ENTITY_ID        = 'entity_id';
    const CUSTOMER_ID      = 'customer_id';
    const AMOUNT           = 'amount';
    const STATUS           = 'status';
    const ACTION           = 'action';
    const ORDER_ID         = 'order_id';
    const TRANSACTION_AT   = 'transaction_at';
    const CURRENCY_CODE    = 'currency_code';
    const CURR_AMOUNT      = 'curr_amount';
    const EXPIRED_STATUS   = 'expired_status';
    const TRANSACTION_NOTE = 'transaction_note';

    /**
     * Get entity ID
     *
     * @return int|null
     */
    public function getEntityId();

    /**
     * Get Customer ID
     *
     * @return int|null
     */
    public function getCustomerId();

    /**
     * Get Amount
     *
     * @return float|null
     */
    public function getAmount();

    /**
     * Get Status
     *
     * @return int|null
     */
    public function getStatus();

    /**
     * Get Action
     *
     * @return string|null
     */
    public function getAction();

    /**
     * Get Order Id
     *
     * @return int|null
     */
    public function getOrderId();

    /**
     * Get Transaction At
     *
     * @return string|null
     */
    public function getTransactionAt();

    /**
     * Get Currency Code
     *
     * @return string|null
     */
    public function getCurrencyCode();

    /**
     * Get Current Amount
     *
     * @return float|null
     */
    public function getCurrAmount();

    /**
     * Get Expired Status
     *
     * @return int|null
     */
    public function getExpiredStatus();

    /**
     * Get Transaction Note
     *
     * @return string|null
     */
    public function getTransactionNote();

    /**
     * Set Entity ID
     *
     * @param int $id
     * @return \Webkul\Auction\Api\Data\WallettransactionInterface
     */
    public function setEntityId($id);

    /**
     * Set Customer ID
     *
     * @param int $customerId
     * @return \Webkul\Auction\Api\Data\WallettransactionInterface
     */
    public function setCustomerId($customerId);

    /**
     * Set Amount
     *
     * @param float $amount
     * @return \Webkul\Auction\Api\Data\WallettransactionInterface
     */
    public function setAmount($amount);

    /**
     * Set Status
     *
     * @param int $status
     * @return \Webkul\Auction\Api\Data\WallettransactionInterface
     */
    public function setStatus($status);

    /**
     * Set Action
     *
     * @param string $action
     * @return \Webkul\Auction\Api\Data\WallettransactionInterface
     */
    public function setAction($action);

    /**
     * Set Order Id
     *
     * @param int $orderId
     * @return \Webkul\Auction\Api\Data\WallettransactionInterface
     */
    public function setOrderId($orderId);

    /**
     * Set Transaction At
     *
     * @param string $transactionAt
     * @return \Webkul\Auction\Api\Data\WallettransactionInterface
     */
    public function setTransactionAt($transactionAt);

    /**
     * Set Currency Code
     *
     * @param string $currencyCode
     * @return \Webkul\Auction\Api\Data\WallettransactionInterface
     */
    public function setCurrencyCode($currencyCode);

    /**
     * Set Current Amount
     *
     * @param float $currAmount
     * @return \Webkul\Auction\Api\Data\WallettransactionInterface
     */
    public function setCurrAmount($currAmount);

    /**
     * Set Expired Status
     *
     * @param int $expiredStatus
     * @return \Webkul\Auction\Api\Data\WallettransactionInterface
     */
    public function setExpiredStatus($expiredStatus);

    /**
     * Set Transaction Note
     *
     * @param string $transactionNote
     * @return \Webkul\Auction\Api\Data\WallettransactionInterface
     */
    public function setTransactionNote($transactionNote);
}
