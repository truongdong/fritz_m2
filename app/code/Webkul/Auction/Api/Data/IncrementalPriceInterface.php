<?php
/**
 * Webkul_Auction Product Product Attribute Adminhtml Block.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Api\Data;

interface IncrementalPriceInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const ENTITY_ID = 'entity_id';
    const USER_ID   = 'user_id';
    const INCVAL    = 'incval';

    /**
     * Get EntityId.
     *
     * @return int|null
     */
    public function getEntityId();

    /**
     * Set EntityId
     *
     * @param int $entityId
     * @return \Webkul\Auction\Api\Data\IncrementalPriceInterface
     */
    public function setEntityId($entityId);

    /**
     * Get UserId.
     *
     * @return string|null
     */
    public function getUserId();

    /**
     * Set UserId
     *
     * @param string $userId
     * @return \Webkul\Auction\Api\Data\IncrementalPriceInterface
     */
    public function setUserId($userId);

    /**
     * Get Incval.
     *
     * @return string|null
     */
    public function getIncval();

    /**
     * Set Incval
     *
     * @param string $incval
     * @return \Webkul\Auction\Api\Data\IncrementalPriceInterface
     */
    public function setIncval($incval);
}
