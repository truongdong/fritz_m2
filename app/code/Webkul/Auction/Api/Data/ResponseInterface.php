<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 *
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Api\Data;

/**
 * Auction Device Token interface.
 *
 * @api
 */
interface ResponseInterface
{
    /**
     * Get response.
     *
     * @return Webkul\Auction\Api\Data\ResponseInterface
     */
    public function getResponse();
}
