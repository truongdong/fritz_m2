<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Api\Data;

interface AuctionfeetransactionInterface
{
    const ENTITY_ID       = 'entity_id';
    const BIDFEE_ID       = 'bidfee_id';
    const CUSTOMER_ID     = 'customer_id';
    const DEDUCTED_AMOUNT = 'deducted_amount';
    const BID_AMOUNT      = 'bid_amount';
    const PERCENT         = 'percent';
    const REFUND_STATUS   = 'refund_status';
    const STATUS          = 'status';
    const CREATED_AT      = 'created_at';

    /**
     * Get Entity ID
     *
     * @return int|null
     */
    public function getEntityId();

    /**
     * Set Entity ID
     *
     * @param int $id
     * @return \Webkul\Auction\Api\Data\AuctionfeetransactionInterface
     */
    public function setEntityId($id);

    /**
     * Get Bidfee ID
     *
     * @return int|null
     */
    public function getBidfeeId();

    /**
     * Set Bidfee ID
     *
     * @param int $bidfeeId
     * @return \Webkul\Auction\Api\Data\AuctionfeetransactionInterface
     */
    public function setBidfeeId($bidfeeId);

    /**
     * Get Customer ID
     *
     * @return int|null
     */
    public function getCustomerId();

    /**
     * Set Customer ID
     *
     * @param int $customerId
     * @return \Webkul\Auction\Api\Data\AuctionfeetransactionInterface
     */
    public function setCustomerId($customerId);

    /**
     * Get Deducted Amount
     *
     * @return float|null
     */
    public function getDeductedAmount();

    /**
     * Set Deducted Amount
     *
     * @param float $deductedAmount
     * @return \Webkul\Auction\Api\Data\AuctionfeetransactionInterface
     */
    public function setDeductedAmount($deductedAmount);

    /**
     * Get Bid Amount
     *
     * @return float|null
     */
    public function getBidAmount();

    /**
     * Set Bid Amount
     *
     * @param float $deductedAmount
     * @return \Webkul\Auction\Api\Data\AuctionfeetransactionInterface
     */
    public function setBidAmount($bidAmount);

    /**
     * Get Percent
     *
     * @return float|null
     */
    public function getPercent();

    /**
     * Set Percent
     *
     * @param float $percent
     * @return \Webkul\Auction\Api\Data\AuctionfeetransactionInterface
     */
    public function setPercent($percent);

    /**
     * Get Refund Status
     *
     * @return int|null
     */
    public function getRefundStatus();

    /**
     * Set Refund Status
     *
     * @param int $refundStatus
     * @return \Webkul\Auction\Api\Data\AuctionfeetransactionInterface
     */
    public function setRefundStatus($refundStatus);

    /**
     * Get Status
     *
     * @return int|null
     */
    public function getStatus();

    /**
     * Set Status
     *
     * @param int $status
     * @return \Webkul\Auction\Api\Data\AuctionfeetransactionInterface
     */
    public function setStatus($status);

    /**
     * Get CreatedAt
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set CreatedAt
     *
     * @param string $createdAt
     * @return \Webkul\Auction\Api\Data\AuctionfeetransactionInterface
     */
    public function setCreatedAt($createdAt);
}
