<?php
/**
 * Webkul_Auction Product Product Attribute Adminhtml Block.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Api\Data;

interface ProductInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const ID                  = 'entity_id';
    const PRODUCT_ID          = 'product_id';
    const CUSTOMER_ID         = 'customer_id';
    const MIN_AMOUNT          = 'min_amount';
    const STARTING_PRICE      = 'starting_price';
    const RESERVE_PRICE       = 'reserve_price';
    const AUCTION_STATUS      = 'auction_status';
    const DAYS                = 'days';
    const MAX_QTY             = 'max_qty';
    const MIN_QTY             = 'min_qty';
    const START_AUCTION_TIME  = 'start_auction_time';
    const STOP_AUCTION_TIME   = 'stop_auction_time';
    const INCREMENT_OPT       = 'increment_opt';
    const INCREMENT_PRICE     = 'increment_price';
    const AUTO_AUCTION_OPT    = 'auto_auction_opt';
    const STATUS              = 'status';
    const CREATED_AT          = 'created_at';
    const MIN_AUTO_AMOUNT     = 'min_auto_amount';

    /**
     * Get EntityId.
     *
     * @return int|null
     */
    public function getId();

     /**
      * Set EntityId
      *
      * @param int $id
      * @return \Webkul\Auction\Api\Data\ProductInterface
      */
    public function setId($id);

    /**
     * Get ProductId.
     *
     * @return int|null
     */
    public function getProductId();

    /**
     * Set ProductId
     *
     * @param int $productId
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setProductId($productId);

    /**
     * Get getCustomerId.
     *
     * @return int|null
     */
    public function getCustomerId();

    /**
     * Set CustomerId
     *
     * @param int $customerId
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setCustomerId($customerId);

    /**
     * Get MinAmount.
     *
     * @return float|null
     */
    public function getMinAmount();

    /**
     * Set MinAmount
     *
     * @param float $minAmount
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setMinAmount($minAmount);

    /**
     * Get StartingPrice.
     *
     * @return float|null
     */
    public function getStartingPrice();

    /**
     * Set StartingPrice
     *
     * @param float $startingPrice
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setStartingPrice($startingPrice);

    /**
     * Get ReservePrice.
     *
     * @return float|null
     */
    public function getReservePrice();

    /**
     * Set ReservePrice
     *
     * @param float $reservePrice
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setReservePrice($reservePrice);

    /**
     * Get AuctionStatus.
     *
     * @return int|null
     */
    public function getAuctionStatus();

    /**
     * Set AuctionStatus
     *
     * @param int $auctionStatus
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setAuctionStatus($auctionStatus);

    /**
     * Get Days.
     *
     * @return int|null
     */
    public function getDays();

    /**
     * Set Days
     *
     * @param int $days
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setDays($days);

    /**
     * Get MaxQty.
     *
     * @return int|null
     */
    public function getMaxQty();

    /**
     * Set MaxQty
     *
     * @param int $maxQty
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setMaxQty($maxQty);

    /**
     * Get MinQty.
     *
     * @return int|null
     */
    public function getMinQty();

    /**
     * Set MinQty
     *
     * @param int $minQty
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setMinQty($minQty);

    /**
     * Get startAuctionTime.
     *
     * @return string|null
     */
    public function getStartAuctionTime();

    /**
     * Set startAuctionTime
     *
     * @param string $startAuctionTime
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setStartAuctionTime($startAuctionTime);

    /**
     * Get StopAuctionTime.
     *
     * @return string|null
     */
    public function getStopAuctionTime();

    /**
     * Set StopAuctionTime
     *
     * @param string $stopAuctionTime
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setStopAuctionTime($stopAuctionTime);

    /**
     * Get IncrementOpt.
     *
     * @return int|null
     */
    public function getIncrementOpt();

    /**
     * Set IncrementOpt
     *
     * @param int $incrementOpt
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setIncrementOpt($incrementOpt);

    /**
     * Get IncrementPrice.
     *
     * @return string|null
     */
    public function getIncrementPrice();

    /**
     * Set IncrementPrice
     *
     * @param string $incrementPrice
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setIncrementPrice($incrementPrice);

    /**
     * Get AutoAuctionOpt.
     *
     * @return int|null
     */
    public function getAutoAuctionOpt();

    /**
     * Set AutoAuctionOpt
     *
     * @param int $autoAuctionOpt
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setAutoAuctionOpt($autoAuctionOpt);

    /**
     * Get Status.
     *
     * @return int|null
     */
    public function getStatus();

    /**
     * Set Status
     *
     * @param int $status
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setStatus($status);

    /**
     * Get CreatedAt.
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set CreatedAt
     *
     * @param string $createdAt
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get MinAutoAmount.
     *
     * @return float|null
     */
    public function getMinAutoAmount();

    /**
     * Set MinAutoAmount
     *
     * @param float $minAutoAmount
     * @return \Webkul\Auction\Api\Data\ProductInterface
     */
    public function setMinAutoAmount($minAutoAmount);
}
