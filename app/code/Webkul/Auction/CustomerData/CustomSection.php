<?php
namespace Webkul\Auction\CustomerData;

use \Magento\Customer\CustomerData\SectionSourceInterface;

class CustomSection implements SectionSourceInterface
{
    private $localeDate;
  
    public function __construct(
        \Magento\Framework\View\Element\Context $context
    ) {
        $this->localeDate = $context->getLocaleDate();
    }

    /**
     * {@inheritdoc}
     */

    public function getSectionData()
    {
        $today = $this->localeDate->date()->format('m/d/y H:i:s');
        return [
            'currentTime' =>strtotime($today)
        ];
    }
}
