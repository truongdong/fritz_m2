<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Model;

use Webkul\Auction\Api\Data;
use Webkul\Auction\Api\WallettransactionRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Webkul\Auction\Model\ResourceModel\Wallettransaction as WallettransactionResource;
use Webkul\Auction\Model\ResourceModel\Wallettransaction\CollectionFactory as WallettransactionCollection;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class WallettransactionRepository
 * Get Wallet transaction
 */
class WallettransactionRepository implements WallettransactionRepositoryInterface
{
    /**
     * @var WallettransactionResource
     */
    protected $resource;

    /**
     * @var WallettransactionFactory
     */
    protected $wallettransactionFactory;

    /**
     * @var WallettransactionCollection
     */
    protected $wallettransactionCollectionFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param WallettransactionResource               $resource
     * @param WallettransactionFactory                $wallettransactionFactory
     * @param Data\WallettransactionInterfaceFactory  $WallettransactionInterfaceFactory
     * @param WallettransactionCollection             $wallettransactionCollectionFactory
     * @param DataObjectHelper                        $dataObjectHelper
     * @param DataObjectProcessor                     $dataObjectProcessor
     * @param StoreManagerInterface                   $storeManager
     */
    public function __construct(
        WallettransactionResource $resource,
        WallettransactionFactory $wallettransactionFactory,
        WallettransactionCollection $wallettransactionCollectionFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->wallettransactionFactory = $wallettransactionFactory;
        $this->wallettransactionCollectionFactory = $wallettransactionCollectionFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * Save Wallet Transaction
     *
     * @param \Webkul\Auction\Api\Data\WallettransactionInterface $wallettransaction
     * @return Wallettransaction
     * @throws CouldNotSaveException
     */
    public function save(Data\WallettransactionInterface $wallettransaction)
    {
        $storeId = $this->storeManager->getStore()->getId();
        $wallettransaction->setStoreId($storeId);
        try {
            $this->resource->save($wallettransaction);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $wallettransaction;
    }

    /**
     * Load Wallet Transaction by given Block Identity
     *
     * @param string $id
     * @return Wallettransaction
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id)
    {
        $wallettransaction = $this->wallettransactionFactory->create();
        $this->resource->load($wallettransaction, $id);
        if (!$wallettransaction->getEntityId()) {
            throw new NoSuchEntityException(__('Wallet Transaction with id "%1" does not exist.', $id));
        }
        return $wallettransaction;
    }

    /**
     * Delete Wallet Transaction
     *
     * @param \Webkul\Auction\Api\Data\WallettransactionInterface $wallettransaction
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Data\WallettransactionInterface $wallettransaction)
    {
        try {
            $this->resource->delete($wallettransaction);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * Delete Wallet Transaction by given Block Identity
     *
     * @param string $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }
}
