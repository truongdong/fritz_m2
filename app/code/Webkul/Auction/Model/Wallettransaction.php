<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Model;

use Webkul\Auction\Api\Data\WallettransactionInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Wallettransaction extends AbstractModel implements WallettransactionInterface, IdentityInterface
{
    const CACHE_TAG = 'auction_wallettransaction';

    /**
     * @var string
     */
    protected $_cacheTag = 'auction_wallettransaction';
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'auction_wallettransaction';
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Webkul\Auction\Model\ResourceModel\Wallettransaction::class);
    }
    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getEntityId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setEntityId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get Customer Id
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Get Amount
     *
     * @return float|null
     */
    public function getAmount()
    {
        return $this->getData(self::AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function setAmount($amount)
    {
        return $this->setData(self::AMOUNT, $amount);
    }

    /**
     * Get Status
     *
     * @return int|null
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get Action
     *
     * @return string|null
     */
    public function getAction()
    {
        return $this->getData(self::ACTION);
    }

    /**
     * {@inheritdoc}
     */
    public function setAction($action)
    {
        return $this->setData(self::ACTION, $action);
    }

    /**
     * Get Order Id
     *
     * @return int|null
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * Get Transaction At
     *
     * @return string|null
     */
    public function getTransactionAt()
    {
        return $this->getData(self::TRANSACTION_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setTransactionAt($transactionAt)
    {
        return $this->setData(self::TRANSACTION_AT, $transactionAt);
    }

    /**
     * Get Currency Code
     *
     * @return string|null
     */
    public function getCurrencyCode()
    {
        return $this->getData(self::CURRENCY_CODE);
    }

    /**
     * {@inheritdoc}
     */
    public function setCurrencyCode($currencyCode)
    {
        return $this->setData(self::CURRENCY_CODE, $currencyCode);
    }

    /**
     * Get Current Amount
     *
     * @return float|null
     */
    public function getCurrAmount()
    {
        return $this->getData(self::CURR_AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function setCurrAmount($currAmount)
    {
        return $this->setData(self::CURR_AMOUNT, $currAmount);
    }

    /**
     * Get Expired Status
     *
     * @return int|null
     */
    public function getExpiredStatus()
    {
        return $this->getData(self::EXPIRED_STATUS);
    }

    /**
     * {@inheritdoc}
     */
    public function setExpiredStatus($expiredStatus)
    {
        return $this->setData(self::EXPIRED_STATUS, $expiredStatus);
    }

    /**
     * Get Transaction Note
     *
     * @return string|null
     */
    public function getTransactionNote()
    {
        return $this->getData(self::TRANSACTION_NOTE);
    }

    /**
     * {@inheritdoc}
     */
    public function setTransactionNote($transactionNote)
    {
        return $this->setData(self::TRANSACTION_NOTE, $transactionNote);
    }
}
