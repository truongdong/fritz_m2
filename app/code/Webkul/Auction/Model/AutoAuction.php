<?php
/**
 * Webkul_Auction Auto Auction Model.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Model;

use Webkul\Auction\Api\Data\AutoAuctionInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class AutoAuction extends AbstractModel implements AutoAuctionInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'wk_auto_auction';

    /**
     * @var string
     */
    protected $_cacheTag = 'wk_auto_auction';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'wk_auto_auction';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init(\Webkul\Auction\Model\ResourceModel\AutoAuction::class);
    }
    /**
     * Get EntityId.
     *
     * @return int|null
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Set EntityId.
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get AuctionId.
     *
     * @return int|null
     */
    public function getAuctionId()
    {
        return $this->getData(self::AUCTION_ID);
    }

    /**
     * Set AuctionId.
     */
    public function setAuctionId($auctionId)
    {
        return $this->setData(self::AUCTION_ID, $auctionId);
    }

    /**
     * Get getCustomerId.
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * Set CustomerId.
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Get ProductId.
     *
     * @return int|null
     */
    public function getProductId()
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * Set ProductId.
     */
    public function setProductId($productId)
    {
        return $this->setData(self::PRODUCT_ID, $productId);
    }

    /**
     * Get Amount.
     *
     * @return float|null
     */
    public function getAmount()
    {
        return $this->getData(self::AMOUNT);
    }

    /**
     * Set Amount.
     */
    public function setAmount($amount)
    {
        return $this->setData(self::AMOUNT, $amount);
    }

    /**
     * Get WinningPrice.
     *
     * @return float|null
     */
    public function getWinningPrice()
    {
        return $this->getData(self::WINNING_PRICE);
    }

    /**
     * Set WinningPrice.
     */
    public function setWinningPrice($winningPrice)
    {
        return $this->setData(self::WINNING_PRICE, $winningPrice);
    }

    /**
     * Get Status.
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * Set Status.
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get Shop.
     *
     * @return int|null
     */
    public function getShop()
    {
        return $this->getData(self::SHOP);
    }

    /**
     * Set Shop.
     */
    public function setShop($shop)
    {
        return $this->setData(self::SHOP, $shop);
    }

    /**
     * Get Flag.
     *
     * @return int|null
     */
    public function getFlag()
    {
        return $this->getData(self::FLAG);
    }

    /**
     * Set Flag.
     */
    public function setFlag($flag)
    {
        return $this->setData(self::FLAG, $flag);
    }

    /**
     * Get CreatedAt.
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
