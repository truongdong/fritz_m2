<?php
/**
 * Webkul Auction Amount Model.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class AuctionProductMShipMap extends AbstractModel implements IdentityInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'wk_auction_product_mshiporder_map';

    /**
     * @var string
     */
    protected $_cacheTag = 'wk_auction_product_mshiporder_map';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'wk_auction_product_mshiporder_map';
    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init(\Webkul\Auction\Model\ResourceModel\AuctionProductMShipMap::class);
    }
    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getEntityId()];
    }
}
