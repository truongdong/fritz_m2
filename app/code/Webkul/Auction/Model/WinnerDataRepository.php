<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Model;

use Webkul\Auction\Api\Data;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Webkul\Auction\Model\ResourceModel\WinnerData as WinnerDataResource;
use Webkul\Auction\Model\ResourceModel\WinnerData\CollectionFactory as WinnerDataCollection;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class WinnerDataRepository
 * Get Winner Data
 */
class WinnerDataRepository implements \Webkul\Auction\Api\WinnerDataRepositoryInterface
{
    /**
     * @var WinnerDataResource
     */
    protected $resource;

    /**
     * @var WinnerDataFactory
     */
    protected $winnerDataFactory;

    /**
     * @var WinnerDataCollection
     */
    protected $winnerDataCollectionFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param WinnerDataResource              $resource
     * @param WinnerDataFactory               $winnerDataFactory
     * @param Data\WinnerDataInterfaceFactory $winnerDataInterfaceFactory
     * @param WinnerDataCollection            $winnerDataCollectionFactory
     * @param DataObjectHelper                $dataObjectHelper
     * @param DataObjectProcessor             $dataObjectProcessor
     * @param StoreManagerInterface           $storeManager
     */
    public function __construct(
        WinnerDataResource $resource,
        WinnerDataFactory $winnerDataFactory,
        WinnerDataCollection $winnerDataCollectionFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->winnerDataFactory = $winnerDataFactory;
        $this->winnerDataCollectionFactory = $winnerDataCollectionFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * Save Winner data
     *
     * @param \Webkul\Auction\Api\Data\WinnerDataInterface $winnerData
     * @return WinnerData
     * @throws CouldNotSaveException
     */
    public function save(Data\WinnerDataInterface $winnerData)
    {
        $storeId = $this->storeManager->getStore()->getId();
        $winnerData->setStoreId($storeId);
        try {
            $this->resource->save($winnerData);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $winnerData;
    }

    /**
     * Load winner data by given Block Identity
     *
     * @param string $id
     * @return WinnerData
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id)
    {
        $winnerData = $this->winnerDataFactory->create();
        $this->resource->load($winnerData, $id);
        if (!$winnerData->getEntityId()) {
            throw new NoSuchEntityException(__('Winner record with id "%1" does not exist.', $id));
        }
        return $winnerData;
    }

    /**
     * Delete Winner Data
     *
     * @param \Webkul\Auction\Api\Data\WinnerDataInterface $winnerData
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Data\WinnerDataInterface $winnerData)
    {
        try {
            $this->resource->delete($winnerData);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * Delete Winner Record by given Block Identity
     *
     * @param string $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }
}
