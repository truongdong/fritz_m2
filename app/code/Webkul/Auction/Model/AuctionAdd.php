<?php
/**
 * Webkul_Auction Winner Data Model.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Model;

use Webkul\Auction\Model\ProductFactory;
use Webkul\Auction\Api\Data\AuctionInterface;
use \Webkul\Auction\Api\Data\ResponseInterface;

class AuctionAdd implements \Webkul\Auction\Api\AuctionAddRepositoryInterface
{
    protected $_productFactory;
    protected $respones;
    protected $_timezone;
    
    public function __construct(
        ProductFactory $productFactory,
        ResponseInterface $respones,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
    ) {
        $this->_productFactory = $productFactory;
        $this->respones = $respones;
        $this->_timezone = $timezone;
    }
    /**
     * Save auction.
     *
     * @param AuctionInterface|Auction $auction
     * @throws TemporaryCouldNotSaveException
     * @throws InputException
     * @throws CouldNotSaveException
     * @throws LocalizedException
     * @return Webkul\Auction\Api\Data\ResponseInterface
     */
    public function save($auction)
    {
         $data = $auction->getData();
         $returnArr = [];
        if (!isset($data['min_qty']) && !isset($data['max_qty'])) {
            $data['min_qty'] = 1;
            $data['max_qty'] = 1;
        }
        $auctionProduct = $this->_productFactory->create();
        $timezone = $this->_timezone;
        if (isset($data['start_auction_time'])) {
            $data['start_auction_time'] = $this->converToTz(
                $data['start_auction_time'],
                $timezone->getDefaultTimezone(),
                $timezone->getConfigTimezone()
            );
        }
        if (isset($data['stop_auction_time'])) {
            $data['stop_auction_time'] = $this->converToTz(
                $data['stop_auction_time'],
                $timezone->getDefaultTimezone(),
                $timezone->getConfigTimezone()
            );
        }
        if (isset($data['starting_price'])) {
            $data['min_amount'] = $data['starting_price'];
            $data['min_auto_amount'] = $data['starting_price'];
        }
        $auctionProduct->setData($data);
        if (isset($data['entity_id'])) {
            $auctionProduct->setEntityId($data['entity_id']);
        } else {
            $auctionProduct->setAuctionStatus(1);
            $auctionProduct->setStatus(0);
        }
        $auctionProduct->save();
        $returnArr['id'] = $auctionProduct->getId();
        return $this->getJsonResponse($returnArr)->getResponse();
    }
    /**
     * convert Datetime from one zone to another
     * @param string $dateTime which we want to convert
     * @param string $toTz timezone in which we want to convert
     * @param string $fromTz timezone from which we want to convert
     */
    protected function converToTz($dateTime = "", $toTz = '', $fromTz = '')
    {
        // timezone by php friendly values
        $date = new \DateTime($dateTime, new \DateTimeZone($fromTz));
        $date->setTimezone(new \DateTimeZone($toTz));
        $dateTime = $date->format('m/d/Y H:i:s');
        return $dateTime;
    }
 
    public function delete($id)
    {
        $response = [];
        $auction =  $this->_productFactory->create()->load($id)->delete();
        if ($auction) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * getJsonResponse returns json response.
     *
     * @param array $responseContent
     *
     * @return Webkul\Auction\Api\Data\ResponseInterface
     */
    protected function getJsonResponse($responseContent = [])
    {
        return $this->respones->setItem($responseContent);
    }
}
