<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Model;

use Webkul\Auction\Api\Data;
use Webkul\Auction\Api\IncrementalPriceRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Webkul\Auction\Model\ResourceModel\IncrementalPrice as IncrementalPriceResource;
use Webkul\Auction\Model\ResourceModel\IncrementalPrice\CollectionFactory as IncrementalPriceCollection;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class IncrementalPriceRepository
 * Incremental Price
 */
class IncrementalPriceRepository implements IncrementalPriceRepositoryInterface
{
    /**
     * @var IncrementalPriceResource
     */
    protected $resource;

    /**
     * @var IncrementalPriceFactory
     */
    protected $incrementalPriceFactory;

    /**
     * @var IncrementalPriceCollection
     */
    protected $incrementalPriceCollectionFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param IncrementalPriceResource              $resource
     * @param IncrementalPriceFactory               $incrementalPriceFactory
     * @param Data\IncrementalPriceInterfaceFactory $incrementalPriceInterfaceFactory
     * @param IncrementalPriceCollection            $incrementalPriceCollectionFactory
     * @param DataObjectHelper                      $dataObjectHelper
     * @param DataObjectProcessor                   $dataObjectProcessor
     * @param StoreManagerInterface                 $storeManager
     */
    public function __construct(
        IncrementalPriceResource $resource,
        IncrementalPriceFactory $incrementalPriceFactory,
        IncrementalPriceCollection $incrementalPriceCollectionFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->incrementalPriceFactory = $incrementalPriceFactory;
        $this->incrementalPriceCollectionFactory = $incrementalPriceCollectionFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * Save Incremental Price
     *
     * @param \Webkul\Auction\Api\Data\IncrementalPriceInterface $incrementalPrice
     * @return IncrementalPrice
     * @throws CouldNotSaveException
     */
    public function save(Data\IncrementalPriceInterface $incrementalPrice)
    {
        $storeId = $this->storeManager->getStore()->getId();
        $incrementalPrice->setStoreId($storeId);
        try {
            $this->resource->save($incrementalPrice);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $incrementalPrice;
    }

    /**
     * Load Incremental Price by given Block Identity
     *
     * @param string $id
     * @return IncrementalPrice
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id)
    {
        $incrementalPrice = $this->incrementalPriceFactory->create();
        $this->resource->load($incrementalPrice, $id);
        if (!$incrementalPrice->getEntityId()) {
            throw new NoSuchEntityException(__('Auction Product with id "%1" does not exist.', $id));
        }
        return $incrementalPrice;
    }

    /**
     * Delete Incremental Price
     *
     * @param \Webkul\Auction\Api\Data\IncrementalPriceInterface $incrementalPrice
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Data\IncrementalPriceInterface $incrementalPrice)
    {
        try {
            $this->resource->delete($incrementalPrice);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * Delete Incremental Price by given Block Identity
     *
     * @param string $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }
}
