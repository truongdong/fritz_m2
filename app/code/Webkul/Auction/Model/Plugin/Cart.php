<?php

/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Model\Plugin;

use Magento\Framework\Exception\LocalizedException;

class Cart
{
    /**
     * @var \Webkul\Auction\Helper\Data
     */
    private $_auctionHelper;
    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $quote;
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $_request;
    protected $orderRegistry;
    /**
     * @param WebkulAuctionHelperData        $auctionHelper
     * @param MagentoCheckoutModelSession    $checkoutSession
     * @param MagentoFrameworkAppRequestHttp $request
     */
    public function __construct(
        \Webkul\Auction\Helper\Data $auctionHelper,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Registry $registry
    ) {
        $this->_auctionHelper = $auctionHelper;
        $this->quote = $checkoutSession->getQuote();
        $this->_request = $request;
        $this->orderRegistry = $registry;
    }
    
    public function beforeAddProduct(
        \Magento\Checkout\Model\Cart $subject,
        $productInfo,
        $requestInfo = null
    ) {
        $params = $this->_request->getParams();
        $moduleName =  $this->_request->getModuleName();
        if ($moduleName!="orderapproval") {
            $flag = 0;
            $productId = 0;
            $items = [];
            $auctionProductId = $this->_auctionHelper->getWalletProductId();
            if (array_key_exists('product', $params)) {
                $productId = $params['product'];
            } elseif (array_key_exists('order_id', $params)) {
                $order = $this->orderRegistry->registry('current_order');
                $items = $order->getItemsCollection();
            }
            $quote = $this->quote;
            $cartData = $quote->getAllItems();
            if ($productId) {
                if ($auctionProductId == $productId) {
                    $flag = 1;
                }
                if (!empty($cartData)) {
                    foreach ($cartData as $item) {
                        $itemProductId = $item->getProductId();
                        if ($auctionProductId == $itemProductId) {
                            if ($flag != 1) {
                                throw new LocalizedException(
                                    __('You can not add other product with auction wallet product')
                                );
                            }
                        } else {
                            if ($flag == 1) {
                                throw new LocalizedException(
                                    __('You can not add auction wallet product with other product')
                                );
                            }
                        }
                    }
                }
            } elseif (!empty($items)) {
                $walletInOrder = $this->checkIfOrderHaveWalletProduct($items, $auctionProductId);
                if (!empty($cartData)) {
                    foreach ($cartData as $item) {
                        $itemProductId = $item->getProductId();
                        if ($auctionProductId == $itemProductId) {
                            if (!$walletInOrder) {
                                throw new LocalizedException(
                                    __('You can not add other product with auction wallet product')
                                );
                            }
                        } else {
                            if ($walletInOrder) {
                                throw new LocalizedException(
                                    __('You can not add auction wallet product with other product')
                                );
                            }
                        }
                    }
                }
            }
        }
        return [$productInfo, $requestInfo];
    }

    public function checkIfOrderHaveWalletProduct($items, $auctionProductId)
    {
        foreach ($items as $item) {
            $productId = $item->getproduct()->getId();
            if ($productId == $auctionProductId) {
                return true;
            }
        }
        return false;
    }
}
