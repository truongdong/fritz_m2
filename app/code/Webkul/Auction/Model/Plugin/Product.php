<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Model\Plugin;

class Product
{
    /**
     * @var Magento\Framework\App\State
     */
    protected $_appState;

    /**
     * @param \Magento\Framework\App\State     $appState
     */
    public function __construct(
        \Magento\Framework\App\State $appState
    ) {
        $this->_appState = $appState;
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $subject
     * @param \Closure $proceed
     * @param [type] $attribute
     * @param boolean $joinType
     * @return void
     */
    public function aroundAddAttributeToSelect(
        \Magento\Catalog\Model\ResourceModel\Product\Collection $subject,
        \Closure $proceed,
        $attribute,
        $joinType = false
    ) {
        $appState = $this->_appState;
        $areCode = $appState->getAreaCode();
        $auctionSku='wk_auction_amount';
        $result = $proceed($attribute, $joinType = false);
        $code = \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE;
        if ($appState->getAreaCode() == $code) {
            $result->addFieldToFilter('sku', ['neq' => $auctionSku]);
        }

        return $result;
    }
}
