<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Model;

use Webkul\Auction\Api\Data;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Webkul\Auction\Model\ResourceModel\Product as ProductResource;
use Webkul\Auction\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class ProductRepository
 * Get Product Info
 */
class ProductRepository implements \Webkul\Auction\Api\ProductRepositoryInterface
{
    /**
     * @var ProductResource
     */
    protected $resource;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var ProductCollection
     */
    protected $productCollectionFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param ProductResource                   $resource
     * @param ProductFactory                    $productFactory
     * @param Data\ProductInterfaceFactory      $productInterfaceFactory
     * @param ProductCollection                 $productCollectionFactory
     * @param DataObjectHelper                  $dataObjectHelper
     * @param DataObjectProcessor               $dataObjectProcessor
     * @param StoreManagerInterface             $storeManager
     */
    public function __construct(
        ProductResource $resource,
        ProductFactory $productFactory,
        ProductCollection $productCollectionFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->productFactory = $productFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * Save Auction Product
     *
     * @param \Webkul\Auction\Api\Data\ProductInterface $product
     * @return Product
     * @throws CouldNotSaveException
     */
    public function save(Data\ProductInterface $product)
    {
        $storeId = $this->storeManager->getStore()->getId();
        $product->setStoreId($storeId);
        try {
            $this->resource->save($product);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $product;
    }

    /**
     * Load Auction Product by given Block Identity
     *
     * @param string $id
     * @return Product
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id)
    {
        $product = $this->productFactory->create();
        $this->resource->load($product, $id);
        if (!$product->getEntityId()) {
            throw new NoSuchEntityException(__('Auction Product with id "%1" does not exist.', $id));
        }
        return $product;
    }

    /**
     * Delete Auction Product
     *
     * @param \Webkul\Auction\Api\Data\ProductInterface $product
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Data\ProductInterface $product)
    {
        try {
            $this->resource->delete($product);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * Delete Auction Product by given Block Identity
     *
     * @param string $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }

    /**
     * getAuctionData by id
     *
     * @param string $id
     * @return array
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function getAuctionData($id)
    {
        return  $this->productCollectionFactory->create()
        ->addFieldToFilter('product_id', $id)
                                    ->addFieldToFilter('status', 0)
                                    ->addFieldToFilter('auction_status', 1)
                                    ->getFirstItem()->getData();
    }

   /**
    * getAuctionEndData   function
    *
    * @param int $id
    * @return array
    */
    public function getAuctionEndData($id)
    {
        return  $this->productCollectionFactory->create()
            ->addFieldToFilter('product_id', $id)
            ->addFieldToFilter('auction_status', 0)
            ->getFirstItem()->getData();
    }
}
