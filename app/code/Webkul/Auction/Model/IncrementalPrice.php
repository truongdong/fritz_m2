<?php
/**
 * Webkul_Auction Incremental Price Model.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Model;

use Webkul\Auction\Api\Data\IncrementalPriceInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class IncrementalPrice extends AbstractModel implements IncrementalPriceInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'wk_auction_incremental_price';

    /**
     * @var string
     */
    protected $_cacheTag = 'wk_auction_incremental_price';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'wk_auction_incremental_price';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init(\Webkul\Auction\Model\ResourceModel\IncrementalPrice::class);
    }
    /**
     * Get EntityId.
     *
     * @return int|null
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Set EntityId.
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get UserId.
     *
     * @return string|null
     */
    public function getUserId()
    {
        return $this->getData(self::USER_ID);
    }

    /**
     * Set UserId.
     */
    public function setUserId($userId)
    {
        return $this->setData(self::USER_ID, $userId);
    }

    /**
     * Get Incval.
     *
     * @return string|null
     */
    public function getIncval()
    {
        return $this->getData(self::INCVAL);
    }

    /**
     * Set Incval.
     */
    public function setIncval($incval)
    {
        return $this->setData(self::INCVAL, $incval);
    }
}
