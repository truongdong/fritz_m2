<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Model;

use Magento\Framework\Model\AbstractModel;
use Webkul\Auction\Model\WalletrecordFactory;
use Webkul\Auction\Model\WallettransactionFactory;

class WalletUpdateData extends AbstractModel
{
    /**
     * @var Webkul\Auction\Model\WalletrecordFactory
     */
    protected $_walletrecord;
    /**
     * @var Webkul\Auction\Model\WallettransactionFactory
     */
    protected $_walletTransaction;

    /**
     * @var Webkul\Auction\Helper\Data
     */
    protected $_auctionHelper;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;
    /**
     * @var \Webkul\Auction\Helper\Email
     */
    protected $_mailHelper;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_localeDate;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Webkul\Auction\Model\ResourceModel\Wallettransaction $resource,
        WalletrecordFactory $walletrecord,
        WallettransactionFactory $transactionFactory,
        \Webkul\Auction\Helper\Data $auctionHelper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\Auction\Helper\Email $mailHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_walletrecord = $walletrecord;
        $this->_walletTransaction = $transactionFactory;
        $this->_auctionHelper = $auctionHelper;
        $this->_date = $date;
        $this->_mailHelper = $mailHelper;
        $this->_messageManager = $messageManager;
        $this->_localeDate = $localeDate;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }
    public function getRecordByCustomerId($customerId)
    {
        $recordId = 0;
        $walletRecordModel = '';
        $walletRecordCollection = $this->_walletrecord
            ->create()
            ->getCollection()
            ->addFieldToFilter('customer_id', ['eq' => $customerId]);
        foreach ($walletRecordCollection as $walletRecord) {
            $recordId = $walletRecord->getEntityId();
        }
        if ($recordId) {
            $walletRecordModel = $this->_walletrecord
                ->create()
                ->load($recordId);
        }
        return $walletRecordModel;
    }
    public function creditAmount($customerId, $params)
    {
        $transactionModelData = $this->setTransactionModelData($customerId, $params);
        if ($transactionModelData) {
            return [
            'success' => 1
            ];
        } else {
            return [
            'success' => 0
            ];
        }
    }

    public function debitAmount($customerId, $params)
    {
        $customerRecord = $this->getRecordByCustomerId($customerId);
        if ($customerRecord!='' &&
            $customerRecord->getEntityId() &&
            $customerRecord->getRemainingAmount() >= $params['walletamount']) {
            $this->setTransactionModelData($customerId, $params);
            return [
                'success' => 1
            ];
        } else {
            $this->_messageManager->addError(
                __(
                    "Respective amount is not available in customer's wallet with customer id %1 to subtract",
                    $customerId
                )
            );
            return [
                'error' => 1
            ];
        }
    }
    public function setTransactionModelData($customerId, $params)
    {
        try {
            $currencycode = $this->_auctionHelper->getBaseCurrencyCode();
            $walletTransactionModel = $this->_walletTransaction->create();
            if ($params['order_id']) {
                $params['expired_status'] = 0;
            } else {
                $params['expired_status'] = 1;
            }
            $walletTransactionModel->setCustomerId($customerId)
            ->setAmount($params['walletamount'])
            ->setCurrAmount($params['curr_amount'])
            ->setStatus($params['status'])
            ->setCurrencyCode($params['curr_code'])
            ->setAction($params['walletactiontype'])
            ->setTransactionNote($params['walletnote'])
            ->setOrderId($params['order_id'])
            ->setExpiredStatus($params['expired_status'])
            ->setTransactionAt($this->_date->gmtDate());
            if (isset($params['auction_id'])) {
                $walletTransactionModel->setCustomerId($customerId)
                ->setAmount($params['walletamount'])
                ->setCurrAmount($params['curr_amount'])
                ->setStatus($params['status'])
                ->setCurrencyCode($params['curr_code'])
                ->setAction($params['walletactiontype'])
                ->setTransactionNote($params['walletnote'])
                ->setOrderId($params['order_id'])
                ->setExpiredStatus($params['expired_status'])
                ->setTransactionAt($this->_date->gmtDate())
                ->setAuctionId($params['auction_id']);
            }
            $walletTransactionModel->save();

            $walletRecordModel = $this->getRecordByCustomerId($customerId);
        
            if ($walletRecordModel!='' && $walletRecordModel->getEntityId()) {
           
                $remainingAmount = $walletRecordModel->getRemainingAmount();
                $totalAmount = $walletRecordModel->getTotalAmount();
                $usedAmount = $walletRecordModel->getUsedAmount();
                $recordId = $walletRecordModel->getEntityId();
                if ($params['walletactiontype']=='debit') {
                    $data = [
                    'used_amount' => $usedAmount + $params['walletamount'],
                    'remaining_amount' => $remainingAmount - $params['walletamount'],
                    'transaction_at' => $this->_date->gmtDate(),
                    ];
                    $finalAmount = $remainingAmount - $params['walletamount'];
                } else {
                    $data = [
                    'total_amount' => $params['walletamount'] + $totalAmount,
                    'remaining_amount' => $params['walletamount'] + $remainingAmount,
                    'transaction_at' => $this->_date->gmtDate(),
                    ];
                    $finalAmount = $params['walletamount'] + $remainingAmount;
                }
                if ($params['status']==1) {
                    $walletRecordModel = $this->_walletrecord->create()
                    ->load($recordId)
                    ->addData($data);
                }
                $saved = $walletRecordModel->setId($recordId)->save();
            } else {
                if ($params['status']==1) {
                    $walletRecordModel = $this->_walletrecord->create();
                    $walletRecordModel->setTotalAmount($params['walletamount'])
                    ->setCustomerId($customerId)
                    ->setRemainingAmount($params['walletamount'])
                    ->setUpdatedAt($this->_date->gmtDate());
                    $saved = $walletRecordModel->save();
                }
                $finalAmount = $params['walletamount'];
            }
            if ($params['status']==1) {
                $date = $this->_localeDate->date(new \DateTime($this->_date->gmtDate()));
                $formattedDate = $date->format('g:ia \o\n l jS F Y');
                $emailParams = [
                'walletamount' => $this->_auctionHelper->getformattedPrice($params['walletamount']),
                'remainingamount' => $this->_auctionHelper->getformattedPrice($finalAmount),
                'action' => $params['walletactiontype'],
                'transaction_at' => $formattedDate,
                'walletnote' => $params['walletnote']
                ];
                $store = $this->_auctionHelper->getStore();
                $this->_mailHelper->sendMailForTransaction(
                    $customerId,
                    $emailParams,
                    $store
                );
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
