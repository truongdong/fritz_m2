<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Model\Creditmemo\Total;

class AuctionDiscount extends \Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal
{
    /**
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Creditmemo $creditmemo)
    {
        $order = $creditmemo->getOrder();
        $orderAuctionDiscount = $order->getAuctionDiscount();
        $orderBaseAuctionDiscount = $order->getBaseAuctionDiscount();
        $creditmemo->setAuctionDiscount(0);
        if ($orderAuctionDiscount) {
            $creditmemo->setGrandTotal($creditmemo->getGrandTotal()+$orderAuctionDiscount);
            $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal()+$orderBaseAuctionDiscount);
            $creditmemo->setAuctionDiscount($orderAuctionDiscount);
        }
        return $this;
    }
}
