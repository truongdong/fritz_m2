<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Model\Order\Pdf;

/**
 * Sales Order Invoice PDF model
 *
 */
class Invoice extends \Magento\Sales\Model\Order\Pdf\Total\DefaultTotal
{
    /**
     * add Auction card discount fee in pdf
     * @return array
     */
    public function getTotalsForDisplay()
    {
    
        $amount = $this->getOrder()->formatPriceTxt($this->getAmount());
        if ($this->getAmountPrefix()) {
            $amount = $this->getAmountPrefix() . $amount;
        }
 
        $title = __($this->getAuctionCouponTitle());
        $fontSize = $this->getFontSize() ? $this->getFontSize() : 7;
        $total = ['amount' =>  $amount, 'label' => $title, 'font_size' => $fontSize];
        return [$total];
    }
    /**
     * get Auction coupon title
     * @return string
     */
    public function getAuctionCouponTitle()
    {
        return $this->getTitle();
    }
}
