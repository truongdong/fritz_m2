<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Model;

use Webkul\Auction\Api\Data\WalletrecordInterface;
use Magento\Framework\DataObject\IdentityInterface;
use \Magento\Framework\Model\AbstractModel;

class Walletrecord extends AbstractModel implements WalletrecordInterface, IdentityInterface
{
    const CACHE_TAG = 'auction_walletrecord';
    /**
     * @var string
     */
    protected $_cacheTag = 'auction_walletrecord';
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'auction_walletrecord';
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Webkul\Auction\Model\ResourceModel\Walletrecord::class);
    }
    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getEntityId()];
    }
    /**
     * Get Entity ID
     *
     * @return int|null
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setEntityId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get Customer Id
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Get Total Amount
     *
     * @return float|null
     */
    public function getTotalAmount()
    {
        return $this->getData(self::TOTAL_AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function setTotalAmount($totalAmount)
    {
        return $this->setData(self::TOTAL_AMOUNT, $totalAmount);
    }

    /**
     * Get Remaining Amount
     *
     * @return float|null
     */
    public function getRemainingAmount()
    {
        return $this->getData(self::REMAINING_AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function setRemainingAmount($remainingAmount)
    {
        return $this->setData(self::REMAINING_AMOUNT, $remainingAmount);
    }

    /**
     * Get Used Amount
     *
     * @return float|null
     */
    public function getUsedAmount()
    {
        return $this->getData(self::USED_AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function setUsedAmount($usedAmount)
    {
        return $this->setData(self::USED_AMOUNT, $usedAmount);
    }

    /**
     * Get Expired Date
     *
     * @return string|null
     */
    public function getExpiredDate()
    {
        return $this->getData(self::EXPIRED_DATE);
    }

    /**
     * {@inheritdoc}
     */
    public function setExpiredDate($expiredDate)
    {
        return $this->setData(self::EXPIRED_DATE, $expiredDate);
    }

    /**
     * Get Transaction At
     *
     * @return string|null
     */
    public function getTransactionAt()
    {
        return $this->getData(self::TRANSACTION_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setTransactionAt($transactionAt)
    {
        return $this->setData(self::TRANSACTION_AT, $transactionAt);
    }
    /**
     * Get Updated At
     *
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }
}
