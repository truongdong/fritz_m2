<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Model;

use Webkul\Auction\Api\AmountRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Webkul\Auction\Model\ResourceModel\Amount as AmountResource;
use Webkul\Auction\Model\ResourceModel\Amount\CollectionFactory as AmountCollection;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class AmountRepositry
 * Incremental Price
 */
class AmountRepositry implements AmountRepositoryInterface
{
    /**
     * @var AmountResource
     */
    protected $resource;

    /**
     * @var IncrementalPriceFactory
     */
    protected $amountFactory;

    /**
     * @var AmountCollection
     */
    protected $AmountCollectionFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param AmountResource              $resource
     * @param IncrementalPriceFactory               $incrementalPriceFactory
     * @param \Webkul\Auction\Api\Data\AmountInterfaceFactory $incrementalPriceInterfaceFactory
     * @param AmountCollection            $AmountCollectionFactory
     * @param DataObjectHelper                      $dataObjectHelper
     * @param DataObjectProcessor                   $dataObjectProcessor
     * @param StoreManagerInterface                 $storeManager
     */
    public function __construct(
        AmountResource $resource,
        AmountFactory $amountFactory,
        AmountCollection $AmountCollectionFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->amountFactory = $amountFactory;
        $this->AmountCollectionFactory = $AmountCollectionFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * Save Incremental Price
     *
     * @param \Webkul\Auction\Api\Data\AmountInterface $incrementalPrice
     * @return IncrementalPrice
     * @throws CouldNotSaveException
     */
    public function save(\Webkul\Auction\Api\Data\AmountInterface $amountInterface)
    {
        try {
            $this->resource->save($amountInterface);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $amountInterface;
    }

    /**
     * Load Incremental Price by given Block Identity
     *
     * @param string $id
     * @return IncrementalPrice
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id)
    {
        $amountData = $this->amountFactory->create();
        $this->resource->load($amountData, $id);
        if (!$amountData->getEntityId()) {
            throw new NoSuchEntityException(__('Auction Product with id "%1" does not exist.', $id));
        }
        return $amountData;
    }

    /**
     * Delete Incremental Price
     *
     * @param \Webkul\Auction\Api\Data\AmountInterface $incrementalPrice
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(\Webkul\Auction\Api\Data\AmountInterface $amountInterface)
    {
        try {
            $this->resource->delete($amountInterface);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * Delete Incremental Price by given Block Identity
     *
     * @param string $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }

    /**
     * @param int $currentProId
     * @param int $aucEntityId
     *
     */

    public function getAmountDataByAuctionId($currentProId = null, $aucEntityId = null)
    {
        $amountData = $this->amountFactory->create()->getCollection()
        ->addFieldToFilter('product_id', ['eq'=>$currentProId])
        ->addFieldToFilter('auction_id', ['eq'=>$aucEntityId])
        ->addFieldToFilter('winning_status', ['eq'=>1])
        ->addFieldToFilter('shop', ['neq'=>0])->getFirstItem();
       
        return $amountData;
    }

     /**
      * @param int $currentProId
      * @param int $aucEntityId
      *
      */

    public function getCurrentAuctionPriceData($currentProId, $aucEntityId)
    {
        $amountData = $this->amountFactory->create()->getCollection()
        ->addFieldToFilter('product_id', ['eq'=>$currentProId])
        ->addFieldToFilter('auction_id', ['eq'=>$aucEntityId])
        ->setOrder('auction_amount', 'DESC')->getFirstItem();

        return $amountData;
    }
}
