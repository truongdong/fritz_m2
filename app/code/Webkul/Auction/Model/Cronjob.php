<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Model;

class Cronjob
{
    /**
     * @var WebkulAuctionHelperData
     */
    protected $_auctionHelper;
    /**
     * @var MagentoCustomerModelCustomerFactory
     */
    protected $_customerModel;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;
    /**
     * @var \Webkul\Auction\Helper\Email
     */
    protected $_mailHelper;

    /**
     * @param WebkulAuctionHelperData                         $auctionHelper
     * @param MagentoCustomerModelCustomerFactory             $customerFactory
     * @param WebkulAuctionModelWallettransactionFactory        $walletTransactionFactory
     * @param MagentoFrameworkStdlibDateTimeDateTime          $date
     * @param WebkulAuctionHelperEmail                        $mailHelper
     */
    public function __construct(
        \Webkul\Auction\Helper\Data $auctionHelper,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Webkul\Auction\Model\WalletUpdateData $walletData,
        \Webkul\Auction\Model\WalletrecordFactory $walletRecordFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\Auction\Helper\Email $mailHelper
    ) {
        $this->_auctionHelper = $auctionHelper;
        $this->_customerModel = $customerFactory;
        $this->_walletData = $walletData;
        $this->_walletRecordFactory = $walletRecordFactory;
        $this->_date = $date;
        $this->_mailHelper = $mailHelper;
    }

    public function execute()
    {
        $helper = $this->_auctionHelper;
        $customerCollection = $this->_customerModel
            ->create()->getCollection();
        if ($customerCollection->getSize()) {
            foreach ($customerCollection as $customer) {
                $customerId = $customer->getEntityId();
                list($expiredStatus,$notifyStatus,$expiredDate) =$helper->checkSellerAmountExpiryStatus(
                    $customerId,
                    $helper->getNotificationDays()
                );
                if ($notifyStatus) {
                    $this->sendCustomerEmailForAmountExpiry($customerId, $expiredDate);
                }
                if ($expiredStatus) {
                    $this->expiredWalletAmount($customerId, $expiredDate);
                }
            }
        }
        return $this;
    }

    public function sendCustomerEmailForAmountExpiry($customerId, $expiredDate)
    {
        $helper = $this->_mailHelper->notifyWalletExpiry($customerId, $expiredDate);
    }

    public function expiredWalletAmount($customerId = 0, $expiredDate = '')
    {
        $remainingAmount = $this->_walletRecordFactory->create()->getCollection()
                      ->addFieldToFilter('customer_id', $customerId)->getFirstItem()->getRemainingAmount();
        if ($remainingAmount > 0) {
            $currentCurrencyCode = $this->_auctionHelper->getCurrentCurrencyCode();
            $transferAmountData = [
                'customerid' => $customerId,
                'walletamount' => $remainingAmount,
                'walletactiontype' => 'debit',
                'curr_code' => $currentCurrencyCode,
                'curr_amount' => $remainingAmount,
                'walletnote' => __('Expired amount on %1 : debited amount', $expiredDate),
                'order_id' => 0,
                'status' => 1
            ];
            $result = $this->_walletData->debitAmount($customerId, $transferAmountData);
        }
    }
}
