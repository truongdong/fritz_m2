<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Model\Quote\Total;

use Magento\Framework\Pricing\PriceCurrencyInterface;

class AuctionDiscount extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
    /**
     * is module multishipping
     */
    const IS_MULTISHIPPING = "multishipping";
    /**
     * Core event manager proxy
     *
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager = null;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    protected $quoteValidator = null;

    /**
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Quote\Model\QuoteValidator $quoteValidator,
     * @param PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Quote\Model\QuoteValidator $quoteValidator,
        PriceCurrencyInterface $priceCurrency,
        \Webkul\Auction\Helper\Data $helper,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->setCode('auction_discount');
        $this->eventManager = $eventManager;
        $this->quoteValidator = $quoteValidator;
        $this->_helper = $helper;
        $this->priceCurrency = $priceCurrency;
        $this->request = $request;
    }
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);
        $items = $shippingAssignment->getItems();
        if (empty($items)) {
            return $this;
        }
        $helper = $this->_helper;
        $address = $shippingAssignment->getShipping()->getAddress();
        $store = $quote->getStore();
        list($status, $auctiondiscount) = $helper->getAuctionDiscount($items);
        if ($status) {
            $currentCurrencyCode = $helper->getCurrentCurrencyCode();
            $baseCurrencyCode = $helper->getBaseCurrencyCode();
            $allowedCurrencies = $helper->getConfigAllowCurrencies();
            $rates = $helper->getCurrencyRates($baseCurrencyCode, array_values($allowedCurrencies));
            if (empty($rates[$currentCurrencyCode])) {
                $rates[$currentCurrencyCode] = 1;
            }
            $baseAuctiondiscount = $helper->baseCurrencyAmount($auctiondiscount);
            $baseAuctiondiscount = -($baseAuctiondiscount);
            $auctiondiscount = -($auctiondiscount);
            $moduleName = $this->getModuleName();
            if ($moduleName == self::IS_MULTISHIPPING) {
                $getItemsQty = $quote->getItemsQty();
                foreach ($address->getAllVisibleItems() as $item) {
                    $getQty = $item->getQty();
                    $baseAuctiondiscount = ($baseAuctiondiscount/$getItemsQty)*$getQty;
                    $auctiondiscount = ($auctiondiscount/$getItemsQty)*$getQty;
                    $address->setData('auction_discount', $auctiondiscount);
                    $address->setData('base_auction_discount', $baseAuctiondiscount);
                    $total->setTotalAmount('auction_discount', $auctiondiscount);
                    $total->setBaseTotalAmount('base_auction_discount', $baseAuctiondiscount);
                    $quote->setAuctionDiscount($auctiondiscount);
                    $quote->setBaseAuctionDiscount($baseAuctiondiscount);
                    $total->setAuctionDiscount($auctiondiscount);
                    $total->setBaseAuctionDiscount($baseAuctiondiscount);
                }
            } else {
                $address->setData('auction_discount', $auctiondiscount);
                $address->setData('base_auction_discount', $baseAuctiondiscount);
                $total->setTotalAmount('auction_discount', $auctiondiscount);
                $total->setBaseTotalAmount('base_auction_discount', $baseAuctiondiscount);
                $quote->setAuctionDiscount($auctiondiscount);
                $quote->setBaseAuctionDiscount($baseAuctiondiscount);
                $total->setAuctionDiscount($auctiondiscount);
                $total->setBaseAuctionDiscount($baseAuctiondiscount);
            }
            
        }
        return $this;
    }

    /**
     * Add shipping totals information to address object
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return array
     */
    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        $moduleName = $this->getModuleName();
        if ($total->getAuctionDiscount()!= 0 && $moduleName != self::IS_MULTISHIPPING) {
            $description = $total->getDiscountDescription();
            return [
                'code'  => $this->getCode(),
                'title' => strlen($description) ? __('Discount (%1)', $description) : __('Discount'),
                'value' => $total->getAuctionDiscount()
            ];
        } else {
            return [
                'code'  => $this->getCode(),
                'title' => $this->getLabel(),
                'value' => $total->getAuctionDiscount()
            ];
        }
    }

    /**
     * Get discount label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getLabel()
    {
        return __('Wallet deducted amount');
    }

    public function getModuleName()
    {
        return $this->request->getModuleName();
    }
}
