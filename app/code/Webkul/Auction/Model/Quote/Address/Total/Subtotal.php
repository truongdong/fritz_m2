<?php

namespace Webkul\Auction\Model\Quote\Address\Total;

use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Webkul\Auction\Model\ProductFactory as AuctionProductFactory;
use Webkul\Auction\Model\WinnerDataFactory;

class Subtotal extends \Magento\Quote\Model\Quote\Address\Total\Subtotal
{
    /**
     * @var \Magento\Customer\Model\Session
     */

    private $customerSession;

    /**
     * @var \Magento\Checkout\Model\Session
     */

    private $checkoutSession;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */

    private $messageManager;

    /**
     * @var \Magento\CatalogInventory\Api\StockStateInterface
     */

    private $stockStateInterface;

    /**
     * @var \Webkul\Auction\Model\WinnerDataFactory
     */

    private $winnerData;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */

    private $configurableProTypeModel;

    /**
     * @var \Webkul\Auction\Model\ProductFactory
     */

    private $auctionProductFactory;

    /**
     * @var \Webkul\Auction\Helper\Data
     */
    private $helperData;

    /**
     * @var TimezoneInterface
     */
    private $_timezoneInterface;

    /**
     * @param RequestInterface      $request
     * @param CustomerSession       $customerSession
     * @param CheckoutSession       $checkoutSession
     * @param ManagerInterface      $messageManager
     * @param StockStateInterface   $stockStateInterface
     * @param Configurable          $configurableProTypeModel
     * @param WinnerDataFactory     $winnerData
     * @param AuctionProductFactory $auctionProductFactory
     */
    public function __construct(
        RequestInterface $request,
        CustomerSession $customerSession,
        CheckoutSession $checkoutSession,
        ManagerInterface $messageManager,
        StockStateInterface $stockStateInterface,
        Configurable $configurableProTypeModel,
        WinnerDataFactory $winnerData,
        AuctionProductFactory $auctionProductFactory,
        \Webkul\Auction\Helper\Data $helperData,
        TimezoneInterface $timezoneInterface,
        \Magento\Quote\Model\QuoteValidator $quoteValidator
    ) {
        $this->request = $request;
        $this->customerSession = $customerSession;
        $this->checkoutSession = $checkoutSession;
        $this->messageManager = $messageManager;
        $this->stockStateInterface = $stockStateInterface;
        $this->configurableProTypeModel = $configurableProTypeModel;
        $this->winnerData = $winnerData;
        $this->helperData = $helperData;
        $this->auctionProductFactory = $auctionProductFactory;
        $this->_timezoneInterface = $timezoneInterface;
        $this->quoteValidator = $quoteValidator;
        parent::__construct($quoteValidator);
    }

    /**
     * {@inheritDoc}
     */
    protected function _calculateRowTotal($item, $finalPrice, $originalPrice)
    {
        
        $moduleName = $this->request->getModuleName();
        if ($moduleName == "multishipping") {
            $product = $item->getProduct();
            $productId = $this->getProductIdIfConfigurable($product);

            $bidProCol = $this->winnerData->create()->getCollection()
                ->addFieldToFilter('product_id', $productId)
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('complete', 0)
                ->setOrder('entity_id', 'desc')
                ->setPageSize(1);
            $customerid = $this->helperData->getCustomerId();
            $maxqty = 0;
            $mainArray = [];
            if (count($bidProCol)) {
                foreach ($bidProCol as $winner) {
                    $day = strtotime($winner->getStopAuctionTime() . ' + ' . $winner->getDays() . ' days');
                    $difference = $day - strtotime($this->_timezoneInterface->date()->format('m/d/y H:i:s'));
                    if ($difference > 0) {
                        $auctionPro = $this->auctionProductFactory->create()->getCollection()
                            ->addFieldToFilter('entity_id', $winner->getAuctionId())
                            ->addFieldToFilter('auction_status', 0)
                            ->getLastItem();
                        if ($auctionPro->getEntityId()) {
                            $customerId = $this->helperData->getCustomerId();
                            $auctionId = $this->helperData->getActiveAuctionId($productId);

                            $maxqty += $winner->getMaxQty();

                            $customerArray['max'] = $winner->getMaxQty();
                            $customerArray['min'] = $winner->getMinQty();
                            $mainArray[$winner->getCustomerId()] = $customerArray;
                            $bidProCol = $this->winnerData->create()->getCollection()
                                ->addFieldToFilter('product_id', $productId)
                                ->addFieldToFilter('complete', 0)
                                ->addFieldToFilter('status', 1)
                                ->addFieldToFilter('customer_id', $customerId)
                                ->addFieldToFilter('auction_id', $auctionId);

                            $biddingProductCollection = $bidProCol->setOrder('auction_id', 'ASC')->getLastItem();
                            $bidWinPrice = $biddingProductCollection->getWinAmount();
                            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                            $storeManager = $objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);
                            $priceCurrency = $objectManager->get(
                                \Magento\Framework\Pricing\PriceCurrencyInterface::class
                            );

                            $currency = null;
                            $store = $storeManager->getStore()->getStoreId();
                            $bidWinPrice = $priceCurrency->convert($bidWinPrice, $store, $currency);
                            $item->setOriginalCustomPrice($bidWinPrice);
                            $item->setCustomPrice($bidWinPrice);
                            $item->getProduct()->setIsSuperMode(true);
                            $item->setPrice($bidWinPrice)->setBaseOriginalPrice($bidWinPrice);
                        }

                    }
                }

            }
        } else {
            if (!$originalPrice) {
                $originalPrice = $finalPrice;
            }
            $item->setPrice($finalPrice)->setBaseOriginalPrice($originalPrice);
        }
        $item->calcRowTotal();
        return $this;
    }

    /**
     * getProductIdIfConfigurable
     * @param Magento\Catalog\Model\Product $product
     * @return int
     */
    private function getProductIdIfConfigurable($product)
    {
        $productId = $product->getEntityId();
        if ($product->getTypeId() == 'configurable') {
            $childPro = $this->configurableProTypeModel->getChildrenIds($product->getId());
            $childProIds = isset($childPro[0]) ? $childPro[0] : [0];

            $biddingCollection = $this->auctionProductFactory->create()->getCollection()
                ->addFieldToFilter('product_id', ['in' => $childProIds])
                ->addFieldToFilter('status', 1);
            if (!empty($biddingCollection)) {
                foreach ($biddingCollection as $bidProduct) {
                    $proId = $bidProduct->getProductId();
                    if (in_array($proId, $proIds)) {
                        $productId = $bidProduct->getProductId();
                        break;
                    }
                }
            }
        }
        return $productId;
    }

    /**
     * checkIfCustomerIsWinner
     * @param cartItemObject $item
     * @param array $mainArray
     * @param int $customerId
     * @return void
     */
    private function checkIfCustomerIsWinner($item, $mainArray, $customerid)
    {
        $customerMaxQty = $mainArray[$customerid]['max'];
        $customerMinQty = $mainArray[$customerid]['min'];
        if ($item->getQty() <= $customerMaxQty && $item->getQty() >= $customerMinQty) {
            $item->setQty($item->getQty());
            $this->saveObj($item);
        } elseif ($item->getQty() > $customerMaxQty) {
            $item->setQty($customerMaxQty);
            $this->saveObj($item);
            $this->messageManager
                ->addNotice(__('Maximum ' . $customerMaxQty . ' quantities are allowed to purchase this item.'));
        } elseif ($item->getQty() < $customerMinQty) {
            $item->setQty($customerMinQty);
            $this->saveObj($item);
            $this->messageManager
                ->addNotice(__('Minimum ' . $customerMinQty . ' quantities are allowed to purchase this item.'));
        }
    }

    /**
     * checkIfNormalCustomer
     * @param cartItemObject $item
     * @param int $productId
     * @param array $maxqty
     * @param array $info
     * @return void
     */
    private function checkIfNormalCustomer($item, $productId, $maxqty, $info)
    {
        $stockQty = $this->stockStateInterface->getStockQty($productId);
        $availqty = $stockQty - $maxqty;
        if ($availqty > 0 && !empty($info[$item->getId()])) {
            if (array_key_exists('qty', $info[$item->getId()])) {
                if ($info[$item->getId()]['qty'] >= $availqty) {
                    $item->setQty($availqty);
                    $this->messageManager
                        ->addNotice(__('Maximum ' . $availqty . ' quantities are allowed to purchase this item.'));
                } else {
                    $item->setQty($info[$item->getId()]['qty']);
                }
                $this->saveObj($item);
            }
        } else {
            $item->setQty($item->getQty());
            $this->saveObj($item);
            $this->messageManager->addNotice('You can not add this quantity for purchase.');
        }
    }

    /**
     * saveObj
     * @param Object
     * @return void
     */
    private function saveObj($object)
    {
        $object->save();
    }
}
