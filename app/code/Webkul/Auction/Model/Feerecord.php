<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Model;

use Webkul\Auction\Api\Data\AuctionfeerecordInterface;
use Magento\Framework\DataObject\IdentityInterface;
use \Magento\Framework\Model\AbstractModel;

class Feerecord extends AbstractModel implements AuctionfeerecordInterface, IdentityInterface
{
    const CACHE_TAG = 'auction_feerecord';
    /**
     * @var string
     */
    protected $_cacheTag = 'auction_feerecord';
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'auction_feerecord';
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Webkul\Auction\Model\ResourceModel\Feerecord::class);
    }
    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getEntityId()];
    }

    /**
     * Get EntityId
     *
     * @return int|null
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Set EntityId.
     */
    public function setEntityId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get CustomerId
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * Set CustomerId.
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Get ProductId
     *
     * @return int|null
     */
    public function getProductId()
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * Set ProductId.
     */
    public function setProductId($productId)
    {
        return $this->setData(self::PRODUCT_ID, $productId);
    }

    /**
     * Get StartAuctionTime
     *
     * @return string|null
     */
    public function getStartAuctionTime()
    {
        return $this->getData(self::START_AUCTION_TIME);
    }

    /**
     * Set StartAuctionTime.
     */
    public function setStartAuctionTime($startAuctionTime)
    {
        return $this->setData(self::START_AUCTION_TIME, $startAuctionTime);
    }

    /**
     * Get StopAuctionTime
     *
     * @return string|null
     */
    public function getStopAuctionTime()
    {
        return $this->getData(self::STOP_AUCTION_TIME);
    }

    /**
     * Set StopAuctionTime.
     */
    public function setStopAuctionTime($stopAuctionTime)
    {
        return $this->setData(self::STOP_AUCTION_TIME, $stopAuctionTime);
    }

    /**
     * Get CreatedAt
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get UpdatedAt
     *
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * Set UpdatedAt.
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }
}
