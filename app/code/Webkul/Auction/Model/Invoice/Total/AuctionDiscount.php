<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Model\Invoice\Total;

class AuctionDiscount extends \Magento\Sales\Model\Order\Invoice\Total\AbstractTotal
{
    /**
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Invoice $invoice)
    {
        $order=$invoice->getOrder();
        $orderAuctionAmountTotal = $order->getAuctionDiscount();
        if ($orderAuctionAmountTotal && count($order->getInvoiceCollection())==0) {
            $invoice->setGrandTotal($invoice->getGrandTotal()+$orderAuctionAmountTotal);
            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal()+$orderAuctionAmountTotal);
        }
        return $this;
    }
}
