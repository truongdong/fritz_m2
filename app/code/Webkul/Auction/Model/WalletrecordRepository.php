<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Model;

use Webkul\Auction\Api\Data;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Webkul\Auction\Model\ResourceModel\Walletrecord as WalletrecordResource;
use Webkul\Auction\Model\ResourceModel\Walletrecord\CollectionFactory as WalletrecordCollection;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class WalletrecordRepository
 * Get Walletrecord Repository
 */
class WalletrecordRepository implements \Webkul\Auction\Api\WalletrecordRepositoryInterface
{
    /**
     * @var WalletrecordResource
     */
    protected $resource;

    /**
     * @var WalletrecordFactory
     */
    protected $walletrecordFactory;

    /**
     * @var WalletrecordCollection
     */
    protected $walletrecordCollectionFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param WalletrecordResource              $resource
     * @param WalletrecordFactory                 $walletrecordFactory
     * @param Data\WalletrecordInterfaceFactory $walletrecordInterfaceFactory
     * @param WalletrecordCollection            $walletrecordCollectionFactory
     * @param DataObjectHelper                  $dataObjectHelper
     * @param DataObjectProcessor               $dataObjectProcessor
     * @param StoreManagerInterface             $storeManager
     */
    public function __construct(
        WalletrecordResource $resource,
        WalletrecordFactory $walletrecordFactory,
        WalletrecordCollection $walletrecordCollectionFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->walletrecordFactory = $walletrecordFactory;
        $this->walletrecordCollectionFactory = $walletrecordCollectionFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * Save Wallet Record
     *
     * @param \Webkul\Auction\Api\Data\WalletrecordInterface $walletrecord
     * @return Walletrecord
     * @throws CouldNotSaveException
     */
    public function save(Data\WalletrecordInterface $walletrecord)
    {
        $storeId = $this->storeManager->getStore()->getId();
        $walletrecord->setStoreId($storeId);
        try {
            $this->resource->save($walletrecord);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $walletrecord;
    }

    /**
     * Load Wallet Record by given Block Identity
     *
     * @param string $id
     * @return Walletrecord
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id)
    {
        $walletrecord = $this->walletrecordFactory->create();
        $this->resource->load($walletrecord, $id);
        if (!$walletrecord->getEntityId()) {
            throw new NoSuchEntityException(__('Wallet record with id "%1" does not exist.', $id));
        }
        return $walletrecord;
    }

    /**
     * Delete Wallet Record
     *
     * @param \Webkul\Auction\Api\Data\WalletrecordInterface $walletrecord
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Data\WalletrecordInterface $walletrecord)
    {
        try {
            $this->resource->delete($walletrecord);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * Delete Wallet Record by given Block Identity
     *
     * @param string $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }

    /**
     * get total wallet record amount
     *
     * @param string $customerId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function getTotalWalletAmount($customerId)
    {
        return  $this->walletrecordCollectionFactory->create()
        ->addFieldToFilter('customer_id', $customerId)->getFirstItem();
    }
}
