<?php
/**
 * Auction Product Model.
 * @category  Webkul
 * @package   Webkul_Auction
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Auction\Model;

use Webkul\Auction\Api\Data\ResponseInterface;

class ResponseData extends \Magento\Framework\DataObject implements \Webkul\Auction\Api\Data\ResponseInterface
{
    /**
     * prepare api response .
     *
     * @return \Webkul\Auction\Api\Data\ResponseInterface
     */
    public function getResponse()
    {
        $data = $this->_data;
        return $data;
    }
}
