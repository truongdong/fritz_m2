<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Auction
 * @author Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\Auction\Model;

use Webkul\Auction\Api\Data\AuctionfeetransactionInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Feetransaction extends AbstractModel implements AuctionfeetransactionInterface, IdentityInterface
{
    const CACHE_TAG = 'auction_feetransaction';

    /**
     * @var string
     */
    protected $_cacheTag = 'auction_feetransaction';
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'auction_feetransaction';
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Webkul\Auction\Model\ResourceModel\Feetransaction::class);
    }
    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getEntityId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Set EntityId.
     */
    public function setEntityId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get Bidfee ID
     *
     * @return int|null
     */
    public function getBidfeeId()
    {
        return $this->getData(self::BIDFEE_ID);
    }

    /**
     * Set Bidfee ID
     */
    public function setBidfeeId($bidfeeId)
    {
        return $this->setData(self::BIDFEE_ID, $bidfeeId);
    }

    /**
     * Get Customer ID
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * Set Customer ID
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Get Deducted Amount
     *
     * @return float|null
     */
    public function getDeductedAmount()
    {
        return $this->getData(self::DEDUCTED_AMOUNT);
    }

    /**
     * Set Deducted Amount
     */
    public function setDeductedAmount($deductedAmount)
    {
        return $this->setData(self::DEDUCTED_AMOUNT, $deductedAmount);
    }

    /**
     * Get Bid Amount
     *
     * @return float|null
     */
    public function getBidAmount()
    {
        return $this->getData(self::BID_AMOUNT);
    }

    /**
     * Set Bid Amount
     */
    public function setBidAmount($bidAmount)
    {
        return $this->setData(self::BID_AMOUNT, $bidAmount);
    }

    /**
     * Get Percent
     *
     * @return float|null
     */
    public function getPercent()
    {
        return $this->getData(self::PERCENT);
    }

    /**
     * Set Percent
     */
    public function setPercent($percent)
    {
        return $this->setData(self::PERCENT, $percent);
    }

    /**
     * Get Refund Status
     *
     * @return int|null
     */
    public function getRefundStatus()
    {
        return $this->getData(self::REFUND_STATUS);
    }

    /**
     * Set Refund Status
     */
    public function setRefundStatus($refundStatus)
    {
        return $this->setData(self::REFUND_STATUS, $refundStatus);
    }

    /**
     * Get Status
     *
     * @return int|null
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * Set Status
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set Status
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
